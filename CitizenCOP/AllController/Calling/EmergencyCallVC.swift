//
//  EmergencyCallVC.swift
//  CitizenCOP
//
//  Created by Navin Patidar on 5/21/19.
//  Copyright © 2019 CitizenCop. All rights reserved.
//

import UIKit
import MessageUI

class EmergencyCallVC: UIViewController {
    // MARK: - --------------IBOutlet
    // MARK: -
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var tvlist: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var lblCityName: UILabel!

    // MARK: - --------------Variable
    // MARK: -
    var aryList = NSMutableArray()
    var aryForListData = NSMutableArray()
    var refresher = UIRefreshControl()
    var ErrorMsg = ""
    var ErrorCount = 0

    // MARK: - --------------Life Cycle
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryTheamColor)
        tvlist.tableFooterView = UIView()
        tvlist.estimatedRowHeight = 80.0
        self.refresher.addTarget(self, action: #selector(RefreshloadData), for: .valueChanged)
        self.tvlist!.addSubview(refresher)
        
        if(dictCityData.value(forKey: "CityData")as! NSArray).count != 0{
            let strCityId = "\(((dictCityData.value(forKey: "CityData")as! NSArray).object(at: 0)as! NSDictionary).value(forKey: "city_id")!)"
            lblCityName.text = "City: \(((dictCityData.value(forKey: "CityData")as! NSArray).object(at: 0)as! NSDictionary).value(forKey: "city_name")!)"
            self.aryList = NSMutableArray()
            self.aryForListData = NSMutableArray()

            self.aryList = getDataFromCoreDataBase(strEntity: "EmergencyCalls", strkey: "emergencyCalls")
            self.aryForListData = getDataFromCoreDataBase(strEntity: "EmergencyCalls", strkey: "emergencyCalls")

            if(self.aryForListData.count == 0){
                callEmergencyNumberAPI(strCityID: strCityId, strLoaderTag: 1)

            }else{
                self.tvlist.reloadData()
            }
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    // MARK: - --------------Pull Refresh
    // MARK: -
    @objc func RefreshloadData() {
        if(dictCityData.value(forKey: "CityData")as! NSArray).count != 0{
            let strCityId = "\(((dictCityData.value(forKey: "CityData")as! NSArray).object(at: 0)as! NSDictionary).value(forKey: "city_id")!)"
            callEmergencyNumberAPI(strCityID: strCityId, strLoaderTag: 0)
        }
    }
    // MARK: - ---------------IBAction
    // MARK: -
    @IBAction func actiononBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        
    }
    @IBAction func actiononHelp(_ sender: UIButton) {
        let testController = mainStoryboard.instantiateViewController(withIdentifier: "HelpVC")as! HelpVC
        testController.strMessage = alertEmergencyHelp
        self.navigationController?.pushViewController(testController, animated: true)
        
    }
    // MARK: - ---------------API CAlling
    // MARK: -
    func callEmergencyNumberAPI(strCityID : String ,strLoaderTag : Int) {
        if !isInternetAvailable() {
            FTIndicator.showToastMessage(alertInternet)
            ErrorMsg = alertInternet
            ErrorCount = 1
            tvlist.reloadData()
        }else{
            let loading = DPBasicLoading(table: tvlist, fontName: "HelveticaNeue")

            strLoaderTag == 1 ? loading.startLoading(text: "Loading...") : nil

            WebServiceClass.callAPIBYPOST(parameter: NSDictionary(), url: API_getEmergencyNo + strCityID) { (responce, status) in
                loading.endLoading()
                self.refresher.endRefreshing()
                if status == "Suceess"{
                    self.aryList = NSMutableArray()
                    self.aryList = (responce.value(forKey: "data")as! NSArray).mutableCopy()as! NSMutableArray
                    self.aryForListData = NSMutableArray()
                    self.aryForListData = (responce.value(forKey: "data")as! NSArray).mutableCopy()as! NSMutableArray
                    deleteAllRecords(strEntity:"EmergencyCalls")
                    saveDataInLocalArray(strEntity: "EmergencyCalls", strKey: "emergencyCalls", data: self.aryForListData)
                    if(self.aryList.count == 0){
                        self.ErrorMsg = alertDataNotFound
                        self.ErrorCount = 1
                    }
                    self.tvlist.reloadData()

                }
                else{
                    self.ErrorMsg = alertSomeError
                    self.ErrorCount = 1
                    self.tvlist.reloadData()

                }
            }
            
            
        }
    }
}
// MARK: - ----------------UITableViewDelegate
// MARK: -

extension EmergencyCallVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return aryList.count != 0 ? aryList.count : ErrorCount

    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(aryList.count != 0){
            let cell = tvlist.dequeueReusableCell(withIdentifier: "EmergencyCell", for: indexPath as IndexPath) as! EmergencyCell
            let dict = aryList.object(at: indexPath.row)as! NSDictionary
             cell.lblTitle.text = "\(dict.value(forKey: "EmergencyService")!)"
            cell.btnNumber1.setTitle("\(dict.value(forKey: "EmergencyNo")!)", for: .normal)
            cell.btnCall.addTarget(self, action: #selector(actionForCall), for: .touchUpInside)
            cell.btnMessage.addTarget(self, action: #selector(actionForMessage), for: .touchUpInside)
            cell.btnCall.tag = indexPath.row
            cell.btnMessage.tag = indexPath.row
            return cell
        }else{
            let cell = tvlist.dequeueReusableCell(withIdentifier: "ErrorCell", for: indexPath as IndexPath) as! ErrorCell
            cell.lblTitle.text = self.ErrorMsg
            cell.btnRetry.addTarget(self, action: #selector(actionONError), for: .touchUpInside)
            return cell
        }
        
       
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(aryList.count != 0){
           
        }
        
    }
    @objc func actionONError(sender : UIButton) {
        let strCityId = "\(((dictCityData.value(forKey: "CityData")as! NSArray).object(at: 0)as! NSDictionary).value(forKey: "city_id")!)"
        callEmergencyNumberAPI(strCityID: strCityId, strLoaderTag: 1)
    }
    
    @objc func actionForCall(sender : UIButton) {
        let dict = aryList.object(at: sender.tag)as! NSDictionary
        if callingFunction(number: "\(dict.value(forKey: "EmergencyNo")!)" as NSString){
            
        }else{
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertCalling, viewcontrol: self)
        }
        
    }
    @objc func actionForMessage(sender : UIButton) {
        let dict = aryList.object(at: sender.tag)as! NSDictionary
        if (MFMessageComposeViewController.canSendText()) {
            let messageVC = MFMessageComposeViewController()
            messageVC.body = "";
            messageVC.recipients = ["\(dict.value(forKey: "EmergencyNo")!)"]
            messageVC.messageComposeDelegate = self
            self.present(messageVC, animated: true, completion: nil)
        }else{
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertCalling, viewcontrol: self)
            
        }
    }
}


// MARK: - ----------------UserDashBoardCell
// MARK: -
class EmergencyCell: UITableViewCell {
    //DashBoard
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnNumber1: UIButton!
    @IBOutlet weak var btnCall: UIButton!
    @IBOutlet weak var btnMessage: UIButton!
    
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}
// MARK: - ----------------UISearchBarDelegate
// MARK: -

extension  EmergencyCallVC : UISearchBarDelegate{
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        var txtAfterUpdate:NSString = searchBar.text! as NSString
        txtAfterUpdate = txtAfterUpdate.replacingCharacters(in: range, with: text) as NSString
        self.searchAutocomplete(Searching: txtAfterUpdate)
        return true
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
        self.searchAutocomplete(Searching: "")
        searchBar.text = ""
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
        self.searchAutocomplete(Searching: "")
    }
    func searchAutocomplete(Searching: NSString) -> Void {
        
        let resultPredicate = NSPredicate(format: "EmergencyService contains[c] %@ OR EmergencyService contains[c] %@", argumentArray: [Searching, Searching])
        if !(Searching.length == 0) {
            let arrayfilter = (self.aryForListData ).filtered(using: resultPredicate)
            let nsMutableArray = NSMutableArray(array: arrayfilter)
            self.aryList = NSMutableArray()
            self.aryList = nsMutableArray.mutableCopy() as! NSMutableArray
            self.tvlist.reloadData()
        }
        else{
            self.aryList = NSMutableArray()
            self.aryList = self.aryForListData.mutableCopy() as! NSMutableArray
            self.tvlist.reloadData()
            self.view.endEditing(true)
            searchBar.text = ""
        }
        if(aryList.count == 0){
        }
    }
}
// MARK: - ----------------MFMessageComposeViewControllerDelegate
// MARK: -
extension EmergencyCallVC : MFMessageComposeViewControllerDelegate{
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        switch (result) {
        case .cancelled:
            print("Message was cancelled")
            dismiss(animated: true, completion: nil)
        case .failed:
            print("Message failed")
            dismiss(animated: true, completion: nil)
        case .sent:
            print("Message was sent")
            dismiss(animated: true, completion: nil)
        default:
            break
        }
    }
    
}
