//
//  CallPoliceVC.swift
//  CitizenCOP
//
//  Created by Navin Patidar on 5/27/19.
//  Copyright © 2019 CitizenCop. All rights reserved.
//

import UIKit
import MessageUI
import SWXMLHash
import StringExtensionHTML
import AEXML

class CallPoliceVC: UIViewController {
    // MARK: - --------------IBOutlet
    // MARK: -
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var tvlist: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var lblCityName: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    
    // MARK: - --------------Variable
    // MARK: -
    var aryList = NSMutableArray()
    var aryForListData = NSMutableArray()
    var refresher = UIRefreshControl()
    var ErrorMsg = ""
    var ErrorCount = 0
    var strlblTittle = String()

    // MARK: - --------------Life Cycle
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        lblTitle.text = strlblTittle

        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryTheamColor)
        tvlist.tableFooterView = UIView()
        tvlist.estimatedRowHeight = 80.0
        self.refresher.addTarget(self, action: #selector(RefreshloadData), for: .valueChanged)
        self.tvlist!.addSubview(refresher)
        if(dictCityData.value(forKey: "CityData")as! NSArray).count != 0{
            let strCityId = "\(((dictCityData.value(forKey: "CityData")as! NSArray).object(at: 0)as! NSDictionary).value(forKey: "city_id")!)"
            lblCityName.text = "City: \(((dictCityData.value(forKey: "CityData")as! NSArray).object(at: 0)as! NSDictionary).value(forKey: "city_name")!)"
            self.aryList = getDataFromCoreDataBase(strEntity: "CallPoliceData", strkey: "callPoliceData")
            self.aryForListData = getDataFromCoreDataBase(strEntity: "CallPoliceData", strkey: "callPoliceData")
            
            if(self.aryForListData.count == 0){
                callPoliceNumberAPI(strCityID: strCityId, strLoaderTag: 1)
            }else{
                self.tvlist.reloadData()
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    // MARK: - --------------Pull Refresh
    // MARK: -
    @objc func RefreshloadData() {
        if(dictCityData.value(forKey: "CityData")as! NSArray).count != 0{
            let strCityId = "\(((dictCityData.value(forKey: "CityData")as! NSArray).object(at: 0)as! NSDictionary).value(forKey: "city_id")!)"
            callPoliceNumberAPI(strCityID: strCityId, strLoaderTag: 0)
        }
    }
    // MARK: - ---------------IBAction
    // MARK: -
    @IBAction func actiononBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        
    }

    @IBAction func actionContactLocation(_ sender: Any) {
    }
    @IBAction func actiononHelp(_ sender: UIButton) {
        let testController = mainStoryboard.instantiateViewController(withIdentifier: "HelpVC")as! HelpVC
        testController.strMessage = alertCallPoliceHelp
        self.navigationController?.pushViewController(testController, animated: true)
        
    }
    // MARK: - ---------------API CAlling
    // MARK: -
    func callPoliceNumberAPI(strCityID : String ,strLoaderTag : Int) {
        if !isInternetAvailable() {
            FTIndicator.showToastMessage(alertInternet)
            ErrorMsg = alertInternet
            ErrorCount = 1
            tvlist.reloadData()
        }else{
            let loading = DPBasicLoading(table: tvlist, fontName: "HelveticaNeue")
            strLoaderTag == 1 ? loading.startLoading(text: "Loading...") : nil
            
            WebServiceClass.callAPIBYGET_XML(parameter: NSDictionary(), url: API_CallPolice + strCityID) { (responce, status) in
                loading.endLoading()
                self.refresher.endRefreshing()
                if status == "Suceess"{
                    self.aryList = NSMutableArray()
                    let xmlData = (responce.value(forKey: "data")as! Data)
                    let xml1 = XMLHash.parse(xmlData)
                    
                    for elem in xml1["NewDataSet"]["Table1"].all {
                        let dict = NSMutableDictionary()
                        dict.setValue("\(elem["id"].element!.text)", forKey: "id")
                        dict.setValue("\(elem["Nameof_Contact"].element!.text)", forKey: "Nameof_Contact")
                        dict.setValue("\(elem["Designation"].element!.text)", forKey: "Designation")
                        dict.setValue("\(elem["CotnactNo"].element!.text)", forKey: "CotnactNo")
                        dict.setValue("\(elem["state_id"].element!.text)", forKey: "state_id")
                        dict.setValue("\(elem["city_id"].element!.text)", forKey: "city_id")
                        dict.setValue("\(elem["LandLineNo"].element!.text)", forKey: "LandLineNo")
                        dict.setValue("\(elem["SeqNo"].element!.text)", forKey: "SeqNo")
                        dict.setValue("\(elem["Category"].element!.text)", forKey: "Category")
                        dict.setValue("\(elem["DesignationId"].element!.text)", forKey: "DesignationId")
                        self.aryList.add(dict)
                    }
                    self.aryForListData = NSMutableArray()
                    self.aryForListData = self.aryList
                    deleteAllRecords(strEntity:"CallPoliceData")
                    saveDataInLocalArray(strEntity: "CallPoliceData", strKey: "callPoliceData", data: self.aryForListData)
                    if(self.aryList.count == 0){
                        self.ErrorMsg = alertDataNotFound
                        self.ErrorCount = 1
                    }
                    self.tvlist.reloadData()
                    
                }
                else{
                    self.ErrorMsg = alertSomeError
                    self.ErrorCount = 1
                    self.tvlist.reloadData()
                    
                }
            }
            
            
        }
    }
}
// MARK: - ----------------UITableViewDelegate
// MARK: -

extension CallPoliceVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return aryList.count != 0 ? aryList.count : ErrorCount
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if(aryList.count != 0){
            let cell = tvlist.dequeueReusableCell(withIdentifier: "AdministrationCell", for: indexPath as IndexPath) as! AdministrationCell
            let dict = aryList.object(at: indexPath.row)as! NSDictionary
            cell.lblDetail.text = "\(dict.value(forKey: "Nameof_Contact")!)"
            cell.lblTitle.text = "\(dict.value(forKey: "Designation")!)"
            let buttonTitleStr1 = NSMutableAttributedString(string:"\(dict.value(forKey: "CotnactNo")!)", attributes:cell.attrs)
            cell.btnNumber1.setAttributedTitle(buttonTitleStr1, for: .normal)
            let buttonTitleStr2 = NSMutableAttributedString(string:"\(dict.value(forKey: "LandLineNo")!)", attributes:cell.attrs)
            
            cell.btnNumber2.tag = indexPath.row
            cell.btnNumber1.tag = indexPath.row

            cell.btnNumber2.setAttributedTitle(buttonTitleStr2, for: .normal)
            cell.btnNumber1.addTarget(self, action: #selector(actionForCallMessage), for: .touchUpInside)
            cell.btnNumber2.addTarget(self, action: #selector(actionForCall), for: .touchUpInside)
            return cell
        }else{
            let cell = tvlist.dequeueReusableCell(withIdentifier: "ErrorCell", for: indexPath as IndexPath) as! ErrorCell
            cell.lblTitle.text = self.ErrorMsg
            cell.btnRetry.addTarget(self, action: #selector(actionONError), for: .touchUpInside)
            return cell
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(aryList.count != 0){
            
        }
        
    }
    @objc func actionONError(sender : UIButton) {
        let strCityId = "\(((dictCityData.value(forKey: "CityData")as! NSArray).object(at: 0)as! NSDictionary).value(forKey: "city_id")!)"
        callPoliceNumberAPI(strCityID: strCityId, strLoaderTag: 1)
    }
    
    @objc func actionForCall(sender : UIButton) {
        let dict = aryList.object(at: sender.tag)as! NSDictionary
        if callingFunction(number: "\(dict.value(forKey: "LandLineNo")!)" as NSString){
            
        }else{
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertCalling, viewcontrol: self)
        }
        
    }
    @objc func actionForCallMessage(sender : UIButton) {
        let dict = aryList.object(at: sender.tag)as! NSDictionary
        let number = "\(dict.value(forKey: "CotnactNo")!)"
        let alertController = UIAlertController(title: "Police", message: "Call/Message", preferredStyle: .actionSheet)
        let action1 = UIAlertAction(title: "Call", style: .default) { (action) in
            print("Default is pressed.....")
            if callingFunction(number: "\(number)" as NSString){
                
            }
            
        }
        let action2 = UIAlertAction(title: "Message", style: .default) { (action) in
            print("Cancel is pressed......")
            if (MFMessageComposeViewController.canSendText()) {
                let messageVC = MFMessageComposeViewController()
                messageVC.body = "";
                messageVC.recipients = ["\(number)"]
                messageVC.messageComposeDelegate = self
                self.present(messageVC, animated: true, completion: nil)
            }
        }
        let action3 = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            print("Destructive is pressed....")
            
        }
        alertController.addAction(action1)
        alertController.addAction(action2)
        alertController.addAction(action3)
        alertController.popoverPresentationController?.sourceRect = sender.frame
        alertController.popoverPresentationController?.sourceView = self.view

        self.present(alertController, animated: true, completion: nil)
        
    }
}



// MARK: - ----------------UISearchBarDelegate
// MARK: -

extension  CallPoliceVC : UISearchBarDelegate{
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        var txtAfterUpdate:NSString = searchBar.text! as NSString
        txtAfterUpdate = txtAfterUpdate.replacingCharacters(in: range, with: text) as NSString
        self.searchAutocomplete(Searching: txtAfterUpdate)
        return true
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
        self.searchAutocomplete(Searching: "")
        searchBar.text = ""
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
        self.searchAutocomplete(Searching: "")
    }
    func searchAutocomplete(Searching: NSString) -> Void {
        
        let resultPredicate = NSPredicate(format: "Nameof_Contact contains[c] %@ OR Designation contains[c] %@", argumentArray: [Searching, Searching])
        if !(Searching.length == 0) {
            let arrayfilter = (self.aryForListData ).filtered(using: resultPredicate)
            let nsMutableArray = NSMutableArray(array: arrayfilter)
            self.aryList = NSMutableArray()
            self.aryList = nsMutableArray.mutableCopy() as! NSMutableArray
            self.tvlist.reloadData()
        }
        else{
            self.aryList = NSMutableArray()
            self.aryList = self.aryForListData.mutableCopy() as! NSMutableArray
            self.tvlist.reloadData()
            self.view.endEditing(true)
            searchBar.text = ""
        }
        if(aryList.count == 0){
        }
    }
}
// MARK: - ----------------MFMessageComposeViewControllerDelegate
// MARK: -
extension CallPoliceVC : MFMessageComposeViewControllerDelegate{
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        switch (result) {
        case .cancelled:
            print("Message was cancelled")
            dismiss(animated: true, completion: nil)
        case .failed:
            print("Message failed")
            dismiss(animated: true, completion: nil)
        case .sent:
            print("Message was sent")
            dismiss(animated: true, completion: nil)
        default:
            break
        }
    }
    
}
