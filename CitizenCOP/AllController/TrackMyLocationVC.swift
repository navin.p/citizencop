//
//  TrackMyLocationVC.swift
//  CitizenCOP
//
//  Created by Navin Patidar on 9/25/19.
//  Copyright © 2019 CitizenCop. All rights reserved.
//

import UIKit
import CoreLocation

class TrackMyLocationVC: UIViewController  {
    
    
    // MARK: - --------------IBOutlet
    // MARK: -
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var txtEnterKey: UITextField!
    @IBOutlet weak var btnGetDevice: UIButton!
    @IBOutlet weak var lblDeviceID: UILabel!
    @IBOutlet weak var btnCopy: UIButton!
    @IBOutlet var viewForStopTracking: UIView!
    @IBOutlet weak var btnSwitch: UISwitch!
    
    // MARK: - ---------------LifeCycle
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryTheamColor)
        btnSubmit.backgroundColor = hexStringToUIColor(hex: primaryDark)
        btnSubmit.layer.cornerRadius = 8.0
        btnGetDevice.backgroundColor = hexStringToUIColor(hex: primaryDark)
        btnGetDevice.layer.cornerRadius = 8.0
        btnCopy.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if(nsud.value(forKey: "CitizenCOP_TrackingStatus") != nil){
            if(nsud.value(forKey: "CitizenCOP_TrackingStatus")as! Bool){
                viewForStopTracking.frame = CGRect(x: 0, y: viewHeader.frame.maxY + 20, width: self.view.frame.width, height: self.view.frame.height)
                btnSwitch.setOn(true, animated: true)
                self.view.addSubview(viewForStopTracking)
            }
        }
    }
    
    // MARK: - ---------------IBAction
    // MARK: -
    @IBAction func actiononBack(_ sender: UIButton) {
        self.view.endEditing(true)
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionOnDeviceID(_ sender: UIButton) {
        lblDeviceID.text = "\(DeviceID)"
        btnCopy.isHidden = false
        
    }
    @IBAction func actionOnCopy(_ sender: UIButton) {
        self.view.endEditing(true)
        
        UIPasteboard.general.string = "\(DeviceID)"
        FTIndicator.showToastMessage("Device ID Copied!")
        
    }
    
    @IBAction func actiononHelp(_ sender: UIButton) {
        self.view.endEditing(true)
        
        let testController = mainStoryboard.instantiateViewController(withIdentifier: "HelpVC")as! HelpVC
        testController.strMessage = alertTrackME
        self.navigationController?.pushViewController(testController, animated: true)
    }
    
    @IBAction func actionOnStopTracking(_ sender: UISwitch) {
        self.view.endEditing(true)
        
        nsud.setValue(false, forKey: "CitizenCOP_TrackingStatus")
        nsud.synchronize()
        FTIndicator.showToastMessage("\(alert_trackingstoped)")
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func actionOnSubmit(_ sender: UIButton) {
        
        self.view.endEditing(true)
        
        if(txtEnterKey.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_DeviceKey, viewcontrol: self)
            
        }else{
            let dictData = NSMutableDictionary()
            dictData.setValue(txtEnterKey.text, forKey: "dkey")
            dictData.setValue(DeviceID, forKey: "imei")
            dictData.setValue("updateimei", forKey: "key")
            customDotLoaderShowOnButton(btn: sender, view: self.view, controller: self)
            
            WebServiceClass.callAPIBYPOST_ResponceInString(parameter: dictData, url: BaseURLTrackLocationKeyREGISTERED) { (responce, status) in
                print(responce)
                customeDotLoaderRemove()
                if status == "Suceess"{
                    let strResponce = responce.value(forKey: "data") as! String
                    if(strResponce == "Key Not Found"){
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: strResponce, viewcontrol: self)
                        
                    }else{
                        
                        let alert = UIAlertController(title: alertMessage, message: "Do you want to Start tracking ?", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction (title: "Start", style: .default, handler: { (nil) in
                            nsud.setValue("\(self.txtEnterKey.text!)", forKey: "CitizenCOP_TrackingKey")
                            nsud.setValue(true, forKey: "CitizenCOP_TrackingStatus")
                            nsud.synchronize()
                            self.navigationController?.popViewController(animated: true)
                            FTIndicator.showToastMessage("\(alert_trackingstart)")
                            
                        }))
                        alert.addAction(UIAlertAction (title: "Close", style: .default, handler: { (nil) in
                            self.navigationController?.popViewController(animated: true)
                        }))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
                else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            }
        }
    }
    
    func callTrackingLocationAPI() {
        if !isInternetAvailable() {
            // FTIndicator.showNotification(withTitle: alertMessage, message: alertInternet)
        }else{
            
            if(nsud.value(forKey: "CitizenCOP_TrackingKey") != nil){
                if(GlobleLat != "0.0" && GlobleLong != "0.0"){
                    let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><InsertLocation xmlns='http://m.citizencop.org/'><Id>0</Id><dky>\(nsud.value(forKey: "CitizenCOP_TrackingKey")!)</dky><lat>\(GlobleLat)</lat><lon>\(GlobleLong)</lon><alt>\(GlobleAltitude)</alt><spd>\(Globlespeed)</spd><acc>\(GlobleAcc)</acc><dvc>mbl</dvc></InsertLocation></soap12:Body></soap12:Envelope>"
                    WebServiceClass.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: BaseURLTrackLocationUpdate, resultype: "InsertLocationResult", responcetype: "InsertLocationResponse") { (responce, status) in
                        customeDotLoaderRemove()
                        if status == "Suceess"{
                            print("responce Tack Location \(responce)")
                        }else{
                        }
                    }
                }else{
                    // FTIndicator.showNotification(withTitle: alertMessage, message: "Location not found!!!!!!!")
                }
            }
        }
    }
    
}
