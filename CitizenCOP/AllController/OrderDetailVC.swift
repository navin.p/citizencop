//
//  OrderDetailVC.swift
//  CitizenCOP
//
//  Created by Navin Patidar on 4/14/20.
//  Copyright © 2020 CitizenCop. All rights reserved.
//

import UIKit

class OrderDetailVC: UIViewController {
    // MARK: - ----IBOutlet
        @IBOutlet weak var view_ForHeader: UIView!
        @IBOutlet weak var lblTitle: UILabel!
        
    @IBOutlet weak var btnOrder: UIButton!

    
    
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblDetail: UILabel!
    @IBOutlet weak var lblQty: UILabel!
    @IBOutlet weak var imgFood: UIImageView!

    @IBOutlet weak var txtName: ACFloatingTextfield!

    @IBOutlet weak var txtMobile: ACFloatingTextfield!
    @IBOutlet weak var txtAlterMobile: ACFloatingTextfield!
    @IBOutlet weak var txtEmail: ACFloatingTextfield!
    @IBOutlet weak var txtSocity: ACFloatingTextfield!
    @IBOutlet weak var txtAddress: ACFloatingTextfield!
    @IBOutlet weak var txtLine1: ACFloatingTextfield!
    @IBOutlet weak var txtLine2: ACFloatingTextfield!
    @IBOutlet weak var txtLandMArk: ACFloatingTextfield!
    @IBOutlet weak var txtWardno: ACFloatingTextfield!
    @IBOutlet weak var txtZOneno: ACFloatingTextfield!

     // MARK:
    // MARK: - ----Variable
    var strTitle = String()
    var strImage = UIImage()
    
    var DictData = NSMutableDictionary()
    var amount = String()
    var qty = String()
    
    var tagForSaveData = 0
    var aryWardlist = NSMutableArray() , aryZonelist = NSMutableArray()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(goOnHome), name: Notification.Name("Back"), object: nil)

        lblTitle.text = strTitle
        view_ForHeader.backgroundColor = hexStringToUIColor(hex: primaryTheamColor)
        btnOrder.backgroundColor = hexStringToUIColor(hex: primaryDark)
        
        lblDetail.text = "\(DictData.value(forKey: "Detail")!)"
        lblAmount.text = "Amount: \(DictData.value(forKey: "AMT")!)"
        lblQty.text = "Quantity: \(DictData.value(forKey: "QTY")!)"
     
        txtEmail.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        txtMobile.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        txtAlterMobile.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
      
        imgFood.layer.borderWidth = 1.0
        imgFood.layer.borderColor = UIColor.lightGray.cgColor
        imgFood.layer.cornerRadius = 6.0
         imgFood.image = strImage
        
       
        
        btnOrder.layer.cornerRadius = 8.0
        self.callZoneAPI(sender: 0)

        
    }
    @objc func goOnHome(){
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: DashBoardVC.self) {
                _ =  self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
    }
    @objc func textFieldDidChange(_ textField: UITextField) {
           let text = textField.text ?? ""
           var trimmedText = text.trimmingCharacters(in: .whitespaces)
           trimmedText = (trimmedText.replacingOccurrences(of: " ", with: ""))
           textField.text = trimmedText
           if(txtMobile == textField || txtAlterMobile == textField){
               textField.text = textField.text?.replacingOccurrences(of: "-", with: "")
                textField.text = textField.text?.replacingOccurrences(of: "(", with: "")
                textField.text = textField.text?.replacingOccurrences(of: ")", with: "")
        }
        
    }
    // MARK: - ---------------IBAction
         // MARK: -
         @IBAction func actiononBack(_ sender: UIButton) {
            self.view.endEditing(true)

             self.navigationController?.popViewController(animated: true)
          
             
         }

       @IBAction func actiononPlaceOrder(_ sender: UIButton) {
        self.view.endEditing(true)
//                  let strAMount = "\(DictData.value(forKey: "AMT")!)".replacingOccurrences(of: "\u{20B9} ", with: "")
//
//        createCheckSumHash(strAmount: "\((strAMount as NSString).doubleValue)", tagForPayType: "")
        
        if (validationForOrder()){
            let strAMount = "\(DictData.value(forKey: "AMT")!)".replacingOccurrences(of: "\u{20B9} ", with: "")
          if(strAMount == "0" || strAMount == ""){
                      showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "", viewcontrol: self)
                   }else{
         createCheckSumHash(strAmount: "\((strAMount as NSString).doubleValue)", tagForPayType: "")
            
          //  createCheckSumHash(strAmount: "1.0", tagForPayType: "")
        }
    }
           
}
    @IBAction func actiononWard(_ sender: UIButton) {
           if(self.txtZOneno!.tag == 0){
               showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select Zone first.", viewcontrol: self)
           }else{
              aryWardlist.count == 0 ? self.callWARDAPI(sender: 1, strZOneID: "\(self.txtZOneno!.tag)") : PopViewWithArray(tag: 15, aryData: aryWardlist, strTitle: "Select Ward")
           }
           
          
       }
       @IBAction func actiononZone(_ sender: UIButton) {
           aryZonelist.count == 0 ? self.callZoneAPI(sender: 1) : PopViewWithArray(tag: 16, aryData: aryZonelist, strTitle: "Select Zone")

       }
    
       //MARK: Other Function's
       func PopViewWithArray(tag: Int , aryData : NSMutableArray , strTitle: String)  {
           if(aryData.count != 0){
               let vc: SelectionClass = mainStoryboard.instantiateViewController(withIdentifier: "SelectionClass") as! SelectionClass
                  vc.strTitle = strTitle
                  vc.strTag = tag
                  vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                  vc.modalTransitionStyle = .coverVertical
                  vc.delegate = self
                  vc.aryList = aryData
                  
                  self.present(vc, animated: true, completion: {})
           }else{
               showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertDataNotFound, viewcontrol: self)
           }
      
       }
    func dateStringToFormatedDateString(dateToConvert: String, dateFormat: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss.s"
        let myDate = dateFormatter.date(from: dateToConvert)!
        dateFormatter.dateFormat = dateFormat
        let dateString = dateFormatter.string(from: myDate)
        return dateString
    }
        //MARK:-----
        //MARK:
        func donateAPICall(dictPaymentData : NSMutableDictionary){
            
            var strCityId = ""
            if(dictCityData.value(forKey: "CityData")as! NSArray).count != 0{
                 strCityId = "\(((dictCityData.value(forKey: "CityData")as! NSArray).object(at: 0)as! NSDictionary).value(forKey: "city_id")!)"
            }
            let dictSendData = NSMutableDictionary()
            dictSendData.setValue("\(DictData.value(forKey: "Category")!)", forKey: "Category")
            dictSendData.setValue(strCityId, forKey: "city_id")
            dictSendData.setValue("\(txtName.text!)", forKey: "Name")
            dictSendData.setValue("\(txtMobile.text!)", forKey: "MobileNo")
            dictSendData.setValue("\(txtAlterMobile.text!)", forKey: "AlternateNo")
            dictSendData.setValue("\(txtEmail.text!)", forKey: "EmailId")
            dictSendData.setValue("\(txtSocity.text!)", forKey: "SocietyName")
            dictSendData.setValue("", forKey: "BuildingName")
            dictSendData.setValue("\(txtAddress.text!)", forKey: "DeliveryAddress")
            dictSendData.setValue("\(txtLine1.text!)", forKey: "DeliveryAddressLine1")
            dictSendData.setValue("\(txtLine2.text!)", forKey: "DeliveryAddressLine2")
            dictSendData.setValue("\(txtLandMArk.text!)", forKey: "Landmark")
            dictSendData.setValue("\(txtZOneno.text!)", forKey: "ZoneNo")
            dictSendData.setValue("\(txtWardno.text!)", forKey: "WardNo")
            
            let strAMount = "\(DictData.value(forKey: "AMT")!)".replacingOccurrences(of: "\u{20B9} ", with: "")
            dictSendData.setValue("\(strAMount)", forKey: "TotalAmount")

            dictSendData.setValue("\(DictData.value(forKey: "QTY")!)", forKey: "Quantity")
            
            dictSendData.setValue("\(dictPaymentData.value(forKey: "TXNID")!)", forKey: "PayTM_TranjectionId")
            dictSendData.setValue("\(dictPaymentData.value(forKey: "STATUS")!)", forKey: "PaymentStatus")
            dictSendData.setValue("\(dictPaymentData.value(forKey: "ORDERID")!)", forKey: "OrderId")
            let date = dateStringToFormatedDateString(dateToConvert: "\(dictPaymentData.value(forKey: "TXNDATE")!)", dateFormat: "MM/dd/yyyy")
            dictSendData.setValue(date, forKey: "TranjectionDate")
       
           dictSendData.setValue("", forKey: "BankName")
            dictSendData.setValue("", forKey: "GatewayName")

            if dictPaymentData["BANKNAME"] != nil {
                dictSendData.setValue("\(dictPaymentData.value(forKey: "BANKNAME")!)", forKey: "BankName")
            }
            if dictPaymentData["GATEWAYNAME"] != nil {
                dictSendData.setValue("\(dictPaymentData.value(forKey: "GATEWAYNAME")!)", forKey: "GatewayName")
            }
            dictSendData.setValue("\(dictPaymentData.value(forKey: "PAYMENTMODE")!)", forKey: "PaymentMode")
            dictSendData.setValue("\(dictPaymentData.value(forKey: "CURRENCY")!)", forKey: "Currency")
            dictSendData.setValue("\(dictPaymentData.value(forKey: "CHECKSUMHASH")!)", forKey: "CheckSum")
            dictSendData.setValue(DeviceID, forKey: "SecureKey")

            var json1 = Data()
            var jsonAdd = NSString()
            
            if JSONSerialization.isValidJSONObject(dictSendData) {
                // Serialize the dictionary
                json1 = try! JSONSerialization.data(withJSONObject: dictSendData, options: .prettyPrinted)
                jsonAdd = String(data: json1, encoding: .utf8)! as NSString
                print("UpdateLeadinfo JSON: \(jsonAdd)")
            }
            if(dictSendData.count == 0){
                jsonAdd = ""
            }
            if !isInternetAvailable() {
                       FTIndicator.showToastMessage(alertInternet)
                   }else{
                       let loader = loader_Show(controller: self, strMessage: "", title: "Please wait...", style: .alert)
                       let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><AddGroceryOrder xmlns='http://m.citizencop.org/'><GroceryOrderMdl>\(jsonAdd)</GroceryOrderMdl></AddGroceryOrder></soap12:Body></soap12:Envelope>"
                       
                       WebServiceClass.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: BaseURL3, resultype: "AddGroceryOrderResult", responcetype: "AddGroceryOrderResponse") { (responce, status) in
                          loader.dismiss(animated: false, completion: nil)
                           if status == "Suceess"{
                               let dictTemp = (responce.value(forKey: "data")as! NSDictionary).mutableCopy()as! NSMutableDictionary
                            if("\(dictTemp.value(forKey: "Result")!)" == "True"){
                                let vc: OrderSuccessVC = mainStoryboard.instantiateViewController(withIdentifier: "OrderSuccessVC") as! OrderSuccessVC
                                vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                                vc.modalTransitionStyle = .coverVertical
                                self.present(vc, animated: true, completion: {})
                            }else{
                                self.tagForSaveData = self.tagForSaveData + 1
                                if(self.tagForSaveData >= 3){
                                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                                }else{
                                    self.donateAPICall(dictPaymentData: dictPaymentData)
                                }
                            }
                           }else{
                               showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                           }
                       }
                   }
       
        }
    func callWARDAPI(sender : Int ,strZOneID : String) {
                 if !isInternetAvailable() {
                     FTIndicator.showToastMessage(alertInternet)
                 }else{
                     let loader = loader_Show(controller: self, strMessage: "", title: "Please wait...", style: .alert)
                     let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><GetWard xmlns='http://m.citizencop.org/'><ZoneId>\(strZOneID)</ZoneId></GetWard></soap12:Body></soap12:Envelope>"
                     
                     WebServiceClass.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: BaseURL3, resultype: "GetWardResult", responcetype: "GetWardResponse") { (responce, status) in
                        loader.dismiss(animated: false, completion: nil)
                         if status == "Suceess"{
                             let dictTemp = (responce.value(forKey: "data")as! NSDictionary).mutableCopy()as! NSMutableDictionary
                          if("\(dictTemp.value(forKey: "Result")!)" == "True"){
                              self.aryWardlist = NSMutableArray()
                              self.aryWardlist = (dictTemp.value(forKey: "Ward")as! NSArray).mutableCopy()as! NSMutableArray
                           if(sender == 1){
                               self.PopViewWithArray(tag: 15, aryData: self.aryWardlist, strTitle: "Select Ward")
                           }
                          }else{
                              showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)

                          }
                           
                         }else{
                             showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                         }
                     }
                 }
             }
       func callZoneAPI(sender : Int) {
                if !isInternetAvailable() {
                    FTIndicator.showToastMessage(alertInternet)
                }else{
                    let loader = loader_Show(controller: self, strMessage: "", title: "Please wait...", style: .alert)
                   var strCityId = ""
                   if(dictCityData.value(forKey: "CityData")as! NSArray).count != 0{
                       strCityId = "\(((dictCityData.value(forKey: "CityData")as! NSArray).object(at: 0)as! NSDictionary).value(forKey: "city_id")!)"
                   }
                   let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><GetZone xmlns='http://m.citizencop.org/'><city_id>\(strCityId)</city_id></GetZone></soap12:Body></soap12:Envelope>"
                    
                    WebServiceClass.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: BaseURL3, resultype: "GetZoneResult", responcetype: "GetZoneResponse") { (responce, status) in
                       loader.dismiss(animated: false, completion: nil)
                        if status == "Suceess"{
                            let dictTemp = (responce.value(forKey: "data")as! NSDictionary).mutableCopy()as! NSMutableDictionary
                         if("\(dictTemp.value(forKey: "Result")!)" == "True"){
                             self.aryZonelist = NSMutableArray()
                             self.aryZonelist = (dictTemp.value(forKey: "Zone")as! NSArray).mutableCopy()as! NSMutableArray
                           if(sender == 1){
                               self.PopViewWithArray(tag: 16, aryData: self.aryZonelist, strTitle: "Select Zone")
                           }
                         }else{
                             showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)

                         }
                          
                        }else{
                            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                        }
                    }
                }
            }
    }

    


// MARK: - ---------------IBAction
// MARK: -
extension OrderDetailVC : PGTransactionDelegate{
    
    func createCheckSumHash(strAmount : String , tagForPayType : String) {
        let dictLogin = NSMutableDictionary()
        dictLogin.setValue("\(DeviceID)", forKey: "User_Id")
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "ddMMyyyyHHmmSSS"
        let result = formatter.string(from: date)
        let orderIdUnique="Order_iOS_\(result)";
        let dict = NSMutableDictionary()
        
        var strSCheckSum = "http://gcapi.citizencop.org/Checksumcsharp/GenerateChecksum.aspx"
    
            dict.setValue(MerchantID, forKey: "MID")
            dict.setValue(orderIdUnique, forKey: "ORDER_ID")
            dict.setValue("\(dictLogin.value(forKey: "User_Id")!)", forKey: "CUST_ID")
            dict.setValue("\(IndustryID)", forKey: "INDUSTRY_TYPE_ID")
            dict.setValue("\(ChannelID)", forKey: "CHANNEL_ID")
            dict.setValue(strAmount, forKey: "TXN_AMOUNT")
            dict.setValue("\(Website)", forKey: "WEBSITE")
            dict.setValue("https://securegw-stage.paytm.in/theia/paytmCallback?ORDER_ID=\(orderIdUnique)", forKey: "CALLBACK_URL")
            strSCheckSum = GENERATE_CHECKSUM
 
        
        print(dict)
        
        
        WebServiceClass.callAPIBYPOST(parameter: dict, url: "\(strSCheckSum)") { (responce, status) in
            
            if(status == "Suceess"){
                let dictData = responce.value(forKey: "data")as! NSDictionary
                let strCheckSum = "\(dictData.value(forKey: "CHECKSUMHASH")!)"
                print(strCheckSum)
                self.createPayment(strAmount: strAmount, checkSumHashh: strCheckSum, orderidNew: orderIdUnique)
            }else{
                let alert = UIAlertController(title: alertMessage, message: alertSomeError, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                    
                }))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    func createPayment(strAmount : String , checkSumHashh : String , orderidNew : String){
        var orderDict = [String : String]()
        let dictLoginData = NSMutableDictionary()
            dictLoginData.setValue("\(DeviceID)", forKey: "User_Id")
        
            orderDict["MID"] = "\(MerchantID)";
            orderDict["CHANNEL_ID"] = "\(ChannelID)";
            orderDict["INDUSTRY_TYPE_ID"] = "\(IndustryID)";
            orderDict["WEBSITE"] = "\(Website)";
          
                orderDict["TXN_AMOUNT"] = "\(strAmount)"
            
            orderDict["ORDER_ID"] = "\(orderidNew)";
            orderDict["CUST_ID"] = "\((String(describing: dictLoginData.value(forKey: "User_Id")!)))";
            orderDict["CALLBACK_URL"] = "\(callBackURL)\(orderidNew)"
            orderDict["CHECKSUMHASH"] = "\(checkSumHashh)";
            print(orderDict)
        let pgOrder = PGOrder(params: orderDict )
        let transaction = PGTransactionViewController.init(transactionFor: pgOrder)
        if(type_Production_Staging == "Staging"){
            transaction!.serverType = eServerTypeStaging
        }else{
                transaction!.serverType = eServerTypeProduction
        }
        transaction!.merchant = PGMerchantConfiguration.default()
        transaction!.loggingEnabled = true
        transaction!.delegate = self
        
        if responds(to: #selector(setter: self.edgesForExtendedLayout)) {
            let headerView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: view.frame.size.width, height: 64.0))
            headerView.backgroundColor = UIColor.clear
            let topBar = UIView(frame: CGRect(x: 0.0, y: 20.0, width: self.view.frame.width, height: 50.0))
            topBar.backgroundColor = UIColor.clear
            headerView.addSubview(topBar)
            if(type_Production_Staging == "Staging"){
                headerView.addSubview(topBar)
                
            }
            let cancelButton = UIButton(type: .system)
            cancelButton.setTitle("Back", for: .normal)
            cancelButton.frame = CGRect(x: 8.0, y: 25.0, width: 40.0, height: 40.0)
            cancelButton.tintColor = UIColor.lightGray
            transaction?.cancelButton = cancelButton
        }else{
            let topBar = UIView(frame: CGRect(x: 0.0, y: 0.0, width: view.frame.size.width, height: 50.0))
            topBar.backgroundColor = UIColor.clear
            if(type_Production_Staging == "Staging"){
                transaction?.topBar = topBar
            }
            let cancelButton = UIButton(type: .system)
            cancelButton.frame = CGRect(x: 8.0, y: 5.0, width: 40.0, height: 40.0)
            cancelButton.setTitle("Back", for: .normal)
            cancelButton.tintColor = UIColor.lightGray
            
            transaction?.cancelButton = cancelButton
        }
        self.navigationController?.pushViewController(transaction!, animated: true)
    }
    
    
    func didFinishedResponse(_ controller: PGTransactionViewController!, response responseString: String!) {
        print(responds)
        
        self.navigationController?.popViewController(animated: false)
        if let data = responseString.data(using: String.Encoding.utf8) {
            do {
                if let jsonresponse = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:Any] , jsonresponse.count > 0{
                    let status = jsonresponse["STATUS"]as! String
                    if(status == "TXN_SUCCESS"){
                        self.donateAPICall(dictPaymentData: (jsonresponse as NSDictionary).mutableCopy()as! NSMutableDictionary)
                    }else if(status == "PENDING"){
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: jsonresponse["RESPMSG"]as! String, viewcontrol: self)

                    }
                    else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                    }
                }
            } catch {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
            }
        }
        
    }
    
    func didCancelTrasaction(_ controller: PGTransactionViewController!) {
        
        self.navigationController?.popViewController(animated: false)
        
        
    }
    
    func errorMisssingParameter(_ controller: PGTransactionViewController!, error: Error!) {
        controller.view.isUserInteractionEnabled = false
        
        
     //   print("error : ",error)
        self.navigationController?.popViewController(animated: false)
        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
        
        
    }
    
    func didSucceedTransaction(_ controller: PGTransactionViewController!, response: [AnyHashable : Any]!) {
        controller.view.isUserInteractionEnabled = false
        
     //   print(response)
        self.navigationController?.popViewController(animated: false)
        
    }
    
    func didFailTransaction(_ controller: PGTransactionViewController!, error: Error!, response: [AnyHashable : Any]!) {
        controller.view.isUserInteractionEnabled = false
        
       // print("error : ",error)
      //  print("didFailTransaction : ",response)
        self.navigationController?.popViewController(animated: false)
        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "FailTransaction", viewcontrol: self)
        
    }
    
    func didCancelTransaction(_ controller: PGTransactionViewController!, error: Error!, response: [AnyHashable : Any]!) {
       // print("error : ",error)
       // print("didCancelTransaction : ",response)
        
    }
    //
    func didFinishCASTransaction(_ controller: PGTransactionViewController!, response: [AnyHashable : Any]!) {
      //  print("didFinishCASTransaction : ",response)
        //  showAlert(controller: controller, title: "cas", message: "")
    }
    
    
}
// MARK:
// MARK:- Validation

extension OrderDetailVC {
    
    func validationForOrder() -> Bool {
        
        if(txtName.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_Name, viewcontrol: self)
            return false
        }
        else if(txtMobile.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_Mobile, viewcontrol: self)
            return false
        }
        else if(txtMobile.text!.count < 10){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_Mobile_limit, viewcontrol: self)
            return false
        }
        else if(txtAlterMobile.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Alternate Mobile Number Is Required!", viewcontrol: self)
            return false
        }
        else if(txtAlterMobile.text!.count < 10){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Alternate Mobile Number Is Invalid!", viewcontrol: self)
            return false
        }
        else if(txtEmail.text!.count != 0){
            if((txtEmail.text?.isValidEmailAddress())!){
                return true
            }
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_InvalidEmail, viewcontrol: self)
            return false
        }
        
        if(txtSocity.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Socity Name/ Builder Name Is Required!", viewcontrol: self)
            return false
        }
        else if(txtAddress.text?.count == 0){
                  showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Address Is Required!", viewcontrol: self)
                  return false
              }
        else if(txtLandMArk.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Landmark Is Required!", viewcontrol: self)
            return false
        }
        else if(txtZOneno.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Zone No Is Required!", viewcontrol: self)
            return false
        }
        else if(txtWardno.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Muncipal Ward No Is Required!", viewcontrol: self)
            return false
        }
        
        return true
    }
   
}
extension String {
    
    func isValidEmailAddress() -> Bool {
        let emailRegEx = "(?:[a-zA-Z0-9!#$%\\&‘*+/=?\\^_`{|}~-]+(?:\\.[a-zA-Z0-9!#$%\\&'*+/=?\\^_`{|}"
            + "~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\"
            + "x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-"
            + "z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5"
            + "]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-"
            + "9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21"
            + "-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])"
        
        let emailTest = NSPredicate(format: "SELF MATCHES[c] %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }
}
// MARK:
// MARK:- UITextFieldDelegate

extension OrderDetailVC : UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing( true)
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
}
//MARK:-
//MARK:- ---------PopUpDelegate

extension OrderDetailVC : PopUpDelegate
{
    func getDataFromPopupDelegate(dictData: NSDictionary, tag: Int) {
        if(tag == 15){
            txtWardno.text = "\(dictData.value(forKey: "WardNo")!)"
            txtWardno.tag = Int("\(dictData.value(forKey: "WardId")!)")!

        }
        else if(tag == 16){
            txtZOneno.text = "\(dictData.value(forKey: "ZoneNo")!)"
            txtZOneno.tag = Int("\(dictData.value(forKey: "ZoneId")!)")!
            self.aryWardlist = NSMutableArray()
            txtWardno.text = ""
            txtWardno.tag = 0
        }
     
    }
}
