//
//  NewsVC.swift
//  CitizenCOP
//
//  Created by Navin Patidar on 5/23/19.
//  Copyright © 2019 CitizenCop. All rights reserved.
//

import UIKit
import SWXMLHash
import StringExtensionHTML
import AEXML

class NewsVC: UIViewController {
    
    // MARK: - --------------IBOutlet
    // MARK: -
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var tvlist: UITableView!
    // MARK: - --------------Variable
    // MARK: -
    var aryList = NSMutableArray()
    var refresher = UIRefreshControl()
    var ErrorMsg = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryTheamColor)
        tvlist.tableFooterView = UIView()
        tvlist.estimatedRowHeight = 80.0
        self.refresher.addTarget(self, action: #selector(RefreshloadData), for: .valueChanged)
        self.tvlist!.addSubview(refresher)
        if(dictCityData.value(forKey: "CityData")as! NSArray).count != 0{
            let strCityId = "\(((dictCityData.value(forKey: "CityData")as! NSArray).object(at: 0)as! NSDictionary).value(forKey: "city_id")!)"
            if(self.aryList.count == 0){
                callNewsAPI(strCityID: strCityId, strLoaderTag: 1)
            }else{
                self.tvlist.reloadData()
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    // MARK: - --------------Pull Refresh
    // MARK: -
    @objc func RefreshloadData() {
        if(dictCityData.value(forKey: "CityData")as! NSArray).count != 0{
            let strCityId = "\(((dictCityData.value(forKey: "CityData")as! NSArray).object(at: 0)as! NSDictionary).value(forKey: "city_id")!)"
            callNewsAPI(strCityID: strCityId, strLoaderTag: 0)
        }
    }
    // MARK: - ---------------IBAction
    // MARK: -
    @IBAction func actiononBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        
    }
    @IBAction func actiononHelp(_ sender: UIButton) {
        let testController = mainStoryboard.instantiateViewController(withIdentifier: "HelpVC")as! HelpVC
        testController.strMessage = alertNewsHelp
        self.navigationController?.pushViewController(testController, animated: true)
        
    }
    // MARK: - ---------------API CAlling
    // MARK: -
    func callNewsAPI(strCityID : String ,strLoaderTag : Int) {
        if !isInternetAvailable() {
            FTIndicator.showToastMessage(alertInternet)
            ErrorMsg = alertInternet
            tvlist.reloadData()
        }else{
            let loading = DPBasicLoading(table: self.tvlist, fontName: "HelveticaNeue")
            strLoaderTag == 1 ? loading.startLoading(text: "Loading...") : nil
            WebServiceClass.callAPIBYGET_XML(parameter: NSDictionary(), url: API_News + strCityID) { (responce, status) in
                loading.endLoading()
                self.refresher.endRefreshing()
                if status == "Suceess"{
                    self.aryList = NSMutableArray()
                    let xmlData = (responce.value(forKey: "data")as! Data)
                    let xml1 = XMLHash.parse(xmlData)
                    for elem in xml1["NewDataSet"]["Table1"].all {
                        let dict = NSMutableDictionary()
                        dict.setValue("\(elem["title"].element!.text)", forKey: "title")
                        dict.setValue("\(elem["Message"].element!.text)", forKey: "Message")
                        dict.setValue("\(elem["ThumbImage"].element!.text)", forKey: "ThumbImage")
                        dict.setValue("\(elem["Image"].element!.text)", forKey: "Image")
                        dict.setValue("\(elem["Time"].element!.text)", forKey: "Time")
                        self.aryList.add(dict)
                    }
                    if(self.aryList.count == 0){
                        self.ErrorMsg = alertDataNotFound
                    }
                    self.tvlist.reloadData()
                }
                else{
                    self.ErrorMsg = alertSomeError
                    self.tvlist.reloadData()
                }
            }
        }
    }
}
// MARK: - ----------------UITableViewDelegate
// MARK: -

extension NewsVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return aryList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tvlist.dequeueReusableCell(withIdentifier: "NewsCell", for: indexPath as IndexPath) as! NewsCell
        let dict = aryList.object(at: indexPath.row)as! NSDictionary
        cell.lblSubtitle.text = "\(dict.value(forKey: "Message")!)".html2String
        cell.lblTitle.text = "\(dict.value(forKey: "title")!)"
        cell.lblDate.text = "\(dict.value(forKey: "Time")!)"
        let urlImage = BaseHostURL + "/\(dict.value(forKey: "Image")!)"
        cell.imageForNews.setImageWith(URL(string: urlImage), placeholderImage: UIImage(named: ""), options: SDWebImageOptions(rawValue: 1), completed: { (image, error, type, url) in
            print(url ?? 0)
        }, usingActivityIndicatorStyle: .gray)
        cell.imageForNews.layer.borderWidth = 1
        cell.imageForNews.layer.masksToBounds = false
        cell.imageForNews.layer.borderColor = UIColor.groupTableViewBackground.cgColor
        cell.imageForNews.layer.cornerRadius = 8.0
        cell.imageForNews.clipsToBounds = true
        cell.imageForNews.contentMode = .scaleAspectFit
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        
        if(DeviceType.IS_IPAD){
           return UITableView.automaticDimension
        }else{
            return 104
        }
       
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(aryList.count != 0){
            let testController = mainStoryboard.instantiateViewController(withIdentifier: "NewsDetailVC")as! NewsDetailVC
            let dict = aryList.object(at: indexPath.row)as! NSDictionary
            testController.dictNews = dict.mutableCopy()as! NSMutableDictionary
            self.navigationController?.pushViewController(testController, animated: true)
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 50.0
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        let customView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 50))
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 50))
        if(aryList.count != 0){
            ErrorMsg = ""
        }else{
            button.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        }
        button.setTitle(ErrorMsg, for: .normal)
        customView.addSubview(button)
        customView.backgroundColor = UIColor.clear
        button.setTitleColor(UIColor.darkGray, for: .normal)
        return customView
    }
    
    @objc func buttonAction(_ sender: UIButton!) {
        let strCityId = "\(((dictCityData.value(forKey: "CityData")as! NSArray).object(at: 0)as! NSDictionary).value(forKey: "city_id")!)"
        callNewsAPI(strCityID: strCityId, strLoaderTag: 1)
    }
    
    
    
}
// MARK: - ----------------NewsCell
// MARK: -
class NewsCell: UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubtitle: UILabel!
    @IBOutlet weak var imageForNews: UIImageView!
    @IBOutlet weak var lblDate: UILabel!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}
