//
//  NewsDetailVC.swift
//  CitizenCOP
//
//  Created by Navin Patidar on 5/23/19.
//  Copyright © 2019 CitizenCop. All rights reserved.
//

import UIKit

class NewsDetailVC: UIViewController {

    // MARK: - --------------IBOutlet
    // MARK: -
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var lbltitle: UILabel!
    var dictNews = NSMutableDictionary()
    @IBOutlet weak var txtDetail: KMPlaceholderTextView!
    @IBOutlet weak var imgNews: UIImageView!
    @IBOutlet weak var lblDate: UILabel!

    // MARK: - --------------Life Cycle
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryTheamColor)
        let strDetail = "\(dictNews.value(forKey: "Message")!)".html2String
        let strTitle = "\(dictNews.value(forKey: "title")!)"
        txtDetail.text = "\(strDetail)"
        lbltitle.text = strTitle
        lblDate.text = "\(dictNews.value(forKey: "Time")!)"

        let urlImage = BaseHostURL + "/\(dictNews.value(forKey: "Image")!)"
        imgNews.setImageWith(URL(string: urlImage), placeholderImage: UIImage(named: ""), options: SDWebImageOptions(rawValue: 1), completed: { (image, error, type, url) in
            print(url ?? 0)
        }, usingActivityIndicatorStyle: .gray)
        imgNews.layer.borderWidth = 1
        imgNews.layer.masksToBounds = false
        imgNews.layer.borderColor = UIColor.groupTableViewBackground.cgColor
        imgNews.layer.cornerRadius = 8.0

    }
    // MARK: - ---------------IBAction
    // MARK: -
    @IBAction func actiononBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        
    }
}
