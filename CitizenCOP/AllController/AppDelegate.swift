//
//  AppDelegate.swift
//  CitizenCOP
//
//  Created by Navin Patidar on 5/17/19.
//  Copyright © 2019 CitizenCop. All rights reserved.
//

import UIKit
import CoreData
import Photos
import Contacts
import IQKeyboardManager
import CoreLocation
import SWXMLHash
import StringExtensionHTML
import AEXML

import Firebase
import UserNotifications
import FirebaseMessaging

import MessageUI

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    var locationManager = CLLocationManager()
    let center = UNUserNotificationCenter.current()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        if #available(iOS 13.0, *) {
            window?.overrideUserInterfaceStyle = .light
        }
        
        if !DeviceType.IS_IPAD {
            mainStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
        }else{
            mainStoryboard = UIStoryboard.init(name: "MainiPad", bundle: nil)
        }
        //        deleteAllRecords(strEntity:"AllCountry")
        //        deleteAllRecords(strEntity:"AllState")
        //        deleteAllRecords(strEntity:"AllCity")
        gotoViewController()
        IQKeyboardManager.shared().isEnabled = true
        FTIndicator.setIndicatorStyleToDefaultStyle()
        //1 FireBase
        AppDelegate().registerForRemoteNotifications()
        FirebaseApp.configure()
        center.delegate = self
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        
    }
    func applicationDidEnterBackground(_ application: UIApplication) {
        
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        UIApplication.shared.applicationIconBadgeNumber = 0
        
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        
        self.saveContext()
    }
    
    
    
    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "CitizenCOP")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    
    class func getContext () -> NSManagedObjectContext {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.persistentContainer.viewContext
    }
    
    
    
    func gotoViewController()  {
        
        if (nsud.value(forKey: "CitizenCOP_CityData") != nil) {
            
            let obj_ISVC : DynemicSplashVC = mainStoryboard.instantiateViewController(withIdentifier: "DynemicSplashVC") as! DynemicSplashVC
            
            let navigationController = UINavigationController(rootViewController: obj_ISVC)
            rootViewController(nav: navigationController)
            
        }else{
            let obj_ISVC : CitySelectionVC = mainStoryboard.instantiateViewController(withIdentifier: "CitySelectionVC") as! CitySelectionVC
            let navigationController = UINavigationController(rootViewController: obj_ISVC)
            
            rootViewController(nav: navigationController)
        }
        
    }
    func rootViewController(nav : UINavigationController)  {
        self.window?.rootViewController = nav
        self.window?.makeKeyAndVisible()
        nav.setNavigationBarHidden(true, animated: true)
    }
    
    
    
}

//MARK:-
//MARK:- ---------CLLocationManagerDelegate
extension AppDelegate : CLLocationManagerDelegate{
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        GlobleLat = "\(locValue.latitude)"
        GlobleLong = "\(locValue.longitude)"
        Globlespeed = "\(String(describing: manager.location?.speed))"
        GlobleAcc = "\(String(describing: manager.location?.horizontalAccuracy))"
        GlobleAltitude = "\(String(describing: manager.location?.altitude))"
        
     
        if(nsud.value(forKey: "CitizenCOP_MySafeZone") != nil) || (nsud.value(forKey: "CitiZenCOP_JoinedGroupLocation") != nil){
            let dictData = (nsud.value(forKey: "CitizenCOP_MySafeZone")as! NSDictionary)
            let ZoneLat = "\(dictData.value(forKey: "ZoneLat")!)"
            let ZoneLong = "\(dictData.value(forKey: "ZoneLong")!)"
            let ZoneRadius = "\(dictData.value(forKey: "ZoneRadius")!)"
            print("Did location updates is called", manager.location!.coordinate)
            let coordinate₀ = locations.last
            let MomentaryLatitudeDesti = Double(ZoneLat)
            let MomentaryLongitudeDesti = Double(ZoneLong)
            let coordinate₁ = CLLocation(latitude: MomentaryLatitudeDesti!, longitude: MomentaryLongitudeDesti!)
            let distanceInMeters = coordinate₀!.distance(from: coordinate₁)
            if(Int(distanceInMeters) > Int(ZoneRadius)!){
                DispatchQueue.main.async {
                    FTIndicator.showToastMessage("OUT oF renge")
                }
            }
        }else{
            locationManager.stopUpdatingLocation()
        }
        
        
        
        
    }
    
}

// MARK: --------UNNotification/Messaging Delegate Method----------

extension AppDelegate : UNUserNotificationCenterDelegate , MessagingDelegate{
    //MARK: RegisterPushNotification
    
    //MARK: RegisterPushNotification
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        Messaging.messaging().apnsToken = deviceToken
        Messaging.messaging().token { (token, error) in
            if let error = error {
                nsud.setValue("", forKey: "CitizenCOP_FCM_Token")
                nsud.synchronize()
                print("Error fetching remote instance ID: \(error.localizedDescription)")
            } else if let token = token {
                nsud.setValue("\(token)", forKey: "CitizenCOP_FCM_Token")
                nsud.synchronize()
                print("Token is \(token)")
            }
        }
    }

    
    
    func registerForRemoteNotifications()  {
        if #available(iOS 10, *) {
            
            
            center.delegate = self
            center.getNotificationSettings(completionHandler: { settings in
                switch settings.authorizationStatus {
                case .authorized, .provisional:
                    print("authorized")
                    DispatchQueue.main.async {
                        UIApplication.shared.registerForRemoteNotifications()
                        Messaging.messaging().delegate = self
                    }
                    
                case .denied:break
                case .notDetermined:break
                case .ephemeral:
                    break
                @unknown default:
                    break
                }
            })
            
        }
        else {
            let settings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            UIApplication.shared.registerUserNotificationSettings(settings)
            DispatchQueue.main.async {
                UIApplication.shared.registerForRemoteNotifications()
            }
            UIApplication.shared.applicationIconBadgeNumber = 0
        }
    }
 
    
    
    
    func hendelNotidata(userInfo :NSDictionary){
        let obj_ISVC : PushNotificationVC = mainStoryboard.instantiateViewController(withIdentifier: "PushNotificationVC") as! PushNotificationVC
        let navigationController = UINavigationController(rootViewController: obj_ISVC)
        rootViewController(nav: navigationController)
    }


    

    //Handle Local Notification Center Delegate methods
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.badge,.alert,.sound])
        let state: UIApplication.State = UIApplication.shared.applicationState
        
        if state == .active {
            print("original identifier was : \(notification.request.content.title)")
            if("\(notification.request.content.title)" == "HelpMe"){
                NotificationCenter.default.post(name: Notification.Name(rawValue: "HelpMeNotif"), object: nil)
            }
        }
        else  {
            
        }
    }
    
    
    
    // application kill
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
      //  FTIndicator.showToastMessage("HI notification AYAYA")
        let userInfo = response.notification.request.content.userInfo
        
        if userInfo.count == 0 {
            let title = response.notification.request.content.title
            let subtitle = response.notification.request.content.subtitle
            if(title == "Report an Incident"){
                PushNotificationVC().createData(strTitle :title , strBody : subtitle ,apsData : NSDictionary(), strPushType: title)
                hendelNotidata(userInfo: userInfo as NSDictionary)
                
            } else if (title == "HelpMe"){
                nsud.set(true, forKey: "HelpMeTimerNoti")
                let obj_ISVC : DashBoardVC = mainStoryboard.instantiateViewController(withIdentifier: "DashBoardVC") as! DashBoardVC
                let navigationController = UINavigationController(rootViewController: obj_ISVC)
                rootViewController(nav: navigationController)
                
            }
        }else{
            let strPushType = "\((userInfo as AnyObject).value(forKey: "PushType")!)"
            if(strPushType == "ForMemberLocationUpdate"){
                if(nsud.value(forKey: "CitiZenCOP_JoinedGroupLocation") != nil){
                    if(nsud.value(forKey: "CitiZenCOP_JoinedGroupLocation")as! Bool){
                        SettingVC().callUpdateMemberLocationAPI()
                    }
                }
            }else{
                let dict = (userInfo as AnyObject)as! NSDictionary
                let aps = (dict.value(forKey: "aps")as! NSDictionary)
                let alert = (aps.value(forKey: "alert")as! NSDictionary)
                let strTitle = (alert.value(forKey: "title")!)
                let strbody = (alert.value(forKey: "body")!)
                PushNotificationVC().createData(strTitle :strTitle as! String , strBody : strbody as! String ,apsData : aps, strPushType: strPushType)
                hendelNotidata(userInfo: userInfo as NSDictionary)
            }
            
        }
        completionHandler()
        
    }
    
    // Forground and background
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
     //   FTIndicator.showToastMessage("HI notification AYAYA")
        let state: UIApplication.State = UIApplication.shared.applicationState
        let strPushType = "\((userInfo as AnyObject).value(forKey: "PushType")!)"
        if(strPushType == "ForMemberLocationUpdate"){
            if(nsud.value(forKey: "CitiZenCOP_JoinedGroupLocation") != nil){
                if(nsud.value(forKey: "CitiZenCOP_JoinedGroupLocation")as! Bool){
                    SettingVC().callUpdateMemberLocationAPI()
                }
            }
        }else{
            let dict = (userInfo as AnyObject)as! NSDictionary
            let aps = (dict.value(forKey: "aps")as! NSDictionary)
            let alert = (aps.value(forKey: "alert")as! NSDictionary)
            let strTitle = (alert.value(forKey: "title")!)
            let strbody = (alert.value(forKey: "body")!)
            if state == .active { //
                PushNotificationVC().createData(strTitle :strTitle as! String , strBody : strbody as! String ,apsData : aps, strPushType: strPushType)
                FTIndicator.showNotification(with:  UIImage(named: "icon152"), title: strTitle as? String, message: strbody as? String) {}
            }
            else   //  .Inactive (background to forground)
            {
                
                PushNotificationVC().createData(strTitle :strTitle as! String , strBody : strbody as! String ,apsData : aps, strPushType: strPushType)
                hendelNotidata(userInfo: userInfo as NSDictionary)
            }
        }
        
        
    }
    
    
}

