//
//  ReportTraficJamVC.swift
//  CitizenCOP
//
//  Created by Navin Patidar on 23/12/21.
//

import UIKit
import MessageUI
import CoreLocation

class ReportTraficJamVC: UIViewController, UINavigationControllerDelegate {

    // MARK: - ---------------IBOutlet
    // MARK: -
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var btnSOS: UIButton!
    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var tv: UITableView!

    @IBOutlet weak var txtView: KMPlaceholderTextView!
    var strlblTittle = String()

    var locationManager = CLLocationManager()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        tv.tableFooterView = UIView()
        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryTheamColor)
       checkLocationPermission()
        lblHeader.text = strlblTittle
        txtView.layer.borderColor = UIColor.darkGray.cgColor
        txtView.layer.borderWidth = 1.0
       // txtView.backgroundColor = UIColor.lightGray
        // Do any additional setup after loading the view.
    }
    // MARK: - ---------------IBAction
    // MARK: -
    @IBAction func actiononBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        
    }
    @IBAction func actiononHelp(_ sender: UIButton) {
        let testController = mainStoryboard.instantiateViewController(withIdentifier: "HelpVC")as! HelpVC
        testController.strMessage = alertHelpSOS
        self.navigationController?.pushViewController(testController, animated: true)
        
    }
    @IBAction func actiononAddContact(_ sender: UIButton) {
        let testController = mainStoryboard.instantiateViewController(withIdentifier: "AddContactVC")as! AddContactVC
        self.navigationController?.pushViewController(testController, animated: true)
    }
    @IBAction func actiononSOS(_ sender: UIButton) {
        let strCityId = "\(((dictCityData.value(forKey: "CityData")as! NSArray).object(at: 0)as! NSDictionary).value(forKey: "city_id")!)"
     
        
        var strLat = GlobleLat
        var strLong = GlobleLong
        
        if strLat.count > 7 {
            strLat = strLat[0...7]
        }
        if strLong.count > 7 {
            strLong = strLong[0...7]

        }
        
    
        let msgBody = "Help Me Report Traffic Jam! My Location :- \nmaps.google.com/maps?f=q&t=m&q=\(strLat),\(strLong)&&&\(txtView.text!)&&&\(strLat)&&&\(strLong)&&&\(strCityId)"
        
        sender.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        UIView.animate(withDuration:1.5,
                       delay: 0,
                       usingSpringWithDamping: 0.2,
                       initialSpringVelocity: 6.0,
                       options: .allowUserInteraction,
                       animations: {
                        sender.transform = .identity
                        if (MFMessageComposeViewController.canSendText()) {
                            let messageVC = MFMessageComposeViewController()
                            messageVC.delegate = self
                            messageVC.body = msgBody;
                            if nsud.value(forKey: "CitizenCOP_HelpContact") != nil {
                                var aryContactList = NSMutableArray()
                                aryContactList = (nsud.value(forKey: "CitizenCOP_HelpContact")as! NSArray).mutableCopy()as! NSMutableArray
                                var strrecipients = ""
                                for item in aryContactList{
                                    strrecipients.append("\(item as AnyObject),")
                                }
                                let strHelpNumber = "\(((dictCityData.value(forKey: "CityData")as! NSArray).object(at: 0)as! NSDictionary).value(forKey: "TrafficJamHelpNo")!)"
                                strrecipients.append("\(strHelpNumber)")
                                strrecipients.append(",")
                                strrecipients.append(DeviceNumberOffice1)
                                strrecipients.append(",")
                                strrecipients.append(DeviceNumberOffice2)
//                                strrecipients.append(",")
//                                strrecipients.append("8516821106")
                                
                                
                                
                                let phoneNumberString = strrecipients
                                let recipientsArray = phoneNumberString.components(separatedBy: ",")
                                messageVC.recipients = recipientsArray

                            }
                            messageVC.messageComposeDelegate = self
                            self.present(messageVC, animated: true, completion: nil)
                        }else{
                            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertCalling, viewcontrol: self)
                   }
            },
                       completion: nil)

    }
    

    // MARK: - ----------------Extra Function
    // MARK: -
    func checkLocationPermission() {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.requestAlwaysAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
        // locationManager.requestLocation()
        print("Location services are not enabled")
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                let alert = UIAlertController(title: "Location Unavailable", message: "Please check to see if device settings doesn't allow Location access", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction (title: "Settings", style: .default, handler: { (nil) in
                    
                    guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                        return
                    }
                    
                    if UIApplication.shared.canOpenURL(settingsUrl) {
                        UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                            print("Settings opened: \(success)") // Prints true
                        })
                    }
                }))
                self.present(alert, animated: true, completion: nil)
            case .authorizedAlways, .authorizedWhenInUse: break
                
            default:
                break
            }
        } else {
            
        }
    }
    
}
// MARK: - ----------------MFMessageComposeViewControllerDelegate
// MARK: -
extension ReportTraficJamVC : MFMessageComposeViewControllerDelegate{
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        switch (result) {
        case .cancelled:
            print("Message was cancelled")
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Message was cancelled", viewcontrol: self)
            dismiss(animated: true, completion: nil)
        case .failed:
            print("Message failed")
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Message failed", viewcontrol: self)

            dismiss(animated: true, completion: nil)
        case .sent:
            print("Message was sent")

            dismiss(animated: false, completion: nil)
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Message Send Succesfully!", viewcontrol: self)

            
        default:
            break
        }
    }
    
}
//MARK:-
//MARK:- ---------CLLocationManagerDelegate
extension ReportTraficJamVC : CLLocationManagerDelegate{
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        GlobleLat = "\(locValue.latitude)"
        GlobleLong = "\(locValue.longitude)"
    }
    
}


extension String {
  subscript(_ i: Int) -> String {
    let idx1 = index(startIndex, offsetBy: i)
    let idx2 = index(idx1, offsetBy: 1)
    return String(self[idx1..<idx2])
  }

  subscript (r: Range<Int>) -> String {
    let start = index(startIndex, offsetBy: r.lowerBound)
    let end = index(startIndex, offsetBy: r.upperBound)
    return String(self[start ..< end])
  }

  subscript (r: CountableClosedRange<Int>) -> String {
    let startIndex =  self.index(self.startIndex, offsetBy: r.lowerBound)
    let endIndex = self.index(startIndex, offsetBy: r.upperBound - r.lowerBound)
    return String(self[startIndex...endIndex])
  }
}
