//
//  AddContactVC.swift
//  CitizenCOP
//
//  Created by Navin Patidar on 5/21/19.
//  Copyright © 2019 CitizenCop. All rights reserved.
//

import UIKit
import ContactsUI
import Contacts
import CoreLocation

class AddContactVC: UIViewController {
    
    // MARK: - --------------IBOutlet
    // MARK: -
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var tvlist: UITableView!
    @IBOutlet weak var txtNumber: ACFloatingTextfield!
    @IBOutlet weak var txtMessage: KMPlaceholderTextView!

    // MARK: - --------------Variable
    // MARK: -
    var aryContactList = NSMutableArray()
    var locationManager = CLLocationManager()

    override func viewDidLoad() {
        super.viewDidLoad()
        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryTheamColor)
        tvlist.tableFooterView = UIView()
        tvlist.estimatedRowHeight = 80.0
        
       
        
        if nsud.value(forKey: "CitizenCOP_HelpContact") != nil {
            aryContactList = NSMutableArray()
            aryContactList = (nsud.value(forKey: "CitizenCOP_HelpContact")as! NSArray).mutableCopy()as! NSMutableArray
            tvlist.reloadData()
            if(aryContactList.count >= 5){
                
            }
        }
         checkLocationPermission()
    }
    // MARK: - ---------------IBAction
    // MARK: -
    @IBAction func actiononBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        
    }
    @IBAction func actiononHelp(_ sender: UIButton) {
        let testController = mainStoryboard.instantiateViewController(withIdentifier: "HelpVC")as! HelpVC
        testController.strMessage = alertHelpSOS

        self.navigationController?.pushViewController(testController, animated: true)
        
    }
    @IBAction func actiononSave(_ sender: UIButton) {
        if(txtNumber.text!.count < 10){
            txtNumber.shake()
        }else{
           
            if(aryContactList.count >= 5){
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertReachedMax, viewcontrol: self)
            }else{
                aryContactList.add(txtNumber.text!)
                nsud.set(aryContactList, forKey: "CitizenCOP_HelpContact")
                nsud.synchronize()
                txtNumber.text = ""
                FTIndicator.showToastMessage(alert_AddMobileNumber)
            }
            self.tvlist.reloadData()
            self.view.endEditing(true)
        }
    }
    @IBAction func actiononAddContact(_ sender: UIButton) {
        
        self.view.endEditing(true)
        let cnPicker = CNContactPickerViewController()
        cnPicker.delegate = self
        self.present(cnPicker, animated: true, completion: nil)
    }
    
    // MARK: - ----------------Extra Function
    // MARK: -
    func checkLocationPermission() {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.requestAlwaysAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
        // locationManager.requestLocation()
        print("Location services are not enabled")
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                let alert = UIAlertController(title: "Location Unavailable", message: "Please check to see if device settings doesn't allow Location access", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction (title: "Settings", style: .default, handler: { (nil) in
                    
                    guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                        return
                    }
                    
                    if UIApplication.shared.canOpenURL(settingsUrl) {
                        UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                            print("Settings opened: \(success)") // Prints true
                        })
                    }
                }))
              self.present(alert, animated: true, completion: nil)
            case .authorizedAlways, .authorizedWhenInUse: break
            
            default:
                break
            }
        } else {
            
        }
    }
    
    
}
// MARK: - ----------------UITableViewDelegate
// MARK: -

extension AddContactVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return aryContactList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tvlist.dequeueReusableCell(withIdentifier: "ContactCell", for: indexPath as IndexPath) as! ContactCell
        cell.lblTitle.text = "Contact \(indexPath.row + 1). " + "\(aryContactList.object(at: indexPath.row))"
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            aryContactList.removeObject(at: indexPath.row)
            nsud.set(aryContactList, forKey: "CitizenCOP_HelpContact")
            nsud.synchronize()
            self.tvlist.reloadData()
        }
    }
   
}


// MARK: - ----------------UserDashBoardCell
// MARK: -
class ContactCell: UITableViewCell {
    //DashBoard
    @IBOutlet weak var lblTitle: UILabel!
   
    
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}

//MARK:-
//MARK:- ---------UITextFieldDelegate

extension AddContactVC : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if ( textField == txtNumber)
        {
            return txtFiledValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 9)
        }
            
        else
        {
            return true
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
}


//MARK:-
//MARK:- ---------CNContactPickerDelegate

extension AddContactVC : CNContactPickerDelegate{
    // MARK: - -------------CNContactPickerViewController
    // MARK: -
    
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contact: CNContact) {
        
        for number in contact.phoneNumbers {
            
            txtNumber.text = ""

            let phoneNumber = number.value
            print("number is = \(phoneNumber)")
            txtNumber.text = phoneNumber.stringValue
        }
           
        }
   
    
    
    func contactPickerDidCancel(_ picker: CNContactPickerViewController) {
        print("Cancel Contact Picker")
    }
}
//MARK:-
//MARK:- ---------CLLocationManagerDelegate
extension AddContactVC : CLLocationManagerDelegate{
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        GlobleLat = "\(locValue.latitude)"
        GlobleLong = "\(locValue.longitude)"
    }
    
}
