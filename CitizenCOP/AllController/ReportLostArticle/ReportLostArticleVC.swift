//
//  ReportLostArticleVC.swift
//  CitizenCOP
//
//  Created by Navin Patidar on 5/31/19.
//  Copyright © 2019 CitizenCop. All rights reserved.
//

import UIKit

class ReportLostArticleVC: UIViewController {
  
    // MARK: - --------------IBOutlet
    // MARK: -
    @IBOutlet weak var lblstateCity: UILabel!

    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var btnIMEIRadio: UIButton!
    @IBOutlet weak var btnComplainRadio: UIButton!
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var btnRegisterComplain: UIButton!
    @IBOutlet weak var txtNumber: ACFloatingTextfield!
    var isImeino = true
    
    @IBOutlet var viewSearchResult: UIView!
    @IBOutlet weak var tvSearchResult: UITableView!
    
    var aryArticle = NSMutableArray()
    var aryGeneralDetail = NSMutableArray()
    var rView = ReportView()
    // MARK: - --------------Variables
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryTheamColor)
        btnSearch.layer.cornerRadius = 5.0
        btnSearch.backgroundColor = hexStringToUIColor(hex: primaryDark)
        btnRegisterComplain.layer.cornerRadius = 5.0
        btnRegisterComplain.backgroundColor = hexStringToUIColor(hex: primaryDark)
        btnIMEIRadio.setImage(UIImage(named: "RadioButton-Selected.png"), for: .normal)
        btnComplainRadio.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        txtNumber.placeholder = "Enter IMEI / ESN Number"
        txtNumber.text = ""
        isImeino = true
        lblstateCity.text = ""
        if(dictCityData.value(forKey: "CityData")as! NSArray).count != 0{
            let strCityname = "\(((dictCityData.value(forKey: "CityData")as! NSArray).object(at: 0)as! NSDictionary).value(forKey: "city_name")!)"
            if(nsud.value(forKey: "CitizenCOP_CityData") != nil){
                let Citizen_StateName = "\((nsud.value(forKey: "CitizenCOP_CityData")as! NSDictionary).value(forKey: "Citizen_StateName")!)"
                lblstateCity.text = "Report any lost article / document in \(strCityname) of \(Citizen_StateName)."
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    // MARK: - --------------Actions
    // MARK: -
    
    @IBAction func action_Back(_ sender: Any) {
        for item in self.view.subviews {
              if(item == viewSearchResult){
                viewSearchResult.tag = 1
            }
        }
        if(viewSearchResult.tag == 1){
            viewSearchResult.removeFromSuperview()
             viewSearchResult.tag = 2
        }else{
           self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func action_Help(_ sender: Any) {
        self.view.endEditing(true)
        let testController = mainStoryboard.instantiateViewController(withIdentifier: "HelpVC")as! HelpVC
        testController.strMessage = alertReportLookUpHelp
        self.navigationController?.pushViewController(testController, animated: true)
    }
    @IBAction func action_Radio(_ sender: UIButton) {
        if(sender.tag == 1){
            btnIMEIRadio.setImage(UIImage(named: "RadioButton-Selected.png"), for: .normal)
            btnComplainRadio.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
            txtNumber.placeholder = "Enter IMEI / ESN Number"
            txtNumber.text = ""
            isImeino = true
        }else{
            btnComplainRadio.setImage(UIImage(named: "RadioButton-Selected.png"), for: .normal)
            btnIMEIRadio.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
            txtNumber.placeholder = "Enter Complaint Number"
           // txtNumber.text = "A81342D3-2019"
             txtNumber.text = ""
            isImeino = false
        }
    }
    @IBAction func action_Search(_ sender: Any) {
        self.view.endEditing(true)
        self.viewSearchResult.removeFromSuperview()

        if !isInternetAvailable() {
            FTIndicator.showToastMessage(alertInternet)
        }else{
            if(txtNumber.text?.count == 0){
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_TravelSafeNo, viewcontrol: self)
            }else{
                if(dictCityData.value(forKey: "CityData")as! NSArray).count != 0{
                    let strCityId = "\(((dictCityData.value(forKey: "CityData")as! NSArray).object(at: 0)as! NSDictionary).value(forKey: "city_id")!)"
                    callLostArticleSearchAPI(strCityID: strCityId)
                }
            }
        }
    }
    
    @IBAction func action_RegisterComplain(_ sender: Any) {
        self.view.endEditing(true)
        let alert = UIAlertController(title: "Terms of Use", message: "1. This feature of CitizenCOP allows you to report any lost article/document in your selected state/city.\n2. Not all reports are subject to investigate/inquiry.\n3. In case of Theft,you are adviced to contact your nearest police station also.\n4. False reporting using CitizenCOP is a punishable offence.", preferredStyle: .alert)
       
        alert.addAction(UIAlertAction(title: "Disagree", style: .default, handler: { action in
        }))
        alert.addAction(UIAlertAction(title: "Agree", style: .default, handler: { action in
            self.view.endEditing(true)
            let testController = mainStoryboard.instantiateViewController(withIdentifier: "RegisterLostArticleVC")as! RegisterLostArticleVC
            self.navigationController?.pushViewController(testController, animated: true)
        }))
        self.present(alert, animated: true)
    }
    // MARK: - --------------API
    // MARK: -
    func callLostArticleSearchAPI(strCityID : String) {
        self.view.endEditing(true)

        if !isInternetAvailable() {
            FTIndicator.showToastMessage(alertInternet)
        }else{
            let strImeino = "&imeino="
            var strImeinoValue = txtNumber.text!
            let strcomplaintno = "&complaintno="
            var strcomplaintnoValue = txtNumber.text!
            if isImeino {
                strcomplaintnoValue = ""
            }else{
                strImeinoValue = ""
            }

            let URL = API_SearchLostArticle + strImeino + strImeinoValue + strcomplaintno  + strcomplaintnoValue
            print(URL)

            customDotLoaderShowOnButton(btn: btnSearch, view: self.view, controller: self)

            WebServiceClass.callAPIBYPOST(parameter: NSDictionary(), url: URL) { (respone, status) in
                customeDotLoaderRemove()
                if status == "Suceess"{
                    self.viewSearchResult.frame = self.view.frame
                    self.view.addSubview(self.viewSearchResult)
                    self.aryArticle = NSMutableArray()
                    self.aryGeneralDetail = NSMutableArray()
                    let dict = respone.value(forKey: "data")as! NSDictionary
                    self.aryGeneralDetail = (dict.value(forKey: "General")as! NSArray).mutableCopy()as! NSMutableArray
                    self.aryArticle = (dict.value(forKey: "Article")as! NSArray).mutableCopy()as! NSMutableArray
                    self.tvSearchResult.reloadData()
                }
                else{
                     self.viewSearchResult.removeFromSuperview()
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)

                }
            }

        }
    }

}


//MARK:-
//MARK:- ---------UITextFieldDelegate

extension ReportLostArticleVC : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if ( textField == txtNumber)
        {
            return txtFiledValidation(textField: textField, string: string, returnOnly: "ALL", limitValue: 45)
        }
            
        else
        {
            return true
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
}
// MARK: - ----------------UITableViewDelegate
// MARK: -

extension ReportLostArticleVC: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return section == 0 ? aryArticle.count : aryGeneralDetail.count
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 40))
        let lbl = UILabel(frame: CGRect(x: 16, y: 5, width: tableView.frame.width - 32, height: 30))
        if section == 0 {
            lbl.text = "Article/Document Detail"
        } else if section == 1{
            lbl.text = "General Detail"
        }
        lbl.textColor = hexStringToUIColor(hex: primaryTheamColor)
        lbl.font = UIFont.boldSystemFont(ofSize: 18)
        view.addSubview(lbl)
        view.backgroundColor = UIColor.groupTableViewBackground
        return view
    }
 
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tvSearchResult.dequeueReusableCell(withIdentifier: "ReportLostArticalSearchResultCellArticle", for: indexPath as IndexPath) as! ReportLostArticalSearchResultCell
            let dict = aryArticle.object(at: indexPath.row)as! NSDictionary
            cell.lblArticleTitle.text = "\(dict.value(forKey: "ArticleName")!)"
            let ArticleName = "\(dict.value(forKey: "ArticleName")!)"
            if(ArticleName == "Missing Person"){
                cell.lblArticleSubTitle.text = "Person Name: \(dict.value(forKey: "ArticleNo")!)\n Description: \("\(dict.value(forKey: "SimNo_Other")!)")"
            }
            else if(ArticleName == "Mobile Phone / SIM Card"){
                cell.lblArticleSubTitle.text = "IMEI / ESN No.: \(dict.value(forKey: "ArticleNo")!)\nMobile Number: \("\(dict.value(forKey: "SimNo_Other")!)")"

            } else if(ArticleName == "Driving License"){
                cell.lblArticleSubTitle.text = "Driving license No.: \(dict.value(forKey: "ArticleNo")!)\nPlace of issue: \("\(dict.value(forKey: "SimNo_Other")!)")"
                
            }else if(ArticleName == "ATM / Debit / Credit Card No."){
                cell.lblArticleSubTitle.text = "Card No.: \(dict.value(forKey: "ArticleNo")!)\nBank Name: \("\(dict.value(forKey: "SimNo_Other")!)")"
            }else if(ArticleName == "Passport"){
                cell.lblArticleSubTitle.text = "Passport No.: \(dict.value(forKey: "ArticleNo")!)\nPlace of issue: \("\(dict.value(forKey: "SimNo_Other")!)")"
            }else if(ArticleName == "PAN Card"){
                cell.lblArticleSubTitle.text = "PAN No.: \(dict.value(forKey: "ArticleNo")!)\nPlace of issue: \("\(dict.value(forKey: "SimNo_Other")!)")"
            }else if(ArticleName == "Aadhar Card"){
                cell.lblArticleSubTitle.text = "Aadhar No.: \(dict.value(forKey: "ArticleNo")!)\nPlace of issue: \("\(dict.value(forKey: "SimNo_Other")!)")"
            }else if(ArticleName == "Vehicle"){
                cell.lblArticleSubTitle.text = "Vehicle No.: \(dict.value(forKey: "ArticleNo")!)\nPlace of issue: \("\(dict.value(forKey: "SimNo_Other")!)")"
            }else if(ArticleName == "Fixed Deposit Receipt"){
                cell.lblArticleSubTitle.text = "FDR No.: \(dict.value(forKey: "ArticleNo")!)\nBank/Baranch Name: \("\(dict.value(forKey: "SimNo_Other")!)")"
            }else if(ArticleName == "Voter ID card"){
                cell.lblArticleSubTitle.text = "Voter No.: \(dict.value(forKey: "ArticleNo")!)\nPlace of issue: \("\(dict.value(forKey: "SimNo_Other")!)")"
            }else if(ArticleName == "Bank Demand Draft"){
                cell.lblArticleSubTitle.text = "DD No.: \(dict.value(forKey: "ArticleNo")!)\nBank/Baranch Name: \("\(dict.value(forKey: "SimNo_Other")!)")"
            }else if(ArticleName == "Cheques"){
                cell.lblArticleSubTitle.text = "Cheques No.: \(dict.value(forKey: "ArticleNo")!)\nBank/Baranch Name: \("\(dict.value(forKey: "SimNo_Other")!)")"
            }else if(ArticleName == "iPad"){
                cell.lblArticleSubTitle.text = "IMEI No.: \(dict.value(forKey: "ArticleNo")!)\nMAC Address: \("\(dict.value(forKey: "SimNo_Other")!)")"
            }else if(ArticleName == "Laptops"){
                cell.lblArticleSubTitle.text = "MAC Address: \(dict.value(forKey: "ArticleNo")!)\nManufacture: \("\(dict.value(forKey: "SimNo_Other")!)")"
            }else if(ArticleName == "Ration Card"){
                cell.lblArticleSubTitle.text = "Ration Card No.: \(dict.value(forKey: "ArticleNo")!)\nPlace of issue: \("\(dict.value(forKey: "SimNo_Other")!)")"
            }else if(ArticleName == "Tablet"){
                cell.lblArticleSubTitle.text = "IMEI No.: \(dict.value(forKey: "ArticleNo")!)\nManufacture: \("\(dict.value(forKey: "SimNo_Other")!)")"
            }else {
                cell.lblArticleSubTitle.text = "Article No.: \(dict.value(forKey: "ArticleNo")!)\nOther Detail: \("\(dict.value(forKey: "SimNo_Other")!)")"
            }
            
            return cell
        } else {
            let cell = tvSearchResult.dequeueReusableCell(withIdentifier: "ReportLostArticalSearchResultCellGeneral", for: indexPath as IndexPath) as! ReportLostArticalSearchResultCell
            let dict = aryGeneralDetail.object(at: indexPath.row)as! NSDictionary
            cell.lblOwnerName.text = "\(dict.value(forKey: "name")!)"
            cell.lblMobileNumber.text = "\(dict.value(forKey: "phoneno")!)"
            cell.lblArticleType.text = "\(dict.value(forKey: "complainttype")!)"
            cell.lblArticleLocation.text = "\(dict.value(forKey: "losttheftlocation")!)"
            cell.lblComplainOn.text = "\(dict.value(forKey: "complaintfrom")!)"
            cell.lblLostDate.text = "\(dict.value(forKey: "losttheftdatetime")!)"
            cell.btnCancle.layer.cornerRadius = 5.0
            cell.btnCancleReport.layer.cornerRadius = 5.0
            cell.btnCancle.backgroundColor = hexStringToUIColor(hex: primaryTheamColor)
            cell.btnCancleReport.backgroundColor = hexStringToUIColor(hex: primaryTheamColor)
            cell.btnCancleReport.setTitle("Cancle Report", for: .normal)
            cell.btnCancle.setTitle("Ok", for: .normal)
            cell.lblCity.text = "\(dict.value(forKey: "CityName")!)"
            cell.lblStatus.text = "\(dict.value(forKey: "status")!)"
            if "\(dict.value(forKey: "status")!)" == "Cancelled"{
                cell.btnCancleReport.alpha = 0.5
                
            }else{
                cell.btnCancleReport.alpha = 1
            }
            cell.btnCancleReport.addTarget(self, action: #selector(btnCancleStatus), for: .touchUpInside)
            cell.btnCancleReport.tag = indexPath.row
            cell.btnCancle.addTarget(self, action: #selector(btnOkStatus), for: .touchUpInside)
            return cell
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      
    }
    
    @objc func btnOkStatus(sender : UIButton){
        for item in self.view.subviews {
              if(item == viewSearchResult){
                viewSearchResult.tag = 1
            }
        }
        if(viewSearchResult.tag == 1){
            viewSearchResult.removeFromSuperview()
             viewSearchResult.tag = 2
        }else{
           self.navigationController?.popViewController(animated: true)
        }
    }
    
    @objc func btnCancleStatus(sender : UIButton){
        let buttonPosition : CGPoint = sender.convert(CGPoint.zero , to: tvSearchResult)
        let indexPath = self.tvSearchResult.indexPathForRow(at: buttonPosition)
        let dictObj: [String : Any] = self.aryGeneralDetail.object(at: indexPath!.row) as! [String : Any]
        let status = dictObj["status"] as! String
        if status == "Cancelled"{
            showAlertWithoutAnyAction(strtitle: "Alert!", strMessage: "Report Already Cancelled", viewcontrol: self)
        }else{
            self.showTextFeildInsideAlert()
        }
    }
    
    func showTextFeildInsideAlert(){
        rView = ReportView.init(frame: CGRect(x: 0, y: self.view.frame.size.height - (DeviceType.IS_IPAD ? (ScreenSize.SCREEN_MAX_LENGTH >= 1113.0) ? 115 : 95 : (ScreenSize.SCREEN_MAX_LENGTH >= 736.0) ? 90 : 70), width: self.view.frame.size.width, height: (DeviceType.IS_IPAD ? (ScreenSize.SCREEN_MAX_LENGTH >= 1113.0) ? 115 : 95 : (ScreenSize.SCREEN_MAX_LENGTH >= 736.0) ? 90 : 70)))
        rView.btnOk.addTarget(self, action: #selector(clickOnOk), for: .touchUpInside)
        rView.btnCancel.addTarget(self, action: #selector(clickOnCancel), for: .touchUpInside)
        self.view.addSubview(rView)
    }
    
    @objc func clickOnOk(sender : UIButton){
        if rView.txtDescription.text != ""{
            self.apiCallingForCancleStatus(sender: sender)
        }else{
            rView.txtDescription.lineColor = hexStringToUIColor(hex: primaryTheamColor)
            rView.txtDescription.shake()
        }
    }
    
    @objc func clickOnCancel(sender : UIButton){
        rView.removeFromSuperview()
    }
    
    func apiCallingForCancleStatus(sender : UIButton){
        if !isInternetAvailable() {
            FTIndicator.showToastMessage(alertInternet)
            
        }else{
            
            let dict = aryGeneralDetail.object(at: sender.tag)as! NSDictionary
            let LostTheftId = "\(dict.value(forKey: "LostTheftId") ?? "")"
       //     CECEBFD5-2023
            let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><UpdateLostTheftStatus xmlns='\(BaseHostURL)/'><LostTheftId>\(LostTheftId)</LostTheftId><Status>Cancel</Status><StatusDesc>\(rView.txtDescription.text ?? "")</StatusDesc></UpdateLostTheftStatus></soap12:Body></soap12:Envelope>"
            let loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
            self.present(loader, animated: false, completion: nil)
            WebServiceClass.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: BaseURLCitizen, resultype: "UpdateLostTheftStatusResult", responcetype: "UpdateLostTheftStatusResponse") { (responce, status) in
                loader.dismiss(animated: false) {
                    if status == "Suceess"{
                        self.rView.removeFromSuperview()
                        let dictTemp = responce.value(forKey: "data")as! NSDictionary
                        if("\(dictTemp.value(forKey: "Result")!)" == "True"){
                            for item in self.view.subviews {
                                if(item == self.viewSearchResult){
                                    self.viewSearchResult.tag = 1
                                }
                            }
                            if(self.viewSearchResult.tag == 1){
                                self.viewSearchResult.removeFromSuperview()
                                self.viewSearchResult.tag = 2
                            }else{
                               self.navigationController?.popViewController(animated: true)
                            }
                        }else{
                            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                        }
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                    }
                }
            }
        }
    }
    
}

class ReportLostArticalSearchResultCell: UITableViewCell {
    
    @IBOutlet weak var lblOwnerName: UILabel!
    @IBOutlet weak var lblMobileNumber: UILabel!
    @IBOutlet weak var lblArticleType: UILabel!
    
    @IBOutlet weak var lblArticleLocation: UILabel!
    @IBOutlet weak var lblComplainOn: UILabel!
    @IBOutlet weak var lblLostDate: UILabel!
    
    @IBOutlet weak var lblArticleTitle: UILabel!
    @IBOutlet weak var lblArticleSubTitle: UILabel!
    @IBOutlet weak var lblArticleTitle_Static: UILabel!
    @IBOutlet weak var lblArticleSubTitle_Static: UILabel!
    @IBOutlet weak var lblCity: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var btnCancleReport: UIButton!
    @IBOutlet weak var btnCancle: UIButton!
    @IBOutlet weak var lblModelNo: UILabel!
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
    
}
