//
//  ReportView.swift
//  CitizenCOP
//
//  Created by Saavan Patidar on 3/23/23.
//

import UIKit

class ReportView: UIView {

    @IBOutlet var contentView: UIView!
    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var btnOk: UIButton!
    @IBOutlet weak var ViewInner: UIView!
    @IBOutlet weak var lblTop: UILabel!
    @IBOutlet weak var btnCancel: UIButton!
    
    @IBOutlet weak var txtStatus: ACFloatingTextfield!
    @IBOutlet weak var txtDescription: ACFloatingTextfield!
    
    override init(frame: CGRect) {
      super.init(frame: UIScreen.main.bounds)
      commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
      super.init(coder: aDecoder)
      commonInit()
    }
    
    private func commonInit() {
        // load the nib file
        Bundle.main.loadNibNamed("ReportView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        lblHeader.backgroundColor = hexStringToUIColor(hex: primaryDark)
        btnOk.layer.cornerRadius = 5.0
        btnOk.backgroundColor = hexStringToUIColor(hex: primaryTheamColor)
        lblTop.backgroundColor = hexStringToUIColor(hex: primaryTheamColor)
        
        ViewInner.layer.borderWidth = 0.5
        ViewInner.layer.cornerRadius = 5.0
        ViewInner.layer.borderColor = UIColor.lightGray.cgColor
        
        btnCancel.layer.cornerRadius = 5.0
        btnCancel.backgroundColor = hexStringToUIColor(hex: primaryTheamColor)
    }

}
