//
//  RegisterLostArticleVC.swift
//  CitizenCOP
//
//  Created by Navin Patidar on 6/3/19.
//  Copyright © 2019 CitizenCop. All rights reserved.
//

import UIKit

class RegisterLostArticleVC: UIViewController {
    
    // MARK: - --------------IBOutlet
    // MARK: -
    @IBOutlet weak var lbltitleMsg: UILabel!
    @IBOutlet weak var imgGeneralInfo: UIImageView!
    @IBOutlet weak var txtName: ACFloatingTextfield!
    @IBOutlet weak var txtFatherNAme: ACFloatingTextfield!
    @IBOutlet weak var txtAddress: ACFloatingTextfield!
    @IBOutlet weak var txtEmail: ACFloatingTextfield!
    @IBOutlet weak var txtReEnterEmail: ACFloatingTextfield!
    @IBOutlet weak var txtContactNumber: ACFloatingTextfield!
    @IBOutlet weak var txtDate: ACFloatingTextfield!
    @IBOutlet weak var txtTime: ACFloatingTextfield!
    @IBOutlet weak var txtLocation: ACFloatingTextfield!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var categorySagement: UISegmentedControl!
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var scrollContain: UIScrollView!
    @IBOutlet weak var viewContain: UIView!
    @IBOutlet weak var txtState: ACFloatingTextfield!
    @IBOutlet weak var txtCity: ACFloatingTextfield!
    @IBOutlet weak var txtPoliceStation: ACFloatingTextfield!
    @IBOutlet weak var txtDescription: ACFloatingTextfield!
    
    

    var strCategoryType = ""
    var strImageName = ""
    var imagePicker = UIImagePickerController()
    var aryPoliceStation = NSMutableArray()

    // MARK: - --------------Life Cycle
    // MARK: -
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryTheamColor)
        btnSubmit.layer.cornerRadius = 5.0
        btnSubmit.backgroundColor = hexStringToUIColor(hex: primaryDark)
        strCategoryType = categorySagement.titleForSegment(at: 0)!
        
        imgGeneralInfo.layer.borderWidth = 1
        imgGeneralInfo.layer.masksToBounds = false
        imgGeneralInfo.layer.borderColor = UIColor.groupTableViewBackground.cgColor
       
        imgGeneralInfo.clipsToBounds = true
        lbltitleMsg.text = ""
        if(dictCityData.value(forKey: "CityData")as! NSArray).count != 0{
            let strCityname = "\(((dictCityData.value(forKey: "CityData")as! NSArray).object(at: 0)as! NSDictionary).value(forKey: "city_name")!)"
            if(nsud.value(forKey: "CitizenCOP_CityData") != nil){
                let Citizen_StateName = "\((nsud.value(forKey: "CitizenCOP_CityData")as! NSDictionary).value(forKey: "Citizen_StateName")!)"
                lbltitleMsg.text = "Report any lost article / document in \(strCityname) of \(Citizen_StateName)."
            }
            
            if(nsud.value(forKey: "CitizenCOP_CityData") != nil){
                let dictCityData = nsud.value(forKey: "CitizenCOP_CityData")as! NSDictionary
                txtState.text = "\(dictCityData.value(forKey: "Citizen_StateName")!)"
                txtCity.text = "\(dictCityData.value(forKey: "Citizen_CityName")!)"
            }
        }
        
        let strCityId = "\(((dictCityData.value(forKey: "CityData")as! NSArray).object(at: 0)as! NSDictionary).value(forKey: "city_id")!)"

        callPoliceStationAPI(strCityID: strCityId)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    override func viewWillLayoutSubviews() {
        scrollContain.contentSize = CGSize(width: ScreenSize.SCREEN_WIDTH, height:DeviceType.IS_IPAD ? 1500 : 1134)
    }
    
    // MARK: - ---------------API CAlling
    // MARK: -
    func callPoliceStationAPI(strCityID : String ) {
        if !isInternetAvailable() {
            FTIndicator.showToastMessage(alertInternet)
            
        }else{
            let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><GetRAIPoliceStation xmlns='\(BaseHostURL)/'><city_id>\(strCityID)</city_id></GetRAIPoliceStation></soap12:Body></soap12:Envelope>"
            let loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
            self.present(loader, animated: false, completion: nil)
            
            WebServiceClass.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: BaseURLCitizenEyestation, resultype: "GetRAIPoliceStationResult", responcetype: "GetRAIPoliceStationResponse") { (responce, status) in
                loader.dismiss(animated: false) {
                    if status == "Suceess"{
                        let dictTemp = responce.value(forKey: "data")as! NSDictionary
                        if("\(dictTemp.value(forKey: "Result")!)" == "True"){
                            
                            let ary = dictTemp.value(forKey: "PoliceStation")as! NSArray
                            if(ary.count != 0){
                                self.aryPoliceStation = NSMutableArray()
                                self.aryPoliceStation = ary.mutableCopy() as! NSMutableArray
                                
                             
                            }else{
                                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                                
                            }
                            
                        }else{
                            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Police Station not found", viewcontrol: self)
                        }
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                    }
                }
                
                
               
            }
        }
    }
    // MARK: - --------------Actions
    // MARK: -
    
    @IBAction func action_Back(_ sender: Any) {
            self.navigationController?.popViewController(animated: true)
        }

    @IBAction func actionOnBrowseImage(_ sender: UIButton) {
        let alert = UIAlertController(title: "", message: "Please Select an Option", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: Alert_Gallery, style: .default , handler:{ (UIAlertAction)in
            self.imagePicker = UIImagePickerController()
            if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
                print("Button capture")
                self.imagePicker.delegate = self
                self.imagePicker.sourceType = .savedPhotosAlbum
                self.imagePicker.allowsEditing = false
                self.present(self.imagePicker, animated: true, completion: nil)
            }
        }))
        
        
        alert.addAction(UIAlertAction(title: Alert_Camera, style: .default , handler:{ (UIAlertAction)in
            self.imagePicker = UIImagePickerController()
            
            if UIImagePickerController.isSourceTypeAvailable(.camera){
                print("Button capture")
                self.imagePicker.delegate = self
                self.imagePicker.sourceType = .camera
                self.imagePicker.allowsEditing = false
                self.present(self.imagePicker, animated: true, completion: nil)
            }
        }))
        
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
        }))
        //alert.popoverPresentationController?.sourceRect = sender.frame
        alert.popoverPresentationController?.sourceView = self.view
        alert.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        alert.popoverPresentationController?.permittedArrowDirections = []
        self.present(alert, animated: true, completion: {
        })
    }
    @IBAction func actionSubmit(_ sender: UIButton) {
        self.view.endEditing(true)
     
        
        if(txtName.text?.count == 0){
         showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_ReportComplain_Name, viewcontrol: self)
        }else if(txtFatherNAme.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_ReportComplain_FName, viewcontrol: self)

        }else if(txtAddress.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_ReportComplain_Address, viewcontrol: self)

        }else if(txtEmail.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_Email, viewcontrol: self)

        } else if !(validateEmail(email: txtEmail.text!)) {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_InvalidEmail, viewcontrol: self)

        }else if(txtEmail.text! != txtReEnterEmail.text!){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_ReportComplain_EmailReEnter, viewcontrol: self)

        }else if(txtContactNumber.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_ReportComplain_ContactNumber, viewcontrol: self)

        }else if(txtContactNumber.text!.count <= 9){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_ReportComplain_ContactNumberinvalid, viewcontrol: self)

        }else if(txtDate.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_ReportComplain_Date, viewcontrol: self)

        }else if(txtTime.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_ReportComplain_Time, viewcontrol: self)

        }else if(txtLocation.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_ReportComplain_Location, viewcontrol: self)

        }else if(txtPoliceStation.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_ReportComplain_policestationID, viewcontrol: self)

        }else if(txtDescription.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_ReportComplain_description, viewcontrol: self)

        }else{
            
            if strImageName.count > 0 {
                let imageData = self.imgGeneralInfo.image?.jpegData(compressionQuality:0.5)
                customDotLoaderShowOnButton(btn: sender, view: self.view, controller: self)

                WebServiceClass.callAPIWithImage(parameter: NSDictionary(), url: API_UploadReportImage, data: imageData!, fileName: "\(self.strImageName)", withName: "userfile") { (responce, status) in
                    print(responce)
                    customeDotLoaderRemove()
                    if(status == "Suceess"){
                        self.goToSubmit()
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                    }
                }
                
            }else{
                self.goToSubmit()
            }
        }
    }
    
    func goToSubmit(){
        let dict = NSMutableDictionary()
        
        let strCityId = "\(((dictCityData.value(forKey: "CityData")as! NSArray).object(at: 0)as! NSDictionary).value(forKey: "city_id")!)"

        dict.setValue(txtName.text!, forKey: "Name")
        dict.setValue(txtFatherNAme.text!, forKey: "FName")
        dict.setValue(txtAddress.text!, forKey: "Address")
        dict.setValue(txtEmail.text!, forKey: "Email")
        dict.setValue(txtContactNumber.text!, forKey: "ContactNumber")
        dict.setValue(txtDate.text!, forKey: "Date")
        dict.setValue(txtTime.text!, forKey: "Time")
        dict.setValue(txtLocation.text!, forKey: "Location")
        dict.setValue(strCategoryType, forKey: "CategoryType")
        dict.setValue(strImageName, forKey: "ImageName")
        dict.setValue(imgGeneralInfo.image, forKey: "ImageData")
        dict.setValue(txtPoliceStation.tag, forKey: "PoliceStationId")
        dict.setValue(strCityId, forKey: "cityid")
        dict.setValue(txtDescription.text, forKey: "Description")
        print(dict)
        let testController = mainStoryboard.instantiateViewController(withIdentifier: "RegisterLostArticleSubmitVC")as! RegisterLostArticleSubmitVC
        testController.dictGeneralData = dict
        testController.strCommonImage = strImageName
        self.navigationController?.pushViewController(testController, animated: true)
    }
    
    @IBAction func actionCategorySelect(_ sender: UISegmentedControl) {
        strCategoryType = sender.titleForSegment(at: sender.selectedSegmentIndex)!
    }
    
    @IBAction func actionONDate(_ sender: Any) {
        self.view.endEditing(true)
        let vc: DateTimeSelectionClass = mainStoryboard.instantiateViewController(withIdentifier: "DateTimeSelectionClass") as! DateTimeSelectionClass
        vc.strMode = "Date"
        vc.strTag = 1
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .coverVertical
        vc.delegateDateTime = self
        self.present(vc, animated: true, completion: {})
    }
    
    @IBAction func actionOnTIme(_ sender: Any) {
        self.view.endEditing(true)
        let vc: DateTimeSelectionClass = mainStoryboard.instantiateViewController(withIdentifier: "DateTimeSelectionClass") as! DateTimeSelectionClass
        vc.strMode = "Time"
        vc.strTag = 2
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .coverVertical
        vc.delegateDateTime = self
        self.present(vc, animated: true, completion: {})
    }
    
    
    @IBAction func actionOnPoliceStation(_ sender: UIButton) {
        print("sdafsdf")
        if self.aryPoliceStation.count != 0 {
            let vc: SelectionClass = mainStoryboard.instantiateViewController(withIdentifier: "SelectionClass") as! SelectionClass
            vc.strTitle = "Select"
            vc.strTag = 18
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .coverVertical
            vc.delegate = self
            vc.aryList = aryPoliceStation
            self.present(vc, animated: true, completion: {})
        }
    }
}

//MARK:-
//MARK:- ---------Date Time Selection Delegate
extension RegisterLostArticleVC : DateTimeDelegate{
    func getDataFromDelegate(time: Date, tag: Int) {
        let dateFormatter = DateFormatter()
        if(tag == 1){ // For Date
            dateFormatter.dateFormat = "MM/dd/yyyy"
            self.txtDate.text = "\(dateFormatter.string(from: time))"
        }else if (tag == 2){ // for time
            dateFormatter.dateFormat = "hh:mm a"
            self.txtTime.text = "\(dateFormatter.string(from: time))"
        }
    }
}
//MARK:-
//MARK:- ---------UITextFieldDelegate

extension RegisterLostArticleVC : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if ( textField == txtContactNumber)
        {
            return txtFiledValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 9)
        }
        if ( textField == txtName || textField == txtFatherNAme || textField == txtDate || textField == txtTime || textField == txtAddress || textField == txtEmail || textField == txtReEnterEmail || textField == txtLocation)
        {
            return txtFiledValidation(textField: textField, string: string, returnOnly: "All", limitValue: 50)
        }
        else
        {
            return true
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
}
// MARK: - ----------------UIImagePickerControllerDelegate
// MARK: -
extension RegisterLostArticleVC : UINavigationControllerDelegate, UIImagePickerControllerDelegate{
    
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        self.dismiss(animated: true, completion: { () -> Void in
            
        })
        self.strImageName = "\(uniqueString()).jpg"
        self.imgGeneralInfo.image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
    }
}



extension RegisterLostArticleVC : PopUpDelegate
{
    func getDataFromPopupDelegate(dictData: NSDictionary, tag: Int) {
        if(tag == 18){
            if dictData.count != 0 {
                txtPoliceStation.text = "\(dictData.value(forKey: "PoliceStationName") ?? "")"
                txtPoliceStation.tag = Int("\(dictData.value(forKey: "PoliceStationId") ?? "0")")!

            }else{
                txtPoliceStation.text = ""
                txtPoliceStation.tag = 0

            }
        }
    }
}
