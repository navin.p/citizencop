//
//  RegisterLostArticleSubmitVC.swift
//  CitizenCOP
//
//  Created by Navin Patidar on 6/4/19.
//  Copyright © 2019 CitizenCop. All rights reserved.
//

import UIKit

class RegisterLostArticleSubmitVC: UIViewController {
    // MARK: - --------------IBOutlet
    // MARK: -
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var btnAdd: UIButton!
    @IBOutlet weak var txt1: ACFloatingTextfield!
    @IBOutlet weak var txt2: ACFloatingTextfield!
    @IBOutlet weak var btnSelectArticle: UIButton!
    @IBOutlet weak var tvList: UITableView!
    @IBOutlet weak var txtModelName: ACFloatingTextfield!
    @IBOutlet weak var imgDoucment: UIImageView!
    
    
    var aryArticleCatrgory = NSMutableArray()
    var aryAlerticleList = NSMutableArray()
    var dictGeneralData = NSMutableDictionary()
    var aryAlerticleImageList = NSMutableArray()
   
    var strTitle1 = ""
    var strTitle2 = ""
    var strCommonImage = ""
    var strCategoryType = ""
    var strImageName = ""
    var imagePicker = UIImagePickerController()

    // MARK: - --------------Life Cycle
    // MARK: -
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tvList.tableFooterView = UIView()
        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryTheamColor)
        btnSubmit.backgroundColor = hexStringToUIColor(hex: primaryDark)
        
        btnAdd.layer.cornerRadius = 5.0
        btnAdd.backgroundColor = hexStringToUIColor(hex: primaryDark)
      
        btnSelectArticle.layer.cornerRadius = 5.0
        txt1.text = ""
        txt2.text = ""
        txt1.placeholder = "Article Name"
        txt2.placeholder = "Article Detail"
        self.strTitle1 = "Article Name"
        self.strTitle2 = "Article Detail"
        btnSelectArticle.setTitle("Select Article/Document", for: .normal)
        btnSelectArticle.tag = 0
        callGetArticleListAPI()
        
        if "\(dictGeneralData.value(forKey: "ImageName") ?? "")" != ""{
            let dict = NSMutableDictionary()
            dict.setValue("\(dictGeneralData.value(forKey: "ImageName") ?? "")", forKey: "ImageName")
            dict.setValue(dictGeneralData.value(forKey:"ImageData") as? UIImage, forKey: "ImageData")
            aryAlerticleImageList.add(dict)
        }

    } // 0507144856.jpg
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
      reloadTblData()
    }
   
    // MARK: - --------------Actions
    // MARK: -
    
    @IBAction func action_Back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func action_DropDown(_ sender: Any) {
        if self.aryArticleCatrgory.count != 0 {
            let vc: SelectionClass = mainStoryboard.instantiateViewController(withIdentifier: "SelectionClass") as! SelectionClass
            vc.strTitle = "Select Article"
            vc.strTag = 8
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .coverVertical
            vc.delegate = self
            vc.aryList = aryArticleCatrgory
            self.present(vc, animated: true, completion: {})
        }
    }
    @IBAction func action_Submit(_ sender: UIButton) {
        if(aryAlerticleList.count != 0){
            let imgName = dictGeneralData.value(forKey: "ImageName")as! String
            if(imgName.count != 0){
                uploadImage(sender: sender)
            }else{
                callSubmitArticleData(sender: sender)
            }
        
        }else{
            
        }
    }
    @IBAction func action_Add(_ sender: Any) {
        self.view.endEditing(true)
        if(btnSelectArticle.tag == 0){
            btnSelectArticle.shake()
        }
       else if(txt1.text?.count == 0){
            txt1.lineColor = hexStringToUIColor(hex: primaryTheamColor)
            txt1.shake()

        }else if(txt2.text?.count == 0){
            txt2.lineColor = hexStringToUIColor(hex: primaryTheamColor)
            txt2.shake()
        }else{
            let dict  = NSMutableDictionary()
          
            dict.setValue(self.strTitle1, forKey: "TitleI")
            dict.setValue(self.strTitle2, forKey: "TitleII")
            
            dict.setValue(txt1.text!, forKey: "ArticleNo")
            dict.setValue(txt2.text!, forKey: "SimNo_Other")
            dict.setValue("\(btnSelectArticle.titleLabel?.text ?? "")", forKey: "articleTypeName")
            dict.setValue(strImageName, forKey: "ImageName")
            dict.setValue("\(btnSelectArticle.tag)", forKey: "ArticleTypeId")
            dict.setValue("\(txtModelName.text ?? "")", forKey: "DeviceModel")
            
            dict.setValue("\(dictGeneralData.value(forKey: "PoliceStationId") ?? "")", forKey: "PoliceStationId")
            dict.setValue("\(dictGeneralData.value(forKey: "cityid") ?? "")", forKey: "cityid")
            dict.setValue("\(dictGeneralData.value(forKey: "Description") ?? "")", forKey: "Description")
            dict.setValue("", forKey: "VerificationImage")
            
            let dictImage = NSMutableDictionary()
            dictImage.setValue(strImageName, forKey: "ImageName")
            dictImage.setValue(imgDoucment.image ?? UIImage(), forKey: "ImageData")
            
            aryAlerticleList.add(dict)
            aryAlerticleImageList.add(dictImage)
            
            txt1.text = ""
            txt2.text = ""
            txtModelName.text = ""
            txtModelName.isHidden = true
            imgDoucment.image = UIImage(named: "NoImage")
            strImageName = ""
            txt1.placeholder = "Article Name"
            txt2.placeholder = "Article Detail"
            self.strTitle1 = "Article Name"
            self.strTitle2 = "Article Detail"
            txt1.lineColor = UIColor.lightGray
            txt2.lineColor = UIColor.lightGray

            btnSelectArticle.setTitle("Select Article/Document", for: .normal)
            btnSelectArticle.tag = 0
            self.view.endEditing(true)
            reloadTblData()
        }
    }
    func reloadTblData() {
        if(aryAlerticleList.count != 0){
            btnSubmit.isHidden = false
        }else{
            btnSubmit.isHidden = true
        }
        self.tvList.reloadData()

    }
    
    @IBAction func actionOnUpload(_ sender: UIButton) {
        let alert = UIAlertController(title: "", message: "Please Select an Option", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: Alert_Gallery, style: .default , handler:{ (UIAlertAction)in
            self.imagePicker = UIImagePickerController()
            if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
                print("Button capture")
                self.imagePicker.delegate = self
                self.imagePicker.sourceType = .savedPhotosAlbum
                self.imagePicker.allowsEditing = false
                self.present(self.imagePicker, animated: true, completion: nil)
            }
        }))
        
        
        alert.addAction(UIAlertAction(title: Alert_Camera, style: .default , handler:{ (UIAlertAction)in
            self.imagePicker = UIImagePickerController()
            
            if UIImagePickerController.isSourceTypeAvailable(.camera){
                print("Button capture")
                self.imagePicker.delegate = self
                self.imagePicker.sourceType = .camera
                self.imagePicker.allowsEditing = false
                self.present(self.imagePicker, animated: true, completion: nil)
            }
        }))
        
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
        }))
        //alert.popoverPresentationController?.sourceRect = sender.frame
        alert.popoverPresentationController?.sourceView = self.view
        alert.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        alert.popoverPresentationController?.permittedArrowDirections = []
        self.present(alert, animated: true, completion: {
        })
    }
    
    
    // MARK: - ---------------API CAlling
    // MARK: -
    func uploadImage(sender : UIButton) {
        
        if aryAlerticleImageList.count > 0{
            
            var tempDict = NSDictionary()
            tempDict = aryAlerticleImageList.lastObject as? NSDictionary ?? [:]
            
            let img = tempDict.value(forKey: "ImageData")as! UIImage
            let imageData = img.jpegData(compressionQuality:0.5)
            let imgName = tempDict.value(forKey: "ImageName")as! String
            customDotLoaderShowOnButton(btn: sender, view: self.view, controller: self)
            
            WebServiceClass.callAPIWithImage(parameter: NSDictionary(), url: API_UploadReportImage, data: imageData!, fileName: "\(imgName)", withName: "userfile") { (responce, status) in
                print(responce)
                customeDotLoaderRemove()

                if(status == "Suceess"){
               
                    self.aryAlerticleImageList.removeLastObject()
                    
                    if self.aryAlerticleImageList.count > 0{
                        self.uploadImage(sender: sender)
                    }else{
                        self.callSubmitArticleData(sender: sender)
                    }
                    
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            }
            
        }
        
    }
    func callSubmitArticleData(sender : UIButton) {
        var json = Data()
        var jsonString = NSString()
        if JSONSerialization.isValidJSONObject(aryAlerticleList) {
            // Serialize the dictionary
            json = try! JSONSerialization.data(withJSONObject: aryAlerticleList, options: .prettyPrinted)
            jsonString = String(data: json, encoding: .utf8)! as NSString
            print("UpdateLeadinfo JSON: \(jsonString)")
        }
        if(aryAlerticleList.count == 0){
            jsonString = ""
        }
        var cityID = NSString()
        var dateTime = NSString()
        if(dictCityData.value(forKey: "CityData")as! NSArray).count != 0{
            cityID = "\(((dictCityData.value(forKey: "CityData")as! NSArray).object(at: 0)as! NSDictionary).value(forKey: "city_id")!)" as NSString
        }
        dateTime = "\(dictGeneralData.value(forKey: "Date")!) \(dictGeneralData.value(forKey: "Time")!)" as NSString
        
        let strURl = API_Insertlosttheftmobile + "&losttheftid=0&name=\(dictGeneralData.value(forKey: "Name")!)&fathername=\(dictGeneralData.value(forKey: "FName")!)&address=\(dictGeneralData.value(forKey: "Address")!)&phoneno=\(dictGeneralData.value(forKey: "ContactNumber")!)&complainttype=\(dictGeneralData.value(forKey: "CategoryType")!)&complaintfrom=CitizenCOP&losttheftlocation=\(dictGeneralData.value(forKey: "Location")!)&emailId=\(dictGeneralData.value(forKey: "Email")!)&losttheftdatetime=\(dateTime)&cityid=\(cityID)&senderimeino=\(DeviceID)&CommonImage=\(strCommonImage)&articles=\(jsonString)&ipaddress=\(getIPAddress())"
        customDotLoaderShowOnButton(btn: sender, view: self.view, controller: self)
        
        WebServiceClass.callAPIBYPOST(parameter: NSDictionary(), url: strURl) { (responce, status) in
            customeDotLoaderRemove()
            if status == "Suceess"{
                let dict = responce.value(forKey: "data")as! NSDictionary
                
                let alert = UIAlertController(title: "Alert", message: "Your complaint has been registered with number as - \(dict.value(forKey: "Result")!)", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                    for controller in self.navigationController!.viewControllers as Array {
                        if controller.isKind(of: DashBoardVC.self) {
                            self.navigationController!.popToViewController(controller, animated: true)
                            break
                        }
                    }
                }))
                alert.addAction(UIAlertAction(title: "Copy", style: .default, handler: { action in
                    FTIndicator.showToastMessage("Your complaint number has been copied.")
                    let pasteboard = UIPasteboard.general
                    pasteboard.string = "\(dict.value(forKey: "Result")!)"
                    for controller in self.navigationController!.viewControllers as Array {
                        if controller.isKind(of: DashBoardVC.self) {
                            self.navigationController!.popToViewController(controller, animated: true)
                            break
                        }
                    }
                }))
                self.present(alert, animated: true, completion: nil)
          
            }
            else{
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
            }
        }
        
    }
    func callGetArticleListAPI() {
        if !isInternetAvailable() {
            self.aryArticleCatrgory = getDataFromCoreDataBase(strEntity: "LostArticleList", strkey: "lostArticleList")
            if(self.aryArticleCatrgory.count != 0){
                let dictData = self.aryArticleCatrgory.object(at: 0)as! NSDictionary
                self.txt1.placeholder = "\(dictData.value(forKey: "Label1")!)"
                self.txt2.placeholder = "\(dictData.value(forKey: "Label2")!)"
                self.btnSelectArticle.setTitle("\(dictData.value(forKey: "ArticleName")!)", for: .normal)
                self.btnSelectArticle.tag = Int("\(dictData.value(forKey: "LostTheftArticleTypeId")!)")!
            }
            FTIndicator.showToastMessage(alertInternet)
        }else{
            
            
            customDotLoaderShowOnFull(message: "", controller: self)
            
            WebServiceClass.callAPIBYPOST(parameter: NSDictionary(), url: API_GetLostTheftArticle) { (responce, status) in
                customeDotLoaderRemove()
                if status == "Suceess"{
                    self.aryArticleCatrgory = NSMutableArray()
                    self.aryArticleCatrgory = (responce.value(forKey: "data")as! NSArray).mutableCopy()as! NSMutableArray
                    deleteAllRecords(strEntity:"LostArticleList")
                    saveDataInLocalArray(strEntity: "LostArticleList", strKey: "lostArticleList", data: self.aryArticleCatrgory)
                    if(self.aryArticleCatrgory.count != 0){
                        let dictData = self.aryArticleCatrgory.object(at: 0)as! NSDictionary
                        self.txt1.placeholder = "\(dictData.value(forKey: "Label1")!)"
                        self.txt2.placeholder = "\(dictData.value(forKey: "Label2")!)"
                        self.btnSelectArticle.setTitle("\(dictData.value(forKey: "ArticleName")!)", for: .normal)
                        
                        if "\(dictData.value(forKey: "ArticleName")!)" == "Mobile Phone / SIM Card"{
                            self.txtModelName.isHidden = false
                        }else{
                            self.txtModelName.isHidden = true
                            self.txtModelName.text = ""
                        }
                        
                        self.btnSelectArticle.tag = Int("\(dictData.value(forKey: "LostTheftArticleTypeId")!)")!
                    }
//                http://mp.citizencop.org/CitizenCopAllHandler.ashx?key=insertlosttheftmobile_multiplearticle&losttheftid=0&name=test&fathername=test&address=indr&phoneno=+91656868686868&complainttype=Lost&complaintfrom=CitizenCOP&losttheftlocation=indorehchf&emailId=priyansi@infocratsweb.com&losttheftdatetime=05/07/2024 11:29 am&cityid=131&senderimeino=51a76d28cc4316d1&DeviceModel=&PoliceStationId=63&Description=gjjg&CommonImage=1715061740147.jpg&ipaddress=192.168.1.5&articles=[{"ArticleTypeId":1,"ArticleNo":"123","SimNo_Other":"6853353535","ImageName":"1715061736808.jpg","VerificationImage":"","articleTypeName":"Mobile Phone \/ SIM Card","DeviceModel":"gxhf"}]
                    
                }
                else{
                    
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                    
                }
            }
            
            
        }
    }
}
// MARK: - ----------------UITableViewDelegate
// MARK: -

extension RegisterLostArticleSubmitVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return aryAlerticleList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tvList.dequeueReusableCell(withIdentifier: "RegisterLostArticleCell", for: indexPath as IndexPath) as! RegisterLostArticleCell
        let dict = aryAlerticleList.object(at: indexPath.row)as! NSDictionary
        cell.lblTitle.text = "\(indexPath.row + 1). \(dict.value(forKey: "TitleI")!): \(dict.value(forKey: "ArticleNo")!)"
        cell.lblDetail.text = "\(dict.value(forKey: "TitleII")!): \(dict.value(forKey: "SimNo_Other")!)"
        if "\(dict.value(forKey: "DeviceModel") ?? "")" == ""{
            cell.lblMobile.text = ""
        }else{
            cell.lblMobile.text = "Model No : \(dict.value(forKey: "DeviceModel") ?? "")"
        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
           self.aryAlerticleList.removeObject(at: indexPath.row)
            let dict = self.aryAlerticleImageList.object(at: indexPath.row)as! NSDictionary
            let imageName = "\(dict.value(forKey: "ImageName") ?? "")"

            for i in self.aryAlerticleImageList {
                let dict = i as? NSDictionary ?? [:]
                let name = "\(dict.value(forKey: "ImageName") ?? "")"
                if imageName == name {
                    self.aryAlerticleImageList.remove(i)
                }
            }
             reloadTblData()
        }
    }
    
}


// MARK: - ----------------RegisterLostArticleCell
// MARK: -
class RegisterLostArticleCell: UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDetail: UILabel!
    @IBOutlet weak var lblMobile: UILabel!
    
   
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}
//MARK:-
//MARK:- ---------PopUpDelegate

extension RegisterLostArticleSubmitVC : PopUpDelegate
{
    func getDataFromPopupDelegate(dictData: NSDictionary, tag: Int) {
        if(tag == 8){
            self.strTitle1 = "\(dictData.value(forKey: "Label1")!)"
            self.strTitle2 = "\(dictData.value(forKey: "Label2")!)"

            self.txt1.placeholder = "\(dictData.value(forKey: "Label1")!)"
            self.txt2.placeholder = "\(dictData.value(forKey: "Label2")!)"
            self.btnSelectArticle.setTitle("\(dictData.value(forKey: "ArticleName")!)", for: .normal)
            if "\(dictData.value(forKey: "ArticleName")!)" == "Mobile Phone / SIM Card"{
                self.txtModelName.isHidden = false
            }else{
                self.txtModelName.isHidden = true
                self.txtModelName.text = ""
            }
            
            self.btnSelectArticle.tag = Int("\(dictData.value(forKey: "LostTheftArticleTypeId")!)")!
        }
    }
}


// MARK: - ----------------UIImagePickerControllerDelegate
// MARK: -
extension RegisterLostArticleSubmitVC : UINavigationControllerDelegate, UIImagePickerControllerDelegate{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        self.dismiss(animated: true, completion: { () -> Void in
            
        })
        self.strImageName = "\(uniqueString()).jpg"
        self.imgDoucment.image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
    }
}
