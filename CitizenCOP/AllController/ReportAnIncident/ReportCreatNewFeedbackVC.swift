//
//  ReportCreatNewFeedbackVC.swift
//  CitizenCOP
//
//  Created by Navin Patidar on 5/30/19.
//  Copyright © 2019 CitizenCop. All rights reserved.
//

import UIKit

class ReportCreatNewFeedbackVC: UIViewController {

   
    // MARK: - ----IBOutlet
    @IBOutlet weak var view_ForHeader: UIView!
    @IBOutlet weak var txtDescription: KMPlaceholderTextView!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var lblCount: UILabel!
   var strComplainNumber = ""
    
    // MARK: - ----Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        view_ForHeader.backgroundColor = hexStringToUIColor(hex: primaryTheamColor)
        txtDescription.layer.borderColor = UIColor.darkGray.cgColor
        txtDescription.delegate = self
        lblCount.text =  "0/250"
        btnSubmit.layer.cornerRadius = 5.0
        btnSubmit.backgroundColor = hexStringToUIColor(hex: primaryDark)
    }
    

    // MARK: - --------------IBAction
    // MARK: -
    @IBAction func actionOnBack(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func actiononHelp(_ sender: UIButton) {
        let testController = mainStoryboard.instantiateViewController(withIdentifier: "HelpVC")as! HelpVC
        testController.strMessage = alertYourReportFeedbackHelp
        self.navigationController?.pushViewController(testController, animated: true)
        
    }
    @IBAction func actiononSubmit(_ sender: UIButton) {
        if(txtDescription.text.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_Feedback, viewcontrol: self)
        }else{
            callAddFeedbackSuggesionAPI(sender: sender)
        }
        
    }
    //MARK:- --------------- API CAlling
    //MARK:
    
    func callAddFeedbackSuggesionAPI(sender : UIButton) {
        if !isInternetAvailable() {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
        }else{
            let dateFormatter : DateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MMM-dd hh:mm:ss a"
            let date = Date()
            let dateString = dateFormatter.string(from: date)
            let strURL = API_InsertComplaintFeedback + "\(txtDescription.text!)&FBDate=\(dateString)&CompId=\(strComplainNumber)"
            customDotLoaderShowOnButton(btn: btnSubmit, view: self.view, controller: self)
            
            WebServiceClass.callAPIBYPOST(parameter: NSDictionary(), url: strURL) { (responce, status) in
                customeDotLoaderRemove()
                if status == "Suceess"{
                    FTIndicator.showNotification(withTitle: alert_FeedbackSubmit, message: "")
                    self.navigationController?.popViewController(animated: true)
                    print(responce)
                }
                else{
                    
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                    
                }
            }
            
        }
    }
}
// MARK: - --------------UITextViewDelegate
// MARK: -
extension ReportCreatNewFeedbackVC : UITextViewDelegate {
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        lblCount.text =  "\(newText.count)/250"
        return newText.count < 250
    }
    
}
