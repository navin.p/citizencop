//
//  ReportListVC.swift
//  CitizenCOP
//
//  Created by Navin Patidar on 5/29/19.
//  Copyright © 2019 CitizenCop. All rights reserved.
//

import UIKit

class ReportListVC: UIViewController {
    // MARK: - ----IBOutlet
    
    @IBOutlet weak var view_ForHeader: UIView!
    @IBOutlet weak var tvlist: UITableView!

    // MARK: - ----Variable
    var ary_ReportData = NSMutableArray()
    var refresher = UIRefreshControl()
    var ErrorMsg = ""
    // MARK: - ----Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()

         view_ForHeader.backgroundColor = hexStringToUIColor(hex: primaryTheamColor)
        tvlist.tableFooterView = UIView()
        tvlist.estimatedRowHeight = 118.0
        
        self.refresher.addTarget(self, action: #selector(RefreshloadData), for: .valueChanged)
        self.tvlist!.addSubview(refresher)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if(ary_ReportData.count == 0){
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                self.callReportDataAPI(strLoaderTag: 1)
            }
        }
    }
    
    // MARK: - --------------Pull Refresh
    // MARK: -
    @objc func RefreshloadData() {
      callReportDataAPI(strLoaderTag: 0)
        
    }
    // MARK: - --------------IBAction
    // MARK: -
    @IBAction func actionOnBack(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func actiononHelp(_ sender: UIButton) {
        let testController = mainStoryboard.instantiateViewController(withIdentifier: "HelpVC")as! HelpVC
        testController.strMessage = alertYourReportHelp
        self.navigationController?.pushViewController(testController, animated: true)
        
    }
    // MARK: - ---------------API CAlling
    // MARK: -
    func callReportDataAPI(strLoaderTag : Int) {
        if !isInternetAvailable() {
            FTIndicator.showNotification(withTitle: alertInternet, message: "")
             ErrorMsg = ""
            self.ary_ReportData = NSMutableArray()
            self.ary_ReportData = getDataFromCoreDataBase(strEntity: "ReportList", strkey: "reportList")
            if(self.ary_ReportData.count == 0){
                ErrorMsg = alertInternet
               
            }
            self.tvlist.reloadData()
            
        }else{
            
            let loading = DPBasicLoading(table: self.tvlist, fontName: "HelveticaNeue")
             strLoaderTag == 1 ? loading.startLoading(text: "Loading...") : nil
           
            let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><GetRAIReportBYImeiNo xmlns='\(BaseHostURL)/'><ImeiNo>\(DeviceID)</ImeiNo></GetRAIReportBYImeiNo></soap12:Body></soap12:Envelope>"
            
            WebServiceClass.callSOAP_APIBY_POST(soapMessage: soapMessage, url: BaseURLCitizen, resultype: "GetRAIReportBYImeiNoResult", responcetype: "GetRAIReportBYImeiNoResponse") { (responce, status) in
                loading.endLoading()
                self.refresher.endRefreshing()
                if status == "Suceess"{
                    self.ary_ReportData = NSMutableArray()
                    self.ary_ReportData = (responce.value(forKey: "data")as! NSArray).mutableCopy()as! NSMutableArray
                    deleteAllRecords(strEntity:"ReportList")
                    saveDataInLocalArray(strEntity: "ReportList", strKey: "reportList", data: self.ary_ReportData)
                    if(self.ary_ReportData.count == 0){
                        self.ErrorMsg = alertDataNotFound
                      
                    }
                    self.tvlist.reloadData()
                }else{
                    self.ErrorMsg = alertSomeError
                   
                    self.tvlist.reloadData()
                }
            }
            
        }
    }
}
// MARK: - ----------------UITableViewDelegate
// MARK: -

extension ReportListVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ary_ReportData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tvlist.dequeueReusableCell(withIdentifier: "ReportCell", for: indexPath as IndexPath) as! ReportCell
            let dict = ary_ReportData.object(at: indexPath.row)as! NSDictionary
            cell.lblTitle.text = "ComplaintNo: " +  "\(dict["ComplaintNo"]!)"
            cell.lblSubTitle.text = "Description: " +  "\(dict["Description"]!)\nDate: \(dict["SendDate"]!)\nTime: \(dict["SendTime"]!) "
            cell.img.layer.borderWidth = 2
            cell.img.layer.masksToBounds = false
            cell.img.layer.borderColor = UIColor.groupTableViewBackground.cgColor
            cell.img.layer.cornerRadius = cell.img.frame.height/2
            cell.img.clipsToBounds = true
            cell.img.contentMode = .scaleAspectFit
            cell.img.image = UIImage(named: "your_reportN")
            let imges = "\(dict.value(forKey: "CitizenCopImage")!)"
            if(imges.count > 0){
                var aryImg = imges.split(separator: ",")
                if(aryImg.count != 0){
                    let urlImage = BaseHostURL + "/\(aryImg[0])"
                    cell.img.setImageWith(URL(string: urlImage), placeholderImage: UIImage(named: "your_reportN"), options: SDWebImageOptions(rawValue: 1), completed: { (image, error, type, url) in
                        print(url ?? 0)
                    }, usingActivityIndicatorStyle: .gray)
                }
            }
            
            
            return cell
       
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(ary_ReportData.count != 0){
            let testController = mainStoryboard.instantiateViewController(withIdentifier: "ReportListDetailVC")as! ReportListDetailVC
            let dict = ary_ReportData.object(at: indexPath.row)as! NSDictionary
             testController.dictReportData = dict.mutableCopy()as! NSMutableDictionary
            self.navigationController?.pushViewController(testController, animated: true)
            
        }
        
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 50.0
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let customView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 50))
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 50))
        if(ary_ReportData.count != 0){
            ErrorMsg = ""
        }else{
        }
        button.setTitle(ErrorMsg, for: .normal)
        customView.addSubview(button)
        customView.backgroundColor = UIColor.clear
        button.setTitleColor(UIColor.darkGray, for: .normal)
        return customView
    }
    @objc func actionONError(sender : UIButton) {
        self.callReportDataAPI(strLoaderTag: 1)
    }
    
}


// MARK: - ---------------ReportCell
// MARK: -
class ReportCell: UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    
    @IBOutlet weak var img: UIImageView!
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}
