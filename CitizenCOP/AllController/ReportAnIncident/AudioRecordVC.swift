//
//  AudioRecordVC.swift
//  CitizenCOP
//
//  Created by Navin Patidar on 5/29/19.
//  Copyright © 2019 CitizenCop. All rights reserved.
//

import UIKit
import AVFoundation

class AudioRecordVC: UIViewController {
    // MARK: - --------------IBOutlet
    // MARK: -
    @IBOutlet weak var btnStop: UIButton!
    @IBOutlet weak var btnRecord: UIButton!
    @IBOutlet weak var lblCOunt: UILabel!
    
    //Variables
    var audioRecorder: AVAudioRecorder!
    var meterTimer:Timer!
    var isAudioRecordingGranted: Bool!
    var strFileName = ""
    open weak var delegate:RecoderDelegate?


    // MARK: - --------------View Life Cycle
    // MARK: -
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnRecord.layer.cornerRadius = 5.0
        btnRecord.backgroundColor = hexStringToUIColor(hex: primaryDark)
        btnStop.layer.cornerRadius = 5.0
        btnStop.backgroundColor = hexStringToUIColor(hex: primaryDark)
        btnRecord.alpha = 1
        btnRecord.isUserInteractionEnabled = true
        btnStop.alpha = 0.5
        btnStop.isUserInteractionEnabled = false
      
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        switch AVAudioSession.sharedInstance().recordPermission {
        case AVAudioSession.RecordPermission.granted:
            isAudioRecordingGranted = true
            break
        case AVAudioSession.RecordPermission.denied:
            let alert = UIAlertController(title: "Microphone Unavailable", message: "Please check to see if device settings doesn't allow Microphone access", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction (title: "Settings", style: .default, handler: { (nil) in
                
                guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                    return
                }
                
                if UIApplication.shared.canOpenURL(settingsUrl) {
                    UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                        print("Settings opened: \(success)") // Prints true
                    })
                }
            }))
            self.present(alert, animated: true, completion: nil)
            isAudioRecordingGranted = false
        case AVAudioSession.RecordPermission.undetermined:
            AVAudioSession.sharedInstance().requestRecordPermission() { [unowned self] allowed in
                DispatchQueue.main.async {
                    if allowed {
                        self.isAudioRecordingGranted = true
                    } else {
                        self.isAudioRecordingGranted = false
                        
                    }
                }
            }
            break
        default:
            break
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        audioRecorder = nil
    }
    // MARK: - --------------Actions
    // MARK: -
    @IBAction func action_Back(_ sender: Any) {
        
       // self.dismiss(animated: true, completion: nil)

    }
    @IBAction func actionOnRecord(_ sender: Any) {
        if isAudioRecordingGranted {
            
            //Create the session.
            let session = AVAudioSession.sharedInstance()
            
            do {
                //Configure the session for recording and playback.
                try! session.setCategory(AVAudioSession.Category.playAndRecord)
                try session.setActive(true)
                //Set up a high-quality recording session.
                let settings = [
                    AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
                    AVSampleRateKey: 44100,
                    AVNumberOfChannelsKey: 2,
                    AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue
                ]
                //Create audio file name URL
                strFileName = "\(uniqueString()).m4a"
                let audioFilename = getDocumentsDirectory().appendingPathComponent(strFileName)
                //Create the audio recording, and assign ourselves as the delegate
                audioRecorder = try AVAudioRecorder(url: audioFilename, settings: settings)
                audioRecorder.delegate = self
                audioRecorder.isMeteringEnabled = true
                audioRecorder.record()
                meterTimer = Timer.scheduledTimer(timeInterval: 0.1, target:self, selector:#selector(self.updateAudioMeter(timer:)), userInfo:nil, repeats:true)
                btnRecord.alpha = 0.5
                btnRecord.isUserInteractionEnabled = false
                btnStop.alpha = 1
                btnStop.isUserInteractionEnabled = true
            }
            catch let error {
                print("Error for start audio recording: \(error.localizedDescription)")
            }
        }
    }
    
    @IBAction func actionOnStop(_ sender: Any) {
         finishAudioRecording(success: true)
        btnRecord.alpha = 1
        btnRecord.isUserInteractionEnabled = true
        btnStop.alpha = 0.5
        btnStop.isUserInteractionEnabled = false
    }
    
  
    func finishAudioRecording(success: Bool) {
        
        audioRecorder.stop()
        audioRecorder = nil
        meterTimer.invalidate()
        
        if success {
            
         
            let AudioData = GetAudioVideoFromDocumentDirectory(strFileName: strFileName)
            let dict = NSMutableDictionary()
            dict.setValue(AudioData, forKey: "audio")
            dict.setValue(strFileName, forKey: "audioName")
            
            delegate?.getDataFromRecordDelegate(dictData: dict, tag: 0)
            self.dismiss(animated: true, completion: nil)
            print("Recording finished successfully.")
        } else {
            print("Recording failed :(")
        }
    }
   @objc func updateAudioMeter(timer: Timer) {
        
        if audioRecorder.isRecording {
           // let hr = Int((audioRecorder.currentTime / 60) / 60)
            let min = Int(audioRecorder.currentTime / 60)
            let sec = Int(audioRecorder.currentTime.truncatingRemainder(dividingBy: 60))
           // let totalTimeString = String(format: "%02d:%02d:%02d", hr, min, sec)
            let totalTimeString = String(format: "%02d:%02d",min, sec)
            if(sec == 59){
                showAlertWithoutAnyAction(strtitle: alertReachedMax, strMessage: "Your audio recorded succesfully!", viewcontrol: self)
                finishAudioRecording(success: true)
                btnRecord.alpha = 1
                btnRecord.isUserInteractionEnabled = true
                btnStop.alpha = 0.5
                btnStop.isUserInteractionEnabled = false
            }
            lblCOunt.text = totalTimeString
            audioRecorder.updateMeters()
        }
    }
    
    func getDocumentsDirectory() -> URL {
        
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
}
 //MARK:- Audio recoder delegate methods
extension AudioRecordVC :  AVAudioRecorderDelegate{
   
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        
        if !flag {
            finishAudioRecording(success: false)
        }
    }
}
//MARK: -------Protocol

protocol RecoderDelegate : class{
    func getDataFromRecordDelegate(dictData : NSDictionary ,tag : Int)
}
