//
//  ReportAnIncidentVC.swift
//  CitizenCOP
//
//  Created by Navin Patidar on 5/23/19.
//  Copyright © 2019 CitizenCop. All rights reserved.
//

import UIKit

class ReportAnIncidentVC: UIViewController {
  
    // MARK: - --------------IBOutlet
    // MARK: -
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var txtDetail: KMPlaceholderTextView!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var btnSubCategory: UIButton!
    @IBOutlet weak var btnCategory: UIButton!
    @IBOutlet weak var btnSubmit: UIButton!
  
    @IBOutlet weak var collection: UICollectionView!
    @IBOutlet weak var heightVideoFile: NSLayoutConstraint!
    @IBOutlet weak var heightCollection: NSLayoutConstraint!
    @IBOutlet weak var heightAudioFile: NSLayoutConstraint!
    @IBOutlet weak var lblVedioName: UILabel!
    @IBOutlet weak var lblAudioName: UILabel!
    @IBOutlet weak var viewVideo: CardView!
    @IBOutlet weak var viewAudio: CardView!
    @IBOutlet weak var lblTitle: UILabel!

    @IBOutlet weak var btnPoliceStation: UIButton!
    @IBOutlet weak var tv: UITableView!

    var aryCatrgory = NSMutableArray()
    var arySubCatrgory = NSMutableArray()
    var aryReportImage = NSMutableArray()
    var imagePicker = UIImagePickerController()
    var dictAudioData = NSMutableDictionary()
    var dictVideoData = NSMutableDictionary()
     var timer = Timer()
    var strAudioName = ""
    var strVideoName = ""
    var strImageName = ""
    var strlblTittle = String()
    var aryPoliceStation = NSMutableArray()

    // MARK: - --------------Life Cycle
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryTheamColor)
        lblTitle.text = strlblTittle
        tv.tableFooterView = UIView()
    }
   
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        btnSubmit.layer.cornerRadius = 5.0
        btnSubmit.backgroundColor = hexStringToUIColor(hex: primaryDark)
        txtName.layer.cornerRadius = 5.0
        txtName.layer.borderColor = UIColor.lightGray.cgColor
        txtName.layer.borderWidth = 1.0
        
        collection.layer.borderWidth = 1
        collection.layer.masksToBounds = false
        collection.layer.cornerRadius = 8.0
        collection.clipsToBounds = true
        collection.layer.borderColor = UIColor.lightGray.cgColor

        self.aryCatrgory = getDataFromCoreDataBase(strEntity: "ReportCategory", strkey: "reportCategory")
        if(aryCatrgory.count == 0){
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                if(dictCityData.value(forKey: "CityData")as! NSArray).count != 0{
                    let strCityId = "\(((dictCityData.value(forKey: "CityData")as! NSArray).object(at: 0)as! NSDictionary).value(forKey: "city_id")!)"
                    self.call_ReportCategoryByAPI(strCityID: strCityId, type: "1")
                }
            }
        }
        
        let strCityId = "\(((dictCityData.value(forKey: "CityData")as! NSArray).object(at: 0)as! NSDictionary).value(forKey: "city_id")!)"
        callPoliceStationAPI(strCityID: strCityId)
        
    }
    
    override func viewWillLayoutSubviews() {
        if(aryAudio.count == 0){
            heightAudioFile.constant = 0.0
            viewAudio.isHidden = true
        }else{
            DeviceType.IS_IPAD ? (self.heightAudioFile.constant = 45.0) :  (self.heightAudioFile.constant = 35.0)

            viewAudio.isHidden = false
        }
        if(aryVideo.count == 0){
            heightVideoFile.constant = 0.0
            viewVideo.isHidden = true

        }else{
            DeviceType.IS_IPAD ? (self.heightVideoFile.constant = 45.0) :  (self.heightVideoFile.constant = 35.0)
            viewVideo.isHidden = false
        }
        
        DeviceType.IS_IPAD ? (self.aryReportImage.count == 0 ? (self.heightCollection.constant = 0.0) :  (self.heightCollection.constant = 125.0)) : (self.aryReportImage.count == 0 ? (self.heightCollection.constant = 0.0) :  (self.heightCollection.constant = 95.0))
        

    }
    // MARK: - ---------------IBAction
    // MARK: -
    @IBAction func actiononBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        
    }
    @IBAction func actiononSelectCategory(_ sender: UIButton) {
        if(aryCatrgory.count == 0){
            if(dictCityData.value(forKey: "CityData")as! NSArray).count != 0{
                let strCityId = "\(((dictCityData.value(forKey: "CityData")as! NSArray).object(at: 0)as! NSDictionary).value(forKey: "city_id")!)"
                self.call_ReportCategoryByAPI(strCityID: strCityId, type: "2")
            }    }
        else{
            PopViewWithArray(tag: 6, aryData: aryCatrgory, strTitle: "Select Category")
        }
    }
    @IBAction func actiononSubSelectCategory(_ sender: UIButton) {
        if(btnCategory.tag != 0){
            if(arySubCatrgory.count == 0){
                self.call_ReportSubCategoryByAPI(strCategoryID: "\(btnCategory.tag)", type: "2")
            }
            else{
                PopViewWithArray(tag: 7, aryData: arySubCatrgory, strTitle: "Select Subcategory")
            }
        }else{
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSelectCategory, viewcontrol: self)
        }
    }
    @IBAction func actiononCapture(_ sender: UIButton) {
        
        if(sender.tag == 1){ // Camera
           
            if(aryReportImage.count >= 5){
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertReachedMax, viewcontrol: self)

            }else{
                imagePicker = UIImagePickerController()
                imagePicker.view.tag = 1

                if UIImagePickerController.isSourceTypeAvailable(.camera){
                    print("Button capture")
                    
                    imagePicker.delegate = self
                    imagePicker.sourceType = .camera
                    imagePicker.allowsEditing = false
                    present(imagePicker, animated: true, completion: nil)
                }
            }
           
        }else if (sender.tag == 2){ // Gallery
           
            if(aryReportImage.count >= 5){
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertReachedMax, viewcontrol: self)
            }else{
                imagePicker = UIImagePickerController()
                 imagePicker.view.tag = 1
                if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
                    print("Button capture")
                    imagePicker.delegate = self
                    imagePicker.sourceType = .savedPhotosAlbum
                    imagePicker.allowsEditing = false
                    present(imagePicker, animated: true, completion: nil)
                }
            }
           
        }else if (sender.tag == 3){ // Video
            imagePicker = UIImagePickerController()
            imagePicker.view.tag = 2
            imagePicker.sourceType = .camera
            imagePicker.cameraDevice = .rear
            imagePicker.videoMaximumDuration = 30
            imagePicker.delegate = self
            imagePicker.videoQuality = .typeLow
            let mediaTypes = UIImagePickerController.availableMediaTypes(for: .camera)
            let videoMediaTypesOnly = (mediaTypes as NSArray?)?.filtered(using: NSPredicate(format: "(SELF contains %@)", "movie"))
            let movieOutputPossible: Bool = videoMediaTypesOnly != nil
            if movieOutputPossible {
                if let videoMediaTypesOnly = videoMediaTypesOnly as? [String] {
                    imagePicker.mediaTypes = videoMediaTypesOnly
                }
                timer = Timer.scheduledTimer(timeInterval: 60.0, target: self, selector: #selector(pickerDismiss), userInfo: nil, repeats: false)
                present(imagePicker, animated: true)
            }
            
        }else if (sender.tag == 4){ // Record
            let vc: AudioRecordVC = self.storyboard!.instantiateViewController(withIdentifier: "AudioRecordVC") as! AudioRecordVC
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .coverVertical
            vc.delegate = self
            self.present(vc, animated: true, completion: {})
        }
        
    }
    @IBAction func actiononSubmit(_ sender: UIButton) {
        if(btnCategory.tag == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSelectCategory, viewcontrol: self)
        }else if (txtDetail.text.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertDescription, viewcontrol: self)
        }else{
            saveReportData()
        }
    }
    @IBAction func actiononHelp(_ sender: UIButton) {
        let testController = mainStoryboard.instantiateViewController(withIdentifier: "HelpVC")as! HelpVC
        testController.strMessage = alertReportincidenceHelp
        self.navigationController?.pushViewController(testController, animated: true)
    }
    
    
    @IBAction func actionVideoRemove(_ sender: Any) {
        heightVideoFile.constant = 0.0
        aryVideo = NSMutableArray()
        strVideoName = ""
        self.lblVedioName.text = ""
    }
    
    @IBAction func actionAudioRemove(_ sender: Any) {
        heightAudioFile.constant = 0.0
        aryAudio = NSMutableArray()
        strAudioName = ""
    }
    
    //MARK: Other Function's
    func PopViewWithArray(tag: Int , aryData : NSMutableArray , strTitle: String)  {
        let vc: SelectionClass = self.storyboard!.instantiateViewController(withIdentifier: "SelectionClass") as! SelectionClass
        vc.strTitle = strTitle
        vc.strTag = tag
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .coverVertical
        vc.delegate = self
        vc.aryList = aryData
        self.present(vc, animated: true, completion: {})
    }
    @objc func pickerDismiss()
    {
        showAlertWithoutAnyAction(strtitle: alertReachedMax, strMessage: "Your video recorded succesfully!", viewcontrol: self)
        imagePicker.stopVideoCapture()
    }
    
    
    @IBAction func actionOnPoliceStation(_ sender: UIButton) {
        if self.aryPoliceStation.count != 0 {
            let vc: SelectionClass = mainStoryboard.instantiateViewController(withIdentifier: "SelectionClass") as! SelectionClass
            vc.strTitle = "Select"
            vc.strTag = 18
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .coverVertical
            vc.delegate = self
            vc.aryList = aryPoliceStation
            self.present(vc, animated: true, completion: {})
        }
    }
    
    
    // MARK: - ---------------API CAlling
    // MARK: -
    func callPoliceStationAPI(strCityID : String ) {
        if !isInternetAvailable() {
            FTIndicator.showToastMessage(alertInternet)
            
        }else{
            let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><GetRAIPoliceStation xmlns='\(BaseHostURL)/'><city_id>\(strCityID)</city_id></GetRAIPoliceStation></soap12:Body></soap12:Envelope>"
            let loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
            self.present(loader, animated: false, completion: nil)
            
            WebServiceClass.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: BaseURLCitizenEyestation, resultype: "GetRAIPoliceStationResult", responcetype: "GetRAIPoliceStationResponse") { (responce, status) in
                loader.dismiss(animated: false) {
                    if status == "Suceess"{
                        let dictTemp = responce.value(forKey: "data")as! NSDictionary
                        if("\(dictTemp.value(forKey: "Result")!)" == "True"){
                            
                            let ary = dictTemp.value(forKey: "PoliceStation")as! NSArray
                            if(ary.count != 0){
                                self.aryPoliceStation = NSMutableArray()
                                self.aryPoliceStation = ary.mutableCopy() as! NSMutableArray
                                
                             
                            }else{
                                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                                
                            }
                            
                        }else{
                            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Police Station not found", viewcontrol: self)
                        }
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                    }
                }
                
                
               
            }
        }
    }
    
    // MARK: - ---------------Save In CoreData
    // MARK: -
    
    func saveReportData() {
        // For Images
        for item in aryReportImage {
            strImageName = strImageName + "\((item as AnyObject).value(forKey: "imageName")!),"
            saveImageDocumentDirectory(strFileName: "\((item as AnyObject).value(forKey: "imageName")!)", image: (item as AnyObject).value(forKey: "image")as! UIImage)
        }
        if(strImageName.count != 0){
            strImageName = String(strImageName.dropLast())
        }
        // For Video

        for item in aryVideo {
            strVideoName = strVideoName + "\((item as AnyObject).value(forKey: "videoName")!),"
            saveAudioVideoDocumentDirectory(strFileName: "\((item as AnyObject).value(forKey: "videoName")!)", data: (item as AnyObject).value(forKey: "video")as! Data)
        }
        if(strVideoName.count != 0){
            strVideoName = String(strVideoName.dropLast())
        }
        // For Audio

        for item in aryAudio {
            strAudioName = strAudioName + "\((item as AnyObject).value(forKey: "audioName")!),"
            saveAudioVideoDocumentDirectory(strFileName: "\((item as AnyObject).value(forKey: "audioName")!)", data: (item as AnyObject).value(forKey: "audio")as! Data)
        }
        if(strAudioName.count != 0){
            strAudioName = String(strAudioName.dropLast())
        }
        
        var strCityId = ""
        if(dictCityData.value(forKey: "CityData")as! NSArray).count != 0{
            strCityId = "\(((dictCityData.value(forKey: "CityData")as! NSArray).object(at: 0)as! NSDictionary).value(forKey: "city_id")!)"
        }
        let dictData = NSMutableDictionary()
        dictData.setValue("\(btnCategory.tag)", forKey: "ID")
        dictData.setValue("\(getCurrentDate_Time(strType: "Date"))", forKey: "SendDate")
        dictData.setValue("\(getCurrentDate_Time(strType: "Time"))", forKey: "SendTime")
        dictData.setValue("\(txtDetail.text!)", forKey: "Description")
        dictData.setValue("\(txtName.text!)", forKey: "contactinfo")
        dictData.setValue("\(GlobleLat)", forKey: "Latitude")
        dictData.setValue("\(GlobleLong)", forKey: "Longtitude")
        dictData.setValue("\(strImageName)", forKey: "CitizenCopImage")
        dictData.setValue("\(DeviceID)", forKey: "imeiNo")
        dictData.setValue("\(strVideoName)", forKey: "video")
        dictData.setValue("\(btnCategory.tag)", forKey: "categoryid")
        dictData.setValue("\(strCityId)", forKey: "cityid")
        dictData.setValue("\(strAudioName)", forKey: "audio")
        dictData.setValue("\(String(describing: getIPAddress()))", forKey: "ipaddress")
        dictData.setValue("\(btnSubCategory.tag)", forKey: "SubCategoryId")
        dictData.setValue("\(PushToken)", forKey: "IphoneVendorId")
        dictData.setValue("InsertComplaintWithDynamicMessage", forKey: "key")
        print(dictData)
        saveDataInLocalDictionary(strEntity: "ReportData", strKey: "reportData", data: dictData)
        
        FTIndicator.showNotification(withTitle: alert_ReportSubmit, message: "")
        self.navigationController?.popViewController(animated: true)
    }
    
    
    // MARK: - ---------------API CAlling
    // MARK: -
    func call_ReportCategoryByAPI(strCityID : String , type : String) {
        if !isInternetAvailable() {
            FTIndicator.showToastMessage(alertInternet)
        }else{
            customDotLoaderShowOnFull(message: "", controller: self)
            WebServiceClass.callAPIBYPOST(parameter: NSDictionary(), url: API_ReportCategoryBy + strCityID) { (responce, status) in
                customeDotLoaderRemove()
                if status == "Suceess"{
                    print(responce)
                    self.aryCatrgory = NSMutableArray()
                    self.aryCatrgory = ((responce.value(forKey: "data")as! NSDictionary).value(forKey: "Category")as! NSArray).mutableCopy()as! NSMutableArray
                    deleteAllRecords(strEntity:"ReportCategory")
                    saveDataInLocalArray(strEntity: "ReportCategory", strKey: "reportCategory", data: self.aryCatrgory)
                }
                else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            }
        }
    }
    
    func call_ReportSubCategoryByAPI(strCategoryID : String, type : String) {
        if !isInternetAvailable() {
            FTIndicator.showToastMessage(alertInternet)
        }else{
            customDotLoaderShowOnFull(message: "", controller: self)
            WebServiceClass.callAPIBYPOST(parameter: NSDictionary(), url: API_ReportSubCategoryBy + strCategoryID) { (responce, status) in
                customeDotLoaderRemove()
                if status == "Suceess"{
                    print(responce)
                    self.arySubCatrgory = NSMutableArray()
                    self.arySubCatrgory = ((responce.value(forKey: "data")as! NSDictionary).value(forKey: "SubCategory")as! NSArray).mutableCopy()as! NSMutableArray
                    self.PopViewWithArray(tag: 7, aryData: self.arySubCatrgory, strTitle: "Select Subcategory")
                }
                else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            }
        }
    }
}
//MARK:-
//MARK:- ---------PopUpDelegate

extension ReportAnIncidentVC : PopUpDelegate
{
    func getDataFromPopupDelegate(dictData: NSDictionary, tag: Int) {
        if(tag == 6){
            
            self.btnCategory.setTitle("\(dictData.value(forKey: "CategoryName")!)", for: .normal)
            self.btnCategory.tag = Int("\(dictData.value(forKey: "CategoryId")!)")!
            self.btnSubCategory.setTitle("Select Category", for: .normal)
            self.btnSubCategory.tag = 0
            self.arySubCatrgory = NSMutableArray()
        }else if(tag == 7){
            self.btnSubCategory.setTitle("\(dictData.value(forKey: "SubCategoryName")!)", for: .normal)
            self.btnSubCategory.tag = Int("\(dictData.value(forKey: "SubCatId")!)")!
        }else if tag == 18 {
            if dictData.count != 0 {
                self.btnPoliceStation.setTitle("\(dictData.value(forKey: "PoliceStationName") ?? "")", for: .normal)
                btnPoliceStation.tag = Int("\(dictData.value(forKey: "PoliceStationId") ?? "0")")!
            }else{
                self.btnPoliceStation.setTitle("\(dictData.value(forKey: "PoliceStationName") ?? "")", for: .normal)
                btnPoliceStation.tag = 0

            }
        }
       
    }
}
// MARK: - ----------------UICollectionViewDelegate
// MARK: -

extension ReportAnIncidentVC : UICollectionViewDelegate ,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout , UIScrollViewDelegate{
    
    private func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        
        DeviceType.IS_IPAD ? (self.aryReportImage.count == 0 ? (self.heightCollection.constant = 0.0) :  (self.heightCollection.constant = 125.0)) : self.aryReportImage.count == 0 ? (self.heightCollection.constant = 0.0) :  (self.heightCollection.constant = 95.0)
        
        
        
        return self.aryReportImage.count
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        cell.alpha = 0.4
        cell.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        UIView.animate(withDuration: 0.5) {
            cell.alpha = 1
            cell.transform = .identity
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ReportImageCell", for: indexPath as IndexPath) as! ReportImageCell
        let dict = aryReportImage.object(at: indexPath.row)as! NSDictionary
        cell.ReportCell_Image.image = (dict.value(forKey: "image")as! UIImage)
        cell.ReportCell_Image.layer.borderWidth = 1
        cell.ReportCell_Image.layer.masksToBounds = false
        cell.ReportCell_Image.layer.borderColor = UIColor.groupTableViewBackground.cgColor
        cell.ReportCell_Image.layer.cornerRadius = 8.0
        cell.ReportCell_Image.clipsToBounds = true
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return   DeviceType.IS_IPAD ? CGSize(width: 120, height:120) : CGSize(width: 90, height:90)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       // let dict = aryReportImage.object(at: indexPath.row)as! NSDictionary
        let alert = UIAlertController(title: "Report an Incident", message: "Please Select an Option", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "View on full screen", style: .default , handler:{ (UIAlertAction)in
            let testController = mainStoryboard.instantiateViewController(withIdentifier: "PreviewImageVC")as! PreviewImageVC
            let dict = self.aryReportImage.object(at: indexPath.row)as! NSDictionary
            testController.img = (dict.value(forKey: "image")as! UIImage)
            self.navigationController?.pushViewController(testController, animated: true)
            print("User click Approve button")
        }))
        
       
        alert.addAction(UIAlertAction(title: "Delete", style: .destructive , handler:{ (UIAlertAction)in
            self.aryReportImage.removeObject(at: indexPath.row)
            self.collection.reloadData()
        }))
        
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
        }))
        //alert.popoverPresentationController?.sourceRect = sender.frame
        alert.popoverPresentationController?.sourceView = self.view
        alert.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        alert.popoverPresentationController?.permittedArrowDirections = []
        self.present(alert, animated: true, completion: {
        })
    }
  
    
    func animateCollection() {
        self.collection.reloadData()
        
        let cells =  self.collection.visibleCells
        let tableHeight: CGFloat =     self.collection.bounds.size.height
        
        for i in cells {
            let cell: UICollectionViewCell = i as UICollectionViewCell
            cell.transform = CGAffineTransform(translationX: 0, y: tableHeight)
        }
        
        var index = 0
        
        for a in cells {
            let cell: UICollectionViewCell = a as UICollectionViewCell
            UIView.animate(withDuration: 1.2, delay: 0.05 * Double(index), usingSpringWithDamping: 0.6, initialSpringVelocity: 0, options: .curveLinear, animations: {
                cell.transform = CGAffineTransform(translationX: 0, y: 0);
            }, completion: nil)
            index += 1
        }
    }
}

class ReportImageCell: UICollectionViewCell {
    //DashBoard
    @IBOutlet weak var ReportCell_Image: UIImageView!
    
}

// MARK: - ----------------UIImagePickerControllerDelegate
// MARK: -
extension ReportAnIncidentVC : UINavigationControllerDelegate, UIImagePickerControllerDelegate{
    
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        self.dismiss(animated: true, completion: { () -> Void in
            
        })
        if(imagePicker.view.tag == 1){ // For Gallery Camera
            let dictData = NSMutableDictionary()
            dictData.setValue(info[UIImagePickerController.InfoKey.originalImage] as? UIImage, forKey: "image")
            dictData.setValue(uniqueString() + ".jpg", forKey: "imageName")
            aryReportImage.add(dictData)
            collection.reloadData()
        }else{ // For Video
            self.timer.invalidate()
            let mediaType = info[UIImagePickerController.InfoKey.mediaType] as? String
            //check the media type string so we can determine if its a video
            if (mediaType == "public.movie") {
                let videoURL = info[UIImagePickerController.InfoKey.mediaURL] as? URL
                do{
                    let videoData = try Data(contentsOf: videoURL!)
                    self.dictVideoData.setValue(videoData, forKey: "video")
                    
                    self.dictVideoData.setValue(uniqueString() + ".mp4", forKey: "videoName")
                     aryVideo.add(dictVideoData)
                    self.heightVideoFile.constant = 35.0
                    self.viewVideo.isHidden = false
                    for item in aryVideo {
                        self.lblVedioName.text = "Video File: \((item as AnyObject).value(forKey: "videoName")!)"
                    }
                }catch {
                    
                }
            }
        }
    }
}
//MARK:-
//MARK:- ---------Recorder Delegate

extension ReportAnIncidentVC : RecoderDelegate
{
    func getDataFromRecordDelegate(dictData: NSDictionary, tag: Int) {
       
        aryAudio.add(dictData)
        self.heightAudioFile.constant = 35.0
        self.viewAudio.isHidden = false

        for item in aryAudio {
            self.lblAudioName.text = "Audio File: \((item as AnyObject).value(forKey: "audioName")!)"
        }
    }
    
    
}
