
//
//  ReportFeedbackVC.swift
//  CitizenCOP
//
//  Created by Navin Patidar on 5/30/19.
//  Copyright © 2019 CitizenCop. All rights reserved.
//

import UIKit

class ReportFeedbackVC: UIViewController {
    // MARK: - ----IBOutlet
    @IBOutlet weak var view_ForHeader: UIView!
    @IBOutlet weak var view_ForAdd: UIView!
    var strComplainNumber = ""

    // MARK: - ----Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        view_ForHeader.backgroundColor = hexStringToUIColor(hex: primaryTheamColor)
        view_ForAdd.backgroundColor = hexStringToUIColor(hex: primaryDark)

    }
    // MARK: - --------------IBAction
    // MARK: -
    @IBAction func actionOnBack(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func actiononHelp(_ sender: UIButton) {
        let testController = mainStoryboard.instantiateViewController(withIdentifier: "HelpVC")as! HelpVC
        testController.strMessage = alertYourReportFeedbackHelp
        self.navigationController?.pushViewController(testController, animated: true)
        
    }
    @IBAction func actiononAdd(_ sender: UIButton) {
        let testController = mainStoryboard.instantiateViewController(withIdentifier: "ReportCreatNewFeedbackVC")as! ReportCreatNewFeedbackVC
        testController.strComplainNumber = strComplainNumber
        self.navigationController?.pushViewController(testController, animated: true)
        
    }
}
