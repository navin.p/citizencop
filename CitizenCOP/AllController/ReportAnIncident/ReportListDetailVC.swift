//
//  ReportListDetailVC.swift
//  CitizenCOP
//
//  Created by Navin Patidar on 5/30/19.
//  Copyright © 2019 CitizenCop. All rights reserved.
//

import UIKit

class ReportListDetailVC: UIViewController {
    // MARK: - ----IBOutlet
    
    @IBOutlet weak var view_ForHeader: UIView!
    @IBOutlet weak var txtComplainNumber: ACFloatingTextfield!
    @IBOutlet weak var txtDate: ACFloatingTextfield!
    @IBOutlet weak var txtTime: ACFloatingTextfield!
    @IBOutlet weak var txtDetail: KMPlaceholderTextView!
    @IBOutlet weak var btnStatus: UIButton!
    @IBOutlet weak var btnFeedBack: UIButton!
    @IBOutlet weak var ratingBar: AARatingBar!

    var dictReportData = NSMutableDictionary()
    
    // MARK: - ----Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        view_ForHeader.backgroundColor = hexStringToUIColor(hex: primaryTheamColor)
        txtDetail.backgroundColor = UIColor.white
        txtComplainNumber.text = "\(dictReportData["ComplaintNo"]!)"
        txtDate.text = "\(dictReportData["SendDate"]!)"
        txtTime.text = "\(dictReportData["SendTime"]!)"
        txtDetail.text = "\(dictReportData["Description"]!)"
         ratingBar.value = 0.0
        let userRating = "\(dictReportData["UserRating"]!)"
        if(userRating != ""){
            if let doubleValue = Double(userRating) {
                 ratingBar.value = CGFloat(doubleValue)
            }
        }
        btnStatus.layer.cornerRadius = 5.0
        btnStatus.backgroundColor = hexStringToUIColor(hex: primaryDark)
        btnFeedBack.layer.cornerRadius = 5.0
        btnFeedBack.backgroundColor = hexStringToUIColor(hex: primaryDark)
        
        ratingBar.ratingDidChange = { ratingValue in
            print(ratingValue)
           self.callAddRatingAPI()
        }
    }
    
    // MARK: - --------------IBAction
    // MARK: -
    @IBAction func actionOnBack(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func actiononHelp(_ sender: UIButton) {
        let testController = mainStoryboard.instantiateViewController(withIdentifier: "HelpVC")as! HelpVC
        testController.strMessage = alertYourReportDetailHelp
        self.navigationController?.pushViewController(testController, animated: true)
        
    }
    @IBAction func actionOnFeedback(_ sender: UIButton) {
        let testController = mainStoryboard.instantiateViewController(withIdentifier: "ReportCreatNewFeedbackVC")as! ReportCreatNewFeedbackVC
        testController.strComplainNumber = "\(dictReportData["ComplaintNo"]!)"
        self.navigationController?.pushViewController(testController, animated: true)
        
//        let testController = mainStoryboard.instantiateViewController(withIdentifier: "ReportFeedbackVC")as! ReportFeedbackVC
//        testController.strComplainNumber = "\(dictReportData["ComplaintNo"]!)"
//        self.navigationController?.pushViewController(testController, animated: true)
    }
    @IBAction func actionOnStatus(_ sender: UIButton) {
        if !isInternetAvailable() {
            FTIndicator.showNotification(withTitle: alertInternet, message: "")
        }else{
            let testController = mainStoryboard.instantiateViewController(withIdentifier: "ReportLookupVC")as! ReportLookupVC
            testController.strComplainNumber = "\(dictReportData["ComplaintNo"]!)"
            self.navigationController?.pushViewController(testController, animated: true)
        }
    }
    // MARK: - ---------------API CAlling
    // MARK: -
    func callAddRatingAPI() {
        if !isInternetAvailable() {
            FTIndicator.showNotification(withTitle: alertInternet, message: "")
            
        }else{
            
            let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><AddRating xmlns='\(BaseHostURL)/'><Id>\(txtComplainNumber.text!)</Id><UserRating>\(Int(self.ratingBar.value))</UserRating><RatingLat>\(GlobleLat)</RatingLat><RatingLong>\(GlobleLong)</RatingLong></AddRating></soap12:Body></soap12:Envelope>"
         
            
            WebServiceClass.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: BaseURLCitizen, resultype: "AddRatingResult", responcetype: "AddRatingResponse") { (responce, status) in
                if status == "Suceess"{
                  FTIndicator.showNotification(withTitle: "Rating Submited successfully.", message: "")
                }else{
                  
                }
            }
            
        }
    }
}
