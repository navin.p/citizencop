//
//  SyncReportDataVC.swift
//  CitizenCOP
//
//  Created by Navin Patidar on 5/28/19.
//  Copyright © 2019 CitizenCop. All rights reserved.
//

import UIKit
import CoreData
import UserNotifications


var aryReportData = NSMutableArray()
var aryImage = NSMutableArray()
var aryVideo = NSMutableArray()
var aryAudio = NSMutableArray()
var object = NSManagedObject()

class SyncReportDataVC: NSObject {
    
 func getReportData() {
    aryImage = NSMutableArray()
    aryVideo = NSMutableArray()
    aryAudio = NSMutableArray()

    let aryTemp = getDataFromLocal(strEntity: "ReportData", strkey: "reportData")
    if(aryTemp.count != 0){
        object = aryTemp[0]as! NSManagedObject
        let dict = object.value(forKey: "reportData")as! NSDictionary
        let imageName = "\(String(describing: dict.value(forKey: "CitizenCopImage")!))"
       
        let audio = "\(String(describing: dict.value(forKey: "audio")!))"
   
        let video = "\(String(describing: dict.value(forKey: "video")!))"

        aryImage = (imageName.split(separator: ",") as NSArray).mutableCopy()as! NSMutableArray
       
        if(audio.count != 0){
            aryAudio.add(audio)
        }
        if(video.count != 0){
            aryVideo.add(video)
        }
        SyncDataToserver()
    }
}
 func SyncDataToserver() {
    if(aryImage.count != 0){
         uploadImage()
    }else if (aryVideo.count != 0){
        uploadVideo()
    }else if (aryAudio.count != 0){
        uploadAudio()
    }else{
        uploadData()
    }
}
   
 func uploadImage() {
    
    let img = GetImageFromDocumentDirectory(strFileName: "\(aryImage.object(at: 0))")
     print(API_UploadReportImage)

     let data = img.jpegData(compressionQuality:0.5)
         
     if  !data!.isEmpty { // If casti
         WebServiceClass.callAPIWithImage(parameter: NSDictionary(), url: API_UploadReportImage, data: data!, fileName: "\(aryImage.object(at: 0))", withName: "\(aryImage.object(at: 0))") { (responce, status) in
                     print(responce)
                     if(status == "Suceess"){
                         DeleteImageFromDocumentDirectory(strFileName: "\(aryImage.object(at: 0))")
                         aryImage.removeObject(at: 0)
                         self.SyncDataToserver()
                     }
                 }
         
     }else{
         DeleteImageFromDocumentDirectory(strFileName: "\(aryImage.object(at: 0))")
         aryImage.removeObject(at: 0)
         self.SyncDataToserver()
     }
     
 

}
 func uploadAudio() {
    let data = GetAudioVideoFromDocumentDirectory(strFileName: "\(aryAudio.object(at: 0))")
    
     if  !data.isEmpty { // If casti

         WebServiceClass.callAPIWithImage(parameter: NSDictionary(), url: API_UploadReportAUDIO, data: data, fileName: "\(aryAudio.object(at: 0))", withName: "userfile") { (responce, status) in
             print(responce)
             if(status == "Suceess"){
                 DeleteImageFromDocumentDirectory(strFileName: "\(aryAudio.object(at: 0))")
                 aryAudio.removeObject(at: 0)
                 self.SyncDataToserver()
             }
         }
     }else{
         DeleteImageFromDocumentDirectory(strFileName: "\(aryAudio.object(at: 0))")
         aryAudio.removeObject(at: 0)
         self.SyncDataToserver()
     }


}
    
 func uploadVideo() {
    let data = GetAudioVideoFromDocumentDirectory(strFileName: "\(aryVideo.object(at: 0))")
     if  !data.isEmpty { // If casti

         WebServiceClass.callAPIWithImage(parameter: NSDictionary(), url: API_UploadReportVideo, data: data, fileName: "\(aryVideo.object(at: 0))", withName: "userfile") { (responce, status) in
             print(responce)
             if(status == "Suceess"){
                 DeleteImageFromDocumentDirectory(strFileName: "\(aryVideo.object(at: 0))")
                 aryVideo.removeObject(at: 0)
                 self.SyncDataToserver()
             }
         }
     }else{
         DeleteImageFromDocumentDirectory(strFileName: "\(aryVideo.object(at: 0))")
         aryVideo.removeObject(at: 0)
         self.SyncDataToserver()
     }
 

}
 func uploadData() {
    let dict = object.value(forKey: "reportData")as! NSDictionary

    let Description = "\(String(describing: dict.value(forKey: "Description")!))"
    let ID = "\(String(describing: dict.value(forKey: "ID")!))"
    let IphoneVendorId = "\(String(describing: dict.value(forKey: "IphoneVendorId")!))"
    let Latitude = "\(String(describing: dict.value(forKey: "Latitude")!))"
    let Longtitude = "\(String(describing: dict.value(forKey: "Longtitude")!))"
    let SendDate = "\(String(describing: dict.value(forKey: "SendDate")!))"
    let SendTime = "\(String(describing: dict.value(forKey: "SendTime")!))"
    let SubCategoryId = "\(String(describing: dict.value(forKey: "SubCategoryId")!))"
    let categoryid = "\(String(describing: dict.value(forKey: "categoryid")!))"
    let cityid = "\(String(describing: dict.value(forKey: "cityid")!))"
    let contactinfo = "\(String(describing: dict.value(forKey: "contactinfo")!))"
    let imeiNo = "\(String(describing: dict.value(forKey: "imeiNo")!))"
    let ipaddress = "\(String(describing: dict.value(forKey: "ipaddress")!))"
    let key = "\(String(describing: dict.value(forKey: "key")!))"
    let image_Name = "\(String(describing: dict.value(forKey: "CitizenCopImage")!))"
    let audio_name = "\(String(describing: dict.value(forKey: "audio")!))"
    let video_name = "\(String(describing: dict.value(forKey: "video")!))"
    
    let StrURL = BaseHostURL + "/CitizenCopAllHandler.ashx?ID=\(ID)&SendDate=\(SendDate)&SendTime=\(SendTime)&Description=\(Description)&contactinfo=\(contactinfo)&Latitude=\(Latitude)&Longtitude=\(Longtitude)&CitizenCopImage=\(image_Name)&imeiNo=\(imeiNo)&video=\(video_name)&categoryid=\(categoryid)&cityid=\(cityid)&audio=\(audio_name)&ipaddress=\(ipaddress)&SubCategoryId=\(SubCategoryId)&IphoneVendorId=\(IphoneVendorId)&key=\(key)"
    if !isInternetAvailable() {
        FTIndicator.showToastMessage(alertInternet)
    }else{
        
        WebServiceClass.callAPIBYPOST(parameter: NSDictionary(), url: StrURL) { (responce, status) in
            print(responce)
            if status == "Suceess"{
              deleteDataFomeCoreData(obj: object)
                
                let aryTemp = (responce.value(forKey: "data")as! NSArray).mutableCopy()as! NSMutableArray
                
                if (aryTemp.count != 0){
                    let dict = aryTemp.object(at: 0)as! NSDictionary
                    self.scheduleNotification(strMessage: "\(dict.value(forKey: "Message")!)")
                }
                
                
                self.getReportData()
            }
            else{
                
                
            }
        }
        
        
    }
    
    }
    
     func scheduleNotification(strMessage : String) {
        //Compose New Notificaion
        let content = UNMutableNotificationContent()
        content.sound = UNNotificationSound.default
        content.title = "Report an Incident"
        content.subtitle = "\(strMessage.html2String)"
        //content.body = strMessage.html2String
        content.badge = 0
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)
        UNUserNotificationCenter.current().delegate = appDelegate

        let identifier = "\(uniqueString())"
        let request = UNNotificationRequest(identifier: identifier, content: content, trigger: trigger)
         UNUserNotificationCenter.current().add(request) { (error) in
            if let error = error {
                print("Error \(error.localizedDescription)")
            }
        }
    
    }
}

     
