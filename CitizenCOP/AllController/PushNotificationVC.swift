//
//  PushNotificationVC.swift
//  CitizenCOP
//
//  Created by Navin Patidar on 10/4/19.
//  Copyright © 2019 CitizenCop. All rights reserved.
//

import UIKit
import CoreData

class PushNotificationVC: UIViewController {
    // MARK: - --------------IBOutlet
    // MARK: -
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var tvlist: UITableView!
   
    // MARK: - --------------Variable
    // MARK: -
    var aryList = NSMutableArray()
    var ErrorMsg = ""
    var strComeFrom = String()
  let button = UIButton()
    // MARK: - --------------Life Cycle
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewHeader.backgroundColor = hexStringToUIColor(hex: primaryTheamColor)
     
        button.frame = CGRect(x: 50, y: self.view.frame.height - 40, width:self.view.frame.width - 100.0, height: 40)
        button.setTitle("Clear All Notiication", for: .normal)
        button.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        button.setTitleColor(hexStringToUIColor(hex: primaryTheamColor), for: .normal)
        self.view.addSubview(button)
      
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        retrieveData()

    }
    // MARK: - ---------------IBAction
    // MARK: -
    @objc func buttonAction(sender: UIButton!) {
              deleteAllData("PushNotification")

    }
    @IBAction func actiononBack(_ sender: UIButton) {
        if(strComeFrom == "DashBoard"){
          self.navigationController?.popViewController(animated: true)
        }else{
            let testController = mainStoryboard.instantiateViewController(withIdentifier: "DashBoardVC")as! DashBoardVC
            self.navigationController?.pushViewController(testController, animated: false)
        }
        
       
    }
    
    // MARK: - --------------DataBase
    // MARK: -
    func createData(strTitle :String , strBody : String ,apsData : NSDictionary , strPushType : String){
        
        //As we know that container is set up in the AppDelegates so we need to refer that container.
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        
        //We need to create a context from this container
        let managedContext = appDelegate.persistentContainer.viewContext
        
        //Now let’s create an entity and new user records.
        let userEntity = NSEntityDescription.entity(forEntityName: "PushNotification", in: managedContext)!
      
        
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd.MM.yyyy.hhmmssa"
        let result = formatter.string(from: date)
        //final, we need to add some data to our newly created record for each keys using
        //here adding 5 data with loop
        
        let user = NSManagedObject(entity: userEntity, insertInto: managedContext)
        user.setValue("\(strTitle)", forKeyPath: "title")
        user.setValue("\(strBody)", forKey: "body")
        user.setValue(apsData, forKey: "aps")
        user.setValue("\(strPushType)", forKey: "pushType")
        user.setValue("\(result)", forKey: "id")
        user.setValue(false, forKey: "read")


     

        //Now we have set all the values. The next step is to save them inside the Core Data
        
        do {
            try managedContext.save()
            
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    
    
    
    
    func retrieveData() {
        let aryListTemp = NSMutableArray()
        aryList = NSMutableArray()

        //As we know that container is set up in the AppDelegates so we need to refer that container.
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        
        //We need to create a context from this container
        let managedContext = appDelegate.persistentContainer.viewContext
        
        //Prepare the request of type NSFetchRequest  for the entity
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "PushNotification")
        
        //        fetchRequest.fetchLimit = 1
        //        fetchRequest.predicate = NSPredicate(format: "username = %@", "Ankur")
        //        fetchRequest.sortDescriptors = [NSSortDescriptor.init(key: "email", ascending: false)]
        //
        do {
            let result = try managedContext.fetch(fetchRequest)
            for data in result as! [NSManagedObject] {
                let dictData = NSMutableDictionary()
                dictData.setValue(data.value(forKey: "title") as! String, forKey: "title")
                dictData.setValue(data.value(forKey: "body") as! String, forKey: "body")
                dictData.setValue(data.value(forKey: "aps")as! NSDictionary, forKey: "aps")
                dictData.setValue(data.value(forKey: "pushType") as! String, forKey: "pushType")
                dictData.setValue(data.value(forKey: "id") as! String, forKey: "id")
                dictData.setValue(data.value(forKey: "read") as! Bool, forKey: "read")
                aryListTemp.add(dictData)
                
            }
            print(aryListTemp)
            if(aryListTemp.count != 0){
                aryList = (aryListTemp.reversed()as NSArray).mutableCopy()as! NSMutableArray
            self.ErrorMsg = ""
            }else{
                self.ErrorMsg = "Notification is not available."
            }
            self.tvlist.reloadData()
            
        } catch {
            
            print("Failed")
        }
    }
    
    func updateData(strTitle : String, strBody : String , id : String){
        
        //As we know that container is set up in the AppDelegates so we need to refer that container.
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        
        //We need to create a context from this container
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "PushNotification")
        let predicate1 = NSPredicate(format: "title = %@", "\(strTitle)")
        let predicate2 = NSPredicate(format: "body = %@", "\(strBody)")
        let predicate3 = NSPredicate(format: "id = %@", "\(id)")
        fetchRequest.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate1,predicate2,predicate3])
        
        do
        {
            let test = try managedContext.fetch(fetchRequest)
            
            let objectUpdate = test[0] as! NSManagedObject
            objectUpdate.setValue(true, forKey: "read")
         
            do{
                try managedContext.save()
            }
            catch
            {
                print(error)
            }
        }
        catch
        {
            print(error)
        }
        
    }
    func deleteAllData(_ entity:String) {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entity)
        fetchRequest.returnsObjectsAsFaults = false
        do {
            let managedContext = appDelegate.persistentContainer.viewContext

            let results = try managedContext.fetch(fetchRequest)
            for object in results {
                guard let objectData = object as? NSManagedObject else {continue}
                managedContext.delete(objectData)
            }
        } catch let error {
            print("Detele all data in \(entity) error :", error)
        }
        retrieveData()
                      
    }
    func deleteData(strTitle : String, strBody : String , id : String , deletAll : Bool){
        
        //As we know that container is set up in the AppDelegates so we need to refer that container.
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        
        //We need to create a context from this container
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "PushNotification")
        
        
        let predicate1 = NSPredicate(format: "title = %@", "\(strTitle)")
        let predicate2 = NSPredicate(format: "body = %@", "\(strBody)")
        let predicate3 = NSPredicate(format: "id = %@", "\(id)")

        fetchRequest.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate1,predicate2,predicate3])
        do
        {
            let test = try managedContext.fetch(fetchRequest)
            
            let objectToDelete = test[0] as! NSManagedObject
            if(deletAll){
                 for object in test {
                           guard let objectData = object as? NSManagedObject else {continue}
                           managedContext.delete(objectData)
                       }
            }else{
                managedContext.delete(objectToDelete)

            }
         
            
            do{
                try managedContext.save()
                 retrieveData()
                
                
            }
            catch
            {
                print(error)
            }
            
        }
        catch
        {
            print(error)
        }
    }
    
}
// MARK: - ----------------UITableViewDelegate
// MARK: -

extension PushNotificationVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return aryList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tvlist.dequeueReusableCell(withIdentifier: "PushNotiCell", for: indexPath as IndexPath) as! PushNotiCell
        let dict = aryList.object(at: indexPath.row)as! NSDictionary
        cell.lblSubtitle.text = "\(dict.value(forKey: "body")!)".html2String
        cell.lblTitle.text = "\(dict.value(forKey: "title")!)"
        if(dict.value(forKey: "read") as! Bool){
            cell.view.isHidden = true
        }else{
            cell.view.isHidden = false

        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(aryList.count != 0){
            let dict = aryList.object(at: indexPath.row)as! NSDictionary
            let strPushType = "\(dict.value(forKey: "pushType")!)"
            let strRead = dict.value(forKey: "read")as! Bool
                if(strPushType == "forNews"){
                    let testController = mainStoryboard.instantiateViewController(withIdentifier: "NewsVC")as! NewsVC
                    self.navigationController?.pushViewController(testController, animated: true)
                }else if (strPushType == "ForSurveyinMain"){
                    let testController = mainStoryboard.instantiateViewController(withIdentifier: "SurveyVC")as! SurveyVC
                    testController.strSurveyID = "\(dict.value(forKey: "body")!)"
                    self.navigationController?.pushViewController(testController, animated: true)
                }else if (strPushType == "PersonalAlert" || strPushType == "Report an Incident"){
                    showAlertWithoutAnyAction(strtitle: "\(dict.value(forKey: "title")!)", strMessage: "\(dict.value(forKey: "body")!)".html2String, viewcontrol: self)
                }
        
           
            updateData(strTitle: "\(dict.value(forKey: "title")!)", strBody: "\(dict.value(forKey: "body")!)", id: "\(dict.value(forKey: "id")!)")
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 50.0
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let customView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 50))
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 50))
        if(aryList.count != 0){
            ErrorMsg = ""
        }else{
            button.removeFromSuperview()
        }
        button.setTitle(ErrorMsg, for: .normal)
        customView.addSubview(button)
        customView.backgroundColor = UIColor.clear
        button.setTitleColor(UIColor.darkGray, for: .normal)
        return customView
    }
     func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let dict = aryList.object(at: indexPath.row)as! NSDictionary

            self.deleteData(strTitle: "\(dict.value(forKey: "title")!)", strBody: "\(dict.value(forKey: "body")!)", id: "\(dict.value(forKey: "id")!)", deletAll: false)
           retrieveData()
        }
    }
}
// MARK: - ----------------PushNotiCell
// MARK: -
class PushNotiCell: UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubtitle: UILabel!
    @IBOutlet weak var view: UIView!

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}
