//
//  VerificationInformPolice.swift
//  CitizenCop
//
//  Created by Akshay Hastekar on 05/06/19.
//  Copyright © 2019 Akshay Hastekar. All rights reserved.
//

import UIKit

class VerificationInformPolice: UIViewController
{

     // MARK: - -------- Outlet -----------
    
    @IBOutlet weak var btnDropDown: UIButton!
    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var viewHeader: UIView!

    
    // MARK: - -------- View 1 Outlet --------
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var const_View1_H: NSLayoutConstraint!
    
    @IBOutlet weak var txtFldNameView1: ACFloatingTextfield!
    @IBOutlet weak var txtFldFatherHusbandNameView1: ACFloatingTextfield!
    @IBOutlet weak var txtFldAddressView1: ACFloatingTextfield!
    @IBOutlet weak var txtFldContactNoView1: ACFloatingTextfield!
    @IBOutlet weak var txtFldEmailView1: ACFloatingTextfield!
    
    @IBOutlet weak var txtFldAadharCardNoView1: ACFloatingTextfield!
    
    @IBOutlet weak var txtFldAreaPoliceStationView1: ACFloatingTextfield!
    
    @IBOutlet weak var txtViewDescriptionView1: UITextView!
    @IBOutlet weak var btnMaleView1: UIButton!
    @IBOutlet weak var btnFemaleView1: UIButton!
    
    @IBOutlet weak var collectionView1: UICollectionView!
    @IBOutlet weak var addImageView1: UIButton!
    
    
    
    // MARK: - -------- View 2 Outlet --------
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var lblTitleForReason: UILabel!

    @IBOutlet weak var const_View2_H: NSLayoutConstraint!
    
    @IBOutlet weak var txtFldNameView2: ACFloatingTextfield!
    @IBOutlet weak var txtFldFatherHusbandNameView2: ACFloatingTextfield!
    @IBOutlet weak var txtFldAddressView2: ACFloatingTextfield!
    @IBOutlet weak var txtFldContactNoView2: ACFloatingTextfield!
    @IBOutlet weak var txtFldEmailView2: ACFloatingTextfield!
    @IBOutlet weak var txtFldAadharCardNoView2: ACFloatingTextfield!
    @IBOutlet weak var txtFldAreaPoliceStationView2: ACFloatingTextfield!
    @IBOutlet weak var txtViewDescriptionView2: UITextView!
    @IBOutlet weak var btnMaleView2: UIButton!
    @IBOutlet weak var btnFemaleView2: UIButton!
    @IBOutlet weak var collectionView2: UICollectionView!
    @IBOutlet weak var btnAddImageView2: UIButton!
    
    var strCatId = ""
    var strOtherDetail = Bool()
var arySendAllImages = NSMutableArray()
    //Mark: --------- Variabel Declaration ------------
    
    var arrCategory = NSMutableArray()
    var finalResponse = NSDictionary()
    var aryReportImageView1 = NSMutableArray()
    var aryReportImageView2 = NSMutableArray()
    var countAryReportImgs = 0
    var imagePicker = UIImagePickerController()
    var dictAudioData = NSMutableDictionary()
    var dictVideoData = NSMutableDictionary()
    var timer = Timer()
    var strAudioName = ""
    var strVideoName = ""
    var strImageName = ""
    var strGender1 = "Male"
    var strGender2 = "Female"
    
    // MARK: - -------- Life Cycle --------
      // MARK:
    override func viewDidLoad()
    {
        super.viewDidLoad()
        const_View2_H.constant = 0.0
        txtViewDescriptionView1.layer.cornerRadius = 5.0
        txtViewDescriptionView1.layer.borderWidth = 1.0
        txtViewDescriptionView1.layer.borderColor = UIColor.lightGray.cgColor
        
        txtViewDescriptionView2.layer.cornerRadius = 5.0
        txtViewDescriptionView2.layer.borderWidth = 1.0
        txtViewDescriptionView2.layer.borderColor = UIColor.lightGray.cgColor
        
        btnSubmit.layer.cornerRadius = 5.0
        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryTheamColor)
        btnSubmit.backgroundColor = hexStringToUIColor(hex: primaryDark)
        btnAddImageView2.backgroundColor = hexStringToUIColor(hex: primaryDark)
        addImageView1.backgroundColor = hexStringToUIColor(hex: primaryDark)
        btnAddImageView2.layer.cornerRadius = 8.0
        addImageView1.layer.cornerRadius = 8.0

        print(dictCityData)
        
        callApiForCategory()
    }

    // MARK: - -------- Action --------
    // MARK:
    @IBAction func actionOnDropDown(_ sender: Any)
    {
        self.arrCategory = NSMutableArray()
        
        if finalResponse.isKind(of: NSDictionary.self )
        {
            self.arrCategory = (finalResponse.value(forKey: "data") as! NSArray).mutableCopy()as! NSMutableArray
            
            if arrCategory.count != 0
            {
                self.PopViewWithArray(tag: 11, aryData: self.arrCategory, strTitle: "Select Category")
            }else{
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertDataNotFound, viewcontrol: self)
            }
        }
    }
    
    @IBAction func actionOnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionOnIHelp(_ sender: UIButton) {
        let testController = mainStoryboard.instantiateViewController(withIdentifier: "HelpVC")as! HelpVC
        testController.strMessage = alertVerificationInformPoliceHelp
        self.navigationController?.pushViewController(testController, animated: true)
    }
    
    
    @IBAction func actionOnSubmit(_ sender: Any) {
        
        self.view.endEditing(true)
        
        let strErrorMEssage = verifiyFields()
        if(strErrorMEssage.count != 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: strErrorMEssage, viewcontrol: self)
        }else{
            if(strOtherDetail){
                arySendAllImages = NSMutableArray()
                for item in aryReportImageView1{
                    arySendAllImages.add(item)
                }
                for item in aryReportImageView2{
                    arySendAllImages.add(item)
                }
                if(arySendAllImages.count != 0){
                    UploadImagesOnServer()
                }else{
                    self.CallAPIDataSendSenderReciverDataOnServer()
                }
                
                
            }else{
                arySendAllImages = NSMutableArray()
                for item in aryReportImageView1{
                    arySendAllImages.add(item)
                }
                if(arySendAllImages.count != 0){
                    UploadImagesOnServer()
                }else{
                    self.CallAPIDataSendSenderReciverDataOnServer()
                }
            }
        }
    
        
    }
    
    // MARK: - -------- Upload Images --------
    func UploadImagesOnServer()  {
        for item in self.arySendAllImages
        {
            let img = (item as AnyObject).value(forKey: "image") as! UIImage
            let imageData = img.jpegData(compressionQuality:0.5)
            
            WebServiceClass.callAPIWithImage(parameter: NSDictionary(), url: API_UploadReportImage, data: imageData!, fileName: "\((item as AnyObject).value(forKey: "imageName")!)", withName: "userfile") { (responce, status) in
                print(responce)
                if(status == "Suceess"){
                    if(self.arySendAllImages.count != 0){
                        self.arySendAllImages.removeObject(at: 0)
                        if(self.arySendAllImages.count != 0){
                            self.UploadImagesOnServer()

                        }else{
                           self.CallAPIDataSendSenderReciverDataOnServer()
                        }
                    }else{
                       self.CallAPIDataSendSenderReciverDataOnServer()
                    }
                  
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)

                }
            }
        }
    }
  

   func CallAPIDataSendSenderReciverDataOnServer()
    {
     
      let  strCityId = "\(((dictCityData.value(forKey: "CityData")as! NSArray).object(at: 0)as! NSDictionary).value(forKey: "city_id")!)"
     
        let strEmeiNo = PushToken
        let strMobileSendDate = getCurrentDate_Time(strType: "Date")
        let strName1 = removeSpace(str: txtFldNameView1.text!)
        let strFatherHusbandName1 = removeSpace(str: txtFldFatherHusbandNameView1.text!)
        let strAddress1 = removeSpace(str: txtFldAddressView1.text!)
        let strContactNumber1 =  txtFldContactNoView1.text!
        let strEmailId1 = txtFldEmailView1.text!
        let strAadharNumber1 = txtFldAddressView1.text!
        let strDescription1 = txtViewDescriptionView1.text.count > 0 ? removeSpace(str: txtViewDescriptionView1.text!) : ""
        let strPoliceArea1 = removeSpace(str: txtFldAreaPoliceStationView1.text!)
       
        var S_Image1 = ""
        var S_Image2 = ""
        var S_Image3 = ""
        
        if(aryReportImageView1.count > 0)
        {
            for (index,item) in aryReportImageView2.enumerated()
            {
                print(index)
               if(aryReportImageView1.contains(item))
               {
                  aryReportImageView1.remove(item)
               }
                
            }
            
            if(aryReportImageView1.count == 1)
            {
              S_Image1 = (aryReportImageView1.object(at: 0) as! NSDictionary).value(forKey: "imageName") as! String
            }
            else if(aryReportImageView1.count == 2)
            {
                S_Image1 = (aryReportImageView1.object(at: 0) as! NSDictionary).value(forKey: "imageName") as! String
              
                S_Image2 = (aryReportImageView1.object(at: 1) as! NSDictionary).value(forKey: "imageName") as! String
            }
            else
            {
                S_Image1 = (aryReportImageView1.object(at: 0) as! NSDictionary).value(forKey: "imageName") as! String
                
                S_Image2 = (aryReportImageView1.object(at: 1) as! NSDictionary).value(forKey: "imageName") as! String
               
                S_Image3 = (aryReportImageView1.object(at: 2) as! NSDictionary).value(forKey: "imageName") as! String
            }
        }
        
        
        let strName2 = removeSpace(str: txtFldNameView1.text!)
        let strFatherHusbandName2 = removeSpace(str: txtFldFatherHusbandNameView2.text!)
        let strAddress2 = removeSpace(str: txtFldAddressView2.text!)
        let strContactNumber2 = txtFldContactNoView2.text!
        let strEmailId2 = txtFldEmailView2.text!
        let strAadharNumber2 = txtFldAddressView2.text!
        let strDescription2 = txtViewDescriptionView2.text.count > 0 ? removeSpace(str: txtViewDescriptionView2.text!) : ""
        let strPoliceArea2 = removeSpace(str: txtFldAreaPoliceStationView2.text!)
        
        var S_Image21 = ""
        var S_Image22 = ""
        var S_Image23 = ""
        
        if(aryReportImageView2.count > 0)
        {
            if(aryReportImageView2.count == 1)
            {
                S_Image21 = (aryReportImageView2.object(at: 0) as! NSDictionary).value(forKey: "imageName") as! String
            }
            else if(aryReportImageView1.count == 2)
            {
                S_Image21 = (aryReportImageView2.object(at: 0) as! NSDictionary).value(forKey: "imageName") as! String
                
                S_Image22 = (aryReportImageView2.object(at: 1) as! NSDictionary).value(forKey: "imageName") as! String
            }
            else
            {
                S_Image21 = (aryReportImageView2.object(at: 0) as! NSDictionary).value(forKey: "imageName") as! String
                
                S_Image22 = (aryReportImageView2.object(at: 1) as! NSDictionary).value(forKey: "imageName") as! String
                
                S_Image23 = (aryReportImageView2.object(at: 2) as! NSDictionary).value(forKey: "imageName") as! String
            }
        }
        
        
        if !isInternetAvailable() {
            FTIndicator.showToastMessage(alertInternet)
        }else{

            let StrURL = API_SubmitInformationData + "cityid\(strCityId)&S_ImeiNo=\(strEmeiNo)&CategoryId=\(strCatId)&MobileSendDate=\(strMobileSendDate)&S_Name=\(strName1)&S_Gender=\(strGender1)&S_F_H_Name=\(strFatherHusbandName1)&S_Address=\(strAddress1)&S_ContactNo=\(strContactNumber1)&S_emailID=\(strEmailId1)&S_Aadhar=\(strAadharNumber1)&S_Description=\(strDescription1)&S_Image1=\(S_Image1)&S_Image2=\(S_Image2)&S_Image3=\(S_Image3)&E_Name=\(strName2)&E_Gender=\(strGender2)&E_F_H_Name=\(strFatherHusbandName2)&E_Address=\(strAddress2)&E_ContactNo=\(strContactNumber2)&E_emailID=\(strEmailId2)&E_Aadhar=\(strAadharNumber2)&E_Description=\(strDescription2)&E_Image1=\(S_Image21)&E_Image2=\(S_Image22)&E_Image3=\(S_Image23)&SenderPS=\(strPoliceArea1)&RecieverPS=\(strPoliceArea2)"
            WebServiceClass.callAPIBYPOST(parameter: NSDictionary(), url: StrURL) { (responce, status) in
                print(responce)
                if status == "Suceess"{
                    let dictResponceData = (responce.value(forKey: "data")as! NSDictionary)
                    let strResult = "\(dictResponceData.value(forKey: "Result")!)"
                    if(strResult == "Ok"){
                        
                        FTIndicator.showNotification(withTitle: alertMessage, message: alert_DataSubmite)
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                    }
                }
                else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            }
        }
    }
    
    
    // MARK: - -------- Verification --------
    // MARK:
    func verifiyFields()-> String
    {
        
        if(strCatId == "")
        {
           return "Please select category."
        }
        // view1 validation
        if(txtFldNameView1.text?.count == 0)
        {
          return "Please enter name."
        }
        if(txtFldFatherHusbandNameView1.text?.count == 0)
        {
            return "Please enter father's/husband's name."
        }
        if(txtFldAddressView1.text?.count == 0)
        {
            return "Please enter address."
        }
        if(txtFldContactNoView1.text?.count == 0)
        {
            return "Please enter contact number."
        }
        if(txtFldContactNoView1.text!.count < 10)
        {
           return "Contact number should be 10 digit."
        }
        if(txtFldEmailView1.text?.count != 0)
        {
            if(validateEmail(email: txtFldEmailView1.text!) == false)
            {
                return "Please enter valid email."
            }
        }
       
        if(txtFldAadharCardNoView1.text?.count != 0)
        {
            if(txtFldAadharCardNoView1.text?.count != 12)
            {
                return "Please enter valid aadhar card number."
            }
        }
      
        if(txtFldAreaPoliceStationView1.text?.count == 0)
        {
            return "Please enter area of police station."
        }
        if(strOtherDetail){
            // view2 validation
            if(txtFldNameView2.text?.count == 0)
            {
                return "Please enter other detail name."
            }
            if(txtFldFatherHusbandNameView2.text?.count == 0)
            {
                return "Please enter other detail father's/husband's name."
            }
            if(txtFldAddressView2.text?.count == 0)
            {
                return "Please enter other detail address."
            }
            if(txtFldContactNoView2.text?.count == 0)
            {
                return "Please enter other detail contact number."
            }
            if(txtFldContactNoView2.text!.count < 10)
            {
                return "Other detail contact number should be 10 digit."
            }
            if(txtFldEmailView2.text?.count != 0)
            {
                if(validateEmail(email: txtFldEmailView2.text!) == false)
                {
                    return "Please enter valid other detail email."
                }
            }
           
            if(txtFldAadharCardNoView2.text?.count != 0)
            {
                if(txtFldAadharCardNoView2.text?.count != 12)
                {
                    return "Please enter other detail valid aadhar card number."
                }
            }
           
            if(txtFldAreaPoliceStationView2.text?.count == 0)
            {
                return "Please enter other detail area of police station."
            }
            
        }
       
        return ""
    }
    
    // MARK: - -------- View 1 Action --------
    
    @IBAction func actionOnMaleView1(_ sender: Any)
    {
        btnMaleView1.setBackgroundImage(UIImage(named: "RadioButton-Selected"), for: .normal)
        btnFemaleView1.setBackgroundImage(UIImage(named: "RadioButton-Unselected"), for: .normal)
        strGender1 = "Male"
        
    }
    
    @IBAction func actionOnFemaleView1(_ sender: Any) {
        
        btnFemaleView1.setBackgroundImage(UIImage(named: "RadioButton-Selected"), for: .normal)
        btnMaleView1.setBackgroundImage(UIImage(named: "RadioButton-Unselected"), for: .normal)
        strGender1 = "Female"
    }
    
    @IBAction func actionOnAddImageView1(_ sender: Any)
    {
        if(aryReportImageView1.count == 3)
        {
            showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: "You can take only three pictures", viewcontrol: self)
        }
        else
        {
            let alert = UIAlertController(title: "", message: "Please Select an Option", preferredStyle: .actionSheet)
            
            alert.addAction(UIAlertAction(title: "Capture Image", style: .default , handler:
                { (UIAlertAction)in
                    
                    // Camera
                    
                    if(self.aryReportImageView1.count >= 5){
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertReachedMax, viewcontrol: self)
                        
                    }
                    else
                    {
                        self.imagePicker = UIImagePickerController()
                        self.imagePicker.view.tag = 1
                        
                        if UIImagePickerController.isSourceTypeAvailable(.camera)
                        {
                            print("Button capture")
                            
                            self.imagePicker.delegate = self
                            self.imagePicker.sourceType = .camera
                            self.imagePicker.allowsEditing = false
                            self.present(self.imagePicker, animated: true, completion: nil)
                        }
                        else
                        {
                            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "No device available", viewcontrol: self)
                            
                        }
                        
                    }
                    
                    
                    
            }))
            
            
            alert.addAction(UIAlertAction(title: "Gallery", style: .default , handler:{ (UIAlertAction)in
                
                if(self.aryReportImageView1.count >= 5){
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertReachedMax, viewcontrol: self)
                }
                else
                {
                    self.imagePicker = UIImagePickerController()
                    self.imagePicker.view.tag = 1
                    if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum)
                    {
                        print("Button capture")
                        self.imagePicker.delegate = self
                        self.imagePicker.sourceType = .savedPhotosAlbum
                        self.imagePicker.allowsEditing = false
                        self.present(self.imagePicker, animated: true, completion: nil)
                    }
                }
                
            }))
            
            alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
            }))
        
            alert.popoverPresentationController?.sourceView = self.view
         
            if(DeviceType.IS_IPAD)
            {
                alert.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
                alert.popoverPresentationController?.permittedArrowDirections = []
            }
            
            self.present(alert, animated: true, completion: {
            })
        }
    }
   
    
    // MARK: - -------- View 2 Action --------
    
    @IBAction func actionOnMaleView2(_ sender: Any)
    {
          btnMaleView2.setBackgroundImage(UIImage(named: "RadioButton-Selected"), for: .normal)
          btnFemaleView2.setBackgroundImage(UIImage(named: "RadioButton-Unselected"), for: .normal)
        strGender2 = "Male"
    }
    
    @IBAction func actionOnFemaleView2(_ sender: Any)
    {
         btnFemaleView2.setBackgroundImage(UIImage(named: "RadioButton-Selected"), for: .normal)
         btnMaleView2.setBackgroundImage(UIImage(named: "RadioButton-Unselected"), for: .normal)
        strGender2 = "Female"
    }
    
    @IBAction func actionOnAddImageView2(_ sender: Any)
    {
        
        if(aryReportImageView1.count == 3)
        {
            showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: "You can take only three pictures", viewcontrol: self)
        }
        
        else
        {
            let alert = UIAlertController(title: "", message: "Please Select an Option", preferredStyle: .actionSheet)
            
            alert.addAction(UIAlertAction(title: "Capture Image", style: .default , handler:
                { (UIAlertAction)in
                    
                    // Camera
                    
                    if(self.aryReportImageView2.count >= 5){
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertReachedMax, viewcontrol: self)
                        
                    }
                    else
                    {
                        self.imagePicker = UIImagePickerController()
                        self.imagePicker.view.tag = 2
                        
                        if UIImagePickerController.isSourceTypeAvailable(.camera)
                        {
                            print("Button capture")
                            
                            self.imagePicker.delegate = self
                            self.imagePicker.sourceType = .camera
                            self.imagePicker.allowsEditing = false
                            self.present(self.imagePicker, animated: true, completion: nil)
                        }
                        else
                        {
                            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "No device available", viewcontrol: self)
                            
                        }
                    }
                    
            }))
            
            
            alert.addAction(UIAlertAction(title: "Gallery", style: .default , handler:{ (UIAlertAction)in
                
                if(self.aryReportImageView2.count >= 5){
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertReachedMax, viewcontrol: self)
                }else{
                    self.imagePicker = UIImagePickerController()
                    self.imagePicker.view.tag = 2
                    if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
                        print("Button capture")
                        self.imagePicker.delegate = self
                        self.imagePicker.sourceType = .savedPhotosAlbum
                        self.imagePicker.allowsEditing = false
                        self.present(self.imagePicker, animated: true, completion: nil)
                    }
                }
                
            }))
            
            alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
            }))
            
            alert.popoverPresentationController?.sourceView = self.view
            
            if(DeviceType.IS_IPAD)
            {
                alert.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
                alert.popoverPresentationController?.permittedArrowDirections = []
            }
            
            self.present(alert, animated: true, completion: {
            })
        }
       
    }
    // MARK: - -------- Other Functions --------
    
    
    func callApiForCategory() -> Void
    {

        if !isInternetAvailable() {
            FTIndicator.showToastMessage(alertInternet)
        }
        else
        {
            let strCityId = "\(((dictCityData.value(forKey: "CityData")as! NSArray).object(at: 0)as! NSDictionary).value(forKey: "city_id")!)"
            let strUrl = "\(API_SubmitInformationCategory)cityid=\(strCityId)"
            let dict = NSDictionary()
            customDotLoaderShowOnFull(message: "", controller: self)
            WebServiceClass.callAPIBYPOST(parameter: dict, url: strUrl)
            { (response, strStatus) in
                customeDotLoaderRemove()
                if (strStatus == "Suceess")
                {
                     print(response)
                    self.finalResponse = response
                    self.arrCategory = NSMutableArray()
                    self.arrCategory = (response.value(forKey: "data") as! NSArray).mutableCopy()as! NSMutableArray
                }
                else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            }
        }
    }
    
    func PopViewWithArray(tag: Int , aryData : NSMutableArray , strTitle: String)  {
        let vc: SelectionClass = self.storyboard!.instantiateViewController(withIdentifier: "SelectionClass") as! SelectionClass
        vc.strTitle = strTitle
        vc.strTag = tag
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .coverVertical
        vc.delegate = self
        vc.aryList = aryData
        self.present(vc, animated: true, completion: {})
    }
    
}
//MARK:- ---------PopUpDelegate

extension VerificationInformPolice : PopUpDelegate
{
    func getDataFromPopupDelegate(dictData: NSDictionary, tag: Int)
    {
            self.btnDropDown.setTitle("\(dictData.value(forKey: "CategoryName")!)", for: .normal)
            strCatId = "\(dictData.value(forKey: "SubmitCategoryId")!)"
        
        if("\(dictData.value(forKey: "OtherDetail")!)" == "Yes"){
               strOtherDetail = true
            self.lblTitleForReason.text = "Other Details:"
           const_View2_H.constant = 900
        }else{
             strOtherDetail = false
              const_View2_H.constant = 0
        }
      
    }
}

// MARK: - ----------------UIImagePickerControllerDelegate
// MARK: -
extension VerificationInformPolice : UINavigationControllerDelegate, UIImagePickerControllerDelegate
{
    
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
       
        self.dismiss(animated: true, completion: { () -> Void in
            if(self.imagePicker.view.tag == 1)
            { // For Gallery Camera
                let dictData = NSMutableDictionary()
                dictData.setValue(info[UIImagePickerController.InfoKey.originalImage] as? UIImage, forKey: "image")
                dictData.setValue(uniqueString() + ".jpg", forKey: "imageName")
                self.aryReportImageView1.add(dictData)
                self.collectionView1.reloadData()
            }
            else if(self.imagePicker.view.tag == 2)
            {
                // For Gallery Camera
                let dictData = NSMutableDictionary()
                dictData.setValue(info[UIImagePickerController.InfoKey.originalImage] as? UIImage, forKey: "image")
                dictData.setValue(uniqueString() + ".jpg", forKey: "imageName")
                self.aryReportImageView2.add(dictData)
                self.collectionView2.reloadData()
            }
        })
        
       
    }
}
// MARK: - ---------------- UICollectionViewDelegate --------------
// MARK: -

extension VerificationInformPolice : UICollectionViewDelegate ,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout , UIScrollViewDelegate{
    
    private func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int
    {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        
        
        /*DeviceType.IS_IPAD ? (self.aryReportImage.count == 0 ? (self.heightCollection.constant = 0.0) :  (self.heightCollection.constant = 125.0)) : self.aryReportImage.count == 0 ? (self.heightCollection.constant = 0.0) :  (self.heightCollection.constant = 95.0)*/
        
        if (collectionView == collectionView1)
        {
            return self.aryReportImageView1.count

        }
        else
        {
            return self.aryReportImageView2.count

        }
        
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        cell.alpha = 0.4
        cell.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        UIView.animate(withDuration: 0.5) {
            cell.alpha = 1
            cell.transform = .identity
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        if (collectionView == collectionView1)
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ReportImageCell", for: indexPath as IndexPath) as! ReportImageCell
            let dict = aryReportImageView1.object(at: indexPath.row)as! NSDictionary
            cell.ReportCell_Image.image = (dict.value(forKey: "image")as! UIImage)
            cell.ReportCell_Image.layer.borderWidth = 1
            cell.ReportCell_Image.layer.masksToBounds = false
            cell.ReportCell_Image.layer.borderColor = UIColor.groupTableViewBackground.cgColor
            cell.ReportCell_Image.layer.cornerRadius = 8.0
            cell.ReportCell_Image.clipsToBounds = true
            return cell
        }
        else
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ReportImageCell", for: indexPath as IndexPath) as! ReportImageCell
            let dict = aryReportImageView2.object(at: indexPath.row)as! NSDictionary
            cell.ReportCell_Image.image = (dict.value(forKey: "image")as! UIImage)
            cell.ReportCell_Image.layer.borderWidth = 1
            cell.ReportCell_Image.layer.masksToBounds = false
            cell.ReportCell_Image.layer.borderColor = UIColor.groupTableViewBackground.cgColor
            cell.ReportCell_Image.layer.cornerRadius = 8.0
            cell.ReportCell_Image.clipsToBounds = true
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return   DeviceType.IS_IPAD ? CGSize(width: 120, height:120) : CGSize(width: 90, height:90)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        // let dict = aryReportImage.object(at: indexPath.row)as! NSDictionary
        if(collectionView == collectionView1)
        {
            let alert = UIAlertController(title: "Alert", message: "Please Select an Option", preferredStyle: .actionSheet)
            
            alert.addAction(UIAlertAction(title: "View on full screen", style: .default , handler:{ (UIAlertAction)in
                let testController = mainStoryboard.instantiateViewController(withIdentifier: "PreviewImageVC")as! PreviewImageVC
                 let dict = self.aryReportImageView1.object(at: indexPath.row)as! NSDictionary
                testController.img = (dict.value(forKey: "image")as! UIImage)
                self.navigationController?.pushViewController(testController, animated: true)

            }))
            
            
            alert.addAction(UIAlertAction(title: "Delete", style: .destructive , handler:{ (UIAlertAction)in
                self.aryReportImageView1.removeObject(at: indexPath.row)
                self.collectionView1.reloadData()
            }))
            
            alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
            }))
            //alert.popoverPresentationController?.sourceRect = sender.frame
            alert.popoverPresentationController?.sourceView = self.view
            if(DeviceType.IS_IPAD)
            {
                alert.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
                alert.popoverPresentationController?.permittedArrowDirections = []
            }
            self.present(alert, animated: true, completion: {
            })
        }
        else
        {
            let alert = UIAlertController(title: "Alert", message: "Please Select an Option", preferredStyle: .actionSheet)
            
            alert.addAction(UIAlertAction(title: "View on full screen", style: .default , handler:{ (UIAlertAction)in
             
                
                let testController = mainStoryboard.instantiateViewController(withIdentifier: "PreviewImageVC")as! PreviewImageVC

                let dict = self.aryReportImageView2.object(at: indexPath.row)as! NSDictionary
                testController.img = (dict.value(forKey: "image")as! UIImage)
                self.navigationController?.pushViewController(testController, animated: true)

            }))
            
            
            alert.addAction(UIAlertAction(title: "Delete", style: .destructive , handler:{ (UIAlertAction)in
                self.aryReportImageView2.removeObject(at: indexPath.row)
                self.collectionView2.reloadData()
            }))
            
            alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
            }))
            //alert.popoverPresentationController?.sourceRect = sender.frame
            alert.popoverPresentationController?.sourceView = self.view
            if(DeviceType.IS_IPAD)
            {
                alert.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
                alert.popoverPresentationController?.permittedArrowDirections = []
            }
            self.present(alert, animated: true, completion: {
            })
        }
        
        
    }
    
 
}

extension VerificationInformPolice : UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        
        if(textField == txtFldNameView1 || textField == txtFldNameView2 || textField == txtFldFatherHusbandNameView1 || textField == txtFldFatherHusbandNameView2 || textField == txtFldEmailView1 || textField == txtFldEmailView2){
            return txtFiledValidation(textField: textField, string: string, returnOnly: "ALL", limitValue: 85)
        }
        if(textField == txtFldAddressView1 || textField == txtFldAddressView2 || textField == txtFldAreaPoliceStationView1 || textField == txtFldAreaPoliceStationView2){
            return txtFiledValidation(textField: textField, string: string, returnOnly: "ALL", limitValue: 200)
        }
        if(textField == txtFldContactNoView1 || textField == txtFldContactNoView2){
            return txtFiledValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 9)
        }
        if(textField == txtFldAadharCardNoView1 || textField == txtFldAadharCardNoView2){
            return txtFiledValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 11)
        }
       
        
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
extension VerificationInformPolice : UITextViewDelegate
{
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        if(text == " ")
        {
            if(txtViewDescriptionView1.text?.count == 0 || txtViewDescriptionView2.text?.count == 0)
            {
                return false
            }
        }
        
        return true
    }
    
}




//class ReportImageCell: UICollectionViewCell {
//    //DashBoard
//    @IBOutlet weak var ReportCell_Image: UIImageView!
//
//}
