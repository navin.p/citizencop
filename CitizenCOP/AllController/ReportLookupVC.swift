//
//  ReportLookupVC.swift
//  CitizenCOP
//
//  Created by Saavan Patidar on 28/05/19.
//  Copyright © 2019 CitizenCop. All rights reserved.
//

import UIKit
import SWXMLHash
import StringExtensionHTML
import AEXML

class ReportLookupVC: UIViewController {

    // MARK: - --------------IBOutlet
    // MARK: -
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var txtFld_CompalintNo: UITextField!
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var tvlist: UITableView!

    
    // MARK: - --------------Variables
    // MARK: -
    var aryListComment = NSMutableArray()
    var aryListStatus = NSMutableArray()
    var strComplainNumber = ""
    
    // MARK: - --------------View Life Cycle
    // MARK: -
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryTheamColor)
        btnSearch.layer.cornerRadius = 5.0
        btnSearch.backgroundColor = hexStringToUIColor(hex: primaryDark)
        tvlist.tableFooterView = UIView()
        tvlist.estimatedRowHeight = 80.0
        tvlist.isHidden = true
        txtFld_CompalintNo.text = strComplainNumber
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
            if(self.strComplainNumber.count != 0){
                let strCityId = "\(((dictCityData.value(forKey: "CityData")as! NSArray).object(at: 0)as! NSDictionary).value(forKey: "city_id")!)"
                self.callReportLookUpAPI(strCityID: strCityId)
            }
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
      
    }
    // MARK: - --------------Actions
    // MARK: -
    
    @IBAction func action_Back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func action_Help(_ sender: Any) {
        self.view.endEditing(true)
        let testController = mainStoryboard.instantiateViewController(withIdentifier: "HelpVC")as! HelpVC
        testController.strMessage = alertReportLookUpHelp
        self.navigationController?.pushViewController(testController, animated: true)
    }
    
    @IBAction func action_Search(_ sender: Any) {
        self.view.endEditing(true)
        if !isInternetAvailable() {
            FTIndicator.showToastMessage(alertInternet)
        }else{
            if(txtFld_CompalintNo.text?.count == 0){
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_ComplaintNo, viewcontrol: self)
            }else{
                if(dictCityData.value(forKey: "CityData")as! NSArray).count != 0{
                    let strCityId = "\(((dictCityData.value(forKey: "CityData")as! NSArray).object(at: 0)as! NSDictionary).value(forKey: "city_id")!)"
                    callReportLookUpAPI(strCityID: strCityId)
                }
            }
        }
    }
    
    // MARK: - --------------API Calling
    // MARK: -
    
    func callReportLookUpAPI(strCityID : String) {
        if !isInternetAvailable() {
            FTIndicator.showToastMessage(alertInternet)
        }else{
            tvlist.isHidden = true
            let strComplaintNo = "&ComplaintNo=\(txtFld_CompalintNo.text!)"
            let strImeiNo = "&imeiNo=\(DeviceID)"
            customDotLoaderShowOnButton(btn: btnSearch, view: self.view, controller: self)
            WebServiceClass.callAPIBYGET_XML(parameter: NSDictionary(), url: API_ReportLookUp + strCityID + strComplaintNo + strImeiNo) { (responce, status) in
                customeDotLoaderRemove()
                //self.refresher.endRefreshing()
                if status == "Suceess"{
                    self.aryListComment = NSMutableArray()
                    self.aryListStatus = NSMutableArray()
                    let xmlData = (responce.value(forKey: "data")as! Data)
                    let xml1 = XMLHash.parse(xmlData)
                    var statusResponse = String()
                    for elem in xml1["NewDataSet"]["Table4"].all {
                        statusResponse = "\(elem["Success"].element!.text)"
                    }
                    if statusResponse == "Ok" {
                        self.tvlist.isHidden = false
                        for elem in xml1["NewDataSet"]["Table1"].all {
                            let dict = NSMutableDictionary()
                            dict.setValue("\(elem["Comment"].element!.text)", forKey: "Comment")
                            dict.setValue("\(elem["CommentDateTime"].element!.text)", forKey: "CommentDateTime")
                            self.aryListComment.add(dict)
                        }
                        for elem in xml1["NewDataSet"]["Table2"].all {
                            
                            let dict = NSMutableDictionary()
                            dict.setValue("\(elem["status"].element!.text)", forKey: "status")
                            dict.setValue("\(elem["SubStatus"].element!.text)", forKey: "SubStatus")
                            
                            self.aryListStatus.add(dict)
                        }
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertComplaintNoNotFound, viewcontrol: self)
                    }
                    self.tvlist.reloadData()
                }
                else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertComplaintNoNotFound, viewcontrol: self)
                }
            }
        }
    }
}


// MARK: - ----------------UITableViewDelegate
// MARK: -

extension ReportLookupVC: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 2
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0 {
            
            return aryListStatus.count
            
        } else {
            
            return aryListComment.count
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        if section == 0 {
            
            return "Status : "
            
        } else {
            
            return "Comments : "
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tvlist.dequeueReusableCell(withIdentifier: "ReportLookUpCell", for: indexPath as IndexPath) as! ReportLookUpCell

        if indexPath.section == 0 {
            
            let dict = aryListStatus.object(at: indexPath.row)as! NSDictionary
            
            let status = "\(dict.value(forKey: "status")!)"
            
            cell.lblTitle.text = "Status : \(status)"
            
            if status == "New" {
                
                cell.lblDetail.text = alert_ComplaintNew
                
            }else {
                
                cell.lblDetail.text = "Sub Status : \(dict.value(forKey: "SubStatus")!)"

                
            }
            
            
        } else {
            
            let dict = aryListComment.object(at: indexPath.row)as! NSDictionary
            cell.lblTitle.text = "Comment : \(dict.value(forKey: "Comment")!)"
            cell.lblDetail.text = "Date & Time : \(dict.value(forKey: "CommentDateTime")!)"
            
        }

            return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
        
}

// MARK: - ----------------ReportLookUpCell
// MARK: -
class ReportLookUpCell: UITableViewCell {
    //ReportLookUpCell
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDetail: UILabel!
    
    var attrs = [
        NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14.0),
        NSAttributedString.Key.foregroundColor : hexStringToUIColor(hex: "287AFF"),
        NSAttributedString.Key.underlineStyle : 1] as [NSAttributedString.Key : Any] as [NSAttributedString.Key : Any]
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}
//MARK:-
//MARK:- ---------UITextFieldDelegate

extension ReportLookupVC : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if ( textField == txtFld_CompalintNo)
        {
            return txtFiledValidation(textField: textField, string: string, returnOnly: "ALL", limitValue: 45)
        }
            
        else
        {
            return true
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
}
