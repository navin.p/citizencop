//
//  TowedVehicleSearchDetailVC.swift
//  CitizenCOP
//
//  Created by Navin Patidar on 6/26/19.
//  Copyright © 2019 CitizenCop. All rights reserved.
//

import UIKit

class TowedVehicleSearchDetailVC: UIViewController {
    // MARK: - --------------IBOutlet
    // MARK: -
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var btnMap1: UIButton!
    @IBOutlet weak var btnMap2: UIButton!

    @IBOutlet weak var lblV_Number: UILabel!
    @IBOutlet weak var lblV_TowingDate: UILabel!
    @IBOutlet weak var lblV_TowingDes: KMPlaceholderTextView!
    @IBOutlet weak var imgView: UIImageView!


    var dictSearchDetail = NSMutableDictionary()
    
    // MARK: - -------------LifeCycle
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryTheamColor)
        btnMap1.layer.cornerRadius = 8.0
        btnMap1.backgroundColor = hexStringToUIColor(hex: primaryDark)
        btnMap2.layer.cornerRadius = 8.0
        btnMap2.backgroundColor = hexStringToUIColor(hex: primaryDark)
        lblV_Number.text = "\(dictSearchDetail.value(forKey: "VehicleNo")!)"
        lblV_TowingDate.text = "Vehicle Towing Date : \(dictSearchDetail.value(forKey: "SendDate")!)"
        lblV_TowingDes.text = "\(dictSearchDetail.value(forKey: "description")!)"
        let strImageName = "\(dictSearchDetail.value(forKey: "vehicleImage")!)"
       var aryImg = strImageName.components(separatedBy: ",")
        if(aryImg.count != 0){
            let urlImage = "\(BaseHostURL)/CrainVehicleImages/\(aryImg[0])"
            self.imgView.setImageWith(URL(string: urlImage), placeholderImage: UIImage(named: "NoImage"), options: SDWebImageOptions(rawValue: 1), completed: { (image, error, type, url) in
                print(url ?? 0)
            }, usingActivityIndicatorStyle: .gray)
        }
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
         self.imgView.addGestureRecognizer(tap)
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        let testController = mainStoryboard.instantiateViewController(withIdentifier: "PreviewImageVC")as! PreviewImageVC
        testController.img = imgView.image!
        self.navigationController?.pushViewController(testController, animated: true)
    }
    // MARK: - ---------------IBAction
    // MARK: -
    @IBAction func actiononBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
   
    @IBAction func actiononMAP(_ sender: UIButton) {
        if(sender.tag == 1){
           openMap(strTitle: "Picup Location", GlobleLat: "\(dictSearchDetail.value(forKey: "PicLatitude")!)", GlobleLong: "\(dictSearchDetail.value(forKey: "PicLongtitude")!)")
        }else {
            openMap(strTitle: "Drop Location", GlobleLat: "\(dictSearchDetail.value(forKey: "DropLatitude")!)", GlobleLong: "\(dictSearchDetail.value(forKey: "DropLongtitude")!)")
        }
    }
}

