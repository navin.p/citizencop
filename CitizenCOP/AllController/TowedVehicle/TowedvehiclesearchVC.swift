//
//  TowedvehiclesearchVC.swift
//  CitizenCOP
//
//  Created by Navin Patidar on 6/25/19.
//  Copyright © 2019 CitizenCop. All rights reserved.
//

import UIKit
import SWXMLHash
import StringExtensionHTML
import AEXML

class TowedvehiclesearchVC: UIViewController {
    
    
    // MARK: - --------------IBOutlet
    // MARK: -
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var picker: UIDatePicker!
    @IBOutlet weak var txtNumber: ACFloatingTextfield!

    var aryList = NSMutableArray()
    
    // MARK: - -------------LifeCycle
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 13.4, *) {
            picker.preferredDatePickerStyle = .wheels
        } else {
            // Fallback on earlier versions
        }
        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryTheamColor)
        btnSearch.layer.cornerRadius = 10.0
        btnSearch.backgroundColor = hexStringToUIColor(hex: primaryDark)
        lblMessage.text = alertTowedvehicle
      
    }
    
    // MARK: - ---------------IBAction
    // MARK: -
    @IBAction func actiononBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actiononHelp(_ sender: UIButton) {
        let testController = mainStoryboard.instantiateViewController(withIdentifier: "HelpVC")as! HelpVC
        testController.strMessage = alertTowedvehicle
        self.navigationController?.pushViewController(testController, animated: true)
    }
 
    @IBAction func actionOnSearch(_ sender: UIButton) {
        self.view.endEditing(true)

        if(txtNumber.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_Vechiclenumber, viewcontrol: self)
        }else{
            callSearchAPI()
        }
    }
    // MARK: - ---------------API CAlling
    // MARK: -
    func callSearchAPI() {
        if !isInternetAvailable() {
            FTIndicator.showToastMessage(alertInternet)
        }else{
            customDotLoaderShowOnButton(btn: btnSearch, view: self.view, controller: self)
          
            let strVehicleNo = "&VehicleNo="
            let strVehicleNoValue = "\(txtNumber.text!)"
            let strSendDate = "&SendDate="
            let dateFormatter : DateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd/MM/yyyy"
            let strSendDateValue = dateFormatter.string(from: picker.date)
            let strkey = "&key="
            let strkeyValue = "searchtoingvehicle"
          
            let URL = API_CrainVehicle + strVehicleNo + strVehicleNoValue + strSendDate  + strSendDateValue + strkey + strkeyValue
            WebServiceClass.callAPIBYPost_XML(parameter: NSDictionary(), url: URL) { (responce, status) in
                customeDotLoaderRemove()
                if status == "Suceess"{
                    self.aryList = NSMutableArray()
                    let xmlData = (responce.value(forKey: "data")as! Data)
                    let xml1 = XMLHash.parse(xmlData)
                    for elem in xml1["NewDataSet"]["Table1"].all {
                        let dict = NSMutableDictionary()
                        dict.setValue("\(elem["CVT_Id"].element!.text)", forKey: "CVT_Id")
                        dict.setValue("\(elem["description"].element!.text)", forKey: "description")
                        dict.setValue("\(elem["SendDate"].element!.text)", forKey: "SendDate")
                        dict.setValue("\(elem["PicLatitude"].element!.text)", forKey: "PicLatitude")
                        dict.setValue("\(elem["PicLongtitude"].element!.text)", forKey: "PicLongtitude")
                        
                        dict.setValue("\(elem["vehicleImage"].element!.text)", forKey: "vehicleImage")
                        dict.setValue("\(elem["VehicleNo"].element!.text)", forKey: "VehicleNo")
                        
                        dict.setValue("\(elem["username"].element!.text)", forKey: "username")
                        dict.setValue("\(elem["Status"].element!.text)", forKey: "Status")
                        dict.setValue("\(elem["DropLatitude"].element!.text)", forKey: "DropLatitude")
                        dict.setValue("\(elem["DropLongtitude"].element!.text)", forKey: "DropLongtitude")
                        dict.setValue("\(elem["city_id"].element!.text)", forKey: "city_id")
                        dict.setValue("\(elem["ContactNo"].element!.text)", forKey: "ContactNo")
                        self.aryList.add(dict)
                    }
                    if(self.aryList.count == 0){
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertDataNotFound, viewcontrol: self)
                    }else{
                        let testController = mainStoryboard.instantiateViewController(withIdentifier: "TowedVehicleSearchDetailVC")as! TowedVehicleSearchDetailVC
                        let dict = self.aryList.object(at: 0)as! NSDictionary
                        testController.dictSearchDetail = dict.mutableCopy()as! NSMutableDictionary
                        self.navigationController?.pushViewController(testController, animated: true)
                    }
                }
                else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            }
            
            
        }
    }
}
//MARK:-
//MARK:- ---------UITextFieldDelegate

extension TowedvehiclesearchVC : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if ( textField == txtNumber)
        {
            return txtFiledValidation(textField: textField, string: string, returnOnly: "ALL", limitValue: 45)
        }
            
        else
        {
            return true
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
}
