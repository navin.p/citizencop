//
//  CitySelectionVC.swift
//  CitizenCOP
//
//  Created by Navin Patidar on 5/17/19.
//  Copyright © 2019 CitizenCop. All rights reserved.
//

import UIKit
import SafariServices

class CitySelectionVC: UIViewController {
    
    // MARK: - ----IBOutlet
    
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var viewContain: UIView!
    @IBOutlet weak var scrollView: UIScrollView!

    @IBOutlet weak var btnBack: UIButton!

    @IBOutlet weak var txtCountry: ACFloatingTextfield!
    @IBOutlet weak var txtState: ACFloatingTextfield!
    @IBOutlet weak var txtCity: ACFloatingTextfield!
    
    
    @IBOutlet weak var widthInfoButton: NSLayoutConstraint!
    
    
    var arryCountry = NSMutableArray()
    var arryState = NSMutableArray()
    var arryCity = NSMutableArray()
    var typeComeFrom = String()
    var strHostUrl = String()
    var isLversionC = String()
    var btnBackgroundView = UIButton()


    // MARK: - ----Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        if #available(iOS 13.0, *) {
            let window = UIApplication.shared.windows.first
            let topPadding = window?.safeAreaInsets.top
            let statusBar = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: topPadding ?? 0.0))
            statusBar.backgroundColor = hexStringToUIColor(hex: primaryTheamColor)
            UIApplication.shared.windows.filter {$0.isKeyWindow}.first?.addSubview(statusBar)
            
        } else {
            let statusBar1: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
            statusBar1.backgroundColor = hexStringToUIColor(hex: primaryTheamColor)
        }
        
        
        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryTheamColor)
        btnSave.backgroundColor = hexStringToUIColor(hex: primaryTheamColor)
        btnSave.layer.cornerRadius = 8.0
        

        if(nsud.value(forKey: "CitizenCOP_CityData") != nil){
            let dictCityData = nsud.value(forKey: "CitizenCOP_CityData")as! NSDictionary
            txtCountry.text = "\(dictCityData.value(forKey: "Citizen_CountryName")!)"
            txtState.text = "\(dictCityData.value(forKey: "Citizen_StateName")!)"
            txtCity.text = "\(dictCityData.value(forKey: "Citizen_CityName")!)"
            txtCountry.tag = dictCityData.value(forKey: "Citizen_CountryID")! as! Int
            txtState.tag = dictCityData.value(forKey: "Citizen_StateID")! as! Int
            txtCity.tag = dictCityData.value(forKey: "Citizen_CityID")! as! Int
            strHostUrl = "\(dictCityData.value(forKey: "Citizen_HostUrl")!)"
            isLversionC = "\(dictCityData.value(forKey: "isLversionC")!)"

        }
        
        typeComeFrom == "ChangeCity" ? btnBack.isHidden = false : (btnBack.isHidden = true)
        
        
        if(dictCityData.count == 0){
            self.widthInfoButton.constant = 0.0
        }else{
//            DeviceType.IS_IPAD ? (self.widthInfoButton.constant = 64.0) : (self.widthInfoButton.constant = 40.0)
//            
//            let child = mainStoryboard.instantiateViewController(withIdentifier: "BottomSheetVC")as! BottomSheetVC
//               child.delegate = self
//              btnBackgroundView.alpha = 0.5
//              btnBackgroundView.frame = self.view.frame
//              btnBackgroundView.backgroundColor = UIColor.black
//              self.view.addSubview(btnBackgroundView)
//                child.strTitle = alertInfo
//                     child.strMessage = ""
//                     child.strViewComeFrom = "CitySectionInfo"
//                     child.frame = CGRect(x: 0, y: self.viewHeader.frame.maxY, width: (self.view.frame.width), height: (self.view.frame.height))
//                     child.view.frame = CGRect(x: 0, y: self.view.frame.maxY, width: (self.view.frame.width), height: (self.view.frame.height))
//                     self.addChild(child)
//                     self.view.addSubview(child.view)
//                     child.didMove(toParent: self)
        }
        

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.btnBackgroundView.removeFromSuperview()
        getCountry_State_CityFromLocal()

    }
    
    
    override func viewWillLayoutSubviews() {
        scrollView.contentSize = CGSize(width: ScreenSize.SCREEN_WIDTH, height: viewContain.frame.height)

    }
    
    @IBAction func actionOnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func actionAlerInfo(_ sender: UIButton) {
        let child = mainStoryboard.instantiateViewController(withIdentifier: "BottomSheetVC")as! BottomSheetVC
         child.delegate = self
        btnBackgroundView.alpha = 0.5
        btnBackgroundView.frame = self.view.frame
        btnBackgroundView.backgroundColor = UIColor.black
        self.view.addSubview(btnBackgroundView)
        
       
        
               child.strTitle = alertInfo
               child.strMessage = ""
               child.strViewComeFrom = "CitySectionInfo"
               child.frame = CGRect(x: 0, y: self.viewHeader.frame.maxY, width: (self.view.frame.width), height: (self.view.frame.height))
               child.view.frame = CGRect(x: 0, y: self.view.frame.maxY, width: (self.view.frame.width), height: (self.view.frame.height))
               self.addChild(child)
               self.view.addSubview(child.view)
               child.didMove(toParent: self)
      }
    @IBAction func actionOnSelection(_ sender: UIButton) {
        if(sender.tag == 1){  // Country
            if(arryCountry.count != 0){
                PopViewWithArray(tag: 1, aryData: arryCountry, strTitle: "Country")
            }else{
                self.callCountryAPI(sender: sender)
            }
        }else if (sender.tag == 2){ // State
            
            if(txtCountry.tag == 0){
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertCountryFirst, viewcontrol: self)
            }else{
                if(arryState.count != 0){
                    PopViewWithArray(tag: 2, aryData: arryState, strTitle: "State")
                    
                }else{
                    self.callstateAPI(sender: sender)
                }
            }
            
            
           
        }else if (sender.tag == 3){ //City
            if(txtState.tag == 0){
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertStateFirst, viewcontrol: self)
            }else{
                if(arryCity.count != 0){
                    PopViewWithArray(tag: 3, aryData: arryCity, strTitle: "City")
                    
                }else{
                    self.callCityAPI(sender: sender)
                }
            }
        }
    }
    
    @IBAction func actionOnSave(_ sender: UIButton) {
        if(txtCountry.tag == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertCountry, viewcontrol: self)
        }else if (txtState.tag == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertState, viewcontrol: self)
        }else if (txtCity.tag == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertCity, viewcontrol: self)
            
        }else{
           // getDashBoardDataAPI(sender: sender)
            getCityStatusWithMobileIconImeiNo(sender: sender)
        }
    }
    
    @IBAction func actionOnSocialButto(_ sender: UIButton) {
        if(isInternetAvailable()){
            var strUrl = ""
            if(sender.tag == 1){  // Facebook
                strUrl = BaseURLfacebook
            }else if (sender.tag == 2){ // Twitter
                strUrl = BaseURLtwitter
                
            }else if (sender.tag == 3){ //Instagram
                strUrl = BaseURLinstagram
            }
            
            let safariVC = SFSafariViewController(url: NSURL(string: strUrl)! as URL)
            self.present(safariVC, animated: true, completion: nil)
        }else{
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
        }
    }
    
    
    //  MARK: API CAlling
    
    func callCountryAPI(sender : UIButton) {
        if !isInternetAvailable() {
            FTIndicator.showToastMessage(alertInternet)
        }else{
            customDotLoaderShowOnFull(message: "", controller: self)
            let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><GetAllCountryList xmlns='http://m.citizencop.org/'></GetAllCountryList></soap12:Body></soap12:Envelope>"
            
            WebServiceClass.callSOAP_APIBY_POST(soapMessage: soapMessage, url: BaseURL, resultype: "GetAllCountryListResult", responcetype: "GetAllCountryListResponse") { (responce, status) in
               customeDotLoaderRemove()
                if status == "Suceess"{
                    self.arryCountry = NSMutableArray()
                    self.arryCountry = (responce.value(forKey: "data")as! NSArray).mutableCopy()as! NSMutableArray
                    deleteAllRecords(strEntity:"AllCountry")
                    saveDataInLocalArray(strEntity: "AllCountry", strKey: "allCountry", data: self.arryCountry)
                    if(sender.tag == 1){
                        self.PopViewWithArray(tag: sender.tag, aryData: self.arryCountry, strTitle: "Country")
                    }
                }else{
                    
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                    
                }
            }
        }
    }
    func callstateAPI(sender : UIButton) {
        if !isInternetAvailable() {
            FTIndicator.showToastMessage(alertInternet)
            
        }else{
            let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><GetAllStateList xmlns='http://m.citizencop.org/'></GetAllStateList></soap12:Body></soap12:Envelope>"
            customDotLoaderShowOnFull(message: "", controller: self)

            WebServiceClass.callSOAP_APIBY_POST(soapMessage: soapMessage, url: BaseURL, resultype: "GetAllStateListResult", responcetype: "GetAllStateListResponse") { (responce, status) in
                customeDotLoaderRemove()

                if status == "Suceess"{
                    self.arryState = NSMutableArray()
                    self.arryState = (responce.value(forKey: "data")as! NSArray).mutableCopy()as! NSMutableArray
                    deleteAllRecords(strEntity:"AllState")
                    saveDataInLocalArray(strEntity: "AllState", strKey: "allState", data: self.arryState)
                    if(sender.tag == 2){
                        self.PopViewWithArray(tag: sender.tag, aryData: self.arryState, strTitle: "State")
                    }
                }else{
                    
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                    
                }
            }
        }
    }
    func callCityAPI(sender : UIButton) {
        if !isInternetAvailable() {
            FTIndicator.showToastMessage(alertInternet)
            
        }else{
            let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><GetAllCityList xmlns='http://m.citizencop.org/'></GetAllCityList></soap12:Body></soap12:Envelope>"
            customDotLoaderShowOnFull(message: "", controller: self)
            WebServiceClass.callSOAP_APIBY_POST(soapMessage: soapMessage, url: BaseURL, resultype: "GetAllCityListResult", responcetype: "GetAllCityListResponse") { (responce, status) in
                customeDotLoaderRemove()

                if status == "Suceess"{
                    self.arryCity = NSMutableArray()
                    self.arryCity = (responce.value(forKey: "data")as! NSArray).mutableCopy()as! NSMutableArray
                    deleteAllRecords(strEntity:"AllCity")
                    saveDataInLocalArray(strEntity: "AllCity", strKey: "allCity", data: self.arryCity)
                    if(sender.tag == 3){
                        self.PopViewWithArray(tag: sender.tag, aryData: self.arryCity, strTitle: "City")
                    }
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                    
                }
            }
        }
        
        
        
    }
    func getDashBoardDataAPI(sender : UIButton) {
        if !isInternetAvailable() {
            FTIndicator.showToastMessage(alertInternet)
        }else{
            customDotLoaderShowOnButton(btn: sender, view: viewContain, controller: self)
            let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><getCityDynamicIconIPhone xmlns='http://m.citizencop.org/'><cityid>\(txtCity.tag)</cityid><ImeiNo>\(DeviceID)</ImeiNo><DeviceType>\(PlatForm)</DeviceType></getCityDynamicIconIPhone></soap12:Body></soap12:Envelope>"
            
            WebServiceClass.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: BaseURL, resultype: "getCityDynamicIconIPhoneResult", responcetype: "getCityDynamicIconIPhoneResponse") { (responce, status) in
                customeDotLoaderRemove()
                if status == "Suceess"{
                    
                    print(responce)
                    // Remove
                   
                    deleteAllRecords(strEntity:"AdvertiseMent")
                    deleteAllRecords(strEntity:"Administration")
                    deleteAllRecords(strEntity:"CallPoliceData")
                   
                    deleteAllRecords(strEntity:"DashBoardCityData")
                    deleteAllRecords(strEntity:"EmergencyCalls")
                    deleteAllRecords(strEntity:"ReportCategory")
                    deleteAllRecords(strEntity:"LostArticleList")
                    deleteAllRecords(strEntity:"FareVehicleType")
                    var status = false
                    if(nsud.value(forKey: "CitiZenCOP_Permission") != nil){
                        status = true
                    }
                    var aryContactList = NSMutableArray()
                    if nsud.value(forKey: "CitizenCOP_HelpContact") != nil {
                        aryContactList = (nsud.value(forKey: "CitizenCOP_HelpContact")as! NSArray).mutableCopy()as? NSMutableArray ?? []
                    }
                    
                    UserDefaults.standard.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
                    if(status){
                        nsud.set(status, forKey: "CitiZenCOP_Permission")
                    }
                    nsud.set(aryContactList, forKey: "CitizenCOP_HelpContact")
                  
                    
                    //ADD
                    let dictCity = NSMutableDictionary()
                    dictCity.setValue(self.txtCity.text, forKey: "Citizen_CityName")
                    dictCity.setValue(self.txtCity.tag, forKey: "Citizen_CityID")
                    dictCity.setValue(self.txtState.text, forKey: "Citizen_StateName")
                    dictCity.setValue(self.txtState.tag, forKey: "Citizen_StateID")
                    dictCity.setValue(self.txtCountry.text, forKey: "Citizen_CountryName")
                    dictCity.setValue(self.txtCountry.tag, forKey: "Citizen_CountryID")
                    dictCity.setValue(self.strHostUrl, forKey: "Citizen_HostUrl")
                    dictCity.setValue(self.isLversionC, forKey: "isLversionC")

                    let dictDashboardData = (responce.value(forKey: "data")as? NSDictionary)?.mutableCopy()as? NSMutableDictionary ?? [:]
                    saveDataInLocalDictionary(strEntity: "DashBoardCityData", strKey: "dashBoardCityData", data: dictDashboardData)
                    
                    nsud.setValue(dictCity, forKey: "CitizenCOP_CityData")
                    nsud.synchronize()
                    dictCityData = NSMutableDictionary()
                    dictCityData = (getDataFromCoreDataBaseDict(strEntity: "DashBoardCityData", strkey: "dashBoardCityData")).mutableCopy()as? NSMutableDictionary ?? [:]
                  
                    let isLiteVersion = "\(((dictCityData.value(forKey: "CityData")as? NSArray)?.object(at: 0)as? NSDictionary ?? [:]).value(forKey: "IsLiteVersion") ?? "")"

                    if(isLiteVersion == "1"){ // Light
                        let child = mainStoryboard.instantiateViewController(withIdentifier: "BottomSheetVC")as! BottomSheetVC
                        self.btnBackgroundView.alpha = 0.5
                        self.btnBackgroundView.frame = self.view.frame
                        self.view.addSubview(self.btnBackgroundView)
                        child.delegate = self
                        self.btnBackgroundView.backgroundColor = UIColor.black
                        
                        child.strTitle = alertInfo
                        child.strMessage = ""
                        child.strViewComeFrom = "CitySection"
                        child.frame = CGRect(x: 0, y: self.viewHeader.frame.maxY, width: (self.view.frame.width), height: (self.view.frame.height))
                        child.view.frame = CGRect(x: 0, y: self.view.frame.maxY, width: (self.view.frame.width), height: (self.view.frame.height))
                        self.addChild(child)
                        self.view.addSubview(child.view)
                        child.didMove(toParent: self)
                    }else{
                        let vc: DynemicSplashVC = mainStoryboard.instantiateViewController(withIdentifier: "DynemicSplashVC") as! DynemicSplashVC
                               self.navigationController?.pushViewController(vc, animated: false)
                    }
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            }
        }
    }
    
    func getCityStatusWithMobileIconImeiNo(sender : UIButton) {
        if !isInternetAvailable() {
            FTIndicator.showToastMessage(alertInternet)
        }else{
            customDotLoaderShowOnButton(btn: sender, view: viewContain, controller: self)
            let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><getCityStatusWithMobileIconImeiNo xmlns='http://m.citizencop.org/'><cityid>\(txtCity.tag)</cityid></getCityStatusWithMobileIconImeiNo></soap12:Body></soap12:Envelope>"
            WebServiceClass.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: BaseURL, resultype: "getCityStatusWithMobileIconImeiNoResult", responcetype: "getCityStatusWithMobileIconImeiNoResponse") { (responce, status) in
                customeDotLoaderRemove()
                if status == "Suceess"{
                    
                    let dictTemp = responce.value(forKey: "data")as! NSDictionary
                        let ary = dictTemp.value(forKey: "CityData")as! NSArray
                        if(ary.count != 0){
                            let dictData = ary.object(at: 0)as! NSDictionary
                            let IsMaskSplashScreen = "\(dictData.value(forKey: "IsMaskSplashScreen") ?? "")"
                            if IsMaskSplashScreen == "1" || IsMaskSplashScreen == "True" || IsMaskSplashScreen == "true" {
                                let SplashVersionMsg  =  "\(dictData.value(forKey: "CityLogo") ?? "")"
                                let splashTime = "\(dictData.value(forKey: "IsMaskSplashScreenTiming") ?? "")"
                                let vc: SplashDynamicVC = mainStoryboard.instantiateViewController(withIdentifier: "SplashDynamicVC") as! SplashDynamicVC
                                vc.SplashVersionMsg = SplashVersionMsg
                                vc.time = Double(splashTime) ?? 0
                                vc.delegate = self
                                self.navigationController?.pushViewController(vc, animated: false)
                            }else{
                                self.getDashBoardDataAPI(sender: UIButton())
                            }
                        }else{
                            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                            
                        }
                }else{
                    
                }
            }
        }
    }
    
    //MARK: Other Function's
    func PopViewWithArray(tag: Int , aryData : NSMutableArray , strTitle: String)  {
        let vc: SelectionClass = mainStoryboard.instantiateViewController(withIdentifier: "SelectionClass") as! SelectionClass
        vc.strTitle = strTitle
        vc.strTag = tag
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .coverVertical
        vc.delegate = self
        if(tag == 2){
            vc.aryList = filterState_CityData(aryData: aryData, stateID: 0, countryId: txtCountry.tag)
            self.present(vc, animated: true, completion: {})
            
        }else if (tag == 3){
            vc.aryList = filterState_CityData(aryData: aryData, stateID: txtState.tag, countryId: 0)
            self.present(vc, animated: true, completion: {})
            
        }else{
            if aryData.count != 0{
                vc.aryList = aryData
                self.present(vc, animated: true, completion: {})
            }
        }
    }
    
    
    func getCountry_State_CityFromLocal() {
        
        self.arryCountry = NSMutableArray()
        self.arryState = NSMutableArray()
        self.arryCity = NSMutableArray()
        
        self.arryCountry = getDataFromCoreDataBase(strEntity: "AllCountry", strkey: "allCountry")
        self.arryState = getDataFromCoreDataBase(strEntity: "AllState", strkey: "allState")
        self.arryCity = getDataFromCoreDataBase(strEntity: "AllCity", strkey: "allCity")
        let dispatchGroup = DispatchGroup()
        
        dispatchGroup.enter()
        if(self.arryCountry.count == 0){
            self.callCountryAPI(sender: UIButton())
        }
//        dispatchGroup.enter()
//        if(self.arryState.count == 0){
//            self.callstateAPI(sender: UIButton())
//        }
//        dispatchGroup.enter()
//        if(self.arryCity.count == 0){
//            self.callCityAPI(sender: UIButton())
//        }
    }
    
    func filterState_CityData(aryData : NSMutableArray , stateID : Int , countryId : Int) -> NSMutableArray {
        let aryFilter = NSMutableArray()
        if(countryId != 0){ // for State
            for item in aryData{
                if(countryId ==  Int("\(((item as AnyObject)as! NSDictionary).value(forKey: "country_id")!)")){
                    aryFilter.add(item)
                }
            }
            
        }else { // For City
            for item in aryData{
                if(stateID ==  Int("\(((item as AnyObject)as! NSDictionary).value(forKey: "state_id")!)")){
                    aryFilter.add(item)
                }
            }
        }
        return aryFilter
    }
}
//MARK:-
//MARK:- ---------PopUpDelegate

extension CitySelectionVC : PopUpDelegate , citySelectionBack
{
    func backPop(isBool: Bool) {
        self.getDashBoardDataAPI(sender: UIButton())
    }
    
    func getDataFromPopupDelegate(dictData: NSDictionary, tag: Int) {
        if(tag == 1){
            
            self.txtCountry.text = "\(dictData.value(forKey: "country_name")!)"
            self.txtCountry.tag = Int("\(dictData.value(forKey: "country_id")!)")!
            self.txtState.text = ""
              self.txtState.tag = 0
             self.txtCity.text = ""
             self.txtCity.tag = 0
        }else if(tag == 2){
            self.txtState.text = "\(dictData.value(forKey: "state_name")!)"
            self.txtState.tag = Int("\(dictData.value(forKey: "state_id")!)")!
            self.txtCity.text = ""
            self.txtCity.tag = 0
            self.strHostUrl = "\(dictData.value(forKey: "HostURL")!)"


        }
        else if(tag == 3){
            self.txtCity.text = "\(dictData.value(forKey: "city_name")!)"
            self.txtCity.tag = Int("\(dictData.value(forKey: "city_id")!)")!
            self.isLversionC = "\(dictData.value(forKey: "IsLiteVersion")!)"

        }
    }
}

//MARK:-
//MARK:- ---------BottomSheetDelegate

extension CitySelectionVC : BottomSheetDelegate
{
    func getDataFromBottomSheetDelegate() {
        self.viewWillAppear(true)
    }
    
  
    
}

