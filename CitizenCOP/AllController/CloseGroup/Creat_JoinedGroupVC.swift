//
//  JoinedGroupListVC.swift
//  CitizenCOP
//
//  Created by Navin Patidar on 6/5/19.
//  Copyright © 2019 CitizenCop. All rights reserved.
//

import UIKit

class Creat_JoinedGroupVC: UIViewController {
  
    // MARK: - --------------IBOutlet
    // MARK: -
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var lblTitle: UILabel!
  
    @IBOutlet weak var btnCreat: UIButton!
    @IBOutlet weak var txtGroupName: ACFloatingTextfield!
    
    @IBOutlet var ViewJoin: UIView!
    @IBOutlet weak var txtEnterKey: ACFloatingTextfield!
    @IBOutlet weak var txtNickName: ACFloatingTextfield!
    @IBOutlet weak var btnJoin: UIButton!

    var strTitle = String()
    var dictData = NSDictionary()

    // MARK: - --------------LifeCycle
    // MARK: -
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblTitle.text = self.strTitle
        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryTheamColor)
        btnJoin.layer.cornerRadius = 5.0
        btnJoin.backgroundColor = hexStringToUIColor(hex: primaryDark)
        btnCreat.layer.cornerRadius = 5.0
        btnCreat.backgroundColor = hexStringToUIColor(hex: primaryDark)
        
        if(self.strTitle == "Rename Group"){
           self.btnCreat.setTitle("RENAME", for: .normal)
            txtGroupName.text = "\(dictData.value(forKey: "GroupName")!)"
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if(self.strTitle == "Join Group"){
            ViewJoin.frame = CGRect(x: 0, y: Int(self.viewHeader.frame.maxY + 20), width: Int(self.view.frame.width), height: Int(self.view.frame.height - self.viewHeader.frame.maxY))
            self.view.addSubview(ViewJoin)
        }
    }
    
    // MARK: - --------------Actions
    // MARK: -
    @IBAction func actionOnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)

    }
    @IBAction func actionOnCreat(_ sender: UIButton) {
        if(txtGroupName.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_CloseGroupName, viewcontrol: self)
        }else{
            strTitle == "Rename Group" ? self.callRenameGroup(sender : sender) : self.callCreatGroupAPI(sender : sender)
            
        }
    }
   
    @IBAction func action_OnJoin(_ sender: UIButton) {
        if(txtEnterKey.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_CloseGroupKey, viewcontrol: self)
        }else if(txtNickName.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_CloseNickName, viewcontrol: self)
        }else{
            callJoinGroupAPI(sender: sender)
        }
    }
    // MARK: - ---------------API CAlling
    // MARK: -
    func callCreatGroupAPI(sender : UIButton) {
        if !isInternetAvailable() {
            FTIndicator.showToastMessage(alertInternet)
        }else{
            customDotLoaderShowOnButton(btn: sender, view: self.view, controller: self)
            
            print(API_CreatGroup + "&GroupName=\(txtGroupName.text!)&ImeiNo=\(DeviceID)")
         //   http://m.citizencop.org/CitizenCopAllHandler.ashx?Key= InsertAdminCloseGroup_ForMultiple&GroupName=“”& ImeiNo=“”

            let strUrl = API_CreatGroup + "&GroupName=\(txtGroupName.text!)&ImeiNo=\(DeviceID)"
            
            WebServiceClass.callAPIBYPOST(parameter: NSDictionary(), url: strUrl) { (responce, status) in
                customeDotLoaderRemove()
                if status == "Suceess"{
                    print(responce)
                    FTIndicator.showNotification(withTitle: alertInfo, message: "\(alertCreatThegroup)")
                    self.navigationController?.popViewController(animated: true)
                }
                else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            }
        }
    }
    func callJoinGroupAPI(sender : UIButton) {
        if !isInternetAvailable() {
            FTIndicator.showToastMessage(alertInternet)
        }else{
            customDotLoaderShowOnButton(btn: sender, view: self.ViewJoin, controller: self)
            
            print(API_JoinedGroup + "&ImeiNo=\(DeviceID)&AdminGenerateKey=\(txtEnterKey.text!)&GroupName=&NickName=\(txtNickName.text!)&Lat=\(GlobleLat)&Lng=\(GlobleLong)&AcceptanceStatus=Yes")
            let groupname = ""
            WebServiceClass.callAPIBYPOST(parameter: NSDictionary(), url: API_JoinedGroup + "&ImeiNo=\(DeviceID)&AdminGenerateKey=\(txtEnterKey.text!)&GroupName=\(groupname)&NickName=\(txtNickName.text!)&Lat=\(GlobleLat)&Lng=\(GlobleLong)&AcceptanceStatus=Yes") { (responce, status) in
                customeDotLoaderRemove()
                if status == "Suceess"{
                    print(responce)
                    FTIndicator.showNotification(withTitle: alertInfo, message: alertjoinedthegroup)
                    if(nsud.value(forKey: "CitiZenCOP_JoinedGroupLocation") == nil){
                        nsud.set(true, forKey: "CitiZenCOP_JoinedGroupLocation")
                        nsud.synchronize()
                    }
                    self.navigationController?.popViewController(animated: true)
                }
                else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            }
        }
    }
    func callRenameGroup(sender : UIButton) {
        if !isInternetAvailable() {
            FTIndicator.showToastMessage(alertInternet)
        }else{
            customDotLoaderShowOnButton(btn: sender, view: self.view, controller: self)
            
            print(API_RenameGroupNameCloseGroup + "&ImeiNo=\(DeviceID)&OldGroupName=\(dictData.value(forKey: "GroupName")!)&NewGroupName=\(txtGroupName.text!)")
            
            WebServiceClass.callAPIBYPOST(parameter: NSDictionary(), url: API_RenameGroupNameCloseGroup + "&ImeiNo=\(DeviceID)&OldGroupName=\(dictData.value(forKey: "GroupName")!)&NewGroupName=\(txtGroupName.text!)") { (responce, status) in
                customeDotLoaderRemove()
                if status == "Suceess"{
                    print(responce)
                    FTIndicator.showNotification(withTitle: alertInfo, message: "\(alertRemoveThegroup)")

                    for controller in self.navigationController!.viewControllers as Array {
                        if controller.isKind(of: CloseGroupVC.self) {
                            self.navigationController!.popToViewController(controller, animated: true)
                            break
                        }
                    }
                    self.navigationController?.popViewController(animated: true)
                }
                else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            }
        }
    }
}

//MARK:-
//MARK:- ---------UITextFieldDelegate

extension Creat_JoinedGroupVC : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if ( textField == txtEnterKey || textField == txtNickName || textField == txtGroupName)
        {
            return txtFiledValidation(textField: textField, string: string, returnOnly: "All", limitValue: 50)
        }
        else
        {
            return true
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
}
