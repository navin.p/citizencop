//
//  SettingVC.swift
//  CitizenCOP
//
//  Created by Navin Patidar on 9/5/19.
//  Copyright © 2019 CitizenCop. All rights reserved.
//

import UIKit

class SettingVC: UIViewController {
    // MARK: - --------------IBOutlet
    // MARK: -
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var switchforLocaton: UISwitch!

    // MARK: - --------------Life Cycle
    // MARK: -
    
    override func viewDidLoad() {
        super.viewDidLoad()
        switchforLocaton.onTintColor = hexStringToUIColor(hex: primaryTheamColor)
        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryTheamColor)
        if(nsud.value(forKey: "CitiZenCOP_JoinedGroupLocation") != nil){
            switchforLocaton.setOn(nsud.value(forKey: "CitiZenCOP_JoinedGroupLocation")as! Bool, animated: true)
        }else{
            switchforLocaton.setOn(false, animated: true)
        }
        
    }
    
    // MARK: - --------------Actions
    // MARK: -
    @IBAction func action_Back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func actionOnSwitch(_ sender: UISwitch) {
        if(sender.isOn){
            callLocationShareForGroupAPI(strStatus: "On")

        }else{
            callLocationShareForGroupAPI(strStatus: "Off")

        }
    }
    
    
    
    // MARK: - --------------API Calling
    // MARK: -
    
    func callLocationShareForGroupAPI(strStatus: String) {
        if !isInternetAvailable() {
           FTIndicator.showNotification(withTitle: alertMessage, message: alertInternet)
        }else{
            let strUrl = API_UpdateOnOffSetting + "&ImeiNo=\(DeviceID)&OnOffSetting=\(strStatus)&LocationType=GPS"
            
            WebServiceClass.callAPIBYPOST(parameter: NSDictionary(), url: strUrl) { (responce, status) in
                print(responce)
                if status == "Suceess"{
                    let dictResponceData = (responce.value(forKey: "data")as! NSDictionary)
                    let strResult = "\(dictResponceData.value(forKey: "Result")!)"
                    if(strResult == "Ok"){
                        if(strStatus == "On"){
                            FTIndicator.showNotification(withTitle: alertMessage, message: "\(alertlocationsharingservices) enabled.")
                            nsud.set(true, forKey: "CitiZenCOP_JoinedGroupLocation")
                            nsud.synchronize()
                        }else{
                           
                            FTIndicator.showNotification(withTitle: alertMessage, message: "\(alertlocationsharingservices) disabled.")

                            nsud.set(false, forKey: "CitiZenCOP_JoinedGroupLocation")
                            nsud.synchronize()
                        }
                    }else{
                        
                    }
                }
                else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            }
            
        }
    }
    
    func callUpdateMemberLocationAPI() {
        if !isInternetAvailable() {
           // FTIndicator.showNotification(withTitle: alertMessage, message: alertInternet)
        }else{
            if(GlobleLat != "0.0" && GlobleLong != "0.0"){
                let strUrl = API_UpdateMemberLocation + "&ImeiNo=\(DeviceID)&Lat=\(GlobleLat)&Lng=\(GlobleLong)&LocationType=gps&LastLocatDateTime"
               // let strUrl = API_UpdateMemberLocation + "&ImeiNo=\(DeviceID)&Lat=123456&Lng=678962&LocationType=gps&LastLocatDateTime"
                print("Location Update URL : ------------\(strUrl)" )
                
                WebServiceClass.callAPIBYPOST(parameter: NSDictionary(), url: strUrl) { (responce, status) in
                 //   print(responce)
                    if status == "Suceess"{
                        let dictResponceData = (responce.value(forKey: "data")as! NSDictionary)
                        let strResult = "\(dictResponceData.value(forKey: "Result")!)"
                        if(strResult == "Ok"){

                        }else{
                            
                        }
                    }
                    else{
                      //  showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                    }
                }
                
            }else{
               // FTIndicator.showNotification(withTitle: alertMessage, message: "Location not found!!!!!!!")
            }
        }
    }
}
