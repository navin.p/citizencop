//
//  CloseGroupDetailVC.swift
//  CitizenCOP
//
//  Created by Navin Patidar on 6/12/19.
//  Copyright © 2019 CitizenCop. All rights reserved.
//

import UIKit
import ContactsUI
import MessageUI

class CloseGroupDetailVC: UIViewController {
    // MARK: - --------------IBOutlet
    // MARK: -
    @IBOutlet weak var viewHeader: UIView! 
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblCreatedDate: UILabel!
    @IBOutlet weak var lblTotalMemberDate: UILabel!
    @IBOutlet weak var btnInvite: UIButton!
    @IBOutlet weak var tvList: UITableView!
    @IBOutlet weak var lblError: UILabel!

    var dictData = NSMutableDictionary()
    var aryjoinedGroupDetail = NSMutableArray()

    // MARK: - --------------LifeCycle
    // MARK: -
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        tvList.tableFooterView = UIView()
        tvList.estimatedRowHeight = 80.0
        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryTheamColor)
        lblTitle.text = "\(dictData.value(forKey: "GroupName")!)"
        lblCreatedDate.text = "Created Date : \(dictData.value(forKey: "CreatedDate")!)"
        lblTotalMemberDate.text = "  Participants: \(dictData.value(forKey: "NoOfMembers")!)"
        lblTotalMemberDate.layer.cornerRadius = 5.0
        lblTotalMemberDate.layer.borderColor = hexStringToUIColor(hex: primaryDark).cgColor
        lblTotalMemberDate.layer.borderWidth = 1.0
        btnInvite.layer.cornerRadius = 5.0
        btnInvite.backgroundColor = hexStringToUIColor(hex: primaryDark)
        callGetCloseGroupMembersAPI()
        lblError.text = ""
    }
    // MARK: - --------------Actions
    // MARK: -
    @IBAction func actionOnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        
    }
    @IBAction func actionOnEdit(_ sender: Any) {
        self.view.endEditing(true)
        let testController = mainStoryboard.instantiateViewController(withIdentifier: "Creat_JoinedGroupVC")as! Creat_JoinedGroupVC
        testController.strTitle = "Rename Group"
        testController.dictData = dictData
        self.navigationController?.pushViewController(testController, animated: true)
    }
    @IBAction func actionOnInvite(_ sender: Any) {
        let cnPicker = CNContactPickerViewController()
        cnPicker.delegate = self
        self.present(cnPicker, animated: true, completion: nil)
    }
    
    
    // MARK: - ---------------API CAlling
    // MARK: -
    func callGetCloseGroupMembersAPI() {
        if !isInternetAvailable() {
            FTIndicator.showToastMessage(alertInternet)
            
        }else{
            let loading = DPBasicLoading(table: self.tvList, fontName: "HelveticaNeue")
            loading.startLoading(text: "Loading...")

            
            
            let strURL = API_GetCloseGroupMembers + "&ImeiNo=\(DeviceID)&GroupName=\(dictData.value(forKey: "GroupName")!)"
            print("Get Member For Group \(strURL)")
            WebServiceClass.callAPIBYPOST(parameter: NSDictionary(), url: strURL) { (responce, status) in
                loading.endLoading()
                self.lblError.text = ""
                if status == "Suceess"{
                    print(responce)
                    self.aryjoinedGroupDetail = NSMutableArray()
                    self.aryjoinedGroupDetail = ((responce.value(forKey: "data")as! NSDictionary).value(forKey: "Members")as! NSArray).mutableCopy()as! NSMutableArray
                    if(self.aryjoinedGroupDetail.count == 0){
                        self.lblError.text = alertGroupMemberNotFound
                    }
                        self.lblTotalMemberDate.text = "  Participants: \(self.aryjoinedGroupDetail.count)"
                }
                else{
                    self.lblError.text = alertMessage
                }
                self.tvList.reloadData()
            }
        }
    }
    
    func callDeleteMemberCloseGroupGroupAPI(index : Int) {
        if !isInternetAvailable() {
            FTIndicator.showToastMessage(alertInternet)
        }else{
            customDotLoaderShowOnFull(message: "", controller: self)
            let dict = self.aryjoinedGroupDetail.object(at: index)as! NSDictionary
            
            let strURL = API_DeleteMemberCloseGroup + "&AdminImeiNo=\(DeviceID)&MemberImeiNo=\(dict.value(forKey: "ImeiNo")!)&NickName=\(dict.value(forKey: "NickName")!)&GroupName=\(dict.value(forKey: "GroupName")!)"
            print("Remove member by Admin = \(strURL)")
            WebServiceClass.callAPIBYPOST(parameter: NSDictionary(), url: strURL) { (responce, status) in
                customeDotLoaderRemove()
                if status == "Suceess"{
                    print(responce)
                    
                    FTIndicator.showNotification(withTitle:alertMessage, message: alertGroupmemberremoved)
                    self.callGetCloseGroupMembersAPI()
                    
                }
                else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            }
        }
    }
}

// MARK: - --------------CNContactPickerDelegate
// MARK: -
extension CloseGroupDetailVC : CNContactPickerDelegate{
    
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contact: CNContact) {
        // user phone number
        var strnumber = ""
        for number in contact.phoneNumbers {
            let phoneNumber = number.value
            print("number is = \(phoneNumber)")
          strnumber = phoneNumber.stringValue
        }
        if(strnumber.count != 0){
            openMessageComposser(number: strnumber)
        }
        
    }
    func openMessageComposser(number : String) {
        if (MFMessageComposeViewController.canSendText()) {
            let controller = MFMessageComposeViewController()
            controller.body = "Hi!! I want to add you in my close group in CitizenCOP please add key <\(dictData.value(forKey: "GenerateKey")!)> in Close Group option in Citizen COP"
            controller.recipients = ["\(number)"]
            controller.messageComposeDelegate = self
            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
                self.present(controller, animated: false, completion: nil)

            })
        }else{
            FTIndicator.showToastMessage(alertCalling)
        }
    }
 
    func contactPickerDidCancel(_ picker: CNContactPickerViewController) {
        print("Cancel Contact Picker")
    }
}

//MARK: --------------MFMessageComposeViewControllerDelegate
//MARK:-
extension CloseGroupDetailVC : MFMessageComposeViewControllerDelegate {
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        self.dismiss(animated: true, completion: nil)
    }
}
// MARK: - ----------------UITableViewDelegate
// MARK: -

extension CloseGroupDetailVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return aryjoinedGroupDetail.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tvList.dequeueReusableCell(withIdentifier: "CloseGroupDetailCell", for: indexPath as IndexPath) as! CloseGroupDetailCell
            let dict = aryjoinedGroupDetail.object(at: indexPath.row)as! NSDictionary
            cell.lblGroupName.text = "\(dict.value(forKey: "NickName")!)"
        cell.lblLastSeen.text = "Last Seen : \(dict.value(forKey: "OnOffDateTime")!)"
          cell.lblLocationStatus.text = "\(dict.value(forKey: "OnOffSetting")!)".uppercased()
        if(cell.lblLocationStatus.text == "ON" || cell.lblLocationStatus.text == "on"){
            cell.lblLocationStatus.textColor = UIColor.blue
            cell.btnMap.isHidden = false
        }else{
            cell.lblLocationStatus.textColor = hexStringToUIColor(hex: primaryTheamColor)
            cell.btnMap.isHidden = true

        }
        
        
            return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
      
            return UITableView.automaticDimension
      
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dict = aryjoinedGroupDetail.object(at: indexPath.row)as! NSDictionary
        if("\(dict.value(forKey: "OnOffSetting")!)".uppercased() == "ON"){
            openMap(strTitle: "\(dict.value(forKey: "NickName")!)", GlobleLat: "\(dict.value(forKey: "Lat")!)", GlobleLong: "\(dict.value(forKey: "Lng")!)")
        }else{
            FTIndicator.showNotification(withTitle: alertMessage, message: "Location sharing services disabled by- \(dict.value(forKey: "NickName")!)")
            
        }
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        let dict = self.aryjoinedGroupDetail.object(at: indexPath.row)as! NSDictionary
        let nickName = "\(dict.value(forKey: "NickName")!)"
        
        let msg = "Do you want to remove \(nickName.uppercased()) from group \(lblTitle.text!.uppercased())."
        
            if (editingStyle == UITableViewCell.EditingStyle.delete) {
                let alert = UIAlertController(title: "Remove?", message: msg, preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "Remove", style: .default, handler: { action in
                    self.callDeleteMemberCloseGroupGroupAPI(index: indexPath.row)
                }))
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                    
                }))
                self.present(alert, animated: true, completion: nil)
                
            }
    }
   
    
}
// MARK: - --------------Close Group
// MARK: -
class CloseGroupDetailCell: UITableViewCell {
    //DashBoard
    @IBOutlet weak var lblGroupName: UILabel!
    @IBOutlet weak var lblLastSeen: UILabel!
    @IBOutlet weak var lblLocationStatus: UILabel!
    @IBOutlet weak var btnMap: UIButton!

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}
