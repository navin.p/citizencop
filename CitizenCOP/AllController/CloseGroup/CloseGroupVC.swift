//
//  CloseGroupVC.swift
//  CitizenCOP
//
//  Created by Navin Patidar on 6/5/19.
//  Copyright © 2019 CitizenCop. All rights reserved.
//

import UIKit

class CloseGroupVC: UIViewController {
   
    // MARK: - --------------IBOutlet
    // MARK: -
    @IBOutlet weak var btnAdd: UIButton!
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var tvList: UITableView!
    @IBOutlet weak var lblError: UILabel!
    @IBOutlet weak var btnSegment: UISegmentedControl!
    @IBOutlet weak var btnSetting: UIButton!

    var ary_MyCloseGroup = NSMutableArray()
    var ary_JoinedGroup = NSMutableArray()
 
    // MARK: - --------------Life Cycle
    // MARK: -
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tvList.tableFooterView = UIView()
        tvList.estimatedRowHeight = 80.0
        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryTheamColor)
        btnSegment.tintColor = hexStringToUIColor(hex: primaryTheamColor)
        tvList.tag = 0
        lblError.text = ""
        let font = UIFont.systemFont(ofSize: DeviceType.IS_IPAD ? 16 : 12)
        btnSegment.setTitleTextAttributes([NSAttributedString.Key.font: font], for: .normal)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if(tvList.tag == 0){
            self.callMyCloseGroupAPI()
            btnSetting.isHidden = true
        }else if (tvList.tag == 1){
           self.callJoinedGroupAPI()
            btnSetting.isHidden = false

        }
    }
    
    // MARK: - --------------Actions
    // MARK: -
    
    
    
    @IBAction func SegmentAction(_ sender: UISegmentedControl) {
        if(sender.selectedSegmentIndex == 0){
            tvList.tag = 0
            btnSetting.isHidden = true

            if(ary_MyCloseGroup.count != 0){
                self.tvList.reloadData()
            }else{
                self.callMyCloseGroupAPI()
            }
        }else if(sender.selectedSegmentIndex == 1){
            tvList.tag = 1
            btnSetting.isHidden = false
            if(ary_JoinedGroup.count != 0){
                self.tvList.reloadData()
            }else{
                self.callJoinedGroupAPI()
            }
        }
        
    }
 
    @IBAction func action_Setting(_ sender: Any) {
        self.view.endEditing(true)
        let testController = mainStoryboard.instantiateViewController(withIdentifier: "SettingVC")as! SettingVC
        self.navigationController?.pushViewController(testController, animated: true)
    }
    
    @IBAction func action_Back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
   
    @IBAction func action_Creat(_ sender: Any) {
        
        self.view.endEditing(true)
        let testController = mainStoryboard.instantiateViewController(withIdentifier: "Creat_JoinedGroupVC")as! Creat_JoinedGroupVC
        
        
        testController.strTitle = self.btnSegment.selectedSegmentIndex == 1 ? "Join Group" : "Creat Group"
        self.navigationController?.pushViewController(testController, animated: true)
        
    }
    
    // MARK: - ---------------API CAlling
    // MARK: -
    func callMyCloseGroupAPI() {
        self.tvList.tag = 0
        if !isInternetAvailable() {
            self.lblError.text = alertInternet
            tvList.reloadData()
        }else{
            let loading = DPBasicLoading(table: self.tvList, fontName: "HelveticaNeue")
            loading.startLoading(text: "Loading...")
            print(API_GroupAvailable + DeviceID)
            WebServiceClass.callAPIBYPOST(parameter: NSDictionary(), url: API_GroupAvailable + DeviceID) { (responce, status) in
                loading.endLoading()
                if status == "Suceess"{
                    print(responce)
                    let dictResponceData = (responce.value(forKey: "data")as! NSDictionary)
                    let strResult = "\(dictResponceData.value(forKey: "Result")!)"
                    self.ary_MyCloseGroup = NSMutableArray()
                    if strResult == "True"{
                        self.ary_MyCloseGroup = NSMutableArray()
                        self.ary_MyCloseGroup = (dictResponceData.value(forKey: "Admin")as! NSArray).mutableCopy()as! NSMutableArray
                        if(self.ary_MyCloseGroup.count == 0){
                            self.lblError.text = alertDataNotFound
                        }
                        self.tvList.reloadData()
                    }else{
                        self.lblError.text = alertDataNotFound
                        self.tvList.reloadData()
                    }
                }
                else{
                    self.ary_MyCloseGroup = NSMutableArray()
                    self.tvList.reloadData()
                    self.lblError.text = alertDataNotFound
                }
            }
        }
    }

    func callJoinedGroupAPI() {
        self.tvList.tag = 1
        if !isInternetAvailable() {
            self.lblError.text = alertInternet
            tvList.reloadData()
        }else{
            let loading = DPBasicLoading(table: self.tvList, fontName: "HelveticaNeue")
            loading.startLoading(text: "Loading...")

            WebServiceClass.callAPIBYPOST(parameter: NSDictionary(), url: API_JoinedGroupList + DeviceID) { (responce, status) in
                loading.endLoading()
                if status == "Suceess"{
                    print(responce)
                    let dictResponceData = (responce.value(forKey: "data")as! NSDictionary)
                    let strResult = "\(dictResponceData.value(forKey: "Result")!)"
                    self.ary_JoinedGroup = NSMutableArray()

                    if strResult == "True"{
                        self.ary_JoinedGroup = (dictResponceData.value(forKey: "Admin")as! NSArray).mutableCopy()as! NSMutableArray
                        if(self.ary_JoinedGroup.count == 0){
                            self.lblError.text = alertDataNotFound
                        }
                        self.tvList.reloadData()
                    }else{
                        self.lblError.text = alertDataNotFound
                        self.tvList.reloadData()
                    }
                }
                else{
                    self.ary_JoinedGroup = NSMutableArray()
                    self.lblError.text = alertSomeError
                    self.tvList.reloadData()
                }
            }
            
        }
    }
    func callDeleteCloseGroupAPI(index : Int) {
        if !isInternetAvailable() {
            FTIndicator.showToastMessage(alertInternet)
          
        }else{
            customDotLoaderShowOnFull(message: "", controller: self)
            let dict = self.ary_MyCloseGroup.object(at: index)as! NSDictionary
              print(API_DeleteGroup + "&ImeiNo=\(DeviceID)&GroupName=\(dict.value(forKey: "GroupName")!)")
            WebServiceClass.callAPIBYPOST(parameter: NSDictionary(), url: API_DeleteGroup + "&ImeiNo=\(DeviceID)&GroupName=\(dict.value(forKey: "GroupName")!)") { (responce, status) in
                customeDotLoaderRemove()
                if status == "Suceess"{
                    print(responce)
                    FTIndicator.showToastMessage("Group Deleted Successfully.")
                  self.callMyCloseGroupAPI()
                }
                else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            }
            
            
        }
    }
    func callRemoveJoinedGroupAPI(index : Int) {
        if !isInternetAvailable() {
            FTIndicator.showToastMessage(alertInternet)
        }else{
            customDotLoaderShowOnFull(message: "", controller: self)
            let dict = self.ary_JoinedGroup.object(at: index)as! NSDictionary
            print(API_DeleteJoinedGroup + "&AdminGenerateKey=\(dict.value(forKey: "GenerateKey")!)&ImeiNo=\(DeviceID)&NickName=&Lat=0.000000&Lng=0.000000&AcceptanceStatus=No")
            WebServiceClass.callAPIBYPOST(parameter: NSDictionary(), url: API_DeleteJoinedGroup + "&AdminGenerateKey=\(dict.value(forKey: "GenerateKey")!)&ImeiNo=\(DeviceID)&NickName=&Lat=0&Lng=0&AcceptanceStatus=No") { (responce, status) in
                customeDotLoaderRemove()
                if status == "Suceess"{
                    print(responce)
                    FTIndicator.showToastMessage("Group removed Successfully.")
                    self.callJoinedGroupAPI()
                }
                else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            }
        }
    }
}
// MARK: - ----------------UITableViewDelegate
// MARK: -

extension CloseGroupVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        self.btnSegment.setTitle("My Group(\(ary_MyCloseGroup.count))", forSegmentAt: 0)
        self.btnSegment.setTitle("Joined Group(\(ary_JoinedGroup.count))", forSegmentAt: 1)

        if(self.tvList.tag == 0){ // My Close Group
            return ary_MyCloseGroup.count
        }else{  // My Joined Group
            return ary_JoinedGroup.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
          if(self.tvList.tag == 0){ // My Close Group
                let cell = tvList.dequeueReusableCell(withIdentifier: "CloseGroupCell", for: indexPath as IndexPath) as! CloseGroupCell
                let dict = ary_MyCloseGroup.object(at: indexPath.row)as! NSDictionary
                cell.lblGroupName.text = "\(dict.value(forKey: "GroupName")!)"
                cell.lbltotalMemberCount.text = "\(dict.value(forKey: "AcceptMember")!)"
                cell.lbltotalMemberCount.layer.cornerRadius = 20
                cell.lbltotalMemberCount.layer.masksToBounds = true
                cell.lbltotalMemberCount.translatesAutoresizingMaskIntoConstraints = false
                cell.lbltotalMemberCount.backgroundColor = hexStringToUIColor(hex: primaryDark)
               
                return cell
           
          }else{   // My Joined Group
                let cell = tvList.dequeueReusableCell(withIdentifier: "JoinedGroupCell", for: indexPath as IndexPath) as! CloseGroupCell
                let dict = ary_JoinedGroup.object(at: indexPath.row)as! NSDictionary
                cell.lblGroupName.text = "\(dict.value(forKey: "GroupName")!)"
                return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(self.tvList.tag == 0){
            return UITableView.automaticDimension
        }else if(self.tvList.tag == 1){
           return ary_JoinedGroup.count != 0 ? 100 : UITableView.automaticDimension
        }else{
            return 0
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       if(self.tvList.tag == 0){
        let testController = mainStoryboard.instantiateViewController(withIdentifier: "CloseGroupDetailVC")as! CloseGroupDetailVC
        let dict = ary_MyCloseGroup.object(at: indexPath.row)as! NSDictionary
        testController.dictData = dict.mutableCopy()as! NSMutableDictionary
        self.navigationController?.pushViewController(testController, animated: true)
        
        }
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if(self.tvList.tag == 0){
            if (editingStyle == UITableViewCell.EditingStyle.delete) {
                let alert = UIAlertController(title: "Delete?", message: "Do you want to Delete group.", preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "Delete", style: .default, handler: { action in
                    self.callDeleteCloseGroupAPI(index: indexPath.row)
                }))
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                    
                }))
                self.present(alert, animated: true, completion: nil)
            }
        }else if (self.tvList.tag == 1) {
            if (editingStyle == UITableViewCell.EditingStyle.delete) {
                let alert = UIAlertController(title: "Remove?", message: "Do you want to Remove group.", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Remove", style: .default, handler: { action in
                    self.callRemoveJoinedGroupAPI(index: indexPath.row)
                }))
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                    
                }))
                self.present(alert, animated: true, completion: nil)
                
            }
        }
       
    }
    @objc func actionONError(sender : UIButton) {
        if(self.tvList.tag == 0){
            self.callMyCloseGroupAPI()
        }else{
           self.callJoinedGroupAPI()
        }
    }
}
// MARK: - --------------Close Group
// MARK: -
class CloseGroupCell: UITableViewCell {
    //DashBoard
    @IBOutlet weak var lblGroupName: UILabel!
    @IBOutlet weak var lbltotalMemberCount: UILabel!

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}
