//
//  OrderHistoryVC.swift
//  CitizenCOP
//
//  Created by Saavan Patidar on 14/04/20.
//  Copyright © 2020 CitizenCop. All rights reserved.
//

import UIKit

class OrderHistoryVC: UIViewController {

    // MARK: - ----IBOutlet
    @IBOutlet weak var view_ForHeader: UIView!
    @IBOutlet weak var tvlist: UITableView!

    // MARK: - ----Variables
    var strTitle = String()
    var arrOfOrders = NSArray()
    var refresher = UIRefreshControl()

    // MARK: - ----View Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        view_ForHeader.backgroundColor = hexStringToUIColor(hex: primaryTheamColor)
        tvlist.tableFooterView = UIView()
        tvlist.estimatedRowHeight = 80.0
        self.refresher.addTarget(self, action: #selector(RefreshloadData), for: .valueChanged)
        self.tvlist!.addSubview(refresher)

        //callOrderHistoryAPI()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        
        callOrderHistoryAPI()

    }
    // MARK: - --------------Pull Refresh
       // MARK: -
       @objc func RefreshloadData() {
          callOrderHistoryAPI()
       }
    
    // MARK: - ----Button Actions

    @IBAction func actiononBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func actiononOrderNow(_ sender: UIButton) {
        
        let testController = mainStoryboard.instantiateViewController(withIdentifier: "OnlineRashanOrderVC")as! OnlineRashanOrderVC
        testController.strTitle = self.strTitle
        self.navigationController?.pushViewController(testController, animated: true)
        
    }
    
    // MARK: - ----Functions

    func callOnNoOrderAvailable() {
        
        self.tvlist.isHidden = true
        
        let alertController = UIAlertController(title: alertInfo, message: "No order available. \n In this history only Standard Rashan Pack are visible.", preferredStyle: .alert)
        // Create the actions
        
        let okAction = UIAlertAction(title: "Order Now", style: UIAlertAction.Style.default)
        {
            UIAlertAction in
            
            let testController = mainStoryboard.instantiateViewController(withIdentifier: "OnlineRashanOrderVC")as! OnlineRashanOrderVC
            testController.strTitle = self.strTitle
            self.navigationController?.pushViewController(testController, animated: true)
            
            
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel)
        {
            UIAlertAction in
            NSLog("Cancel Pressed")
            self.navigationController?.popViewController(animated: false)

        }
        // Add the actions
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    // MARK: - ----Api Calling Functions


    func callOrderHistoryAPI() {
        
        if !isInternetAvailable() {
            
            FTIndicator.showToastMessage(alertInternet)
            
        }else{
            
            
          let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><GetGroceryOrder xmlns='http://m.citizencop.org/'><SecureKey>\(DeviceID)</SecureKey></GetGroceryOrder></soap12:Body></soap12:Envelope>"
                        
            customDotLoaderShowOnFull(message: "", controller: self)

            WebServiceClass.callSOAP_APIBY_POST(soapMessage: soapMessage, url: BaseURL3, resultype: "GetGroceryOrderResult", responcetype: "GetGroceryOrderResponse") { (responce, status) in
                customeDotLoaderRemove()
                self.refresher.endRefreshing()

                if status == "Suceess"{
                    
                    self.arrOfOrders = NSArray()
                    
                    if responce.value(forKey: "data") is NSArray {
                        
                        self.arrOfOrders = responce.value(forKey: "data") as! NSArray
                        
                        if self.arrOfOrders.count == 0 {
                            
                            self.callOnNoOrderAvailable()
                            
                        } else {
                            
                            self.tvlist.isHidden = false
                            self.tvlist.reloadData()

                        }
                                                
                    }else{
                        
                        self.callOnNoOrderAvailable()

                    }
                    
                    
                }else{
                    
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                    
                }
            }
        }
    }
    
}


// MARK: - ----------------UITableViewDelegate
// MARK: -

extension OrderHistoryVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrOfOrders.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tvlist.dequeueReusableCell(withIdentifier: "OrderHistoryCell", for: indexPath as IndexPath) as! OrderHistoryCell
        let dict = arrOfOrders.object(at: indexPath.row)as! NSDictionary
        
        if "\(dict.value(forKey: "Category")!)" == "Standard Rashan Pack" {
            
            cell.imgViewCategoryType.image = UIImage(named: "rashan_img.png")

        } else {
            
            cell.imgViewCategoryType.image = UIImage(named: "pori_bhaji.png")

        }
        
        cell.lblGroceryId.text = "Grocery Order Id: \(dict.value(forKey: "GroceryOrderId")!)"
        cell.lblName.text = "Name: \(dict.value(forKey: "Name")!)"
        cell.lblMobileNo.text = "Mobile No: \(dict.value(forKey: "MobileNo")!)"
        cell.lblAddress.text = "Address: \(dict.value(forKey: "DeliveryAddress")!)"
        cell.lblLandmark.text = "Landmark: \(dict.value(forKey: "Landmark")!)"
        cell.lblTransactionId.text = "Transaction Id: \(dict.value(forKey: "PayTM_TranjectionId")!)"
        cell.lblPaidVia.text = "Paid Via: \(dict.value(forKey: "GatewayName")!)"
        cell.lblAmount.text = "Amount: ₹ \(dict.value(forKey: "TotalAmount")!)"
        cell.lblQuantity.text = "Quantity: \(dict.value(forKey: "Quantity")!)"
        cell.lblDate.text = "Date: \(dict.value(forKey: "CreatedDate1")!)"
        
        tvlist.separatorColor = hexStringToUIColor(hex: primaryTheamColor)

        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
           return UITableView.automaticDimension
       
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
       
        return 0.0
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let vw = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 40))
        vw.backgroundColor = hexStringToUIColor(hex: "E8E8E8")
        
        let lblHeader = UILabel()
        lblHeader.frame = vw.frame
        lblHeader.text = "Your Order Detail"
        lblHeader.font = UIFont.systemFont(ofSize: 20)
        lblHeader.font = UIFont(name: "Helvetica-Bold", size: 20)
        lblHeader.textColor = hexStringToUIColor(hex: primaryTheamColor)
        lblHeader.textAlignment = .center
        vw.addSubview(lblHeader)
        
        return vw
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
                
        return 40.0

    }

    

}
// MARK: - ----------------NewsCell
// MARK: -
class OrderHistoryCell: UITableViewCell {
    
    @IBOutlet weak var lblGroceryId: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblMobileNo: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblLandmark: UILabel!
    @IBOutlet weak var lblPaidVia: UILabel!
    @IBOutlet weak var lblTransactionId: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblQuantity: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    
    @IBOutlet weak var imgViewCategoryType: UIImageView!

    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}
