//
//  DynemicSplashVC.swift
//  CitizenCOP
//
//  Created by Navin Patidar on 5/17/19.
//  Copyright © 2019 CitizenCop. All rights reserved.
//

import UIKit

class DynemicSplashVC: UIViewController {
    
    @IBOutlet weak var imgView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        if let statusBar = UIApplication.shared.value(forKey: "statusBar") as? UIView {
//            statusBar.backgroundColor = UIColor.clear
//        }
        
        if #available(iOS 13.0, *) {
            let window = UIApplication.shared.windows.first
            let topPadding = window?.safeAreaInsets.top
            let statusBar = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: topPadding ?? 0.0))
            statusBar.backgroundColor = hexStringToUIColor(hex: primaryTheamColor)
            UIApplication.shared.windows.filter {$0.isKeyWindow}.first?.addSubview(statusBar)
            
        }
        
        if(nsud.value(forKey: "CitizenCOP_CityData") != nil){
            let isLversionC = "\((nsud.value(forKey: "CitizenCOP_CityData")as! NSDictionary).value(forKey: "isLversionC")!)"
            if(isLversionC == "0"){
                let hostUrl = "\((nsud.value(forKey: "CitizenCOP_CityData")as! NSDictionary).value(forKey: "Citizen_HostUrl")!)"
                 BaseHostURL = hostUrl
            }else{
               BaseHostURL = "http://m.citizencop.org";
            }
        }
      //  BaseHostURL = "http://m.citizencop.org";

        let dictDashData = getDataFromCoreDataBaseDict(strEntity: "DashBoardCityData", strkey: "dashBoardCityData")
        if(dictDashData.value(forKey: "CityData")as! NSArray).count != 0{
            let imgUrl = ((dictDashData.value(forKey: "CityData")as! NSArray).object(at: 0)as! NSDictionary).value(forKey: "CityLogo")as! String
            let urlImage = "https://m.citizencop.org/images/\(imgUrl)"
            if(urlImage.count != 0){
                imgView.setImageWith(URL(string: urlImage), placeholderImage: UIImage(named: ""), options: SDWebImageOptions(rawValue: 1), completed: { (image, error, type, url) in
                    print(url ?? 0)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) { // Change `2.0` to the desired number of seconds.
                    
                            let testController = mainStoryboard.instantiateViewController(withIdentifier: "DashBoardVC")as! DashBoardVC
                            self.navigationController?.pushViewController(testController, animated: true)
                    }
                    
                }, usingActivityIndicatorStyle: .gray)
            }else{
                    let testController = mainStoryboard.instantiateViewController(withIdentifier: "DashBoardVC")as! DashBoardVC
                    self.navigationController?.pushViewController(testController, animated: true)
                
             
            }
            
        }
        else{
            let testController = mainStoryboard.instantiateViewController(withIdentifier: "CitySelectionVC")as! CitySelectionVC
            self.navigationController?.pushViewController(testController, animated: true)
        }
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
