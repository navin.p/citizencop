//
//  CitizenEye_ForfotPassVC.swift
//  CitizenCOP
//
//  Created by Navin Patidar on 05/08/22.
//

import UIKit

class CitizenEye_ForgotPassVC: UIViewController {

  
    // MARK: - --------------IBOutlet
     // MARK: -
    var strTitle = ""
     @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var txt_PersonalD_EmailAddress: ACFloatingTextfield!
    @IBOutlet weak var btnsubmit: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        txt_PersonalD_EmailAddress.delegate = self
     
        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryTheamColor)
        self.lblTitle.text = strTitle
        btnsubmit.layer.cornerRadius = 8.0
        btnsubmit.backgroundColor = hexStringToUIColor(hex: primaryDark)
        
    }
    
    // MARK: - ---------------IBAction
       // MARK: -
       @IBAction func actiononBack(_ sender: UIButton) {
           self.navigationController?.popViewController(animated: true)
       }
   
    @IBAction func actiononSubmit(_ sender: UIButton) {
        self.view.endEditing(true)
        if(txt_PersonalD_EmailAddress.text == ""){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Email address is required!", viewcontrol: self)
        }else if !(validateEmail(email: txt_PersonalD_EmailAddress.text!)){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Email address is invalid!", viewcontrol: self)
        }else{
            callForgotAPI(sender: sender)
        }
    }
   
    //MARK:*********
    //MARK: API Calling
    func callForgotAPI(sender : UIButton) {
        if !isInternetAvailable() {
            FTIndicator.showToastMessage(alertInternet)
            
        }else{
            let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><ForgetPassword xmlns='\(BaseHostURL)/'><emailid>\(txt_PersonalD_EmailAddress.text!)</emailid></ForgetPassword></soap12:Body></soap12:Envelope>"
            let loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
            
            self.present(loader, animated: false, completion: nil)
            
            WebServiceClass.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: BaseURLCitizenEye, resultype: "ForgetPasswordResult", responcetype: "ForgetPasswordResponse") { (responce, status) in
                loader.dismiss(animated: false) {
                    if status == "Suceess"{
                        let dictTemp = responce.value(forKey: "data")as! NSDictionary
                        if("\(dictTemp.value(forKey: "Result")!)" == "True"){
                            let alert = UIAlertController(title: alertMessage, message: "\(dictTemp.value(forKey: "Msg")!)", preferredStyle: UIAlertController.Style.alert)
                            
                            // add the actions (buttons)
                            alert.addAction(UIAlertAction (title: "OK", style: .default, handler: { (nil) in
                                self.navigationController?.popViewController(animated: true)

                            }))
                            self.present(alert, animated: true, completion: nil)
                        }else{
                            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                        }
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                    }
                }
            }
        }
    }
    
   
}
// MARK: - ---------------UITextFieldDelegate
// MARK: -

extension CitizenEye_ForgotPassVC : UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if ( textField == txt_PersonalD_EmailAddress)  {
            return  txtFiledValidation(textField: textField, string: string, returnOnly: "ALL", limitValue: 90)
        }
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true

    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.view.endEditing(true)
    }
}
