//
//  AddCameraDetailsVC.swift
//  CitizenCOP
//
//  Created by Navin Patidar on 12/3/19.
//  Copyright © 2019 CitizenCop. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit

class AddCameraDetailsVC: UIViewController {
    // MARK: - ------------IBOutlet
    // MARK: -
    var locationManager = CLLocationManager()
    @IBOutlet weak var viewHeader: CardView!
    
    @IBOutlet weak var txtVAddress: KMPlaceholderTextView!
    
    
    @IBOutlet weak var txt_NoOfCamera: ACFloatingTextfield!
    @IBOutlet weak var txt_Area: ACFloatingTextfield!
    @IBOutlet weak var txt_PersonContactName: ACFloatingTextfield!
    @IBOutlet weak var txt_PersonContactNumber: ACFloatingTextfield!
    @IBOutlet weak var txt_CameraURL: ACFloatingTextfield!
    @IBOutlet weak var txt_PortNo: ACFloatingTextfield!
    @IBOutlet weak var txt_CameraCompanyName: ACFloatingTextfield!

    @IBOutlet weak var txt_UserName: ACFloatingTextfield!
    @IBOutlet weak var txt_Password: ACFloatingTextfield!
    @IBOutlet weak var btnSubmit: UIButton!
    
    @IBOutlet weak var img_Camera: UIImageView!
    @IBOutlet weak var btnBrowse: UIButton!

    @IBOutlet weak var viewContain: UIView!
    @IBOutlet weak var switchisInternet: UISwitch!

    @IBOutlet weak var stkView: UIStackView!
    @IBOutlet weak var txt_ownerName: ACFloatingTextfield!
    @IBOutlet weak var txt_shopName: ACFloatingTextfield!
    
    

    var strLatAddress = String()
    var strLongAddress = String()
    var aryAreaList = NSMutableArray()
    var dictEyeUserData = NSMutableDictionary()
    var dictCameraImage = NSMutableDictionary()
    var imagePicker = UIImagePickerController()

    // MARK: - --------------Life Cycle
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryTheamColor)
        btnSubmit.layer.cornerRadius = 8.0
        btnSubmit.backgroundColor = hexStringToUIColor(hex: primaryDark)
        btnBrowse.layer.cornerRadius = 12.0
        btnBrowse.backgroundColor = hexStringToUIColor(hex: primaryDark)
        
        print(dictEyeUserData)
        dictEyeUserData = (nsud.value(forKey: "CitizenCOP_CitizenEyeData")as! NSDictionary).mutableCopy()as! NSMutableDictionary
        print(dictEyeUserData)
        checkLocationPermission()
        getAddressFromLatLon(pdblLatitude: GlobleLat, withLongitude: GlobleLong)
        callAreaDropDownAPI()
    }
    override func viewDidDisappear(_ animated: Bool) {
        locationManager.stopUpdatingLocation()
    }
    // MARK: - --------------IBAction
    // MARK: -
    @IBAction func actionOnBack(_ sender: UIButton) {
                    self.navigationController?.popViewController(animated: true)

    }
    @IBAction func actionOnSelectAddress(_ sender: UIButton) {
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "AddressGetFromMapVC")as! AddressGetFromMapVC
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .coverVertical
        vc.delegate = self
        self.present(vc, animated: true, completion: nil)
        
    }
    
    @IBAction func actionOnArea(_ sender: UIButton) {
        let vc: SelectionClass = mainStoryboard.instantiateViewController(withIdentifier: "SelectionClass") as! SelectionClass
        vc.strTag = 12
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .coverVertical
        vc.delegate = self
        vc.strTitle = "Select Area"
        if aryAreaList.count != 0{
            vc.aryList = aryAreaList
            self.present(vc, animated: true, completion: {})
        }else{
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertDataNotFound, viewcontrol: self)
        }
    }
    
    @IBAction func actionOnSwitch(_ sender: Any) {
        if(switchisInternet.isOn){
            stkView.isHidden = false
        }else{
            stkView.isHidden = true
        }
    }
    @IBAction func actionOnSubmit(_ sender: UIButton) {
        self.view.endEditing(true)
        if(validation().count == 0){

            
            
            let dictSendData = NSMutableDictionary()
            dictSendData.setValue("0", forKey: "cctvcameralocationid")
            dictSendData.setValue("\(dictEyeUserData.value(forKey: "userid")!)", forKey: "userid")
            dictSendData.setValue("\(txt_Area.tag)", forKey: "locationareaid")
            dictSendData.setValue("\(txt_Area.tag)", forKey: "cityid")
            dictSendData.setValue("\(strLatAddress)", forKey: "latitude")
            dictSendData.setValue("\(strLongAddress)", forKey: "longitude")
            dictSendData.setValue("\(txtVAddress.text!)", forKey: "location")
            dictSendData.setValue("\(txt_NoOfCamera.text!)", forKey: "noofcamera")
            dictSendData.setValue("\(txt_CameraURL.text!)", forKey: "cameraurl")
            dictSendData.setValue("\(txt_PortNo.text!)", forKey: "portno")
            dictSendData.setValue("\(txt_UserName.text!)", forKey: "username")
            dictSendData.setValue("\(txt_Password.text!)", forKey: "password")
            dictSendData.setValue("\(txt_PersonContactName.text!)", forKey: "pocname")
            dictSendData.setValue("\(txt_PersonContactNumber.text!)", forKey: "poccontactno")
            dictSendData.setValue("\(txt_ownerName.text!)", forKey: "ownername")
            dictSendData.setValue("\(txt_shopName.text!)", forKey: "shopname")
            dictSendData.setValue("", forKey: "pocemailid")
            
            
            var strIsInternet = "true"
            if(switchisInternet.isOn){
                strIsInternet = "true"
            }else{
                strIsInternet = "false"
            }
            
            dictSendData.setValue(strIsInternet, forKey: "isinternet")
            dictSendData.setValue("\(txt_CameraCompanyName.text ?? "")", forKey: "cameraname")
            var imgname = ""
            if(dictCameraImage.count != 0){
                imgname = "\(dictCameraImage.value(forKey: "imageName") ?? "")"
            }
            dictSendData.setValue(imgname, forKey: "qrcodeimage")
            dictSendData.setValue("", forKey: "cameraapplicationdetail")

            UploadImagesOnServer(dictData: dictSendData)
        }else{
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: validation(), viewcontrol: self)
        }
    }
    @IBAction func actionOnBrowse(_ sender: UIButton) {
        let alert = UIAlertController(title: "", message: "Please Select an Option", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Capture Image", style: .default , handler:
            { (UIAlertAction)in
                
      
                    
                    if UIImagePickerController.isSourceTypeAvailable(.camera)
                    {
                        print("Button capture")
                        
                        self.imagePicker.delegate = self
                        self.imagePicker.sourceType = .camera
                        self.imagePicker.allowsEditing = false
                        self.present(self.imagePicker, animated: true, completion: nil)
                    }
                    else
                    {
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "No device available", viewcontrol: self)
                        
                    }
                    
         
                
                
        }))
        
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default , handler:{ (UIAlertAction)in
            
             
                if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum)
                {
                    print("Button capture")
                    self.imagePicker.delegate = self
                    self.imagePicker.sourceType = .savedPhotosAlbum
                    self.imagePicker.allowsEditing = false
                    self.present(self.imagePicker, animated: true, completion: nil)
                }
            
        }))
        
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
        }))
    
        alert.popoverPresentationController?.sourceView = self.view
     
        if(DeviceType.IS_IPAD)
        {
            alert.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            alert.popoverPresentationController?.permittedArrowDirections = []
        }
        
        self.present(alert, animated: true, completion: {
        })
    }
    func getAddressFromLatLon(pdblLatitude: String, withLongitude pdblLongitude: String) {
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        let lat: Double = Double("\(pdblLatitude)")!
        //21.228124
        let lon: Double = Double("\(pdblLongitude)")!
        //72.833770
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = lat
        center.longitude = lon
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                if (error != nil)
                {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                    return
                }
                let pm = placemarks! as [CLPlacemark]
                if pm.count > 0 {
                    let pm = placemarks![0]
           
                    var addressString : String = ""
                    if pm.subLocality != nil {
                        addressString = addressString + pm.subLocality! + ", "
                    }
                    if pm.thoroughfare != nil {
                        addressString = addressString + pm.thoroughfare! + ", "
                    }
                    if pm.locality != nil {
                        addressString = addressString + pm.locality! + ", "
                    }
                    if pm.country != nil {
                        addressString = addressString + pm.country! + ", "
                    }
                    if pm.postalCode != nil {
                        addressString = addressString + pm.postalCode! + " "
                    }
                    self.txtVAddress.text = addressString
                    print(addressString)
                }
        })
    }
    func validation() -> String {
        if(txtVAddress.text.count == 0){
            return "Location is required!"
        }
        else if(txt_NoOfCamera.text!.count == 0){
            return "No. of external cameras / Road facing cameras is required!"
        }
        else if(txt_Area.text!.count == 0){
            return "Area is required!"
        }
        else if(txt_PersonContactName.text!.count == 0){
            return "Person Contact Name is required!"
        }else if(txt_PersonContactNumber.text!.count == 0){
            return "Person Mobile Number is required!"
        }
        else if ((txt_PersonContactNumber.text?.count)! < 10){
            return alert_Mobile_limit
        }else if txt_ownerName.text == "" {
            return alert_ownerName
        }else if txt_shopName.text == "" {
            return alert_ShopName
        }
        if(switchisInternet.isOn){
            if(txt_CameraURL.text!.count == 0){
                return "Camera URL is required!"
            }else if(txt_UserName.text!.count == 0){
                return "User Name is required!"
            }else if(txt_Password.text!.count == 0){
                return "Password is required!"
            }
        }

      
//
        return ""
    }
    // MARK: - ----------------Extra Function
    // MARK: -
    func checkLocationPermission() {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.requestAlwaysAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
        // locationManager.requestLocation()
        print("Location services are not enabled")
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                let alert = UIAlertController(title: "Location Unavailable", message: "Please check to see if device settings doesn't allow Location access", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction (title: "Settings", style: .default, handler: { (nil) in
                    
                    guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                        return
                    }
                    
                    if UIApplication.shared.canOpenURL(settingsUrl) {
                        UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                            print("Settings opened: \(success)") // Prints true
                        })
                    }
                }))
                self.present(alert, animated: true, completion: nil)
            case .authorizedAlways, .authorizedWhenInUse: break
                
            default:
                break
            }
        } else {
            
        }
    }
    
    // MARK: - ---------------API Calling
    // MARK: -
    func callAreaDropDownAPI() {
        self.view.endEditing(true)
        if !isInternetAvailable() {
            FTIndicator.showToastMessage(alertInternet)
            
        }else{
            let strCityId = "\(((dictCityData.value(forKey: "CityData")as! NSArray).object(at: 0)as! NSDictionary).value(forKey: "city_id")!)"
            
            
            let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><GetLocationAreaDropdown xmlns='\(BaseHostURL)/'><cityid>\(strCityId)</cityid></GetLocationAreaDropdown></soap12:Body></soap12:Envelope>"
            
            customDotLoaderShowOnFull(message: "", controller: self)
            
            WebServiceClass.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: BaseURLCitizenEye, resultype: "GetLocationAreaDropdownResult", responcetype: "GetLocationAreaDropdownResponse") { (responce, status) in
                customeDotLoaderRemove()
                if status == "Suceess"{
                    let dictTemp = (responce.value(forKey: "data")as! NSDictionary).mutableCopy()as! NSMutableDictionary
                    if("\(dictTemp.value(forKey: "Result")!)" == "True"){
                        self.aryAreaList =  NSMutableArray()
                        self.aryAreaList = (dictTemp.value(forKey: "LocationArea")as! NSArray).mutableCopy()as! NSMutableArray
                        
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(alertDataNotFound)", viewcontrol: self)
                    }
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            }
        }
    }
    func UploadImagesOnServer(dictData : NSMutableDictionary)  {
        let loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
        
        self.present(loader, animated: false, completion: nil)
        if(dictCameraImage.count != 0){
            let img = dictCameraImage.value(forKey: "image") as! UIImage
            let imageData = img.jpegData(compressionQuality:0.5)
            
            WebServiceClass.callAPIWithImage(parameter: NSDictionary(), url: API_UploadCameraImage, data: imageData!, fileName: "\(dictCameraImage.value(forKey: "imageName")!)", withName: "userfile") { (responce, status) in
                print(responce)
                loader.dismiss(animated: false) {
                    if(status == "Suceess"){
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                            self.callSaveCameraDetailAPI(dictData: dictData)
                        }
                      
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)

                    }
                }
             
            }
        }else{
            self.callSaveCameraDetailAPI(dictData: dictData)

        }
         
    }
  

    
    func callSaveCameraDetailAPI(dictData  : NSMutableDictionary ) {
        self.view.endEditing(true)
        if !isInternetAvailable() {
            FTIndicator.showToastMessage(alertInternet)
            
        }else{
            
            var json = Data()
            var jsonString = NSString()
            if JSONSerialization.isValidJSONObject(dictData) {
                // Serialize the dictionary
                json = try! JSONSerialization.data(withJSONObject: dictData, options: .prettyPrinted)
                jsonString = String(data: json, encoding: .utf8)! as NSString
                print("UpdateLeadinfo JSON: \(jsonString)")
            }
            
            let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><AddUpdateCCTVCameraLocation xmlns='\(BaseHostURL)/'><CCTVCameraLocationMdl>\(jsonString)</CCTVCameraLocationMdl></AddUpdateCCTVCameraLocation></soap12:Body></soap12:Envelope>"
            let loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
            
            self.present(loader, animated: false, completion: nil)
            WebServiceClass.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: BaseURLCitizenEye, resultype: "AddUpdateCCTVCameraLocationResult", responcetype: "AddUpdateCCTVCameraLocationResponse") { (responce, status) in
                loader.dismiss(animated: false) {
                    if status == "Suceess"{
                        
                        let dictTemp = (responce.value(forKey: "data")as! NSDictionary).mutableCopy()as! NSMutableDictionary
                        if("\(dictTemp.value(forKey: "Result")!)" == "True"){
                            let alert = UIAlertController(title: alertMessage, message: "\(dictTemp.value(forKey: "Msg")!)", preferredStyle: UIAlertController.Style.alert)
                            // add the actions (buttons)
                            alert.addAction(UIAlertAction (title: "OK", style: .default, handler: { (nil) in
                                for controller in self.navigationController!.viewControllers as Array {
                                    if controller.isKind(of: CameraHistroyVC.self) {
                                        self.navigationController!.popToViewController(controller, animated: true)
                                        break
                                    }
                                }
                            }))
                            self.present(alert, animated: true, completion: nil)
                        }else{
                            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(dictTemp.value(forKey: "Msg")!)", viewcontrol: self)
                        }
                     }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                    }
                }
                
           
            }
        }
    }
}

//MARK:-
//MARK:- ---------AddressFromMapScreenDelegate------

extension AddCameraDetailsVC: AddressFromMapScreenDelegate {
    func GetAddressFromMapScreen(dictData: NSDictionary, tag: Int) {
        self.txtVAddress.text = (dictData.value(forKey: "address")as! String)
        strLatAddress = (dictData.value(forKey: "lat")as! String)
        strLongAddress = (dictData.value(forKey: "long")as! String)
        
    }
    
    
}
//MARK:-
//MARK:- ---------PopUpDelegate

extension AddCameraDetailsVC : PopUpDelegate
{
    func getDataFromPopupDelegate(dictData: NSDictionary, tag: Int) {
        txt_Area.text =  "\(dictData.value(forKey: "locationarea")!)"
        txt_Area.tag =  Int("\(dictData.value(forKey: "locationareaid")!)")!
    }
}
// MARK: - ---------------UITextFieldDelegate
// MARK: -

extension AddCameraDetailsVC : UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if (textField == txt_NoOfCamera )  {
            return  txtFiledValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 90)
        }
        else if (textField == txt_PersonContactNumber )  {
            return  txtFiledValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 9)
        }
        else{
            return  txtFiledValidation(textField: textField, string: string, returnOnly: "ALL", limitValue: 150)
        }
        
        
        
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
        
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.view.endEditing(true)
    }
}
// MARK: - ----------------UIImagePickerControllerDelegate
// MARK: -

extension AddCameraDetailsVC : UINavigationControllerDelegate, UIImagePickerControllerDelegate
{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        
        self.view.endEditing(true)
       dictCameraImage = NSMutableDictionary()
       dictCameraImage.setValue(info[UIImagePickerController.InfoKey.originalImage] as? UIImage, forKey: "image")
       dictCameraImage.setValue(uniqueString() + ".jpg", forKey: "imageName")

       img_Camera.image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        
        self.dismiss(animated: true, completion: { () -> Void in
            
        })
        
    }
 
 
}
//MARK:-
//MARK:- ---------CLLocationManagerDelegate
extension AddCameraDetailsVC : CLLocationManagerDelegate{
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        GlobleLat = "\(locValue.latitude)"
        GlobleLong = "\(locValue.longitude)"
    }
}
