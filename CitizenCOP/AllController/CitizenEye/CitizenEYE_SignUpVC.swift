//
//  CitizenEYE_SignUpVC.swift
//  CitizenCOP
//
//  Created by Navin Patidar on 05/08/22.
//

import UIKit

class CitizenEYE_SignUpVC: UIViewController {

    // MARK: - --------------IBOutlet
     // MARK: -
    var strTitle = ""
    var aryPoliceStation = NSMutableArray() , aryArea = NSMutableArray()
    
     @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var txt_EmailAddress: ACFloatingTextfield!
    @IBOutlet weak var txt_Password: ACFloatingTextfield!
    @IBOutlet weak var txt_Name: ACFloatingTextfield!
    @IBOutlet weak var txt_Policestation: ACFloatingTextfield!
    @IBOutlet weak var txt_Area: ACFloatingTextfield!
    @IBOutlet weak var txt_Mobile: ACFloatingTextfield!
    @IBOutlet weak var txt_UserType: ACFloatingTextfield!
    @IBOutlet weak var txt_businessType: ACFloatingTextfield!
    @IBOutlet weak var txt_Address: ACFloatingTextfield!
    
    @IBOutlet weak var btnSignup: UIButton!
    
    
   
    override func viewDidLoad() {
        super.viewDidLoad()
        txt_EmailAddress.delegate = self
        txt_Password.delegate = self
        txt_Name.delegate = self
        txt_Mobile.delegate = self

        lblTitle.text = strTitle
        if #available(iOS 13.0, *) {
            txt_Password?.isSecureTextEntry = true
            txt_Password!.enablePasswordToggle()
           
        } else {
            
        }
        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryTheamColor)
        self.lblTitle.text = strTitle
        btnSignup.layer.cornerRadius = 8.0
        btnSignup.backgroundColor = hexStringToUIColor(hex: primaryDark)
        let strCityId = "\(((dictCityData.value(forKey: "CityData")as! NSArray).object(at: 0)as! NSDictionary).value(forKey: "city_id")!)"

        callPoliceStationAPI(strCityID: strCityId)
    }
    
    // MARK: - ---------------IBAction
       // MARK: -
       @IBAction func actiononBack(_ sender: UIButton) {
           self.navigationController?.popViewController(animated: true)
       }
    @IBAction func actiononLogin(_ sender: UIButton) {
        self.view.endEditing(true)
        if(txt_Policestation.text == ""){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: " Please select Police station!", viewcontrol: self)
        }
       else if(txt_Area.text == ""){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: " Please select area!", viewcontrol: self)
        }
        else if(txt_Name.text == ""){
             showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Name is required!", viewcontrol: self)
         }
        else if  (txt_EmailAddress.text != "") && !(validateEmail(email: txt_EmailAddress.text!)){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Email address is invalid!", viewcontrol: self)
        }else if(txt_Password.text == ""){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Password is required!", viewcontrol: self)
        }else if txt_UserType.text == "" {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "User Type is required!", viewcontrol: self)
        }else if txt_businessType.text == "" {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Business Type is required!", viewcontrol: self)
        }else if txt_Address.text == "" {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Address is required!", viewcontrol: self)
        }
      
        else{
            callSignupAPI(sender: sender)
        }
    }
    
    @IBAction func actionSelectPoliceStation(_ sender: UIButton) {
        if self.aryPoliceStation.count != 0 {
            let vc: SelectionClass = mainStoryboard.instantiateViewController(withIdentifier: "SelectionClass") as! SelectionClass
            vc.strTitle = "Select"
            vc.strTag = 18
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .coverVertical
            vc.delegate = self
            vc.aryList = aryPoliceStation
            self.present(vc, animated: true, completion: {})
        }
    }
    @IBAction func actionSelectArea(_ sender: UIButton) {
        if(txt_Policestation.tag != 0){
            callLocationAPI(strID: "\(txt_Policestation.tag)")
        }else{
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select police station first!", viewcontrol: self)
        }
    }
    
    @IBAction func actionSelectUserType(_ sender: UIButton) {

        let arrayUserType = NSMutableArray()
        let userType1 = ["userType" : "Organization"]
        let userType2 = ["userType" : "Indivisual"]
        arrayUserType.add(userType1)
        arrayUserType.add(userType2)
        
        let vc: SelectionClass = mainStoryboard.instantiateViewController(withIdentifier: "SelectionClass") as! SelectionClass
        vc.strTitle = "Select User Type"
        vc.strTag = 20
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .coverVertical
        vc.delegate = self
        vc.aryList = arrayUserType
        self.present(vc, animated: true, completion: {})
    }
    
}
// MARK: - ---------------UITextFieldDelegate
// MARK: -

extension CitizenEYE_SignUpVC : UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if (textField == txt_Mobile )  {
            return  txtFiledValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 11)
        }
        return  txtFiledValidation(textField: textField, string: string, returnOnly: "ALL", limitValue: 90)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true

    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.view.endEditing(true)
    }
    // MARK: - ---------------API CAlling
    // MARK: -
    func callPoliceStationAPI(strCityID : String ) {
        if !isInternetAvailable() {
            FTIndicator.showToastMessage(alertInternet)
            
        }else{
            let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><GetPoliceStationCondition xmlns='\(BaseHostURL)/'><city_id>\(strCityID)</city_id></GetPoliceStationCondition></soap12:Body></soap12:Envelope>"
            let loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
            
            self.present(loader, animated: false, completion: nil)
            
            WebServiceClass.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: BaseURLCitizenEyestation, resultype: "GetPoliceStationConditionResult", responcetype: "GetPoliceStationConditionResponse") { (responce, status) in
                loader.dismiss(animated: false) {
                    if status == "Suceess"{
                        let dictTemp = responce.value(forKey: "data")as! NSDictionary
                        if("\(dictTemp.value(forKey: "Result")!)" == "True"){
                            
                            let ary = dictTemp.value(forKey: "PoliceStation")as! NSArray
                            if(ary.count != 0){
                                self.aryPoliceStation = NSMutableArray()
                                self.aryPoliceStation = ary.mutableCopy() as! NSMutableArray
                                
                             
                            }else{
                                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                                
                            }
                            
                        }else{
                            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Police Station not found", viewcontrol: self)
                        }
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                    }
                }
                
                
               
            }
        }
    }
    
    func callLocationAPI(strID : String ) {
        if !isInternetAvailable() {
            FTIndicator.showToastMessage(alertInternet)
           
        }else{
            let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><GetLocationAreaDropdownByPSId xmlns='\(BaseHostURL)/'><ps_id>\(strID)</ps_id></GetLocationAreaDropdownByPSId></soap12:Body></soap12:Envelope>"
            let loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
            
            self.present(loader, animated: false, completion: nil)
            
            WebServiceClass.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: BaseURLCitizenArea, resultype: "GetLocationAreaDropdownByPSIdResult", responcetype: "GetLocationAreaDropdownByPSIdResponse") { (responce, status) in
                loader.dismiss(animated: false) {
                    if status == "Suceess"{
                        let dictTemp = responce.value(forKey: "data")as! NSDictionary
                        if("\(dictTemp.value(forKey: "Result")!)" == "True"){
                            
                            let ary = dictTemp.value(forKey: "LocationArea")as! NSArray
                            if(ary.count != 0){
                                    let vc: SelectionClass = mainStoryboard.instantiateViewController(withIdentifier: "SelectionClass") as! SelectionClass
                                    vc.strTitle = "Select"
                                    vc.strTag = 19
                                    vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                                    vc.modalTransitionStyle = .coverVertical
                                    vc.delegate = self
                                vc.aryList = ary.mutableCopy() as! NSMutableArray
                                    self.present(vc, animated: true, completion: {})
                             
                            }else{
                                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                                
                            }
                            
                        }else{
                            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Area not found!", viewcontrol: self)
                        }
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                    }
                }
                
                
               
            }
        }
    }
 
   func callSignupAPI(sender : UIButton) {
       self.view.endEditing(true)
       if !isInternetAvailable() {
           FTIndicator.showToastMessage(alertInternet)
           
       }else{
        let strCityId = "\(((dictCityData.value(forKey: "CityData")as! NSArray).object(at: 0)as! NSDictionary).value(forKey: "city_id")!)"
           let strNumber  = "\(txt_Mobile.text!)".replacingOccurrences(of: " ", with: "")
           let dictData = NSMutableDictionary()
           dictData.setValue("\(txt_Name.text!)", forKey: "name")
           dictData.setValue("\(txt_EmailAddress.text!)", forKey: "emailid")
           dictData.setValue("\(strNumber)".replacingOccurrences(of: "-", with: ""), forKey: "mobilenumber")
           dictData.setValue("\(strCityId)", forKey: "cityid")
           
           dictData.setValue("\(txt_Password.text!)", forKey: "password")
           dictData.setValue("\(txt_Policestation.tag)", forKey: "ps_id")
           dictData.setValue("\(txt_Area.tag)", forKey: "locationareaid")
           dictData.setValue("\(txt_UserType.text!)", forKey: "usertype")
           dictData.setValue("\(txt_businessType.text!)", forKey: "businesstype")
           dictData.setValue("\(txt_Address.text!)", forKey: "address")
 
           var json = Data()
           var jsonString = NSString()
           if JSONSerialization.isValidJSONObject(dictData) {
               // Serialize the dictionary
               json = try! JSONSerialization.data(withJSONObject: dictData, options: .prettyPrinted)
               jsonString = String(data: json, encoding: .utf8)! as NSString
               print("UpdateLeadinfo JSON: \(jsonString)")
           }

           let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><AddUserRegistrationAndroid xmlns='\(BaseHostURL)/'><UserOTPMdl>\(jsonString)</UserOTPMdl></AddUserRegistrationAndroid></soap12:Body></soap12:Envelope>"

           let loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
           
           self.present(loader, animated: false, completion: nil)
           


           WebServiceClass.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: BaseURLCitizenEye, resultype: "AddUserRegistrationAndroidResult", responcetype: "AddUserRegistrationAndroidResponse") { (responce, status) in
               loader.dismiss(animated: false) {
                   if status == "Suceess"{

                       let dictTemp = (responce.value(forKey: "data")as! NSDictionary).mutableCopy()as! NSMutableDictionary
                       if("\(dictTemp.value(forKey: "Result")!)" == "True"){

                           let alert = UIAlertController(title: alertMessage, message: "\(dictTemp.value(forKey: "Msg")!)", preferredStyle: UIAlertController.Style.alert)
                           
                           // add the actions (buttons)
                           alert.addAction(UIAlertAction (title: "OK", style: .default, handler: { (nil) in
                               for controller in self.navigationController!.viewControllers as Array {
                                   if controller.isKind(of: CitizenEye_LoginVC.self) {
                                       self.navigationController!.popToViewController(controller, animated: true)
                                       break
                                   }
                               }
                               
                           
                           }))
                           self.present(alert, animated: true, completion: nil)
                           
                           

                       }else{
                           showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(dictTemp.value(forKey: "Msg")!)", viewcontrol: self)

                       }


                   }else{

                       showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)

                   }
               }
         
           }
    }
   }
}
//MARK:-
//MARK:- ---------PopUpDelegate

extension CitizenEYE_SignUpVC : PopUpDelegate
{
    func getDataFromPopupDelegate(dictData: NSDictionary, tag: Int) {
        if(tag == 18){
            if dictData.count != 0 {
                txt_Policestation.text = "\(dictData.value(forKey: "PoliceStationName") ?? "")"
                txt_Policestation.tag = Int("\(dictData.value(forKey: "PoliceStationId") ?? "0")")!

            }else{
                txt_Policestation.text = ""
                txt_Policestation.tag = 0

            }
        }
        else if(tag == 19){
            if dictData.count != 0 {
                txt_Area.text = "\(dictData.value(forKey: "LocationArea") ?? "")"
                txt_Area.tag = Int("\(dictData.value(forKey: "LocationAreaId") ?? "0")")!

            }else{
                txt_Area.text = ""
                txt_Area.tag = 0

            }
        }
        else if (tag == 20){
            if dictData.count != 0 {
                txt_UserType.text = "\(dictData.value(forKey: "userType") ?? "")"
            }else{
                txt_UserType.text = ""
            }
        }
    }
}
