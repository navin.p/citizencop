//
//  CitizenEyeVC.swift
//  CitizenCOP
//
//  Created by Navin Patidar on 12/2/19.
//  Copyright © 2019 CitizenCop. All rights reserved.
//

import UIKit

class CitizenEyeVC: UIViewController {
    // MARK: - --------------IBOutlet
     // MARK: -
     @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var txt_PersonalD_name: ACFloatingTextfield!
    @IBOutlet weak var txt_PersonalD_CountryCode: ACFloatingTextfield!
      @IBOutlet weak var txt_PersonalD_MobileNumber: ACFloatingTextfield!
     @IBOutlet weak var txt_PersonalD_Email: ACFloatingTextfield!
        @IBOutlet weak var btnPersonalDetailSubmit: UIButton!
    
    var arryCountry = NSMutableArray()
    // MARK: -
    var strTitle = String()
    // MARK: - ------------Life Cycle
      // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryTheamColor)
        self.lblTitle.text = strTitle
         btnPersonalDetailSubmit.layer.cornerRadius = 8.0
            btnPersonalDetailSubmit.backgroundColor = hexStringToUIColor(hex: primaryDark)
        
        
        if((nsud.value(forKey: "CitizenCOP_CitizenEyeData")) != nil){
            if((nsud.value(forKey: "CitizenCOP_CitizenEyeData")as! NSDictionary).count != 0){
                let testController = mainStoryboard.instantiateViewController(withIdentifier: "AddCameraDetailsVC")as! AddCameraDetailsVC
                self.navigationController?.pushViewController(testController, animated: false)
            }
        }
        

        txt_PersonalD_Email.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        txt_PersonalD_MobileNumber.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)

    }
    
    // MARK: - ---------------IBAction
       // MARK: -
       @IBAction func actiononBack(_ sender: UIButton) {
           self.navigationController?.popViewController(animated: true)
       }
   
    
    @IBAction func actionOnPersonalDetailNext(_ sender: UIButton) {
        if (txt_PersonalD_name.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_Name, viewcontrol: self)
            
        }else if (txt_PersonalD_MobileNumber.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_Mobile, viewcontrol: self)
            
        }else if ((txt_PersonalD_MobileNumber.text?.count)! < 10){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_Mobile_limit, viewcontrol: self)
        }else if (txt_PersonalD_Email.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_Email, viewcontrol: self)
        }else if !(validateEmail(email: txt_PersonalD_Email.text!)){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_InvalidEmail, viewcontrol: self)
        }else{
            callSavePersonalDetailAPI(sender: sender)
        }
        
        
    }
    @objc func textFieldDidChange(_ textField: UITextField) {

               let text = textField.text ?? ""

               let trimmedText = text.trimmingCharacters(in: .whitespaces)

               textField.text = trimmedText
        if(txt_PersonalD_MobileNumber == textField){
            textField.text = textField.text?.replacingOccurrences(of: "-", with: "")
            textField.text = textField.text?.replacingOccurrences(of: " ", with: "")

        }
             
           }
       // MARK: - ---------------Call API
          // MARK: -
    func callSavePersonalDetailAPI(sender : UIButton) {
        self.view.endEditing(true)
        if !isInternetAvailable() {
            FTIndicator.showToastMessage(alertInternet)
            
        }else{
         let strCityId = "\(((dictCityData.value(forKey: "CityData")as! NSArray).object(at: 0)as! NSDictionary).value(forKey: "city_id")!)"
            let strNumber  = "\(txt_PersonalD_MobileNumber.text!)".replacingOccurrences(of: " ", with: "")
            let dictData = NSMutableDictionary()
            dictData.setValue("\(txt_PersonalD_name.text!)", forKey: "name")
            dictData.setValue("\(txt_PersonalD_Email.text!)", forKey: "emailid")
            dictData.setValue("\(strNumber)".replacingOccurrences(of: "-", with: ""), forKey: "mobilenumber")
            dictData.setValue("\(strCityId)", forKey: "cityid")
//
            var json = Data()
            var jsonString = NSString()
            if JSONSerialization.isValidJSONObject(dictData) {
                // Serialize the dictionary
                json = try! JSONSerialization.data(withJSONObject: dictData, options: .prettyPrinted)
                jsonString = String(data: json, encoding: .utf8)! as NSString
                print("UpdateLeadinfo JSON: \(jsonString)")
            }

            let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><AddUserRegistration xmlns='\(BaseHostURL)/'><UserOTPMdl>\(jsonString)</UserOTPMdl></AddUserRegistration></soap12:Body></soap12:Envelope>"

            customDotLoaderShowOnButton(btn: sender, view: self.view, controller: self)



            WebServiceClass.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: BaseURLCitizenEye, resultype: "AddUserRegistrationResult", responcetype: "AddUserRegistrationResponse") { (responce, status) in
                customeDotLoaderRemove()

                if status == "Suceess"{

                    let dictTemp = (responce.value(forKey: "data")as! NSDictionary).mutableCopy()as! NSMutableDictionary
                    if("\(dictTemp.value(forKey: "Result")!)" == "True"){

                        let alert = UIAlertController(title: alertMessage, message: "\(dictTemp.value(forKey: "Msg")!)", preferredStyle: UIAlertController.Style.alert)
                        
                        // add the actions (buttons)
                        alert.addAction(UIAlertAction (title: "OK", style: .default, handler: { (nil) in
                            
                            let testController = mainStoryboard.instantiateViewController(withIdentifier: "OtpVerifyVC")as! OtpVerifyVC
                            testController.strTitle = "History"
                            
                            testController.dictData = dictData
                            self.navigationController?.pushViewController(testController, animated: true)
                        }))
                        self.present(alert, animated: true, completion: nil)
                        
                        

                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(dictTemp.value(forKey: "Msg")!)", viewcontrol: self)

                    }


                }else{

                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)

                }
            }
     }
    }
    //  MARK: API CAlling
    
    func callCountryAPI(sender : UIButton) {
        if !isInternetAvailable() {
            FTIndicator.showToastMessage(alertInternet)
        }else{
            customDotLoaderShowOnFull(message: "", controller: self)
            let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><GetAllCountryList xmlns='http://m.citizencop.org/'></GetAllCountryList></soap12:Body></soap12:Envelope>"
            
            WebServiceClass.callSOAP_APIBY_POST(soapMessage: soapMessage, url: BaseURL, resultype: "GetAllCountryListResult", responcetype: "GetAllCountryListResponse") { (responce, status) in
               customeDotLoaderRemove()
                if status == "Suceess"{
                    self.arryCountry = NSMutableArray()
                    self.arryCountry = (responce.value(forKey: "data")as! NSArray).mutableCopy()as! NSMutableArray
                    
                    if(self.arryCountry.count != 0){
                        
                    }
                    deleteAllRecords(strEntity:"AllCountry")
                    saveDataInLocalArray(strEntity: "AllCountry", strKey: "allCountry", data: self.arryCountry)
                    
                }else{
                    
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                    
                }
            }
        }
    }
}
// MARK: - ---------------UITextFieldDelegate
// MARK: -

extension CitizenEyeVC : UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
//        if (textField == txtName || textField == txtEmail)  {
//            return  txtFiledValidation(textField: txtName, string: string, returnOnly: "ALL", limitValue: 90)
//        }
//        if textField == txtMobile  {
//            return (txtFiledValidation(textField: txtMobile, string: string, returnOnly: "NUMBER", limitValue: 15))
//        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true

    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.view.endEditing(true)
    }
}
