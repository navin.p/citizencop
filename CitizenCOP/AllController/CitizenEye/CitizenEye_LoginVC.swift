//
//  CitizenEye_LoginVC.swift
//  CitizenCOP
//
//  Created by Navin Patidar on 05/08/22.
//

import UIKit

class CitizenEye_LoginVC: UIViewController {
    // MARK: - --------------IBOutlet
     // MARK: -
    var strTitle = ""
     @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var txt_PersonalD_EmailAddress: ACFloatingTextfield!
    @IBOutlet weak var txt_PersonalD_Password: ACFloatingTextfield!
    @IBOutlet weak var btnLogin: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
//        txt_PersonalD_EmailAddress.text = "kapil.k@Infocratsweb.com"
//        txt_PersonalD_Password.text = "3984"
        txt_PersonalD_EmailAddress.delegate = self
        txt_PersonalD_Password.delegate = self
        lblTitle.text = strTitle
        if #available(iOS 13.0, *) {
            txt_PersonalD_Password?.isSecureTextEntry = true
            txt_PersonalD_Password!.enablePasswordToggle()
           
        } else {
            
        }
        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryTheamColor)
        self.lblTitle.text = strTitle
        btnLogin.layer.cornerRadius = 8.0
        btnLogin.backgroundColor = hexStringToUIColor(hex: primaryDark)
        
    }
    
    
    // MARK: - ---------------IBAction
       // MARK: -
       @IBAction func actiononBack(_ sender: UIButton) {
           self.navigationController?.popViewController(animated: true)
       }
   
    @IBAction func actiononLogin(_ sender: UIButton) {
        self.view.endEditing(true)
        if(txt_PersonalD_EmailAddress.text == ""){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Email address or mobile is required!", viewcontrol: self)
        }
//        else if !(validateEmail(email: txt_PersonalD_EmailAddress.text!)){
//            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Email address is invalid!", viewcontrol: self)
//        }
        else if(txt_PersonalD_Password.text == ""){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Password is invalid!", viewcontrol: self)
        }else{
//            let testController = mainStoryboard.instantiateViewController(withIdentifier: "CameraHistroyVC")as! CameraHistroyVC
//            testController.strTitle = "History"
//            self.navigationController?.pushViewController(testController, animated: true)
            callOTPVerifyAPI(sender: sender)
        }
    }
    @IBAction func actiononForgotPassword(_ sender: UIButton) {
        let testController = UIStoryboard.init(name: "CitizenEYE", bundle: nil).instantiateViewController(withIdentifier: "CitizenEye_ForgotPassVC")as! CitizenEye_ForgotPassVC
        testController.strTitle = strTitle
        
        self.navigationController?.pushViewController(testController, animated: true)
    }
    @IBAction func actiononForgotNewUser(_ sender: UIButton) {
        let testController = UIStoryboard.init(name: "CitizenEYE", bundle: nil).instantiateViewController(withIdentifier: "CitizenEYE_SignUpVC")as! CitizenEYE_SignUpVC
        testController.strTitle = strTitle
        self.navigationController?.pushViewController(testController, animated: true)
    }
    
    
    //MARK:*********
    //MARK: API Calling
    func callOTPVerifyAPI(sender : UIButton) {
        if !isInternetAvailable() {
            FTIndicator.showToastMessage(alertInternet)
            
        }else{
            let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><OTPVerificationAndroid xmlns='\(BaseHostURL)/'><emailid>\(txt_PersonalD_EmailAddress.text!)</emailid><password>\(txt_PersonalD_Password.text!)</password></OTPVerificationAndroid></soap12:Body></soap12:Envelope>"
            let loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
            
            self.present(loader, animated: false, completion: nil)
            
            WebServiceClass.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: BaseURLCitizenEye, resultype: "OTPVerificationAndroidResult", responcetype: "OTPVerificationAndroidResponse") { (responce, status) in
                loader.dismiss(animated: false) {
                    if status == "Suceess"{
                        let dictTemp = responce.value(forKey: "data")as! NSDictionary
                        if("\(dictTemp.value(forKey: "Result")!)" == "True"){
                            
                            let ary = dictTemp.value(forKey: "LoginDetail")as! NSArray
                            if(ary.count != 0){
                                let dictData = ary.object(at: 0)as! NSDictionary
                                nsud.setValue(removeNullFromDict(dict: dictData.mutableCopy()as! NSMutableDictionary), forKey: "CitizenCOP_CitizenEyeData")
                                let testController = UIStoryboard.init(name: "CitizenEYE", bundle: nil).instantiateViewController(withIdentifier: "CameraHistroyVC")as! CameraHistroyVC
                                testController.strTitle = "History"
                                testController.dictData = dictData
                                self.navigationController?.pushViewController(testController, animated: true)
                            }else{
                                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                            }
                            
                        }else{
                            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "User not found!", viewcontrol: self)
                        }
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                    }
                }
            }
        }
    }
    
   
  

}
// MARK: - ---------------UITextFieldDelegate
// MARK: -

extension CitizenEye_LoginVC : UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if (textField == txt_PersonalD_Password || textField == txt_PersonalD_EmailAddress)  {
            return  txtFiledValidation(textField: textField, string: string, returnOnly: "ALL", limitValue: 90)
        }
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true

    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.view.endEditing(true)
    }
}
@available(iOS 13.0, *)
extension ACFloatingTextfield {
fileprivate func setPasswordToggleImage(_ button: UIButton) {
    if(isSecureTextEntry){
        button.setImage(UIImage(systemName: "eye"), for: .normal)
      
    }else{
      
        button.setImage(UIImage(systemName: "eye.slash"), for: .normal)
    }
}
func enablePasswordToggle(){
    let button = UIButton(type: .custom)
    setPasswordToggleImage(button)
    button.imageEdgeInsets = UIEdgeInsets(top: 0, left: -16, bottom: 0, right: 0)
    button.frame = CGRect(x: CGFloat(self.frame.size.width - 25), y: CGFloat(5), width: CGFloat(25), height: CGFloat(25))
    button.addTarget(self, action: #selector(self.togglePasswordView), for: .touchUpInside)
    button.tintColor = hexStringToUIColor(hex: primaryTheamColor)
    self.rightView = button
    self.rightViewMode = .always
}
@objc func togglePasswordView(_ sender: Any) {
    self.isSecureTextEntry = !self.isSecureTextEntry
    setPasswordToggleImage(sender as! UIButton)
}
}
