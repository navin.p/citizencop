//
//  CameraHistroyVC.swift
//  CitizenCOP
//
//  Created by Navin Patidar on 12/5/19.
//  Copyright © 2019 CitizenCop. All rights reserved.
//

import UIKit

class CameraHistroyVC: UIViewController ,UIGestureRecognizerDelegate , backToRedius{
    
    func backRadius(value: String) {
        self.GetCCTVCameraLocationByRadius(radius: value)
    }
    
    // MARK: - --------------IBOutlet
    // MARK: -
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var tvlist: UITableView!
    @IBOutlet weak var btnAdd: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnRadius: UIButton!
    @IBOutlet weak var viewHeaderHeight: NSLayoutConstraint!
    
    var keyCounter = 250
    var arryHistory = NSMutableArray()
    var refresher = UIRefreshControl()
    var ErrorMsg = ""
    var strTitle = ""
    var attrs = [
        NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14.0),
        NSAttributedString.Key.foregroundColor : UIColor.blue,
        NSAttributedString.Key.underlineStyle : 1] as [NSAttributedString.Key : Any] as [NSAttributedString.Key : Any]
    var dictData = NSDictionary()
    var arrRadius = NSMutableArray()
    var dictEyeUserData = NSDictionary()
    
    
    
    // MARK: - ------------Life Cycle
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryTheamColor)
        tvlist.tableFooterView = UIView()
        tvlist.estimatedRowHeight = 80.0
       // self.refresher.addTarget(self, action: #selector(RefreshloadData), for: .valueChanged)
        //self.tvlist!.addSubview(refresher)
       // callGetCameraDetailAPI(strLoaderTag: 1)
        btnAdd.backgroundColor = hexStringToUIColor(hex: primaryDark)
        lblTitle.text = strTitle
        var value = 250
        for j in 1...83{
            let strValue = value * j
            arrRadius.add(strValue)
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.view.endEditing(true)
        self.ErrorMsg = ""
        //callGetCameraDetailAPI(strLoaderTag: 1)
        
        self.dictEyeUserData = (nsud.value(forKey: "CitizenCOP_CitizenEyeData")as! NSDictionary).mutableCopy()as! NSMutableDictionary
        print(dictEyeUserData)
        
        let roleId = "\(dictEyeUserData.value(forKey: "role") ?? "")"
        if roleId == "cameraadmin" || roleId == "thana" || roleId == "admin"{
            self.GetCCTVCameraLocationByRadius(radius: "0")
        }else{
            self.btnRadius.isHidden = true
            if DeviceType.IS_IPAD {
                self.viewHeaderHeight.constant = 84
            }else{
                self.viewHeaderHeight.constant = 70
            }
            self.refresher.addTarget(self, action: #selector(RefreshloadData), for: .valueChanged)
            self.tvlist!.addSubview(refresher)
            callGetCameraDetailAPI(strLoaderTag: 1)
        }
        
    }
    
    // MARK: - --------------Pull Refresh
    // MARK: -
    @objc func RefreshloadData() {
        callGetCameraDetailAPI(strLoaderTag: 0)
    }
    // MARK: - ---------------IBAction
    // MARK: -
    @IBAction func actiononBack(_ sender: UIButton) {
        
        
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: DashBoardVC.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
    }
    @IBAction func actiononAdd(_ sender: UIButton) {
        let testController = UIStoryboard.init(name: "CitizenEYE", bundle: nil).instantiateViewController(withIdentifier: "AddCameraDetailsVC")as! AddCameraDetailsVC
        self.navigationController?.pushViewController(testController, animated: true)
    }
    
    @IBAction func actiononLogout(_ sender: UIButton) {
        let alert = UIAlertController(title: "Alert", message: "Are you sure want to logout?", preferredStyle: UIAlertController.Style.alert)
        
        // add the actions (buttons)
        alert.addAction(UIAlertAction (title: "Yes", style: .default, handler: { (nil) in
            nsud.set(nil, forKey: "CitizenCOP_CitizenEyeData")
            self.navigationController?.popViewController(animated: true)
        }))
        alert.addAction(UIAlertAction (title: "No", style: .default, handler: { (nil) in
            
            
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    @IBAction func actionOnSelectRadius(_ sender: UIButton) {
        print(self.arrRadius)
        let vc: SelectionClass = mainStoryboard.instantiateViewController(withIdentifier: "SelectionClass") as! SelectionClass
        vc.strTitle = "Select Radius"
        vc.aryList = self.arrRadius
        vc.isCitizenEye = true
        vc.citizenEyeDelegate = self
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .coverVertical
        self.present(vc, animated: true, completion: {})
    }
    
    
    // MARK: - ---------------Call API
    // MARK: -
    func callGetCameraDetailAPI(strLoaderTag : Int) {
        self.ErrorMsg = ""
        
        self.view.endEditing(true)
        if !isInternetAvailable() {
            FTIndicator.showToastMessage(alertInternet)
            
        }else{
            
            
            let loading = DPBasicLoading(table: tvlist, fontName: "HelveticaNeue")
            
            strLoaderTag == 1 ? loading.startLoading(text: "Loading...") : nil
            let dictEyeUserData = (nsud.value(forKey: "CitizenCOP_CitizenEyeData")as! NSDictionary).mutableCopy()as! NSMutableDictionary
            let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><GetCCTVCameraLocation xmlns='\(BaseHostURL)/'><userid>\("\(dictEyeUserData.value(forKey: "userid")!)")</userid></GetCCTVCameraLocation></soap12:Body></soap12:Envelope>"
            
            
            
            
            WebServiceClass.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: BaseURLCitizenEye, resultype: "GetCCTVCameraLocationResult", responcetype: "GetCCTVCameraLocationResponse") { (responce, status) in
                loading.endLoading()
                self.refresher.endRefreshing()
                if status == "Suceess"{
                    
                    let dictTemp = (responce.value(forKey: "data")as! NSDictionary).mutableCopy()as! NSMutableDictionary
                    if("\(dictTemp.value(forKey: "Result")!)" == "True"){
                        self.ErrorMsg = ""
                        self.arryHistory = NSMutableArray()
                        self.arryHistory = (dictTemp.value(forKey: "CameraLocation")as! NSArray).mutableCopy()as! NSMutableArray
                        
                        
                    }else{
                        
                        self.ErrorMsg = alertDataNotFound
                    }
                    
                    self.tvlist.reloadData()
                    
                }else{
                    self.ErrorMsg = alertSomeError
                    self.tvlist.reloadData()
                    
                    
                }
            }
        }
    }
    
    func changeDateFormate(date:String) ->String {
        
        let inputFormatter = DateFormatter()
        inputFormatter.dateFormat = "YYYY-MM-dd"

        let outputFormatter = DateFormatter()
        outputFormatter.dateFormat = "dd-MM-YYYY"

        let showDate = inputFormatter.date(from: date)
        let resultString = outputFormatter.string(from: showDate!)
        
        return resultString
        
    }
    
    //MARK: API Calling
    func GetCCTVCameraLocationByRadius(radius : String) {
        if !isInternetAvailable() {
            FTIndicator.showToastMessage(alertInternet)
            
        }else{
            
            let dictEyeUserData = (nsud.value(forKey: "CitizenCOP_CitizenEyeData")as! NSDictionary).mutableCopy()as! NSMutableDictionary
            
            let dictSendData = NSMutableDictionary()
            dictSendData.setValue("0", forKey: "userid")
            dictSendData.setValue("0", forKey: "latitude")
            dictSendData.setValue("0", forKey: "longitude")
            dictSendData.setValue(radius, forKey: "radius")
            
            if "\(dictEyeUserData.value(forKey: "ZoneId") ?? "")" == "" {
                dictSendData.setValue("0", forKey: "zoneid")
            }else{
                dictSendData.setValue("\(dictEyeUserData.value(forKey: "ZoneId") ?? "")", forKey: "zoneid")
            }
            
            var json = Data()
            var jsonString = NSString()
            if JSONSerialization.isValidJSONObject(dictSendData) {
                json = try! JSONSerialization.data(withJSONObject: dictSendData, options: .prettyPrinted)
                jsonString = String(data: json, encoding: .utf8)! as NSString
            }
            
            let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><GetCCTVCameraLocationByRadius xmlns='\(BaseHostURL)/'><CameraLocationSearchMdl>\(jsonString)</CameraLocationSearchMdl></GetCCTVCameraLocationByRadius></soap12:Body></soap12:Envelope>"
            
            let loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
            
            self.present(loader, animated: false, completion: nil)
            
            WebServiceClass.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: BaseURLCitizenEye, resultype: "GetCCTVCameraLocationByRadiusResult", responcetype: "GetCCTVCameraLocationByRadiusResponse") { (responce, status) in
                loader.dismiss(animated: false) {
                    if status == "Suceess"{
                        
                        let dictTemp = (responce.value(forKey: "data")as! NSDictionary).mutableCopy()as! NSMutableDictionary
                        if("\(dictTemp.value(forKey: "Result")!)" == "True"){
                            self.ErrorMsg = ""
                            self.arryHistory = NSMutableArray()
                            self.arryHistory = (dictTemp.value(forKey: "CameraLocation")as! NSArray).mutableCopy()as! NSMutableArray
                            self.tvlist.reloadData()
                        }else{
                            self.ErrorMsg = alertDataNotFound
                        }
                        
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                    }
                }
            }
        }
    }
    
    @objc func activity(sender : UIButton){
        let buttonPosition:CGPoint = sender.convert(CGPoint.zero, to:self.tvlist)
        let indexPath = self.tvlist.indexPathForRow(at: buttonPosition)
        let dict = arryHistory[indexPath!.row] as? NSDictionary
        let locationLat = "\(dict?.value(forKey: "latitude") ?? "")"
        let locationlong = "\(dict?.value(forKey: "longitude") ?? "")"
        UIApplication.shared.openURL(NSURL(string:"http://maps.apple.com/?ll=\(locationLat),\(locationlong)")! as URL)
    }
    
    
    @objc func imgActivity(sender : UIButton){
        let buttonPosition:CGPoint = sender.convert(CGPoint.zero, to:self.tvlist)
        let indexPath = self.tvlist.indexPathForRow(at: buttonPosition)
        let dict = arryHistory[indexPath!.row] as? NSDictionary
        let qrcodeimage = "\(BaseHostURL)/CitizenEyeImage/\(dict?.value(forKey: "qrcodeimage") ?? "")"
        let testController = UIStoryboard.init(name: "CitizenEYE", bundle: nil).instantiateViewController(withIdentifier: "imageShowViewController")as! imageShowViewController
        testController.qrcodeimage = qrcodeimage
        self.navigationController?.pushViewController(testController, animated: true)
    }
    
}
// MARK: - ----------------UITableViewDelegate
// MARK: -

extension CameraHistroyVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arryHistory.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let roleId = "\(dictEyeUserData.value(forKey: "role") ?? "")"
        if roleId == "cameraadmin" || roleId == "thana" || roleId == "admin"{
            
            let cell = tvlist.dequeueReusableCell(withIdentifier: "HistoryCameraThaneAdmin", for: indexPath as IndexPath) as! HistoryCameraThaneAdmin
            let dict =  removeNullFromDict(dict: (arryHistory.object(at: indexPath.row)as! NSDictionary).mutableCopy() as! NSMutableDictionary)
            
            cell.lblPoliceStation.text = "\(dict.value(forKey: "PoliceStationName") ?? "")"
            cell.lblCitizenEye.text = "\(dict.value(forKey: "name") ?? "")"
            cell.lblCItizenMobileNo.text = "\(dict.value(forKey: "mobilenumber") ?? "")"
            cell.lblOwnerName.text = "\(dict.value(forKey: "ownername") ?? "")"
            cell.lblShopName.text = "\(dict.value(forKey: "shopname") ?? "")"
            cell.lblNumberOfCamera.text = "\(dict.value(forKey: "noofcamera") ?? "")"
            cell.lblLocationArea.text = "\(dict.value(forKey: "locationarea") ?? "")"
            cell.lblPersonName.text = "\(dict.value(forKey: "pocname") ?? "")"
            cell.lblPersonMobileNo.text = "\(dict.value(forKey: "poccontactno") ?? "")"
            cell.lblCameraIsOnInternet.text = "\(dict.value(forKey: "poccontactno") ?? "")"
            cell.lblCameraURl.text = "\(dict.value(forKey: "cameraurl") ?? "")"
            cell.lblCameraCompanyName.text = "\(dict.value(forKey: "cameraname") ?? "")"
            cell.lblPortNumber.text = "\(dict.value(forKey: "portno") ?? "")"
            
            let date = "\(dict.value(forKey: "createddate") ?? "")"
            let changeDate = date.components(separatedBy: "T")
            cell.lblDate.text = changeDateFormate(date: changeDate[0])
            
            if "\(dict.value(forKey: "isinternet") ?? "")" == "1" || "\(dict.value(forKey: "isinternet") ?? "")".lowercased() == "Ture".lowercased(){
                cell.lblCameraIsOnInternet.text = "Yes"
            }else{
                cell.lblCameraIsOnInternet.text = "No"
            }
             
            let qrcodeimage = "\(BaseHostURL)/CitizenEyeImage/\(dict.value(forKey: "qrcodeimage") ?? "")"
            cell.imgDoc.setImageWith(URL(string: qrcodeimage), placeholderImage: UIImage(named: "NoImage"), options: SDWebImageOptions(rawValue: 1), completed: { (image, error, type, url) in
            }, usingActivityIndicatorStyle: .gray)
            
            cell.btnMapLink.setTitle("http://maps.apple.com/?ll=\(dict.value(forKey: "latitude") ?? ""),\(dict.value(forKey: "longitude") ?? "")", for: .normal)
            cell.btnMapLink.addTarget(self, action: #selector(activity), for: .touchUpInside)
            cell.btnImage.addTarget(self, action: #selector(imgActivity), for: .touchUpInside)
            
            if(indexPath.row % 2 == 0){
                cell.viewInicateColor.backgroundColor = hexStringToUIColor(hex: "#a863ff")
            }else{
                cell.viewInicateColor.backgroundColor = hexStringToUIColor(hex: "#f93ed5")
                
            }
            
            return cell
            
        }else{
            let cell = tvlist.dequeueReusableCell(withIdentifier: "History", for: indexPath as IndexPath) as! History
            let dict = removeNullFromDict(dict: (arryHistory.object(at: indexPath.row)as! NSDictionary).mutableCopy() as! NSMutableDictionary)
            
            cell.lblcameraurl.text = ("\(dict.value(forKey: "cameraurl")!)" == "") ? "N/A" : "\(dict.value(forKey: "cameraurl")!)"
            
            cell.lblnoofcamera.text = ("\(dict.value(forKey: "noofcamera")!)" == "") ? "N/A" : "\(dict.value(forKey: "noofcamera")!)"
            cell.lbllocation.text = ("\(dict.value(forKey: "location")!)" == "") ? "N/A" : "\(dict.value(forKey: "location")!)"
            
            cell.lblNamePOC.text = ("\(dict.value(forKey: "pocname")!)" == "") ? "N/A" : "\(dict.value(forKey: "pocname")!)"
            
            cell.lblCompanyName.text = ("\(dict.value(forKey: "cameraname")!)" == "") ? "N/A" : "\(dict.value(forKey: "cameraname")!)"
            
            let isInternet = ("\(dict.value(forKey: "isinternet")!)" == "") ? "N/A" : "\(dict.value(forKey: "isinternet")!)"
            if(isInternet.lowercased() == "1" || isInternet.lowercased() == "true"){
                cell.lblIsinternet.text = "Yes"
            }
            else if(isInternet.lowercased() == "0" || isInternet.lowercased() == "false"){
                cell.lblIsinternet.text = "No"
            }else{
                cell.lblIsinternet.text = isInternet
            }
            
            let qrcodeimage = "\(BaseHostURL)/CitizenEyeImage/\(dict.value(forKey: "qrcodeimage") ?? "")"
            //  http://mp.citizencop.org/CitizenEyeImage/0807155936.jpg
            
            
            cell.imgView.setImageWith(URL(string: qrcodeimage), placeholderImage: UIImage(named: "NoImage"), options: SDWebImageOptions(rawValue: 1), completed: { (image, error, type, url) in
            }, usingActivityIndicatorStyle: .gray)
            
            
            cell.lblcameraurl.text = ("\(dict.value(forKey: "cameraurl")!)" == "") ? "N/A" : "\(dict.value(forKey: "cameraurl")!)"
            let strAssignFromDateTime = "\(dict.value(forKey: "createddate") ?? "")"
            cell.lblcreateddate.text  = changeStringDateToGivenFormatWithoutT(strDate: strAssignFromDateTime, strRequiredFormat: "MM/dd/yyyy hh:mm a")
            
            if(indexPath.row % 2 == 0){
                cell.viewInicateColor.backgroundColor = hexStringToUIColor(hex: "CFA749")
            }else{
                cell.viewInicateColor.backgroundColor = hexStringToUIColor(hex: "354751")
                
            }
            
            return cell
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 50.0
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let customView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 50))
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 50))
        if(arryHistory.count != 0){
            ErrorMsg = ""
        }
        button.setTitle(ErrorMsg, for: .normal)
        customView.addSubview(button)
        customView.backgroundColor = UIColor.clear
        button.setTitleColor(UIColor.darkGray, for: .normal)
        return customView
    }
    
}
// MARK: - ----------------NewsCell
// MARK: -
class History: UITableViewCell {
    @IBOutlet weak var lbllocation: UILabel!
    @IBOutlet weak var lblnoofcamera: UILabel!
    @IBOutlet weak var lblcameraurl: UILabel!
    @IBOutlet weak var lblcreateddate: UILabel!
    @IBOutlet weak var viewInicateColor: UIView!
    @IBOutlet weak var txtUrl: UITextView!
    @IBOutlet weak var lblCompanyName: UILabel!
    @IBOutlet weak var lblIsinternet: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblNamePOC: UILabel!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}


class HistoryCameraThaneAdmin : UITableViewCell {
    
    @IBOutlet weak var lblCitizenEye: UILabel!
    @IBOutlet weak var lblCItizenMobileNo: UILabel!
    @IBOutlet weak var lblOwnerName: UILabel!
    @IBOutlet weak var lblShopName: UILabel!
    @IBOutlet weak var lblPoliceStation: UILabel!
    @IBOutlet weak var lblNumberOfCamera: UILabel!
    @IBOutlet weak var lblLocationArea: UILabel!
    @IBOutlet weak var lblPersonName: UILabel!
    @IBOutlet weak var lblPersonMobileNo: UILabel!
    @IBOutlet weak var lblCameraIsOnInternet: UILabel!
    @IBOutlet weak var lblCameraURl: UILabel!
    @IBOutlet weak var lblCameraCompanyName: UILabel!
    @IBOutlet weak var lblPortNumber: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lbMapLink: UILabel!
    @IBOutlet weak var imgDoc: UIImageView!
    @IBOutlet weak var btnMapLink: UIButton!
    @IBOutlet weak var btnImage: UIButton!
    @IBOutlet weak var viewInicateColor: UIView!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}
