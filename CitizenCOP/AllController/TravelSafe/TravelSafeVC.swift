//
//  TravelSafeVC.swift
//  CitizenCOP
//
//  Created by Saavan Patidar on 28/05/19.
//  Copyright © 2019 CitizenCop. All rights reserved.
//

import UIKit
import SWXMLHash
import StringExtensionHTML
import AEXML

class TravelSafeVC: UIViewController {

    // MARK: - --------------IBOutlet
    // MARK: -
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var txtFld_No: UITextField!
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var btnBadgeNo: UIButton!
    @IBOutlet weak var btnVehicleNo: UIButton!
    @IBOutlet weak var btnInformAboutTravel: UIButton!
    @IBOutlet var detailsMainViewBackground: UIView!
    @IBOutlet weak var tvlist: UITableView!

    @IBOutlet weak var lblTitle: UILabel!
    
    // MARK: - --------------Variables
    // MARK: -
    var dictData = NSMutableDictionary()
    var dictWhiteListVerification = NSMutableDictionary()
    var isBadgeNo = true
    var strlblTittle = String()

    // MARK: - --------------View Life Cycle
    // MARK: -
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblTitle.text = strlblTittle

        // Do any additional setup after loading the view.
        tvlist.tableFooterView = UIView()
        tvlist.estimatedRowHeight = 80.0
        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryTheamColor)
        btnSearch.layer.cornerRadius = 5.0
        btnSearch.backgroundColor = hexStringToUIColor(hex: primaryDark)
        btnInformAboutTravel.layer.cornerRadius = 5.0
        btnInformAboutTravel.backgroundColor = hexStringToUIColor(hex: primaryDark)
        btnBadgeNo.setImage(UIImage(named: "RadioButton-Selected.png"), for: .normal)
        btnVehicleNo.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        txtFld_No.placeholder = "Enter Badge Number"
        txtFld_No.text = ""
        isBadgeNo = true
        
    }
    
    
    // MARK: - --------------Actions
    // MARK: -
    @IBAction func action_Back(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func action_Help(_ sender: Any) {
        
        let testController = mainStoryboard.instantiateViewController(withIdentifier: "HelpVC")as! HelpVC
        testController.strMessage = alertTravelSafeHelp
        self.navigationController?.pushViewController(testController, animated: true)
        
    }
    
    @IBAction func action_InformAboutTravel(_ sender: Any) {
        let testController = mainStoryboard.instantiateViewController(withIdentifier: "InformAboutTravelVC")as! InformAboutTravelVC
        testController.strNumber = txtFld_No.text!
        testController.strlblTittle = lblTitle.text!
        self.navigationController?.pushViewController(testController, animated: true)
    }
    
    @IBAction func action_BadgeNo(_ sender: UIButton) {
        
        btnBadgeNo.setImage(UIImage(named: "RadioButton-Selected.png"), for: .normal)
        btnVehicleNo.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        txtFld_No.placeholder = "Enter Badge Number"
        txtFld_No.text = ""
        isBadgeNo = true
        
    }
    
    @IBAction func action_VehicleNo(_ sender: UIButton) {
        
        btnVehicleNo.setImage(UIImage(named: "RadioButton-Selected.png"), for: .normal)
        btnBadgeNo.setImage(UIImage(named: "RadioButton-Unselected.png"), for: .normal)
        txtFld_No.placeholder = "Enter Vehicle Number"
        txtFld_No.text = ""
        isBadgeNo = false
        
    }
    
    @IBAction func action_RemoveDetailsVew(_ sender: Any) {
        
        detailsMainViewBackground.removeFromSuperview()
        
    }
    
    
    @IBAction func action_Search(_ sender: Any) {
        self.view.endEditing(true)
        if !isInternetAvailable() {
            FTIndicator.showToastMessage(alertInternet)
        }else{
            if(txtFld_No.text?.count == 0){
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_TravelSafeNo, viewcontrol: self)
            }else{
                if(dictCityData.value(forKey: "CityData")as! NSArray).count != 0{
                    let strCityId = "\(((dictCityData.value(forKey: "CityData")as! NSArray).object(at: 0)as! NSDictionary).value(forKey: "city_id")!)"
                    callTravelSafeAPI(strCityID: strCityId)
                }
            }
        }
    }
    
    // MARK: - --------------Functions
    // MARK: -
    
    @objc func actionForCall(sender : UIButton) {
        
        if callingFunction(number: "\((sender.titleLabel?.text!)!)" as NSString){
            
        }
        
    }
    
    @objc func actionForAddress(sender : UIButton) {
        
        
        
    }
    // MARK: - --------------API
    // MARK: -
    func callTravelSafeAPI(strCityID : String) {
        if !isInternetAvailable() {
            
            FTIndicator.showToastMessage(alertInternet)
            
        }else{
            
            let strBadgeNo = "&BadgeNo="
            var strBadgeNoValue = txtFld_No.text
            let strVehicleNo = "&VehicleNo="
            var strVehicleNoValue = txtFld_No.text
            let strImeiNo = "&IMEINumber="
            let strImeiNoValue = "1234123412341234"
            
            if isBadgeNo {
                
                strVehicleNoValue = ""
                
            }else{
                
                strBadgeNoValue = ""
                
            }
            
            let URL = API_TravelSafe + strCityID + strBadgeNo + strBadgeNoValue!  + strVehicleNo + strVehicleNoValue! + strImeiNo + strImeiNoValue
            
            print(URL)
            
            customDotLoaderShowOnButton(btn: btnSearch, view: self.view, controller: self)
            
            WebServiceClass.callAPIBYPOST(parameter: NSDictionary(), url: URL) { (responce, status) in
                customeDotLoaderRemove()
                if status == "Suceess"{
                    
                    self.dictData = NSMutableDictionary()
                    self.dictData = (responce.value(forKey: "data")as! NSDictionary).mutableCopy()as! NSMutableDictionary
                    print(self.dictData)
                    
                    let strResult = "\(self.dictData.value(forKey: "Result")!)"
                    
                    if (strResult == "False") || (strResult == "false") {
                        
                        let strMsg = "\(self.dictData.value(forKey: "Message")!)"
                        
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: strMsg, viewcontrol: self)
                        
                    }else{
                        
                        // data exist Show to details view
                        
                        let frameFor_detailsMainViewBackground = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
                        self.detailsMainViewBackground.frame = frameFor_detailsMainViewBackground
                        self.view.addSubview(self.detailsMainViewBackground)
                        
                        let arrOfData = (self.dictData.value(forKey: "WhiteListVerification")as! NSArray).mutableCopy()as! NSMutableArray
                        
                        if arrOfData.count > 0 {
                            
                            self.dictWhiteListVerification = (arrOfData[0] as! NSDictionary).mutableCopy()as! NSMutableDictionary
                            
                            self.tvlist.reloadData()

                        }
                        
                        
                    }
                    
                    
                }
                else{
                    
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                    
                }
            }
            
        }
    }

}


// MARK: - ----------------UITableViewDelegate
// MARK: -

extension TravelSafeVC: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 3
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
       return 1
        
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        if section == 0 {
            
            return "Driver Detail"
            
        } else if section == 1{
            
            return "Owner Detail"
            
        } else {
            
            return "Vehicle Detail"
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            
            let cell = tvlist.dequeueReusableCell(withIdentifier: "TravelSafeVCDriverDetailCell", for: indexPath as IndexPath) as! TravelSafeVCDriverDetailCell
            
            cell.lblDriverName.text = "\(self.dictWhiteListVerification.value(forKey: "FirstName")!)"
            cell.btnDriverNo.setTitle("\(self.dictWhiteListVerification.value(forKey: "ContactNo")!)", for: .normal)
            cell.btnDriverAddress.setTitle("\(self.dictWhiteListVerification.value(forKey: "Address")!)", for: .normal)
            cell.lblDriverLicenseNo.text = "License No : \(self.dictWhiteListVerification.value(forKey: "DrivingLicenseNo")!)"
            cell.lblDriverType.text = "Type : \(self.dictWhiteListVerification.value(forKey: "WLCategoryName")!)"
            cell.lblDriverPS.text = "Residential PS : \(self.dictWhiteListVerification.value(forKey: "ResidentPS")!)"
            cell.lblDriverCriminalRecord.text = "Criminal History : \(self.dictWhiteListVerification.value(forKey: "CrimeHistory")!)"
            let icone = "\(self.dictWhiteListVerification.value(forKey: "Images")!)"
            
            cell.btnDriverNo.addTarget(self, action: #selector(actionForCall), for: .touchUpInside)
            cell.btnDriverAddress.addTarget(self, action: #selector(actionForAddress), for: .touchUpInside)
             cell.imgViewDriver.layer.borderWidth = 1
             cell.imgViewDriver.layer.masksToBounds = false
             cell.imgViewDriver.layer.borderColor = UIColor.groupTableViewBackground.cgColor
             cell.imgViewDriver.layer.cornerRadius =  cell.imgViewDriver.frame.height/2
             cell.imgViewDriver.clipsToBounds = true
            
            cell.imgViewDriver.contentMode = .scaleAspectFit
            cell.imgViewDriver.setImageWith(URL(string: API_TravelSafeImgDownload + icone), placeholderImage: UIImage(named: "NoImage"), options: SDWebImageOptions(rawValue: 1), completed: { (image, error, type, url) in
                print(url ?? 0)
            }, usingActivityIndicatorStyle: .gray)
            
            return cell
            
        } else if indexPath.section == 1{
            
            let cell = tvlist.dequeueReusableCell(withIdentifier: "TravelSafeVCOwnerDetailCell", for: indexPath as IndexPath) as! TravelSafeVCOwnerDetailCell
            
            cell.lblOwnerName.text = "\(self.dictWhiteListVerification.value(forKey: "VehicleOwnerName")!)"
            cell.btnOwnerNo.setTitle("\(self.dictWhiteListVerification.value(forKey: "VehicleOwnerContactNo")!)", for: .normal)
            cell.btnOwnerAddress.setTitle("\(self.dictWhiteListVerification.value(forKey: "VehicleOwnerAddress")!)", for: .normal)
            cell.lblCriminalRecord.text = "Criminal History : \(self.dictWhiteListVerification.value(forKey: "VehicleOwnerCrimeHistory")!)"
            cell.btnOwnerNo.addTarget(self, action: #selector(actionForCall), for: .touchUpInside)
            cell.btnOwnerAddress.addTarget(self, action: #selector(actionForAddress), for: .touchUpInside)
            
            let icone = "\(self.dictWhiteListVerification.value(forKey: "VehicleOwnerPhoto")!)"
            cell.imgViewOwner.layer.borderWidth = 1
            cell.imgViewOwner.layer.masksToBounds = false
            cell.imgViewOwner.layer.borderColor = UIColor.groupTableViewBackground.cgColor
            cell.imgViewOwner.layer.cornerRadius =  cell.imgViewOwner.frame.height/2
            cell.imgViewOwner.clipsToBounds = true
            cell.imgViewOwner.contentMode = .scaleAspectFit

            cell.imgViewOwner.setImageWith(URL(string: API_TravelSafeImgDownload + icone), placeholderImage: UIImage(named: "NoImage"), options: SDWebImageOptions(rawValue: 1), completed: { (image, error, type, url) in
                print(url ?? 0)
            }, usingActivityIndicatorStyle: .gray)
            
            return cell
            
        } else {
            
            let cell = tvlist.dequeueReusableCell(withIdentifier: "TravelSafeVCVehicleDetailCell", for: indexPath as IndexPath) as! TravelSafeVCVehicleDetailCell
            
            cell.lblVehicleNo.text = "Vehicle No : \(self.dictWhiteListVerification.value(forKey: "VehicleNo")!)"
            cell.lblModelYear.text = "Model & Year : \(self.dictWhiteListVerification.value(forKey: "VehicleModelYear")!)"
            cell.lblpermit.text = "Permit : \(self.dictWhiteListVerification.value(forKey: "VehicleParmitLimitDate")!)"
            cell.lblFitness.text = "Fitness : \(self.dictWhiteListVerification.value(forKey: "VehicleFitnessDate")!)"
            cell.lblInsurance.text = "Insurance : \(self.dictWhiteListVerification.value(forKey: "VehicleBimaLimitDate")!)"

            return cell
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let strType = "Type : \(self.dictWhiteListVerification.value(forKey: "WLCategoryName")!)"
        
        if  strType.contains("Driver") {
            
            return UITableView.automaticDimension
            
        } else {
            
            return 0
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == 0 {
            
            let indexPath = tableView.indexPathForSelectedRow //optional, to get from any UIButton for example
            let currentCell = tableView.cellForRow(at: indexPath!) as! TravelSafeVCDriverDetailCell
            let testController = mainStoryboard.instantiateViewController(withIdentifier: "PreviewImageVC")as! PreviewImageVC
            testController.img = currentCell.imgViewDriver!.image!
            self.navigationController?.pushViewController(testController, animated: true)
            
        } else if indexPath.section == 1{
            
            let indexPath = tableView.indexPathForSelectedRow //optional, to get from any UIButton for example
            let currentCell = tableView.cellForRow(at: indexPath!) as! TravelSafeVCOwnerDetailCell
            let testController = mainStoryboard.instantiateViewController(withIdentifier: "PreviewImageVC")as! PreviewImageVC
            testController.img = currentCell.imgViewOwner!.image!
            self.navigationController?.pushViewController(testController, animated: true)
            
        }
        
    }
    
}

// MARK: - ----------------TravelSafeVCCell
// MARK: -
class TravelSafeVCVehicleDetailCell: UITableViewCell {
    
    @IBOutlet weak var lblVehicleNo: UILabel!
    @IBOutlet weak var lblModelYear: UILabel!
    @IBOutlet weak var lblpermit: UILabel!
    @IBOutlet weak var lblInsurance: UILabel!
    @IBOutlet weak var lblFitness: UILabel!
    
    
    var attrs = [
        NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14.0),
        NSAttributedString.Key.foregroundColor : hexStringToUIColor(hex: "287AFF"),
        NSAttributedString.Key.underlineStyle : 1] as [NSAttributedString.Key : Any] as [NSAttributedString.Key : Any]
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
    
}

class TravelSafeVCOwnerDetailCell: UITableViewCell {
    
    @IBOutlet weak var imgViewOwner: UIImageView!
    @IBOutlet weak var lblOwnerName: UILabel!
    @IBOutlet weak var btnOwnerNo: UIButton!
    @IBOutlet weak var btnOwnerAddress: UIButton!
    @IBOutlet weak var lblCriminalRecord: UILabel!
    
    
    var attrs = [
        NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14.0),
        NSAttributedString.Key.foregroundColor : hexStringToUIColor(hex: "287AFF"),
        NSAttributedString.Key.underlineStyle : 1] as [NSAttributedString.Key : Any] as [NSAttributedString.Key : Any]
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
    
}

class TravelSafeVCDriverDetailCell: UITableViewCell {
    
    @IBOutlet weak var imgViewDriver: UIImageView!
    @IBOutlet weak var lblDriverName: UILabel!
    @IBOutlet weak var btnDriverNo: UIButton!
    @IBOutlet weak var lblDriverLicenseNo: UILabel!
    @IBOutlet weak var btnDriverAddress: UIButton!
    @IBOutlet weak var lblDriverType: UILabel!
    @IBOutlet weak var lblDriverPS: UILabel!
    @IBOutlet weak var lblDriverCriminalRecord: UILabel!
    
    
    var attrs = [
        NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14.0),
        NSAttributedString.Key.foregroundColor : hexStringToUIColor(hex: "287AFF"),
        NSAttributedString.Key.underlineStyle : 1] as [NSAttributedString.Key : Any] as [NSAttributedString.Key : Any]
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
    
}
//MARK:-
//MARK:- ---------UITextFieldDelegate

extension TravelSafeVC : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if ( textField == txtFld_No)
        {
            return txtFiledValidation(textField: textField, string: string, returnOnly: "ALL", limitValue: 45)
        }
       
        else
        {
            return true
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
}
