//
//  InformAboutTravelVC.swift
//  CitizenCOP
//
//  Created by Navin Patidar on 5/29/19.
//  Copyright © 2019 CitizenCop. All rights reserved.
//

import UIKit
import MessageUI

class InformAboutTravelVC: UIViewController {
    
    // MARK: - --------------IBOutlet
    // MARK: -
    @IBOutlet weak var btnInform: UIButton!
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var scroll: UIScrollView!
    @IBOutlet weak var txtSource: ACFloatingTextfield!
    @IBOutlet weak var txtDestination: ACFloatingTextfield!
    @IBOutlet weak var txtVehicleNumber: ACFloatingTextfield!
    
    @IBOutlet weak var lblTitle: UILabel!
    var strlblTittle = String()

    
    
    var strNumber = String()
    // MARK: - --------------LifeCycle
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryTheamColor)
        btnInform.layer.cornerRadius = 5.0
        btnInform.backgroundColor = hexStringToUIColor(hex: primaryDark)
        // Do any additional setup after loading the view.
        txtVehicleNumber.text = strNumber
        lblTitle.text = strlblTittle

    }
    
    // MARK: - --------------Actions
    // MARK: -
    @IBAction func action_Back(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    @IBAction func actiononAddContact(_ sender: UIButton) {
        let testController = mainStoryboard.instantiateViewController(withIdentifier: "AddContactVC")as! AddContactVC
        self.navigationController?.pushViewController(testController, animated: true)
    }
    @IBAction func action_On_InformNow(_ sender: Any) {
        self.view.endEditing(true)
        if nsud.value(forKey: "CitizenCOP_HelpContact") != nil {
            let aryContactList = (nsud.value(forKey: "CitizenCOP_HelpContact")as! NSArray).mutableCopy()as! NSMutableArray
            if(aryContactList.count == 0){
                let testController = mainStoryboard.instantiateViewController(withIdentifier: "AddContactVC")as! AddContactVC
                self.navigationController?.pushViewController(testController, animated: true)
            }else{
                if(txtSource.text?.count == 0){
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_Source, viewcontrol: self)
                }else if (txtDestination.text?.count == 0){
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_Destination, viewcontrol: self)
                }else if (txtVehicleNumber.text?.count == 0){
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_TravelSafeNo, viewcontrol: self)
                }else{
                    if (MFMessageComposeViewController.canSendText()) {
                        let messageVC = MFMessageComposeViewController()
                        messageVC.body = "Hello! I am Using Vechicle No. \(txtVehicleNumber.text!) to travel from \(txtSource.text!) to \(txtDestination.text!)";
                        if nsud.value(forKey: "CitizenCOP_HelpContact") != nil {
                            var aryContactList = NSMutableArray()
                            aryContactList = (nsud.value(forKey: "CitizenCOP_HelpContact")as! NSArray).mutableCopy()as! NSMutableArray
                            var strrecipients = ""
                            for item in aryContactList{
                                strrecipients = strrecipients + "\(item as AnyObject),"
                            }
                            messageVC.recipients = [strrecipients]
                            
                        }
                        messageVC.messageComposeDelegate = self
                        self.present(messageVC, animated: true, completion: nil)
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertCalling, viewcontrol: self)

                    }
                }
            }
        }else{
            let testController = mainStoryboard.instantiateViewController(withIdentifier: "AddContactVC")as! AddContactVC
            self.navigationController?.pushViewController(testController, animated: true)
        }
        
        
        
    }
}
// MARK: - ----------------MFMessageComposeViewControllerDelegate
// MARK: -
extension InformAboutTravelVC : MFMessageComposeViewControllerDelegate{
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        switch (result) {
        case .cancelled:
            print("Message was cancelled")
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Message was cancelled", viewcontrol: self)
            dismiss(animated: true, completion: nil)
        case .failed:
            print("Message failed")
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Message failed", viewcontrol: self)
            
            dismiss(animated: true, completion: nil)
        case .sent:
            print("Message was sent")
            dismiss(animated: true, completion: nil)
        default:
            break
        }
    }
    
}
//MARK:-
//MARK:- ---------UITextFieldDelegate

extension InformAboutTravelVC : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if ( textField == txtVehicleNumber)
        {
            return txtFiledValidation(textField: textField, string: string, returnOnly: "ALL", limitValue: 25)
        }
        if ( textField == txtSource || textField == txtDestination)
        {
            return txtFiledValidation(textField: textField, string: string, returnOnly: "ALL", limitValue: 150)
        }
        else
        {
            return true
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
}
