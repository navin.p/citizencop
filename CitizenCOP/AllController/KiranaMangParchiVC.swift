//
//  KiranaMangParchiVC.swift
//  CitizenCOP
//
//  Created by Saavan Patidar on 18/04/20.
//  Copyright © 2020 CitizenCop. All rights reserved.
//

import UIKit

class KiranaMangParchiVC: UIViewController {

    // MARK: - ----IBOutlet
    @IBOutlet weak var view_ForHeader: UIView!
    @IBOutlet weak var tvlist: UITableView!
    @IBOutlet weak var txtWardNo: ACFloatingTextfield!
    @IBOutlet weak var txtZoneNo: ACFloatingTextfield!
    @IBOutlet weak var txtRouteNo: ACFloatingTextfield!
    @IBOutlet weak var txtName: ACFloatingTextfield!
    @IBOutlet weak var txtMobileNo: ACFloatingTextfield!
    @IBOutlet weak var txtAddress: ACFloatingTextfield!
    @IBOutlet weak var btnOrderNow: UIButton!


    // MARK: - ----Variables
    var strTitle = String()
    var arrOfItemList = NSMutableArray() , aryWardlist = NSMutableArray() , aryZonelist = NSMutableArray()
    var arrOfItemListSelected = NSMutableArray()
    var arrUnit_Aata = NSMutableArray()
    var arrUnit_Chawal = NSMutableArray()
    var arrUnit_Dal = NSMutableArray()
    var arrUnit_Tel = NSMutableArray()
    var arrUnit_Shakar = NSMutableArray()
    var arrUnit_ChaiPatti = NSMutableArray()
    var arrUnit_Aalu = NSMutableArray()
    var arrUnit_Pyaj = NSMutableArray()
    var arrUnit_DudhPowder = NSMutableArray()
    var arrUnit_Sabun = NSMutableArray()
    var arrUnit_Namak = NSMutableArray()
    var arrUnit_LalMirch = NSMutableArray()
    var arrUnit_Haldi = NSMutableArray()
    var arrUnit_DhaniyaPowder = NSMutableArray()
    var arrUnit_GaramMasala = NSMutableArray()
    var strTag = 0

    // MARK: - ----View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view_ForHeader.backgroundColor = hexStringToUIColor(hex: primaryTheamColor)

        // Do any additional setup after loading the view.
        arrOfItemList = [["ItemName":"आटा","Quantity":"5","Unit":"KG","UnitHindi":"क़ि.ग्रा.","Checked":"false"],
                    ["ItemName":"चावल","Quantity":"1","Unit":"KG","UnitHindi":"क़ि.ग्रा.","Checked":"false"],
                    ["ItemName":"दाल","Quantity":"500","Unit":"GM","UnitHindi":"ग्रा.","Checked":"false"],
                    ["ItemName":"खाद्य तेल","Quantity":"500","Unit":"ML","UnitHindi":"मिली.","Checked":"false"],
                    ["ItemName":"शक्कर","Quantity":"1","Unit":"KG","UnitHindi":"क़ि.ग्रा.","Checked":"false"],
                    ["ItemName":"चाय पत्ती","Quantity":"250","Unit":"GM","UnitHindi":"ग्रा.","Checked":"false"],
                    ["ItemName":"आलू","Quantity":"1","Unit":"KG","UnitHindi":"क़ि.ग्रा.","Checked":"false"],
                    ["ItemName":"प्याज","Quantity":"1","Unit":"KG","UnitHindi":"क़ि.ग्रा.","Checked":"false"],
                    ["ItemName":"दूध पावडर","Quantity":"250","Unit":"GM","UnitHindi":"ग्रा.","Checked":"false"],
                    ["ItemName":"साबुन","Quantity":"1","Unit":"piece","UnitHindi":"नग","Checked":"false"],
                    ["ItemName":"नमक","Quantity":"1","Unit":"KG","UnitHindi":"क़ि.ग्रा.","Checked":"false"],
                    ["ItemName":"लाल मिर्च","Quantity":"200","Unit":"GM","UnitHindi":"ग्रा.","Checked":"false"],
                    ["ItemName":"हल्दी","Quantity":"200","Unit":"GM","UnitHindi":"ग्रा.","Checked":"false"],
                    ["ItemName":"धनिया पावडर","Quantity":"200","Unit":"GM","UnitHindi":"ग्रा.","Checked":"false"],
                    ["ItemName":"गरम मसाला","Quantity":"200","Unit":"GM","UnitHindi":"ग्रा.","Checked":"false"]]

        arrUnit_Aata = [["ItemName":"आटा","Quantity":"5","Unit":"KG","UnitHindi":"क़ि.ग्रा."],
                    ["ItemName":"आटा","Quantity":"10","Unit":"KG","UnitHindi":"क़ि.ग्रा."]]
        
        arrUnit_Chawal = [["ItemName":"चावल","Quantity":"1","Unit":"KG","UnitHindi":"क़ि.ग्रा."],
                    ["ItemName":"चावल","Quantity":"2","Unit":"KG","UnitHindi":"क़ि.ग्रा."],
                    ["ItemName":"चावल","Quantity":"5","Unit":"KG","UnitHindi":"क़ि.ग्रा."]]
        
        arrUnit_Dal = [["ItemName":"दाल","Quantity":"500","Unit":"GM","UnitHindi":"ग्रा."],
                    ["ItemName":"दाल","Quantity":"1","Unit":"KG","UnitHindi":"क़ि.ग्रा."],
                    ["ItemName":"दाल","Quantity":"2","Unit":"KG","UnitHindi":"क़ि.ग्रा."]]
        
        arrUnit_Tel = [["ItemName":"खाद्य तेल","Quantity":"500","Unit":"ML","UnitHindi":"मिली."],
                    ["ItemName":"खाद्य तेल","Quantity":"1","Unit":"L","UnitHindi":"ली."],
                    ["ItemName":"खाद्य तेल","Quantity":"2","Unit":"L","UnitHindi":"ली."]]
        
        arrUnit_Shakar = [["ItemName":"शक्कर","Quantity":"1","Unit":"KG","UnitHindi":"क़ि.ग्रा."],
                    ["ItemName":"शक्कर","Quantity":"2","Unit":"KG","UnitHindi":"क़ि.ग्रा."],
                    ["ItemName":"शक्कर","Quantity":"5","Unit":"KG","UnitHindi":"क़ि.ग्रा."]]
        
        arrUnit_ChaiPatti = [["ItemName":"चाय पत्ती","Quantity":"250","Unit":"GM","UnitHindi":"ग्रा."],
                    ["ItemName":"चाय पत्ती","Quantity":"500","Unit":"GM","UnitHindi":"ग्रा."],
                    ["ItemName":"चाय पत्ती","Quantity":"1","Unit":"KG","UnitHindi":"क़ि.ग्रा."]]
        
         arrUnit_Aalu = [["ItemName":"आलू","Quantity":"1","Unit":"KG","UnitHindi":"क़ि.ग्रा."],
                    ["ItemName":"आलू","Quantity":"2","Unit":"KG","UnitHindi":"क़ि.ग्रा."],
                    ["ItemName":"आलू","Quantity":"5","Unit":"KG","UnitHindi":"क़ि.ग्रा."]]
        
        arrUnit_Pyaj = [["ItemName":"प्याज","Quantity":"1","Unit":"KG","UnitHindi":"क़ि.ग्रा."],
                    ["ItemName":"प्याज","Quantity":"2","Unit":"KG","UnitHindi":"क़ि.ग्रा."],
                    ["ItemName":"प्याज","Quantity":"5","Unit":"KG","UnitHindi":"क़ि.ग्रा."]]
        
        arrUnit_DudhPowder = [["ItemName":"दूध पावडर","Quantity":"250","Unit":"GM","UnitHindi":"ग्रा."],
                    ["ItemName":"दूध पावडर","Quantity":"500","Unit":"GM","UnitHindi":"ग्रा."]]
        
        arrUnit_Sabun = [["ItemName":"साबुन","Quantity":"1","Unit":"piece","UnitHindi":"नग"],
                    ["ItemName":"साबुन","Quantity":"2","Unit":"piece","UnitHindi":"नग"],
                    ["ItemName":"साबुन","Quantity":"5","Unit":"piece","UnitHindi":"नग"]]
        
        arrUnit_Namak = [["ItemName":"नमक","Quantity":"1","Unit":"KG","UnitHindi":"क़ि.ग्रा."],
                   ["ItemName":"नमक","Quantity":"2","Unit":"KG","UnitHindi":"क़ि.ग्रा."]]
        
        arrUnit_LalMirch = [["ItemName":"लाल मिर्च","Quantity":"200","Unit":"GM","UnitHindi":"ग्रा."]]
        
        arrUnit_Haldi = [["ItemName":"हल्दी","Quantity":"200","Unit":"GM","UnitHindi":"ग्रा."]]

        arrUnit_DhaniyaPowder = [["ItemName":"धनिया पावडर","Quantity":"200","Unit":"GM","UnitHindi":"ग्रा."]]

        arrUnit_GaramMasala = [["ItemName":"गरम मसाला","Quantity":"200","Unit":"GM","UnitHindi":"ग्रा."]]

        
        btnOrderNow.layer.borderColor = UIColor.gray.cgColor
        btnOrderNow.layer.cornerRadius = btnOrderNow.frame.size.height/2
        btnOrderNow.layer.borderWidth = 1.0
        self.callZoneAPI(sender: 0)
    }
    
    // MARK: - ----Button Actions

    @IBAction func actiononBack(_ sender: UIButton) {
          for controller in self.navigationController!.viewControllers as Array {
                                     if controller.isKind(of: DashBoardVC.self) {
                                         _ =  self.navigationController!.popToViewController(controller, animated: true)
                                         break
                                     }
                                 }
       // self.navigationController?.popViewController(animated: false)
    }
    @IBAction func actiononWard(_ sender: UIButton) {
        if(self.txtZoneNo!.tag == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select Zone first.", viewcontrol: self)
        }else{
           aryWardlist.count == 0 ? self.callWARDAPI(sender: 1, strZOneID: "\(self.txtZoneNo!.tag)") : PopViewWithArray(tag: 15, aryData: aryWardlist, strTitle: "Select Ward")
        }
        
       
    }
    @IBAction func actiononZone(_ sender: UIButton) {
        aryZonelist.count == 0 ? self.callZoneAPI(sender: 1) : PopViewWithArray(tag: 16, aryData: aryZonelist, strTitle: "Select Zone")

    }
    @IBAction func actiononOrderNow(_ sender: UIButton) {
        
        if(validation()){

            var strMsg = ""
            let arrOfFinalOrderToSend = NSMutableArray()

            for k in 0 ..< arrOfItemList.count {

                var str = String()
                
                let dictData = arrOfItemList[k] as! NSDictionary
                
                if "\(dictData.value(forKey: "Checked") ?? "")" == "true" {
                    
                    var dictMutable = NSMutableDictionary()
                    let dict = arrOfItemList.object(at: k)as! NSDictionary
                    dictMutable = dict.mutableCopy() as! NSMutableDictionary
                    dictMutable.setValue("\(dictData.value(forKey: "ItemName")!)", forKey: "ItemName")
                    dictMutable.setValue("\(dictData.value(forKey: "Quantity")!)", forKey: "Quantity")
                    dictMutable.setValue("\(dictData.value(forKey: "Unit")!)", forKey: "Unit")
                    arrOfFinalOrderToSend.add(dictMutable)

                    str = "\(dictData.value(forKey: "ItemName") ?? "")" + ": \(dictData.value(forKey: "Quantity") ?? "")" + "  \(dictData.value(forKey: "UnitHindi") ?? "")"
                    
                    if strMsg.count > 0{
                        
                        strMsg = strMsg + "\n " + str
                        
                    }else{
                        
                        strMsg = strMsg + "\n" + str
                        
                    }
                    
                }

            }
            
            strMsg = strMsg + "\n\n\n " + "---मांगकर्ता का विवरण---"

            if txtZoneNo.text!.count > 0 {
                
                strMsg = strMsg + "\n\n " + "झोन क्र.: " + txtZoneNo.text!

            }
            if txtWardNo.text!.count > 0 {
                
                strMsg = strMsg + "\n " + "वार्ड क्र.: " + txtWardNo.text!

            }
            
            if txtRouteNo.text!.count > 0 {
                
                strMsg = strMsg + "\n " + "रुट क्र.: " + txtRouteNo.text!
                
            }
            
            if txtName.text!.count > 0 {
                
                strMsg = strMsg + "\n\n\n " + "नाम: " + txtName.text!
                
            }
            
            if txtMobileNo.text!.count > 0 {
                
                strMsg = strMsg + "\n " + "मोबाइल नंबर: " + txtMobileNo.text!
                
            }
            
            if txtAddress.text!.count > 0 {
                
                strMsg = strMsg + "\n " + "पता: " + txtAddress.text!
                
            }

            
            if arrOfFinalOrderToSend.count != 0 {
                
                let alertCOntroller = UIAlertController(title: "Order List.", message: strMsg, preferredStyle: .alert)
                
                let alertAction = UIAlertAction(title: "Order Now", style: .default, handler: { (action) in
                    print(arrOfFinalOrderToSend)
                    var strCityId = ""
                    if(dictCityData.value(forKey: "CityData")as! NSArray).count != 0{
                        strCityId = "\(((dictCityData.value(forKey: "CityData")as! NSArray).object(at: 0)as! NSDictionary).value(forKey: "city_id")!)"
                    }
                    let dict = NSMutableDictionary()
                    dict.setValue(dict, forKey: "GroceryRecieptItemMdl")
                    dict.setValue(self.strTitle, forKey: "Category")
                    dict.setValue(strCityId, forKey: "city_id")
                    dict.setValue(self.txtName.text, forKey: "Name")
                    dict.setValue(self.txtMobileNo.text, forKey: "MobileNo")
                    dict.setValue(self.txtAddress.text, forKey: "DeliveryAddress")
                    dict.setValue(self.txtZoneNo.text, forKey: "ZoneNo")
                    dict.setValue(self.txtWardNo.text, forKey: "WardNo")
                    dict.setValue(self.txtRouteNo.text, forKey: "RouteNo")
                    dict.setValue(DeviceID, forKey: "SecureKey")
                    
                    let aryOrder = NSMutableArray()
                    for item in arrOfFinalOrderToSend{
                        let dict = NSMutableDictionary()
                        dict.setValue("\((item as AnyObject).value(forKey: "ItemName")!)", forKey: "ItemName")
                        dict.setValue("\((item as AnyObject).value(forKey: "Quantity")!)", forKey: "Quantity")
                        dict.setValue("\((item as AnyObject).value(forKey: "Unit")!)", forKey: "Unit")
                        aryOrder.add(dict)
                    }
                    dict.setValue(aryOrder, forKey: "GroceryRecieptItemMdl")
                    
                    self.callAddGroceryReciept(dictSendData: dict)
                    
                })
                let alertCancel = UIAlertAction(title: "Cancel", style: .default, handler: { (action) in
                    
                    
                })
                    alertCOntroller.addAction(alertAction)
                    alertCOntroller.addAction(alertCancel)
                self.present(alertCOntroller, animated: true, completion: nil)
                
            }
            
        }
    }
    
    //MARK: Other Function's
    func PopViewWithArray(tag: Int , aryData : NSMutableArray , strTitle: String)  {
        if(aryData.count != 0){
            let vc: SelectionClass = mainStoryboard.instantiateViewController(withIdentifier: "SelectionClass") as! SelectionClass
               vc.strTitle = strTitle
               vc.strTag = tag
               vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
               vc.modalTransitionStyle = .coverVertical
               vc.delegate = self
               vc.aryList = aryData
               
               self.present(vc, animated: true, completion: {})
        }else{
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertDataNotFound, viewcontrol: self)
        }
   
    }
    //MARK: -----------------------------------Functions----------------------------------------
    
    @objc func actionOnCheckBoxBtn(sender: UIButton!)
    {
        
        let dictData = arrOfItemList[sender.tag] as! NSDictionary
        
        if(sender.currentImage == UIImage(named: "CheckBox-Selected"))
        {
            sender.setImage(UIImage(named: "CheckBox-Unselected"), for: .normal)
            var dictMutable = NSMutableDictionary()
            dictMutable = dictData.mutableCopy() as! NSMutableDictionary
            dictMutable.setValue("false", forKey: "Checked")
            self.arrOfItemList.replaceObject(at: sender.tag, with: dictMutable)
            
        }
        else
        {
            sender.setImage(UIImage(named: "CheckBox-Selected"), for: .normal)
            var dictMutable = NSMutableDictionary()
            dictMutable = dictData.mutableCopy() as! NSMutableDictionary
            dictMutable.setValue("true", forKey: "Checked")
            self.arrOfItemList.replaceObject(at: sender.tag, with: dictMutable)
        }
        
    }
    
    @objc func actionOnQtyBtn(sender: UIButton!)
    {
        
        strTag = sender.tag
        
        switch (sender.tag) {
          case 0:
            self.PopViewWithArray(tag: 14, aryData: arrUnit_Aata, strTitle: "Select")
            break;
          case 1:
            self.PopViewWithArray(tag: 14, aryData: arrUnit_Chawal, strTitle: "Select")
            break;
          case 2:
            self.PopViewWithArray(tag: 14, aryData: arrUnit_Dal, strTitle: "Select")
            break;
          case 3:
            self.PopViewWithArray(tag: 14, aryData: arrUnit_Tel, strTitle: "Select")
            break;
          case 4:
            self.PopViewWithArray(tag: 14, aryData: arrUnit_Shakar, strTitle: "Select")
            break;
          case 5:
            self.PopViewWithArray(tag: 14, aryData: arrUnit_ChaiPatti, strTitle: "Select")
            break;
          case 6:
            self.PopViewWithArray(tag: 14, aryData: arrUnit_Aalu, strTitle: "Select")
            break;
          case 7:
            self.PopViewWithArray(tag: 14, aryData: arrUnit_Pyaj, strTitle: "Select")
            break;
          case 8:
            self.PopViewWithArray(tag: 14, aryData: arrUnit_DudhPowder, strTitle: "Select")
            break;
          case 9:
            self.PopViewWithArray(tag: 14, aryData: arrUnit_Sabun, strTitle: "Select")
            break;
          case 10:
            self.PopViewWithArray(tag: 14, aryData: arrUnit_Namak, strTitle: "Select")
            break;
          case 11:
            self.PopViewWithArray(tag: 14, aryData: arrUnit_LalMirch, strTitle: "Select")
            break;
          case 12:
            self.PopViewWithArray(tag: 14, aryData: arrUnit_Haldi, strTitle: "Select")
            break;
          case 13:
            self.PopViewWithArray(tag: 14, aryData: arrUnit_DhaniyaPowder, strTitle: "Select")
            break;
          default:
            self.PopViewWithArray(tag: 14, aryData: arrUnit_GaramMasala, strTitle: "Select")
            break;
        }
        
    }
    
    //MARK: -----------------------------------Validation----------------------------------------

    func validation() -> Bool {

        if(txtZoneNo.text == "")
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter zone no.", viewcontrol: self)
            return false
            
        }
        if(txtWardNo.text == "")
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter ward no.", viewcontrol: self)
            return false
            
        }
        if(txtName.text == "")
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter full name.", viewcontrol: self)
            return false
            
        }
        if(txtMobileNo.text == "")
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter mobile no.", viewcontrol: self)
            return false
            
        }
        else if(txtMobileNo.text!.count < 10){
                   showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_Mobile_limit, viewcontrol: self)
                   return false
               }
        if(txtAddress.text == "")
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter address.", viewcontrol: self)
            return false
            
        }
        let resultPredicate = NSPredicate(format: "Checked contains[c] %@", argumentArray: ["true"])
        let arrayfilter = (self.arrOfItemList ).filtered(using: resultPredicate)
        let nsMutableArray = NSMutableArray(array: arrayfilter)
        if(nsMutableArray.count < 3)
        {
            
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please select minimum 3 items.", viewcontrol: self)
            return false
            
        }
        return true

    }
    //MARK: -----------------------------------API Calling----------------------------------------

          func callAddGroceryReciept(dictSendData : NSMutableDictionary) {
              if !isInternetAvailable() {
                  FTIndicator.showToastMessage(alertInternet)
              }else{
                var json1 = Data()
                var jsonAdd = NSString()
                
                if JSONSerialization.isValidJSONObject(dictSendData) {
                    // Serialize the dictionary
                    json1 = try! JSONSerialization.data(withJSONObject: dictSendData, options: .prettyPrinted)
                    jsonAdd = String(data: json1, encoding: .utf8)! as NSString
                    print("UpdateLeadinfo JSON: \(jsonAdd)")
                }
                if(dictSendData.count == 0){
                    jsonAdd = ""
                }
             
                    let loader = loader_Show(controller: self, strMessage: "", title: "Please wait...", style: .alert)
                    let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><AddGroceryReciept xmlns='http://m.citizencop.org/'><GroceryRecieptMdl>\(jsonAdd)</GroceryRecieptMdl></AddGroceryReciept></soap12:Body></soap12:Envelope>"
                 
                    WebServiceClass.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: BaseURL3, resultype: "AddGroceryRecieptResult", responcetype: "AddGroceryRecieptResponse") { (responce, status) in
                     loader.dismiss(animated: false, completion: nil)
                      if status == "Suceess"{
                          let dictTemp = (responce.value(forKey: "data")as! NSDictionary).mutableCopy()as! NSMutableDictionary
                       if("\(dictTemp.value(forKey: "Result")!)" == "True"){
                        let alert = UIAlertController(title: alertInfo, message: "\(dictTemp.value(forKey: "Msg")!)", preferredStyle: UIAlertController.Style.alert)
                           
                           // add the actions (buttons)
                           alert.addAction(UIAlertAction (title: "OK", style: .default, handler: { (nil) in
                            for controller in self.navigationController!.viewControllers as Array {
                                if controller.isKind(of: DashBoardVC.self) {
                                    _ =  self.navigationController!.popToViewController(controller, animated: true)
                                    break
                                }
                            }
                           }))
                           self.present(alert, animated: true, completion: nil)
                        for controller in self.navigationController!.viewControllers as Array {
                            if controller.isKind(of: DashBoardVC.self) {
                                _ =  self.navigationController!.popToViewController(controller, animated: true)
                                break
                            }
                        }
                       }else{
                           showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(dictTemp.value(forKey: "Msg")!)", viewcontrol: self)

                       }
                        
                      }else{
                          showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                      }
                  }
          }
}
    
                
    func callWARDAPI(sender : Int ,strZOneID : String) {
              if !isInternetAvailable() {
                  FTIndicator.showToastMessage(alertInternet)
              }else{
                  let loader = loader_Show(controller: self, strMessage: "", title: "Please wait...", style: .alert)
                  let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><GetWard xmlns='http://m.citizencop.org/'><ZoneId>\(strZOneID)</ZoneId></GetWard></soap12:Body></soap12:Envelope>"
                  
                  WebServiceClass.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: BaseURL3, resultype: "GetWardResult", responcetype: "GetWardResponse") { (responce, status) in
                     loader.dismiss(animated: false, completion: nil)
                      if status == "Suceess"{
                          let dictTemp = (responce.value(forKey: "data")as! NSDictionary).mutableCopy()as! NSMutableDictionary
                       if("\(dictTemp.value(forKey: "Result")!)" == "True"){
                           self.aryWardlist = NSMutableArray()
                           self.aryWardlist = (dictTemp.value(forKey: "Ward")as! NSArray).mutableCopy()as! NSMutableArray
                        if(sender == 1){
                            self.PopViewWithArray(tag: 15, aryData: self.aryWardlist, strTitle: "Select Ward")
                        }
                       }else{
                           showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)

                       }
                        
                      }else{
                          showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                      }
                  }
              }
          }
    func callZoneAPI(sender : Int) {
             if !isInternetAvailable() {
                 FTIndicator.showToastMessage(alertInternet)
             }else{
                 let loader = loader_Show(controller: self, strMessage: "", title: "Please wait...", style: .alert)
                var strCityId = ""
                if(dictCityData.value(forKey: "CityData")as! NSArray).count != 0{
                    strCityId = "\(((dictCityData.value(forKey: "CityData")as! NSArray).object(at: 0)as! NSDictionary).value(forKey: "city_id")!)"
                }
                let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><GetZone xmlns='http://m.citizencop.org/'><city_id>\(strCityId)</city_id></GetZone></soap12:Body></soap12:Envelope>"
                 
                 WebServiceClass.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: BaseURL3, resultype: "GetZoneResult", responcetype: "GetZoneResponse") { (responce, status) in
                    loader.dismiss(animated: false, completion: nil)
                     if status == "Suceess"{
                         let dictTemp = (responce.value(forKey: "data")as! NSDictionary).mutableCopy()as! NSMutableDictionary
                      if("\(dictTemp.value(forKey: "Result")!)" == "True"){
                          self.aryZonelist = NSMutableArray()
                          self.aryZonelist = (dictTemp.value(forKey: "Zone")as! NSArray).mutableCopy()as! NSMutableArray
                        if(sender == 1){
                            self.PopViewWithArray(tag: 16, aryData: self.aryZonelist, strTitle: "Select Zone")
                        }
                      }else{
                          showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)

                      }
                       
                     }else{
                         showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                     }
                 }
             }
         }
}
// MARK: - ----------------UITableViewDelegate
// MARK: -

extension KiranaMangParchiVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrOfItemList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tvlist.dequeueReusableCell(withIdentifier: "KiranaMangParchiCell", for: indexPath as IndexPath) as! KiranaMangParchiCell
                
        let dict = arrOfItemList.object(at: indexPath.row)as! NSDictionary
        cell.lblItemName.text = dict["ItemName"]as? String
        
        let strNo = "\(indexPath.row + 1)."
        cell.btnCheckBox.setTitle(strNo, for: .normal)
        cell.btnCheckBox.setImage(UIImage(named: "CheckBox-Selected"), for: .normal)
        //CheckBox-Unselected
        
        if "\(dict.value(forKey: "Checked")!)" == "true"{
            
            cell.btnCheckBox.setImage(UIImage(named: "CheckBox-Selected"), for: .normal)

        } else {
            
            cell.btnCheckBox.setImage(UIImage(named: "CheckBox-Unselected"), for: .normal)

        }
        
        cell.txtFldQty.text = "\(dict.value(forKey: "Quantity")!)" + " \(dict.value(forKey: "UnitHindi")!)"
        
        cell.btnCheckBox.tag = indexPath.row
        cell.bntQty.tag = indexPath.row
        cell.btnCheckBox.addTarget(self, action: #selector(actionOnCheckBoxBtn), for: .touchUpInside)
        cell.bntQty.addTarget(self, action: #selector(actionOnQtyBtn), for: .touchUpInside)
        
        cell.bntQty.layer.borderColor = UIColor.gray.cgColor
        cell.bntQty.layer.cornerRadius = 5.0
        cell.bntQty.layer.borderWidth = 1.0
        
        return cell
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 52
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
 
}
// MARK: - ----------------NewsCell
// MARK: -
class KiranaMangParchiCell: UITableViewCell {

    @IBOutlet weak var lblItemName: UILabel!
    @IBOutlet weak var btnCheckBox: UIButton!
    @IBOutlet weak var txtFldQty: ACFloatingTextfield!
    @IBOutlet weak var bntQty: UIButton!

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}


//MARK:-
//MARK:- ---------PopUpDelegate

extension KiranaMangParchiVC : PopUpDelegate
{
    func getDataFromPopupDelegate(dictData: NSDictionary, tag: Int) {
        if(tag == 15){
            txtWardNo.text = "\(dictData.value(forKey: "WardNo")!)"
            txtWardNo.tag = Int("\(dictData.value(forKey: "WardId")!)")!

        }
        else if(tag == 16){
            self.aryWardlist = NSMutableArray()
            txtWardNo.text = ""
            txtWardNo.tag = 0
            txtZoneNo.text = "\(dictData.value(forKey: "ZoneNo")!)"
            txtZoneNo.tag = Int("\(dictData.value(forKey: "ZoneId")!)")!

        }
        else{
            var dictMutable = NSMutableDictionary()
            let dict = arrOfItemList.object(at: strTag)as! NSDictionary
            dictMutable = dict.mutableCopy() as! NSMutableDictionary
            dictMutable.setValue("\(dictData.value(forKey: "Quantity")!)", forKey: "Quantity")
            dictMutable.setValue("\(dictData.value(forKey: "Unit")!)", forKey: "Unit")
            dictMutable.setValue("\(dictData.value(forKey: "UnitHindi")!)", forKey: "UnitHindi")
            self.arrOfItemList.replaceObject(at: strTag, with: dictMutable)
            self.tvlist.reloadData()
        }
    }
}
