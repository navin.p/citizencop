//
//  PledgeVC.swift
//  CitizenCOP
//
//  Created by Navin Patidar on 7/15/19.
//  Copyright © 2019 CitizenCop. All rights reserved.
//

import UIKit

class PledgeVC: UIViewController {
    
    // MARK: - ----IBOutlet
    
    @IBOutlet weak var view_ForHeader: CardView!
    @IBOutlet weak var imagePledge: UIImageView!
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnAgree: UIButton!
    @IBOutlet weak var btnSkip: UIButton!
    @IBOutlet weak var btnReminder: UIButton!
    
    @IBOutlet weak var bottomConstraintBotton: NSLayoutConstraint!
    
    @IBOutlet weak var heightContraintButton: NSLayoutConstraint!
    var frame = CGRect()
    
    var aryPledgeData =  NSMutableArray()
    open weak var delegate:PledgeDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // view_ForHeader.backgroundColor = hexStringToUIColor(hex: primaryDark)
        btnAgree.layer.cornerRadius = 2.0
        btnSkip.layer.cornerRadius = 2.0
        btnReminder.layer.cornerRadius = 2.0
        btnAgree.layer.borderColor = hexStringToUIColor(hex: primaryTheamColor).cgColor
        btnSkip.layer.borderColor = hexStringToUIColor(hex: primaryTheamColor).cgColor
        btnReminder.layer.borderColor = hexStringToUIColor(hex: primaryTheamColor).cgColor
        btnAgree.layer.borderWidth = 0.5
        btnSkip.layer.borderWidth = 0.5
        btnReminder.layer.borderWidth = 0.5
        
        btnAgree.setTitleColor(hexStringToUIColor(hex: primaryTheamColor), for: .normal)
        btnSkip.setTitleColor(hexStringToUIColor(hex: primaryTheamColor), for: .normal)
        btnReminder.setTitleColor(hexStringToUIColor(hex: primaryTheamColor), for: .normal)
        self.heightContraintButton.constant = 30.0
        self.view.layer.cornerRadius = 10.0
        
        if(aryPledgeData.count != 0){
            let dict  = aryPledgeData.object(at: 0)as! NSDictionary
            webView.loadHTMLString("\(dict.value(forKey: "Pledge")!)", baseURL: nil)
            lblTitle.text = "\(dict.value(forKey: "PledgeTitle")!)"
            
            
            let urlImage = "\(BaseURL2)PledgeLogo//\(dict.value(forKey: "PledgeLogo")!)"
            imagePledge.setImageWith(URL(string: urlImage), placeholderImage: UIImage(named: ""), options: SDWebImageOptions(rawValue: 1), completed: { (image, error, type, url) in
                print(url ?? 0)
            }, usingActivityIndicatorStyle: .gray)
            
            if(nsud.value(forKey: "CitiZenCOP_PledgeStatus") != nil){
                let strPledgeStatus = nsud.value(forKey: "CitiZenCOP_PledgeStatus") as! String
                if (strPledgeStatus == "AGREE"){
                    btnAgree.isHidden = true
                    btnSkip.isHidden = true
                    btnReminder.isHidden = true
                    self.heightContraintButton.constant = 0.0
                }
            }
        }
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        UIView.animate(withDuration: 0.3, animations: { [weak self] in
            self?.bottomConstraintBotton.constant = self!.frame.origin.y + 5
            self!.view.frame = self!.frame
        })
        
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        
    }
    
    // MARK: - --------------IBAction
    // MARK: -
    @IBAction func actionOnBack(_ sender: UIButton) {
        let date = Date()
             let dateFormatter = DateFormatter()
             dateFormatter.dateFormat = "dd"
             let day  = dateFormatter.string(from: date)
             nsud.set(day, forKey: "CItizenCOP_PledgeUpdateDate")
        self.delegate?.getDataFromPledgeDelegate()
        UIView.animate(withDuration: 0.6, animations: { [weak self] in
            self!.willMove(toParent: nil)
            self!.removeFromParent()
            self!.view.removeFromSuperview()
        })
        //  self.dismiss(animated: true) {}
    }
    
    @IBAction func actionOnIAgree(_ sender: Any) {
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd"
        let day  = dateFormatter.string(from: date)
        nsud.set(day, forKey: "CItizenCOP_PledgeUpdateDate")
        callAgreePledgeAPI()
    }
    
    @IBAction func actionOnReminder(_ sender: Any) {
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd"
        let day  = dateFormatter.string(from: date)
        nsud.set(day, forKey: "CItizenCOP_PledgeUpdateDate")
       
        dateFormatter.dateFormat = "dd"
        let dateString = dateFormatter.string(from: Date())
        nsud.set("\(dateString)", forKey: "REMINDER_Date")
        nsud.set("REMINDER", forKey: "CitiZenCOP_PledgeStatus")
        self.delegate?.getDataFromPledgeDelegate()
        UIView.animate(withDuration: 0.6, animations: { [weak self] in
            self!.willMove(toParent: nil)
            self!.removeFromParent()
            self!.view.removeFromSuperview()
        })
        
    }
    @IBAction func actionOnSkip(_ sender: Any) {
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd"
        let day  = dateFormatter.string(from: date)
        nsud.set(day, forKey: "CItizenCOP_PledgeUpdateDate")
        callSkipPledgeAPI()
    }
    // MARK: - --------------API Calling
    // MARK: -
    
    func callAgreePledgeAPI() {
        if !isInternetAvailable() {
            FTIndicator.showNotification(withTitle: alertMessage, message: alertInternet)
        }else{
            customDotLoaderShowOnButton(btn: btnAgree, view: self.view, controller: self)
            var strURl = ""
            if(dictCityData.value(forKey: "CityData")as! NSArray).count != 0{
                let dict  = aryPledgeData.object(at: 0)as! NSDictionary
                
                let strCityId = "\(((dictCityData.value(forKey: "CityData")as! NSArray).object(at: 0)as! NSDictionary).value(forKey: "city_id")!)"
                
                let dateFormatter : DateFormatter = DateFormatter()
                dateFormatter.dateFormat = "MM/dd/yyyy"
                let dateString = dateFormatter.string(from: Date())
                
                strURl = "\(API_PledgeAgree)" + "&PledgeContentId=\("\(dict.value(forKey: "PledgeContentId")!)")&cityid=\(strCityId)&imeino=\(DeviceID)&pledgestatus=yes&pledgedate=\(dateString)"
            }
            
            WebServiceClass.callAPIBYPOST(parameter: NSDictionary(), url: strURl) { (responce, status) in
                customeDotLoaderRemove()
                if status == "Suceess"{
                    nsud.set("AGREE", forKey: "CitiZenCOP_PledgeStatus")
                    self.delegate?.getDataFromPledgeDelegate()
                    UIView.animate(withDuration: 0.6, animations: { [weak self] in
                        self!.willMove(toParent: nil)
                        self!.removeFromParent()
                        self!.view.removeFromSuperview()
                    })
                }
                else{
                    
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                    
                }
            }
        }
    }
    func callSkipPledgeAPI() {
        if !isInternetAvailable() {
            FTIndicator.showNotification(withTitle: alertMessage, message: alertInternet)
        }else{
            customDotLoaderShowOnButton(btn: btnSkip, view: self.view, controller: self)
            var strURl = ""
            if(dictCityData.value(forKey: "CityData")as! NSArray).count != 0{
                let dict  = aryPledgeData.object(at: 0)as! NSDictionary
                
                let strCityId = "\(((dictCityData.value(forKey: "CityData")as! NSArray).object(at: 0)as! NSDictionary).value(forKey: "city_id")!)"
                
                let dateFormatter : DateFormatter = DateFormatter()
                dateFormatter.dateFormat = "MM/dd/yyyy"
                let dateString = dateFormatter.string(from: Date())
                
                strURl = "\(API_PledgeAgree)" + "&PledgeContentId=\("\(dict.value(forKey: "PledgeContentId")!)")&cityid=\(strCityId)&imeino=\(DeviceID)&pledgestatus=no&pledgedate=\(dateString)"
            }
            
            WebServiceClass.callAPIBYPOST(parameter: NSDictionary(), url: strURl) { (responce, status) in
                customeDotLoaderRemove()
                if status == "Suceess"{
                    nsud.set("SKIP", forKey: "CitiZenCOP_PledgeStatus")
                    self.delegate?.getDataFromPledgeDelegate()
                    UIView.animate(withDuration: 0.6, animations: { [weak self] in
                        self!.willMove(toParent: nil)
                        self!.removeFromParent()
                        self!.view.removeFromSuperview()
                    })
                }
                else{
                    
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                    
                }
            }
        }
    }
}
//MARK: -------Protocol

protocol PledgeDelegate : class{
    func getDataFromPledgeDelegate()
}
