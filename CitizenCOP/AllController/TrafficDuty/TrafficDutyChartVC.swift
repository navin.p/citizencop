//
//  TrafficDutyChartVC.swift
//  CitizenCOP
//
//  Created by Navin Patidar on 11/28/19.
//  Copyright © 2019 CitizenCop. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit

class TrafficDutyChartVC: UIViewController {
  
    // MARK: - --------------IBOutlet
    // MARK: -
    
    @IBOutlet weak var view_ForHeader: UIView!
    @IBOutlet weak var tvlist: UITableView!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblPoliceControlRoomNumber: UILabel!
    @IBOutlet weak var lblTrafficControlNumber: UILabel!
    @IBOutlet weak var demoLabel1: MarqueeLabel!
   @IBOutlet weak var lblStationName: UILabel!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var navingation_Item: UINavigationItem!
    @IBOutlet weak var lblTitle: UILabel!

    private var locationManager: CLLocationManager!
      private var currentLocation: CLLocation?
    
    
    // MARK: - --------------Variable
    // MARK: -
    var ErrorMsg = ""
    var aryData =  NSArray()
    var strTitle = String()
    
    // MARK: - ----Life Cycle
      // MARK:
    override func viewDidLoad() {
        super.viewDidLoad()
        view_ForHeader.backgroundColor = hexStringToUIColor(hex: primaryTheamColor)
       // aryData =  NSArray()
        if(aryData.count == 0){
            self.ErrorMsg = alert_TrafficDuty
        }else{
            
            let dict = aryData.object(at: 0)as! NSDictionary
            let   TrafficDutyPoint = "\(dict.value(forKey: "TrafficDutyPoint")!)"
            let   PoliceStation = "\(dict.value(forKey: "PoliceStation")!)"
            let   PoliceControlRoomNo = "\(dict.value(forKey: "PoliceControlRoomNo")!)"
            let   TrafficHelpLineNo = "\(dict.value(forKey: "TrafficHelpLineNo")!)"
            
            lblTrafficControlNumber.text = "Traffic Control Room No. \(TrafficHelpLineNo)"
            lblPoliceControlRoomNumber.text = "Police Control Room No. \(PoliceControlRoomNo)"
            lblStationName.text = "Nearest Police Station : \(PoliceStation)"
            demoLabel1.text = "\(TrafficDutyPoint)"
            demoLabel1.tag = 101
            demoLabel1.type = .continuous
            demoLabel1.animationCurve = .easeInOut


            // Text string, fade length, leading buffe
        }
        self.tvlist.reloadData()
        self.lblTitle.text = strTitle
            getCurrentLocation()
       
        
    }
    
    // MARK: - ---------------IBAction
       // MARK: -
       @IBAction func actiononBack(_ sender: UIButton) {
           self.navigationController?.popViewController(animated: true)
       }
 func getCurrentLocation()
   {
     mapReload()
       locationManager = CLLocationManager()
       locationManager.delegate = self
       locationManager.desiredAccuracy = kCLLocationAccuracyBest
       locationManager.requestAlwaysAuthorization()
       if CLLocationManager.locationServicesEnabled() {
           switch CLLocationManager.authorizationStatus() {
           case .notDetermined, .restricted, .denied:
               showLocationAccessAlertAction(viewcontrol: self)
           case .authorizedAlways, .authorizedWhenInUse:
               print("Access")
               locationManager.startUpdatingLocation()
           @unknown default:
              break
           }
       } else {
           print("Location services are not enabled")
       }
   }
    func getAddressFromLatLon(pdblLatitude: String, withLongitude pdblLongitude: String) {
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        let lat: Double = Double("\(pdblLatitude)")!
        //21.228124
        let lon: Double = Double("\(pdblLongitude)")!
        //72.833770
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = lat
        center.longitude = lon
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                if (error != nil)
                {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                    return
                }
                let pm = placemarks! as [CLPlacemark]
                if pm.count > 0 {
                    let pm = placemarks![0]
           
                    var addressString : String = ""
                    if pm.subLocality != nil {
                        addressString = addressString + pm.subLocality! + ", "
                    }
                    if pm.thoroughfare != nil {
                        addressString = addressString + pm.thoroughfare! + ", "
                    }
                    if pm.locality != nil {
                        addressString = addressString + pm.locality! + ", "
                    }
                    if pm.country != nil {
                        addressString = addressString + pm.country! + ", "
                    }
                    if pm.postalCode != nil {
                        addressString = addressString + pm.postalCode! + " "
                    }
                    self.lblAddress.text = addressString
                    print(addressString)
                }
        })
    }
    func mapReload()  {
         let viewRegion = MKCoordinateRegion(center: mapView.centerCoordinate, latitudinalMeters: 200, longitudinalMeters: 200)
         mapView.setRegion(viewRegion, animated: false)
         mapView.delegate = self
         let buttonItem = MKUserTrackingBarButtonItem(mapView: mapView)
         self.navingation_Item.rightBarButtonItem = buttonItem
         self.mapView.showAnnotations(mapView.annotations, animated: true)
        
     }
     func getTransitETA() {
        
        
    let request = MKLocalSearch.Request()
    
             request.region = self.mapView.region
             let search = MKLocalSearch(request: request)
             search.start { response, _ in
                 guard let response = response else {
                     return
                 }
             }
     }
}
// MARK: - ----------------NewsCell
// MARK: -
class DutyCell: UITableViewCell {
    @IBOutlet weak var lblPossition: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var btnNumber: UIButton!
    @IBOutlet weak var viewAttendance: CardView!
    @IBOutlet weak var lblLineColor: UILabel!

  
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}
// MARK: - ----------------UITableViewDelegate
// MARK: -

extension TrafficDutyChartVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return aryData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tvlist.dequeueReusableCell(withIdentifier: "DutyCell", for: indexPath as IndexPath) as! DutyCell
        let dict = aryData.object(at: indexPath.row)as! NSDictionary
        
        cell.lblPossition.text = ("\(dict.value(forKey: "Designation")!)" == "") ? "NA" : "\(dict.value(forKey: "Designation")!)-"
       
        cell.lblName.text = ("\(dict.value(forKey: "OfficerName")!)" == "") ? "NA" : "\(dict.value(forKey: "OfficerName")!)"
        cell.lblTime.text = ("\(dict.value(forKey: "Designation")!)" == "") ? "NA" : "\(dict.value(forKey: "Designation")!)"

        
        cell.btnNumber.setTitle(("\(dict.value(forKey: "OfficerMobileNo")!)" == "") ? "NA" : "\(dict.value(forKey: "OfficerMobileNo")!)", for: .normal)

        
        
        let strAssignFromDateTime = "\(dict.value(forKey: "AssignFromDateTime")!)"
        let AssignFromDateTime = dateFormetViaFormatter(from: "dd/MM/yyyy hh:mm:ss a", to: "hh:mm a", datestr: strAssignFromDateTime)
        
        let strAssignToDateTime = "\(dict.value(forKey: "AssignToDateTime")!)"
        let AssignToDateTime = dateFormetViaFormatter(from: "dd/MM/yyyy hh:mm:ss a", to: "hh:mm a", datestr: strAssignToDateTime)
        
        
        
        cell.lblTime.text = "Duty Time- \(AssignFromDateTime) To \(AssignToDateTime)"
        
        cell.btnNumber.tag = indexPath.row
        cell.btnNumber.addTarget(self, action: #selector(buttonAction(_:)), for:.touchUpInside)

        
        if("\(dict.value(forKey: "IsMarkAttendance")!)" == "true"){
            cell.viewAttendance.backgroundColor = UIColor.green
        }else{
            cell.viewAttendance.backgroundColor = UIColor.gray
        }
        
        if(indexPath.row % 2 == 0){
            cell.lblLineColor.backgroundColor = hexStringToUIColor(hex: "CFA749")
        }else{
            cell.lblLineColor.backgroundColor = hexStringToUIColor(hex: "354751")

        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        
           return UITableView.automaticDimension
        
       
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 50.0
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        let customView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 50))
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 50))
        if(aryData.count != 0){
            ErrorMsg = ""
        }else{
            button.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        }
        button.setTitle(ErrorMsg, for: .normal)
        customView.addSubview(button)
        customView.backgroundColor = UIColor.clear
        button.setTitleColor(UIColor.darkGray, for: .normal)
        return customView
    }
    
    @objc func buttonAction(_ sender: UIButton!) {
      let dict = aryData.object(at: sender.tag)as! NSDictionary
        if !(callingFunction(number: "\(dict.value(forKey: "OfficerMobileNo")!)" as NSString)){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertCalling, viewcontrol: self)
        }
        
    }
}
// MARK: - ---------------MKMapViewDelegate
// MARK: -


extension TrafficDutyChartVC: MKMapViewDelegate ,CLLocationManagerDelegate  {
    
    func mapViewDidFinishLoadingMap(_ mapView: MKMapView) {
        print( mapView.centerCoordinate.latitude)
        getAddressFromLatLon(pdblLatitude: "\(mapView.centerCoordinate.latitude)", withLongitude: "\(mapView.centerCoordinate.longitude)")

    }
  
    func mapView(_ mapView: MKMapView, didChange mode: MKUserTrackingMode, animated: Bool) {
        
    }
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
         getAddressFromLatLon(pdblLatitude: "\(mapView.centerCoordinate.latitude)", withLongitude: "\(mapView.centerCoordinate.longitude)")
    }

    // CLLocationManagerDelegate
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        defer { currentLocation = locations.last }
        
        if currentLocation != nil {
            // Zoom to user location
              self.mapReload()
            if(nsud.value(forKey: "CitizenCOP_MySafeZone") == nil) || (nsud.value(forKey: "CitiZenCOP_JoinedGroupLocation") == nil){
                 locationManager.stopUpdatingLocation()
            }
               
            }
        }
    }

