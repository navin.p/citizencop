//
//  INeedHelpSupportVC.swift
//  CitizenCOP
//
//  Created by NavinPatidar on 6/7/21.
//  Copyright © 2021 CitizenCop. All rights reserved.
//

import UIKit
import CoreLocation
class INeedHelpSupportVC: UIViewController {
    // MARK: - --------------IBOutlet
    // MARK: -
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var txtDetail: KMPlaceholderTextView!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtNumber: UITextField!
    @IBOutlet weak var btnState: UIButton!
    @IBOutlet weak var btnCity: UIButton!
    
    
    @IBOutlet weak var btnSubCategory: UIButton!
    @IBOutlet weak var btnCategory: UIButton!
    @IBOutlet weak var btnSubmit: UIButton!
  
    @IBOutlet weak var collection: UICollectionView!
    @IBOutlet weak var heightVideoFile: NSLayoutConstraint!
    @IBOutlet weak var heightCollection: NSLayoutConstraint!
    @IBOutlet weak var heightAudioFile: NSLayoutConstraint!
    @IBOutlet weak var lblVedioName: UILabel!
    @IBOutlet weak var lblAudioName: UILabel!
    @IBOutlet weak var viewVideo: CardView!
    @IBOutlet weak var viewAudio: CardView!
    @IBOutlet weak var lblTitle: UILabel!

    @IBOutlet weak var tv: UITableView!

    var aryCatrgory = NSMutableArray()
    var arySubCatrgory = NSMutableArray()
    
    var aryState = NSMutableArray()
    var aryCity = NSMutableArray()
    
    var aryReportImage = NSMutableArray()
    var imagePicker = UIImagePickerController()
    var dictAudioData = NSMutableDictionary()
    var dictVideoData = NSMutableDictionary()
     var timer = Timer()
    var strAudioName = ""
    var strVideoName = ""
    var strImageName = ""
    var strlblTittle = String()
    var countryId = 0
    var cityURL = ""
    var strAddress = ""
    var loaderupload = UIAlertController()

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        txtName.delegate = self
        txtNumber.delegate = self
        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryTheamColor)
        lblTitle.text = strlblTittle
        tv.tableFooterView = UIView()
        if(nsud.value(forKey: "CitizenCOP_CityData") != nil){
            let dictCityData = nsud.value(forKey: "CitizenCOP_CityData")as! NSDictionary
            btnState.setTitle("\(dictCityData.value(forKey: "Citizen_StateName")!)", for: .normal)
            btnState.tag = dictCityData.value(forKey: "Citizen_StateID")! as! Int
            btnCity.setTitle("\(dictCityData.value(forKey: "Citizen_CityName")!)", for: .normal)
            btnCity.tag = dictCityData.value(forKey: "Citizen_CityID")! as! Int
            countryId = dictCityData.value(forKey: "Citizen_CountryID")! as! Int
            cityURL = BaseHostURL
    }
    }
    override func viewWillAppear(_ animated: Bool) { 
        super.viewWillAppear(true)
        btnSubmit.layer.cornerRadius = 5.0
        btnSubmit.backgroundColor = hexStringToUIColor(hex: primaryDark)
        txtName.layer.cornerRadius = 5.0
        txtName.layer.borderColor = UIColor.lightGray.cgColor
        txtName.layer.borderWidth = 1.0
        
        txtNumber.layer.cornerRadius = 5.0
        txtNumber.layer.borderColor = UIColor.lightGray.cgColor
        txtNumber.layer.borderWidth = 1.0
        
        collection.layer.borderWidth = 1
        collection.layer.masksToBounds = false
        collection.layer.cornerRadius = 8.0
        collection.clipsToBounds = true
        collection.layer.borderColor = UIColor.lightGray.cgColor

        self.aryCatrgory = getDataFromCoreDataBase(strEntity: "HelpCategory", strkey: "helpCategory")
        if(aryCatrgory.count == 0){
            self.call_CategoryByAPI()
        }
        strAddress = getAddressFromLatLonSync(pdblLatitude: "\(GlobleLat)", withLongitude: "\(GlobleLong)")

    }
    
    override func viewWillLayoutSubviews() {
        if(aryAudio.count == 0){
            heightAudioFile.constant = 0.0
            viewAudio.isHidden = true
        }else{
            DeviceType.IS_IPAD ? (self.heightAudioFile.constant = 45.0) :  (self.heightAudioFile.constant = 35.0)

            viewAudio.isHidden = false
        }
        if(aryVideo.count == 0){
            heightVideoFile.constant = 0.0
            viewVideo.isHidden = true

        }else{
            DeviceType.IS_IPAD ? (self.heightVideoFile.constant = 45.0) :  (self.heightVideoFile.constant = 35.0)
            viewVideo.isHidden = false
        }
        
        DeviceType.IS_IPAD ? (self.aryReportImage.count == 0 ? (self.heightCollection.constant = 0.0) :  (self.heightCollection.constant = 125.0)) : (self.aryReportImage.count == 0 ? (self.heightCollection.constant = 0.0) :  (self.heightCollection.constant = 95.0))
        

    }

    // MARK: - ---------------IBAction
    // MARK: -
    @IBAction func actiononBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func actiononSelectState(_ sender: UIButton) {
        sender.tag = 2
            if(aryState.count != 0){
                PopViewWithArray(tag: 2, aryData: aryState, strTitle: "State")
                
            }else{
                self.callstateAPI(sender: sender)
            }
        
        
    }
    @IBAction func actiononSubSelectCity(_ sender: UIButton) { //City
        sender.tag = 3

        if(btnState.tag == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertStateFirst, viewcontrol: self)
        }else{
            if(aryCity.count != 0){
                PopViewWithArray(tag: 3, aryData: aryCity, strTitle: "City")
                
            }else{
                self.callCityAPI(sender: sender)
            }
        }
    }
    
    
    
    
    @IBAction func actiononSelectCategory(_ sender: UIButton) {
        sender.tag = 13
        if(aryCatrgory.count == 0){
            self.call_CategoryByAPI()
        }
        else{
            PopViewWithArray(tag: 13, aryData: aryCatrgory, strTitle: "Select Category")
        }
    }
    @IBAction func actiononSubSelectCategory(_ sender: UIButton) {
        sender.tag = 17

        if(btnCategory.tag != 0){
            if(arySubCatrgory.count == 0){
                self.call_ReportSubCategoryByAPI(strCategoryID: "\(btnCategory.titleLabel?.text ?? "")")
            }
            else{
                PopViewWithArray(tag: 17, aryData: arySubCatrgory, strTitle: "Select Subcategory")
            }
        }else{
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSelectCategory, viewcontrol: self)
        }
    }
    @IBAction func actiononCapture(_ sender: UIButton) {
        
        if(sender.tag == 1){ // Camera
           
            if(aryReportImage.count >= 5){
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertReachedMax, viewcontrol: self)

            }else{
                imagePicker = UIImagePickerController()
                imagePicker.view.tag = 1

                if UIImagePickerController.isSourceTypeAvailable(.camera){
                    print("Button capture")
                    
                    imagePicker.delegate = self
                    imagePicker.sourceType = .camera
                    imagePicker.allowsEditing = false
                    present(imagePicker, animated: true, completion: nil)
                }
            }
           
        }else if (sender.tag == 2){ // Gallery
           
            if(aryReportImage.count >= 5){
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertReachedMax, viewcontrol: self)
            }else{
                imagePicker = UIImagePickerController()
                 imagePicker.view.tag = 1
                if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
                    print("Button capture")
                    imagePicker.delegate = self
                    imagePicker.sourceType = .savedPhotosAlbum
                    imagePicker.allowsEditing = false
                    present(imagePicker, animated: true, completion: nil)
                }
            }
           
        }else if (sender.tag == 3){ // Video
            imagePicker = UIImagePickerController()
            imagePicker.view.tag = 2
            imagePicker.sourceType = .camera
            imagePicker.cameraDevice = .rear
            imagePicker.videoMaximumDuration = 30
            imagePicker.delegate = self
            imagePicker.videoQuality = .typeLow
            let mediaTypes = UIImagePickerController.availableMediaTypes(for: .camera)
            let videoMediaTypesOnly = (mediaTypes as NSArray?)?.filtered(using: NSPredicate(format: "(SELF contains %@)", "movie"))
            let movieOutputPossible: Bool = videoMediaTypesOnly != nil
            if movieOutputPossible {
                if let videoMediaTypesOnly = videoMediaTypesOnly as? [String] {
                    imagePicker.mediaTypes = videoMediaTypesOnly
                }
                timer = Timer.scheduledTimer(timeInterval: 60.0, target: self, selector: #selector(pickerDismiss), userInfo: nil, repeats: false)
                present(imagePicker, animated: true)
            }
            
        }else if (sender.tag == 4){ // Record
            let vc: AudioRecordVC = self.storyboard!.instantiateViewController(withIdentifier: "AudioRecordVC") as! AudioRecordVC
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .coverVertical
            vc.delegate = self
            self.present(vc, animated: true, completion: {})
        }
        
    }
    @IBAction func actiononSubmit(_ sender: UIButton) {
        if(btnState.tag == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertState, viewcontrol: self)
        }
       else if(btnCity.tag == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertCity, viewcontrol: self)
        }
       else if(btnCategory.tag == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSelectCategory, viewcontrol: self)
        }
       else if(btnSubCategory.tag == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSelectSubCategory, viewcontrol: self)
        }
       else if (txtDetail.text.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertDescription, viewcontrol: self)
       } else if (txtName.text!.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_Name, viewcontrol: self)
        } else if (txtNumber.text!.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_Contact, viewcontrol: self)
        }else if (txtNumber.text!.count < 10){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_Contactinvalid, viewcontrol: self)
        }
       else{
            saveReportData()
        }
    }
    @IBAction func actiononHelp(_ sender: UIButton) {
        let testController = mainStoryboard.instantiateViewController(withIdentifier: "HelpVC")as! HelpVC
        testController.strMessage = alertReportincidenceHelp
        self.navigationController?.pushViewController(testController, animated: true)
    }
    
    
    @IBAction func actionVideoRemove(_ sender: Any) {
        heightVideoFile.constant = 0.0
        aryVideo = NSMutableArray()
        strVideoName = ""
        self.lblVedioName.text = ""
    }
    
    @IBAction func actionAudioRemove(_ sender: Any) {
        heightAudioFile.constant = 0.0
        aryAudio = NSMutableArray()
        strAudioName = ""
    }
    
    //MARK: Other Function's
    func PopViewWithArray(tag: Int , aryData : NSMutableArray , strTitle: String)  {
        let vc: SelectionClass = self.storyboard!.instantiateViewController(withIdentifier: "SelectionClass") as! SelectionClass
        vc.strTitle = strTitle
        vc.strTag = tag
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .coverVertical
        vc.delegate = self
        if(tag == 2){
            vc.aryList = filterState_CityData(aryData: aryData, stateID: 0, countryId: countryId)
            
        }else if (tag == 3){
            vc.aryList = filterState_CityData(aryData: aryData, stateID: btnState.tag, countryId: 0)
            
        }else{
            vc.aryList = aryData

        }
        self.present(vc, animated: true, completion: {})
    }
    @objc func pickerDismiss()
    {
        showAlertWithoutAnyAction(strtitle: alertReachedMax, strMessage: "Your video recorded succesfully!", viewcontrol: self)
        imagePicker.stopVideoCapture()
    }
    
    func filterState_CityData(aryData : NSMutableArray , stateID : Int , countryId : Int) -> NSMutableArray {
        let aryFilter = NSMutableArray()
        if(countryId != 0){ // for State
            for item in aryData{
                if(countryId ==  Int("\(((item as AnyObject)as! NSDictionary).value(forKey: "country_id")!)")){
                    aryFilter.add(item)
                }
            }
            
        }else { // For City
            for item in aryData{
                if(stateID ==  Int("\(((item as AnyObject)as! NSDictionary).value(forKey: "state_id")!)")){
                    aryFilter.add(item)
                }
            }
        }
        return aryFilter
    }
    
    func getAddressFromLatLonSync(pdblLatitude: String, withLongitude pdblLongitude: String) -> String {
            var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        
        let addressString = ""
        
            let lat: Double = Double("\(pdblLatitude)")!
            //21.228124
            let lon: Double = Double("\(pdblLongitude)")!
            //72.833770
            let ceo: CLGeocoder = CLGeocoder()
            center.latitude = lat
            center.longitude = lon

            let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)


            ceo.reverseGeocodeLocation(loc, completionHandler:
                {(placemarks, error) in
                    if (error != nil)
                    {
                        print("reverse geodcode fail: \(error!.localizedDescription)")
                    }
                    if(placemarks != nil){
                        let pm = placemarks! as [CLPlacemark]
                        if pm.count > 0 {
                            let pm = placemarks![0]
                            print(pm.country)
                            print(pm.locality)
                            print(pm.subLocality)
                            print(pm.thoroughfare)
                            print(pm.postalCode)
                            print(pm.subThoroughfare)
                            var addressString : String = ""
                            if pm.subLocality != nil {
                                addressString = addressString + pm.subLocality! + ", "
                            }
                            if pm.thoroughfare != nil {
                                addressString = addressString + pm.thoroughfare! + ", "
                            }
                            if pm.locality != nil {
                                addressString = addressString + pm.locality! + ", "
                            }
                            if pm.country != nil {
                                addressString = addressString + pm.country! + ", "
                            }
                            if pm.postalCode != nil {
                                addressString = addressString + pm.postalCode! + " "
                            }
                            self.strAddress = addressString
                            print(addressString)
                      }
                    }
                  
            })
        return addressString

        }
    
    // MARK: - ---------------Save In CoreData
    // MARK: -
    
    func saveReportData() {
        // For Images
        for item in aryReportImage {
            strImageName = strImageName + "\((item as AnyObject).value(forKey: "imageName")!),"
            saveImageDocumentDirectory(strFileName: "\((item as AnyObject).value(forKey: "imageName")!)", image: (item as AnyObject).value(forKey: "image")as! UIImage)
        }
        if(strImageName.count != 0){
            strImageName = String(strImageName.dropLast())
        }
//        // For Video
//
//        for item in aryVideo {
//            strVideoName = strVideoName + "\((item as AnyObject).value(forKey: "videoName")!),"
//            saveAudioVideoDocumentDirectory(strFileName: "\((item as AnyObject).value(forKey: "videoName")!)", data: (item as AnyObject).value(forKey: "video")as! Data)
//        }
//        if(strVideoName.count != 0){
//            strVideoName = String(strVideoName.dropLast())
//        }
//        // For Audio
//
//        for item in aryAudio {
//            strAudioName = strAudioName + "\((item as AnyObject).value(forKey: "audioName")!),"
//            saveAudioVideoDocumentDirectory(strFileName: "\((item as AnyObject).value(forKey: "audioName")!)", data: (item as AnyObject).value(forKey: "audio")as! Data)
//        }
//        if(strAudioName.count != 0){
//            strAudioName = String(strAudioName.dropLast())
//        }
        
   
     
        let dictData = NSMutableDictionary()
        dictData.setValue("0", forKey: "RequestByUserId")
        dictData.setValue("\(btnState.tag)", forKey: "state_id")
        dictData.setValue("\(btnCity.tag)", forKey: "city_id")
        dictData.setValue("\(btnCategory.titleLabel!.text!)", forKey: "Category")
        dictData.setValue("\(btnSubCategory.titleLabel!.text!)", forKey: "SubCategory")
        dictData.setValue("\(strImageName)", forKey: "Images")
        dictData.setValue("\(txtDetail.text!)", forKey: "Description")
        dictData.setValue("\(GlobleLat)", forKey: "Latitude")
        dictData.setValue("\(GlobleLong)", forKey: "Longitude")
    
        dictData.setValue("\(strAddress)", forKey: "Location")
        dictData.setValue("\(DeviceID)", forKey: "SecureKey")
        dictData.setValue("\(txtName.text!)", forKey: "ContactName")
        dictData.setValue("\(txtNumber.text!)", forKey: "ContactNo")
       
        print(dictData)
        SyncDataToserver(dict : dictData)
    }
    
    
    // MARK: - ---------------API CAlling
    // MARK: -
    func SyncDataToserver(dict : NSMutableDictionary) {
        loaderupload = loader_Show(controller: self, strMessage: "", title: "Please wait...", style: .alert)
        
       if(aryReportImage.count != 0){
        uploadImage(dict: dict)
       }
//       else if (aryVideo.count != 0){
//           uploadVideo()
//       }else if (aryAudio.count != 0){
//           uploadAudio()
//       }
       else{
           uploadData(dict: dict)
       }
   }
    func uploadImage(dict : NSMutableDictionary) {
       
       let imgData = (aryReportImage.object(at: 0) as! NSDictionary)
        let image = imgData.value(forKey: "image")as! UIImage
       let imageData = image.jpegData(compressionQuality:0.5)

        WebServiceClass.callAPIWithImage(parameter: NSDictionary(), url: API_UploadReportImage, data: imageData!, fileName: "\(aryReportImage.object(at: 0))", withName: "userfile") { [self] (responce, status) in
           print(responce)
           if(status == "Suceess"){
               DeleteImageFromDocumentDirectory(strFileName: "\(aryReportImage.object(at: 0))")

            aryReportImage.removeObject(at: 0)
               self.SyncDataToserver(dict : dict)
           }else{
            loaderupload.dismiss(animated: false) {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
            }
          

           }
       }
   }
    func uploadData(dict : NSMutableDictionary)  {
        if !isInternetAvailable() {
            FTIndicator.showToastMessage(alertInternet)
        }else{
            var json = Data()
            var jsonString = NSString()
            if JSONSerialization.isValidJSONObject(dict) {
                // Serialize the dictionary
                json = try! JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted)
                jsonString = String(data: json, encoding: .utf8)! as NSString
                print("UpdateLeadinfo JSON: \(jsonString)")
            }
            let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><AddRequestForHelpSupport xmlns='http://mp.citizencop.org/'><HelpSupportRequestMdl>\(jsonString)</HelpSupportRequestMdl></AddRequestForHelpSupport></soap12:Body></soap12:Envelope>"
            
            WebServiceClass.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: BaseURL4, resultype: "AddRequestForHelpSupportResult", responcetype: "AddRequestForHelpSupportResponse") { [self] (responce, status) in
print(responce)
                loaderupload.dismiss(animated: false) {
                    if status == "Suceess"{

                        let dictTemp = (responce.value(forKey: "data")as! NSDictionary).mutableCopy()as! NSMutableDictionary
                        if("\(dictTemp.value(forKey: "Result")!)" == "True"){

                            let alert = UIAlertController(title: alertMessage, message: "\(dictTemp.value(forKey: "Msg")!)", preferredStyle: UIAlertController.Style.alert)
                               
                               // add the actions (buttons)
                               alert.addAction(UIAlertAction (title: "OK", style: .default, handler: { (nil) in
     
                                self.navigationController?.popViewController(animated: true)
                               }))
                               self.present(alert, animated: true, completion: nil)
                            
                            

                        }else{
                            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "\(dictTemp.value(forKey: "Msg")!)", viewcontrol: self)

                        }


                    }else{

                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)

                    }
                }
               
            }
            
            
        }
    }
    
    func call_CategoryByAPI() {
        if !isInternetAvailable() {
            FTIndicator.showToastMessage(alertInternet)
        }else{
            let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><GetHelpSupportCategory xmlns='http://mp.citizencop.org/'></GetHelpSupportCategory></soap12:Body></soap12:Envelope>"
            let loader = loader_Show(controller: self, strMessage: "", title: "Please wait...", style: .alert)

            WebServiceClass.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: BaseURL4, resultype: "GetHelpSupportCategoryResult", responcetype: "GetHelpSupportCategoryResponse") { (responce, status) in
                loader.dismiss(animated: false, completion: nil)
                if status == "Suceess"{
                    let dictTemp = (responce.value(forKey: "data")as! NSDictionary).mutableCopy()as! NSMutableDictionary
                    if("\(dictTemp.value(forKey: "Result")!)" == "True"){
                        self.aryCatrgory = NSMutableArray()
                        self.aryCatrgory = (dictTemp.value(forKey: "Category")as! NSArray).mutableCopy()as! NSMutableArray
                        deleteAllRecords(strEntity:"HelpCategory")
                        saveDataInLocalArray(strEntity: "HelpCategory", strKey: "helpCategory", data: self.aryCatrgory)
                        self.PopViewWithArray(tag: 13, aryData: self.aryCatrgory, strTitle: "Select Category")
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                        
                    }
                  
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            }
        }
    }
    
    func call_ReportSubCategoryByAPI(strCategoryID : String) {
        if !isInternetAvailable() {
            FTIndicator.showToastMessage(alertInternet)
        }else{
            let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><GetHelpSupportSubCategory xmlns='http://mp.citizencop.org/'><Category>\(strCategoryID)</Category></GetHelpSupportSubCategory></soap12:Body></soap12:Envelope>"
            let loader = loader_Show(controller: self, strMessage: "", title: "Please wait...", style: .alert)
            WebServiceClass.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: BaseURL4, resultype: "GetHelpSupportSubCategoryResult", responcetype: "GetHelpSupportSubCategoryResponse") { (responce, status) in
                loader.dismiss(animated: false, completion: nil)
                if status == "Suceess"{
                    let dictTemp = (responce.value(forKey: "data")as! NSDictionary).mutableCopy()as! NSMutableDictionary
                    if("\(dictTemp.value(forKey: "Result")!)" == "True"){
                        self.arySubCatrgory = NSMutableArray()
                        self.arySubCatrgory = (dictTemp.value(forKey: "SubCategory")as! NSArray).mutableCopy()as! NSMutableArray
                        self.PopViewWithArray(tag: 17, aryData: self.arySubCatrgory, strTitle: "Select Subcategory")
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                    }
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            }
        }
    }
    func callstateAPI(sender : UIButton) {
        if !isInternetAvailable() {
            FTIndicator.showToastMessage(alertInternet)
            
        }else{
            let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><GetAllStateList xmlns='http://m.citizencop.org/'></GetAllStateList></soap12:Body></soap12:Envelope>"
            customDotLoaderShowOnFull(message: "", controller: self)

            WebServiceClass.callSOAP_APIBY_POST(soapMessage: soapMessage, url: BaseURL, resultype: "GetAllStateListResult", responcetype: "GetAllStateListResponse") { (responce, status) in
                customeDotLoaderRemove()

                if status == "Suceess"{
                    self.aryState = NSMutableArray()
                    self.aryState = (responce.value(forKey: "data")as! NSArray).mutableCopy()as! NSMutableArray
                    deleteAllRecords(strEntity:"AllState")
                    saveDataInLocalArray(strEntity: "AllState", strKey: "allState", data: self.aryState)
                    if(sender.tag == 2){
                        self.PopViewWithArray(tag: sender.tag, aryData: self.aryState, strTitle: "State")
                    }
                }else{
                    
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                    
                }
            }
        }
    }
    func callCityAPI(sender : UIButton) {
        if !isInternetAvailable() {
            FTIndicator.showToastMessage(alertInternet)
            
        }else{
            let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><GetAllCityList xmlns='http://m.citizencop.org/'></GetAllCityList></soap12:Body></soap12:Envelope>"
            customDotLoaderShowOnFull(message: "", controller: self)
            WebServiceClass.callSOAP_APIBY_POST(soapMessage: soapMessage, url: BaseURL, resultype: "GetAllCityListResult", responcetype: "GetAllCityListResponse") { (responce, status) in
                customeDotLoaderRemove()

                if status == "Suceess"{
                    self.aryCity = NSMutableArray()
                    self.aryCity = (responce.value(forKey: "data")as! NSArray).mutableCopy()as! NSMutableArray
                    deleteAllRecords(strEntity:"AllCity")
                    saveDataInLocalArray(strEntity: "AllCity", strKey: "allCity", data: self.aryCity)
                    if(sender.tag == 3){
                        self.PopViewWithArray(tag: sender.tag, aryData: self.aryCity, strTitle: "City")
                    }
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                    
                }
            }
        }
   
    }
}
//MARK:-
//MARK:- ---------PopUpDelegate

extension INeedHelpSupportVC : PopUpDelegate
{
    func getDataFromPopupDelegate(dictData: NSDictionary, tag: Int) {
        if(tag == 13){
            
            self.btnCategory.setTitle("\(dictData.value(forKey: "Category")!)", for: .normal)
            self.btnSubCategory.setTitle("--Select Sub Category-", for: .normal)
            self.arySubCatrgory = NSMutableArray()
        }else if(tag == 17){
            self.btnSubCategory.setTitle("\(dictData.value(forKey: "SubCategory")!)", for: .normal)

        } else if(tag == 2){
            self.btnState.setTitle("\(dictData.value(forKey: "state_name")!)", for: .normal)
            self.btnState.tag = Int("\(dictData.value(forKey: "state_id")!)")!
            self.btnCity.setTitle("--Select City--", for: .normal)
            self.btnCity.tag = 0
            self.cityURL = "\(dictData.value(forKey: "HostURL")!)"

        }
        else if(tag == 3){
            self.btnCity.setTitle("\(dictData.value(forKey: "city_name")!)", for: .normal)
            self.btnCity.tag = Int("\(dictData.value(forKey: "city_id")!)")!
        }
    }
}
// MARK: - ----------------UICollectionViewDelegate
// MARK: -

extension INeedHelpSupportVC : UICollectionViewDelegate ,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout , UIScrollViewDelegate{
    
    private func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        
        DeviceType.IS_IPAD ? (self.aryReportImage.count == 0 ? (self.heightCollection.constant = 0.0) :  (self.heightCollection.constant = 125.0)) : self.aryReportImage.count == 0 ? (self.heightCollection.constant = 0.0) :  (self.heightCollection.constant = 95.0)
        
        
        
        return self.aryReportImage.count
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        cell.alpha = 0.4
        cell.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        UIView.animate(withDuration: 0.5) {
            cell.alpha = 1
            cell.transform = .identity
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ReportImageCell", for: indexPath as IndexPath) as! ReportImageCell
        let dict = aryReportImage.object(at: indexPath.row)as! NSDictionary
        cell.ReportCell_Image.image = (dict.value(forKey: "image")as! UIImage)
        cell.ReportCell_Image.layer.borderWidth = 1
        cell.ReportCell_Image.layer.masksToBounds = false
        cell.ReportCell_Image.layer.borderColor = UIColor.groupTableViewBackground.cgColor
        cell.ReportCell_Image.layer.cornerRadius = 8.0
        cell.ReportCell_Image.clipsToBounds = true
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return   DeviceType.IS_IPAD ? CGSize(width: 120, height:120) : CGSize(width: 90, height:90)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       // let dict = aryReportImage.object(at: indexPath.row)as! NSDictionary
        let alert = UIAlertController(title: "Report an Incident", message: "Please Select an Option", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "View on full screen", style: .default , handler:{ (UIAlertAction)in
            let testController = mainStoryboard.instantiateViewController(withIdentifier: "PreviewImageVC")as! PreviewImageVC
            let dict = self.aryReportImage.object(at: indexPath.row)as! NSDictionary
            testController.img = (dict.value(forKey: "image")as! UIImage)
            self.navigationController?.pushViewController(testController, animated: true)
            print("User click Approve button")
        }))
        
       
        alert.addAction(UIAlertAction(title: "Delete", style: .destructive , handler:{ (UIAlertAction)in
            self.aryReportImage.removeObject(at: indexPath.row)
            self.collection.reloadData()
        }))
        
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
        }))
        //alert.popoverPresentationController?.sourceRect = sender.frame
        alert.popoverPresentationController?.sourceView = self.view
        alert.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        alert.popoverPresentationController?.permittedArrowDirections = []
        self.present(alert, animated: true, completion: {
        })
    }
  
    
    func animateCollection() {
        self.collection.reloadData()
        
        let cells =  self.collection.visibleCells
        let tableHeight: CGFloat =     self.collection.bounds.size.height
        
        for i in cells {
            let cell: UICollectionViewCell = i as UICollectionViewCell
            cell.transform = CGAffineTransform(translationX: 0, y: tableHeight)
        }
        
        var index = 0
        
        for a in cells {
            let cell: UICollectionViewCell = a as UICollectionViewCell
            UIView.animate(withDuration: 1.2, delay: 0.05 * Double(index), usingSpringWithDamping: 0.6, initialSpringVelocity: 0, options: .curveLinear, animations: {
                cell.transform = CGAffineTransform(translationX: 0, y: 0);
            }, completion: nil)
            index += 1
        }
    }
}



// MARK: - ----------------UIImagePickerControllerDelegate
// MARK: -
extension INeedHelpSupportVC : UINavigationControllerDelegate, UIImagePickerControllerDelegate{
    
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        self.dismiss(animated: true, completion: { () -> Void in
            
        })
        if(imagePicker.view.tag == 1){ // For Gallery Camera
            let dictData = NSMutableDictionary()
            dictData.setValue(info[UIImagePickerController.InfoKey.originalImage] as? UIImage, forKey: "image")
            dictData.setValue(uniqueString() + ".jpg", forKey: "imageName")
            aryReportImage.add(dictData)
            collection.reloadData()
        }else{ // For Video
            self.timer.invalidate()
            let mediaType = info[UIImagePickerController.InfoKey.mediaType] as? String
            //check the media type string so we can determine if its a video
            if (mediaType == "public.movie") {
                let videoURL = info[UIImagePickerController.InfoKey.mediaURL] as? URL
                do{
                    let videoData = try Data(contentsOf: videoURL!)
                    self.dictVideoData.setValue(videoData, forKey: "video")
                    
                    self.dictVideoData.setValue(uniqueString() + ".mp4", forKey: "videoName")
                     aryVideo.add(dictVideoData)
                    self.heightVideoFile.constant = 35.0
                    self.viewVideo.isHidden = false
                    for item in aryVideo {
                        self.lblVedioName.text = "Video File: \((item as AnyObject).value(forKey: "videoName")!)"
                    }
                }catch {
                    
                }
            }
        }
    }
}
//MARK:-
//MARK:- ---------Recorder Delegate

extension INeedHelpSupportVC : RecoderDelegate
{
    func getDataFromRecordDelegate(dictData: NSDictionary, tag: Int) {
       
        aryAudio.add(dictData)
        self.heightAudioFile.constant = 35.0
        self.viewAudio.isHidden = false

        for item in aryAudio {
            self.lblAudioName.text = "Audio File: \((item as AnyObject).value(forKey: "audioName")!)"
        }
    }
    
    
}
// MARK: - ---------------UITextFieldDelegate
// MARK: -

extension INeedHelpSupportVC : UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if (textField == txtName)  {
            return  txtFiledValidation(textField: txtName, string: string, returnOnly: "ALL", limitValue: 90)
        }
        if textField == txtNumber  {
            return (txtFiledValidation(textField: txtNumber, string: string, returnOnly: "NUMBER", limitValue: 9))
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true

    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.view.endEditing(true)
    }
}
