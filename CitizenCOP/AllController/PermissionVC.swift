//
//  PermissionVC.swift
//  TargetDemo
//
//  Created by Navin Patidar on 8/28/19.
//  Copyright © 2019 Navin Patidar. All rights reserved.
//

import UIKit
import Photos
import CoreLocation
import AVFoundation
import UserNotifications
import Firebase
import FirebaseMessaging

class PermissionVC: UIViewController {
    
    
    // MARK: - ----IBOutlet
    
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var tvlist: UITableView!
   
    var aryPermissionList = NSMutableArray()
     var locationManager = CLLocationManager()
     var notificationStatus = PermissionsStatus.disabled
  
    
    override func viewDidLoad() {
        super.viewDidLoad()
        var dict = NSMutableDictionary()
        dict.setValue("pa_photo_library_icon", forKey: "image")
        dict.setValue(PermissionsType.photoLibrary, forKey: "title")
        aryPermissionList.add(dict)
        dict = NSMutableDictionary()
        dict.setValue("pa_camera_icon", forKey: "image")
        dict.setValue(PermissionsType.camera, forKey: "title")
        aryPermissionList.add(dict)
        dict = NSMutableDictionary()
        dict.setValue("pa_contacts_icon", forKey: "image")
        dict.setValue(PermissionsType.contacts, forKey: "title")
        aryPermissionList.add(dict)
        dict = NSMutableDictionary()
        dict.setValue("pa_notification_icon", forKey: "image")
        dict.setValue(PermissionsType.notifications, forKey: "title")
        aryPermissionList.add(dict)
        dict = NSMutableDictionary()
        dict.setValue("pa_location_icon", forKey: "image")
        dict.setValue(PermissionsType.location, forKey: "title")
        aryPermissionList.add(dict)
        dict = NSMutableDictionary()
        dict.setValue("pa_microphone_icon", forKey: "image")
        dict.setValue(PermissionsType.microphone, forKey: "title")
        aryPermissionList.add(dict)
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("CheckPermissions"), object: nil)

        locationManager.delegate = self
        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryTheamColor)

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        tvlist.reloadData()
        var heightCustome = 550
        if(DeviceType.IS_IPHONE_5){
            heightCustome = 420
        }else if (DeviceType.IS_IPHONE_6){
            heightCustome = 500
        }
        
        let alertController = UIAlertController(title: "Allow CitizenCOP to access", message: nil, preferredStyle: .actionSheet)
        let customView = UIView()
        customView.addSubview(tvlist)
        alertController.view.addSubview(customView)
        customView.translatesAutoresizingMaskIntoConstraints = false
        customView.topAnchor.constraint(equalTo: alertController.view.topAnchor, constant: 30).isActive = true
        customView.rightAnchor.constraint(equalTo: alertController.view.rightAnchor, constant: 0).isActive = true
        customView.leftAnchor.constraint(equalTo: alertController.view.leftAnchor, constant: 0).isActive = true
        customView.heightAnchor.constraint(equalToConstant: CGFloat(Int(heightCustome))).isActive = true
        
        
        tvlist.topAnchor.constraint(equalTo: customView.topAnchor, constant: 0).isActive = true
        tvlist.rightAnchor.constraint(equalTo: customView.rightAnchor, constant: 0).isActive = true
        tvlist.leftAnchor.constraint(equalTo: customView.leftAnchor, constant: 0).isActive = true
        tvlist.heightAnchor.constraint(equalToConstant: CGFloat(Int(heightCustome))).isActive = true
        
        
        
        
        alertController.view.translatesAutoresizingMaskIntoConstraints = false
        alertController.view.heightAnchor.constraint(equalToConstant: 640).isActive = true
        
      //  customView.backgroundColor = .green
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            self.dismiss(animated: false, completion: {
                nsud.set(true, forKey: "CitiZenCOP_Permission")
               nsud.synchronize()
            })
        }
       
        alertController.addAction(cancelAction)
        if let popoverController = alertController.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            popoverController.permittedArrowDirections = []
        }
        
        self.present(alertController, animated: true, completion: nil)
        
    }
    @objc func methodOfReceivedNotification(notification: NSNotification){
        DispatchQueue.main.async {
             self.tvlist.reloadData()
        }
        
    }
    // MARK: - -------- Action --------
    // MARK:
    @IBAction func actionOnNext(_ sender: Any)
    {
      nsud.set(true, forKey: "CitiZenCOP_Permission")
        
        let vc: DashBoardVC = mainStoryboard.instantiateViewController(withIdentifier: "DashBoardVC") as! DashBoardVC
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    @IBAction func actionOnShareUUID(_ sender: Any)
    {
        let text = DeviceID
        let image = UIImage(named: "icon152")
        let shareAll = [text , image!] as [Any]
        let activityViewController = UIActivityViewController(activityItems: shareAll, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        self.present(activityViewController, animated: true, completion: nil)
    }
    
}
// MARK: - ----------------UITableViewDelegate
// MARK: -

extension PermissionVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return aryPermissionList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tvlist.dequeueReusableCell(withIdentifier: "PermissionCell", for: indexPath as IndexPath) as! PermissionCell
        let dict = aryPermissionList.object(at: indexPath.row)as! NSDictionary
        var status = PermissionsStatus.checking
        cell.btnimg.setImage(UIImage(named: "\(dict.value(forKey: "image")!)"), for: .normal)
        if(dict["title"]as! PermissionsType == PermissionsType.photoLibrary){
            status = PermissionClass().checkPermission(type: PermissionsType.photoLibrary)
            cell.lblTitle.text = "Photo Library"
            cell.lblSubTitle.text = NSLocalizedString("\(PermissionsDetailKey.NSPhotoLibraryUsageDescription)", tableName: "InfoPlist", bundle: Bundle.main, value: "", comment: "")
        }else if(dict["title"]as! PermissionsType == PermissionsType.camera){
            status = PermissionClass().checkPermission(type: PermissionsType.camera)
            cell.lblTitle.text = "Camera"
            cell.lblSubTitle.text = NSLocalizedString("\(PermissionsDetailKey.NSCameraUsageDescription)", tableName: "InfoPlist", bundle: Bundle.main, value: "", comment: "")
        }else if(dict["title"]as! PermissionsType == PermissionsType.contacts){
            status = PermissionClass().checkPermission(type: PermissionsType.contacts)
            cell.lblTitle.text = "Contacts"
            cell.lblSubTitle.text = NSLocalizedString("\(PermissionsDetailKey.NSContactsUsageDescription)", tableName: "InfoPlist", bundle: Bundle.main, value: "", comment: "")
        }else if(dict["title"]as! PermissionsType == PermissionsType.microphone){
            status = PermissionClass().checkPermission(type: PermissionsType.microphone)
            cell.lblTitle.text = "Microphone"
            cell.lblSubTitle.text = NSLocalizedString("\(PermissionsDetailKey.NSMicrophoneUsageDescription)", tableName: "InfoPlist", bundle: Bundle.main, value: "", comment: "")
        }
        
        else if(dict["title"]as! PermissionsType == PermissionsType.notifications){
            _ = self.NotificationUpdate(type: "Check")
            status = self.notificationStatus
                cell.lblTitle.text = "Notifications"
                cell.lblSubTitle.text = NSLocalizedString("\(PermissionsDetailKey.NSNotificationUsageDescription)", tableName: "InfoPlist", bundle: Bundle.main, value: "", comment: "")
          
        }
        
        else if(dict["title"]as! PermissionsType == PermissionsType.location){
            status = self.locationUpdate(type: "Check")
            cell.lblTitle.text = "location"
            cell.lblSubTitle.text = NSLocalizedString("\(PermissionsDetailKey.NSLocationAlwaysUsageDescription)", tableName: "InfoPlist", bundle: Bundle.main, value: "", comment: "")
        }
        

        switch status {
        case PermissionsStatus.disabled:
            cell.lblStatus.text = "Allow"
            cell.accessoryType = UITableViewCell.AccessoryType.none;

        case PermissionsStatus.enabled:
            cell.lblStatus.text = ""
            cell.accessoryType = UITableViewCell.AccessoryType.checkmark;

        case PermissionsStatus.denied:
            cell.lblStatus.text = "Allow"
            cell.accessoryType = UITableViewCell.AccessoryType.none;

        default:
            cell.lblStatus.text = "Allow"
            cell.accessoryType = UITableViewCell.AccessoryType.none;

            }
            
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dict = aryPermissionList.object(at: indexPath.row)as! NSDictionary
        var status = PermissionsStatus.checking

        if(dict["title"]as! PermissionsType == PermissionsType.photoLibrary){
            status = PermissionClass().checkPermission(type: PermissionsType.photoLibrary)
              _ = PermissionClass().UpdatePermission(type: dict["title"]as! PermissionsType, oldStatus: status)
        }else if(dict["title"]as! PermissionsType == PermissionsType.camera){
            status = PermissionClass().checkPermission(type: PermissionsType.camera)
              _ = PermissionClass().UpdatePermission(type: dict["title"]as! PermissionsType, oldStatus: status)
        }
        else if(dict["title"]as! PermissionsType == PermissionsType.contacts){
            status = PermissionClass().checkPermission(type: PermissionsType.contacts)
              _ = PermissionClass().UpdatePermission(type: dict["title"]as! PermissionsType, oldStatus: status)
        }else if(dict["title"]as! PermissionsType == PermissionsType.microphone){
            status = PermissionClass().checkPermission(type: PermissionsType.microphone)
            _ = PermissionClass().UpdatePermission(type: dict["title"]as! PermissionsType, oldStatus: status)
        }
        else if(dict["title"]as! PermissionsType == PermissionsType.notifications){
            _ = NotificationUpdate(type: "Update")
            NotificationCenter.default.post(name: Notification.Name("CheckPermissions"), object: nil)

        }
        
        else if(dict["title"]as! PermissionsType == PermissionsType.location){
            _ = locationUpdate(type: "")
            NotificationCenter.default.post(name: Notification.Name("CheckPermissions"), object: nil)

        }
      
    }
    // MARK: - ----------------Notification

    func NotificationUpdate(type: String) -> PermissionsStatus {

     
        if(type == "Check"){
            AppDelegate().center.getNotificationSettings(completionHandler: { settings in
                switch settings.authorizationStatus {
                case .authorized, .provisional:
                    
                    self.notificationStatus = PermissionsStatus.enabled
                    print("authorized")
                         DispatchQueue.main.async {
                                if(self.tvlist.tag != 999){
                                    self.tvlist.tag = 999
                                    UIApplication.shared.registerForRemoteNotifications()
                                    self.tvlist.reloadData()
                            }
                        }
                  
                case .denied:
                    self.notificationStatus = PermissionsStatus.disabled
                case .notDetermined:
                    self.notificationStatus = PermissionsStatus.disabled
                @unknown default:
                    break
                }
            })
            return self.notificationStatus
            
        }else{
            
            let options : UNAuthorizationOptions = [.alert, .sound, .badge, .carPlay]
            UNUserNotificationCenter.current().requestAuthorization(options: options) { (authorized, error) in
                if let err = error{
                    print(err.localizedDescription)
                }
                guard authorized else {
                    print("Authorization failed")
                    DispatchQueue.main.async {
                        self.tvlist.reloadData()
                        let settingsURL = URL(string: UIApplication.openSettingsURLString)
                        UIApplication.shared.open(settingsURL!, options: [:]) { (status) in
                        }
                        
                    }
                    return
                }
              
                DispatchQueue.main.async {
                    self.tvlist.reloadData()
                    UIApplication.shared.registerForRemoteNotifications()
                }
            }
        }
        
   
        return PermissionsStatus.disabled

    }
    // MARK: - ---------------Location

    func locationUpdate(type: String) -> PermissionsStatus {
        if(type == "Check"){
            
            if CLLocationManager.locationServicesEnabled() {
                switch(CLLocationManager.authorizationStatus()) {
                case .notDetermined, .restricted, .denied:
                     return PermissionsStatus.disabled
                case .authorizedAlways, .authorizedWhenInUse:
                     return PermissionsStatus.enabled
                @unknown default:
                    break
                }
            } else {
               return PermissionsStatus.disabled
            }
            
        }else{
            
            locationManager.requestAlwaysAuthorization()
            if CLLocationManager.authorizationStatus() == .denied {
                let settingsURL = URL(string: UIApplication.openSettingsURLString)
                UIApplication.shared.open(settingsURL!, options: [:]) { (status) in
                }
            }
        }
        return PermissionsStatus.disabled

    }
    
}
//MARK:-
//MARK:- ---------CLLocationManagerDelegate
extension PermissionVC : CLLocationManagerDelegate{
    
    public func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
       self.tvlist.reloadData()

    }
    
    
}
// MARK: - ----------------UserDashBoardCell
// MARK: -
class PermissionCell: UITableViewCell {
    //DashBoard
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var btnimg: UIButton!

    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}

// MARK: --------UNNotification/Messaging Delegate Method----------

extension PermissionVC : UNUserNotificationCenterDelegate{
   
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
//        if let token = InstanceID.instanceID().token(withAuthorizedEntity: <#String#>, scope: <#String#>, handler: <#InstanceIDTokenHandler#>) {
//            nsud.setValue("\(token)", forKey: "CitizenCOP_FCM_Token")
//            nsud.synchronize()
//            print("FCM_Token----------------- : \(String(describing: token))")
//        }
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        nsud.setValue("5437788560965635874098656heyfget87", forKey: "CitizenCOP_FCM_Token")
        nsud.synchronize()
    }
    
    
}
