//
//  KnowYourCopVC.swift
//  CitizenCOP
//
//  Created by Saavan Patidar on 12/04/22.
//

import UIKit
import CoreLocation
import MapKit

class KnowYourCopVC: UIViewController ,UIGestureRecognizerDelegate{

    //MARK: Outlets
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var viewFooter: UIView!
    @IBOutlet var mapView: MKMapView!
    @IBOutlet var lblPoliceStation: UILabel!
    @IBOutlet var lblDutyPoint: MarqueeLabel!
    @IBOutlet var lblAddress: UILabel!
    @IBOutlet weak var lblPoliceControlRoomNumber: UILabel!
    @IBOutlet weak var lblTrafficControlNumber: UILabel!
    
    //MARK: UIVIEW
  
    @IBOutlet var viewOne: UIView!
    @IBOutlet var viewThree: UIView!
    @IBOutlet var viewFive: UIView!
    @IBOutlet var viewSeven : UIView!
    @IBOutlet var viewNine : UIView!
    
    
    //MARK: UILABEL
    @IBOutlet var lblACPMobileNo: UILabel!
    @IBOutlet var lblADCPName: UILabel!
    @IBOutlet var lblADCPMobileNo: UILabel!
    @IBOutlet var lblDCPName: UILabel!
    @IBOutlet var lblDCPMobileNo: UILabel!
    
    @IBOutlet var lblBITInchargeName : UILabel!
    @IBOutlet var lblBITInchargeMobileNo : UILabel!
    @IBOutlet var lblTIName : UILabel!
    @IBOutlet var lblTiMObileNo : UILabel!
    @IBOutlet var lblACPName : UILabel!
    
    @IBOutlet var navButton: UINavigationItem!
    
    
    @IBOutlet var stackView: UIStackView!
    //MARK: Variables
    var aryData =  NSArray()
    var strTitle = String()
    private var locationManager: CLLocationManager!
    private var currentLocation: CLLocation?
    
    //MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        self.lblTitle.text = strTitle
        viewFooter.backgroundColor = hexStringToUIColor(hex: primaryTheamColor)
        self.getCurrentLocation()
        self.viewBorder()
       
        if(aryData.count == 0){
            //self.ErrorMsg = alert_TrafficDuty
            self.stackView.isHidden = true
        }else{

            let dict = aryData.object(at: 0)as! NSDictionary
            let  TrafficDutyPoint = "\(dict.value(forKey: "TrafficDutyPoint")!)"
            let  PoliceStation = "\(dict.value(forKey: "PoliceStation")!)"

            let   PoliceControlRoomNo = "\(dict.value(forKey: "PoliceControlRoomNo")!)"
            let   TrafficHelpLineNo = "\(dict.value(forKey: "TrafficHelpLineNo")!)"

          //  self.lblTrafficControlNumber.text = "Traffic Control Room No. \(TrafficHelpLineNo)"
        //    self.lblPoliceControlRoomNumber.text = "Police Control Room No. \(PoliceControlRoomNo)"

            self.lblDutyPoint.text = TrafficDutyPoint
            self.lblPoliceStation.text = "Police Station - \(PoliceStation)"

            self.lblBITInchargeName.text = "\(dict.value(forKey: "BitInchargeName")!)"
            self.lblBITInchargeMobileNo.text = "\(dict.value(forKey: "BitInchargeMobNo")!)"
            
            let tap = UITapGestureRecognizer(target: self, action: #selector(BITIncharge))
            tap.delegate = self
            lblBITInchargeMobileNo.isUserInteractionEnabled = true
            lblBITInchargeMobileNo.addGestureRecognizer(tap)

            self.lblTIName.text = "\(dict.value(forKey: "TIName")!)"
            self.lblTiMObileNo.text = "\(dict.value(forKey: "TIMobNo")!)"
            
            let tapOnTINumber = UITapGestureRecognizer(target: self, action: #selector(CallTI))
            tapOnTINumber.delegate = self
            lblTiMObileNo.isUserInteractionEnabled = true
            lblTiMObileNo.addGestureRecognizer(tapOnTINumber)

            self.lblACPName.text = "\(dict.value(forKey: "ACPName")!)"
            self.lblACPMobileNo.text = "\(dict.value(forKey: "ACPMobNo")!)"
            
            let tapOnACPNumber = UITapGestureRecognizer(target: self, action: #selector(CallACP))
            tapOnACPNumber.delegate = self
            lblACPMobileNo.isUserInteractionEnabled = true
            lblACPMobileNo.addGestureRecognizer(tapOnACPNumber)

            self.lblADCPName.text = "\(dict.value(forKey: "ADCPName")!)"
            self.lblADCPMobileNo.text = "\(dict.value(forKey: "ADCPMobNo")!)"
            
            let tapOnADCPNumber = UITapGestureRecognizer(target: self, action: #selector(CallADCP))
            tapOnADCPNumber.delegate = self
            lblADCPMobileNo.isUserInteractionEnabled = true
            lblADCPMobileNo.addGestureRecognizer(tapOnADCPNumber)

            self.lblDCPName.text = "\(dict.value(forKey: "DCPName")!)"
            self.lblDCPMobileNo.text = "\(dict.value(forKey: "DCPMobNo")!)"
            
            let tapOnDCPNumber = UITapGestureRecognizer(target: self, action: #selector(CallDCP))
            tapOnDCPNumber.delegate = self
            lblDCPMobileNo.isUserInteractionEnabled = true
            lblDCPMobileNo.addGestureRecognizer(tapOnDCPNumber)
            // Text string, fade length, leading buffe
        }
    }
    
    
    func viewBorder(){
        self.shadow(Vw: viewOne)
        self.shadow(Vw: viewThree)
        self.shadow(Vw: viewFive)
        self.shadow(Vw: viewSeven)
        self.shadow(Vw: viewNine)
    }
    
    func shadow(Vw : UIView)
    {
        Vw.layer.masksToBounds = false
        Vw.layer.shadowColor =  UIColor.lightGray.cgColor
        Vw.layer.shadowOffset = CGSize(width: 0, height: 1)
        Vw.layer.shadowRadius = 5.0
        Vw.layer.shadowOpacity = 15.0
        Vw.layer.cornerRadius = 5.0
    }
    
    func getCurrentLocation()
    {
        mapReload()
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                showLocationAccessAlertAction(viewcontrol: self)
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
                locationManager.startUpdatingLocation()
            @unknown default:
                break
            }
        } else {
            print("Location services are not enabled")
        }
    }
    func getAddressFromLatLon(pdblLatitude: String, withLongitude pdblLongitude: String) {
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        let lat: Double = Double("\(pdblLatitude)")!
        //21.228124
        let lon: Double = Double("\(pdblLongitude)")!
        //72.833770
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = lat
        center.longitude = lon
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        ceo.reverseGeocodeLocation(loc, completionHandler:
                                    {(placemarks, error) in
            if (error != nil)
            {
                print("reverse geodcode fail: \(error!.localizedDescription)")
                return
            }
            let pm = placemarks! as [CLPlacemark]
            if pm.count > 0 {
                let pm = placemarks![0]
                
                var addressString : String = ""
                if pm.subLocality != nil {
                    addressString = addressString + pm.subLocality! + ", "
                }
                if pm.thoroughfare != nil {
                    addressString = addressString + pm.thoroughfare! + ", "
                }
                if pm.locality != nil {
                    addressString = addressString + pm.locality! + ", "
                }
                if pm.country != nil {
                    addressString = addressString + pm.country! + ", "
                }
                if pm.postalCode != nil {
                    addressString = addressString + pm.postalCode! + " "
                }
                self.lblAddress.text = addressString
                print(addressString)
            }
        })
    }
    
    func mapReload()  {
//        let viewRegion = MKCoordinateRegion(center: mapView.centerCoordinate, latitudinalMeters: 200, longitudinalMeters: 200)
//        mapView.setRegion(viewRegion, animated: false)
//        mapView.delegate = self
//        let buttonItem = MKUserTrackingBarButtonItem(mapView: mapView)
//        self.navButton.rightBarButtonItem = buttonItem
//
//       // self.navingation_Item.butt
//        self.mapView.showAnnotations(mapView.annotations, animated: true)
        
            mapView.mapType = MKMapType.standard
            mapView.delegate = self
        
            let buttonItem = MKUserTrackingBarButtonItem(mapView: mapView)
            self.navButton.rightBarButtonItem = buttonItem
        
            let dict = aryData.object(at: 0)as! NSDictionary
        
            if dict.count == 0 {
                
            }else{
                let lat = "\(dict.value(forKey: "Latitude")!)"
                let long = "\(dict.value(forKey: "Longitude")!)"
                
                let latD = Double(lat) ?? 0.0
                let longD = Double(long) ?? 0.0
                
                let location = CLLocationCoordinate2D(latitude: latD,longitude: longD)
                let span = MKCoordinateSpan(latitudeDelta: 0.05, longitudeDelta: 0.05)
                let region = MKCoordinateRegion(center: location, span: span)
                mapView.setRegion(region, animated: true)

                let annotation = MKPointAnnotation()
                annotation.coordinate = location
                mapView.addAnnotation(annotation)
            }
             
    }
    
    //MARK: Action On Button
    @IBAction func actionOnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    //MARK: Gesture Action
    
    @objc func BITIncharge(){
        let dict = aryData.object(at: 0)as! NSDictionary
        let number = "\(dict.value(forKey: "BitInchargeMobNo")!)"
        print("Number")
        print(number)
        self.dialNumber(number: number)
    }
    
    @objc func CallTI(){
        let dict = aryData.object(at: 0)as! NSDictionary
        let number = "\(dict.value(forKey: "TIMobNo")!)"
        print("TI")
        print(number)
        self.dialNumber(number: number)
    }
    
    @objc func CallACP(){
        let dict = aryData.object(at: 0)as! NSDictionary
        let number = "\(dict.value(forKey: "ACPMobNo")!)"
        print("CallACP")
        print(number)
        self.dialNumber(number: number)
    }
    
    @objc func CallADCP(){
        let dict = aryData.object(at: 0)as! NSDictionary
        let number = "\(dict.value(forKey: "ADCPMobNo")!)"
        print("CallADCP")
        print(number)
        self.dialNumber(number: number)
    }
    
    @objc func CallDCP(){
        let dict = aryData.object(at: 0)as! NSDictionary
        let number = "\(dict.value(forKey: "DCPMobNo")!)"
        print("CallDCP")
        print(number)
        self.dialNumber(number: number)
    }
    
    //MARK: Phone Calling Function
    
    func dialNumber(number : String)  {
        let url: NSURL = URL(string: "TEL://\(number)")! as NSURL
        UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
    }
    

}


extension KnowYourCopVC: MKMapViewDelegate ,CLLocationManagerDelegate  {
    
    func mapViewDidFinishLoadingMap(_ mapView: MKMapView) {
        print( mapView.centerCoordinate.latitude)
        
        getAddressFromLatLon(pdblLatitude: "\(mapView.centerCoordinate.latitude)", withLongitude: "\(mapView.centerCoordinate.longitude)")
        
       // getAddressFromLatLon(pdblLatitude: "22.7283708", withLongitude: "75.8744198")
        
    }
    
    func mapView(_ mapView: MKMapView, didChange mode: MKUserTrackingMode, animated: Bool) {
        
    }
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        getAddressFromLatLon(pdblLatitude: "\(mapView.centerCoordinate.latitude)", withLongitude: "\(mapView.centerCoordinate.longitude)")
    }
    
    // CLLocationManagerDelegate
    // CLLocationManagerDelegate
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        defer { currentLocation = locations.last }
        
        if currentLocation != nil {
            // Zoom to user location
            self.mapReload()
            if(nsud.value(forKey: "CitizenCOP_MySafeZone") == nil) || (nsud.value(forKey: "CitiZenCOP_JoinedGroupLocation") == nil){
                locationManager.stopUpdatingLocation()
            }
            
        }
    }
}
