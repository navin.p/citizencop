//
//  WebViewVC.swift
//  CitizenCOP
//
//  Created by Navin Patidar on 5/17/19.
//  Copyright © 2019 CitizenCop. All rights reserved.
//

import UIKit

class WebViewVC: UIViewController {
   
    // MARK: - ----IBOutlet
    
    @IBOutlet weak var view_ForHeader: UIView!
    @IBOutlet weak var lblTitle: UILabel!
      @IBOutlet weak var viewWeb: UIWebView!
    // MARK: - ----Variable
    var strURL = String()
    var strTitle = String()
    var type = String()

    override func viewDidLoad() {
        super.viewDidLoad()
        view_ForHeader.backgroundColor = hexStringToUIColor(hex: primaryTheamColor)
        lblTitle.text = strTitle
        viewWeb.scalesPageToFit = true;
        if !(isInternetAvailable()){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
        }else{
            viewWeb.loadRequest(URLRequest(url: URL(string: strURL)!))
            viewWeb.scrollView.maximumZoomScale = 8
            viewWeb.scrollView.minimumZoomScale = 1
            viewWeb.delegate = self
            
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
       
    }
    // MARK: - --------------IBAction
    // MARK: -
    @IBAction func actionOnBack(_ sender: UIButton) {
        if(type == "Present"){
            self.dismiss(animated: true, completion: nil)
        }else{
          (self.navigationController?.popViewController(animated: true))
        }
    }

}
// MARK :
//MARK : UIWebViewDelegate
extension WebViewVC : UIWebViewDelegate{
    func webViewDidStartLoad(_ webView: UIWebView) {
        customDotLoaderShowOnWeb(frame: CGRect(x: 0, y: view_ForHeader.frame.maxY, width: self.view.frame.width, height: (self.view.frame.height - view_ForHeader.frame.maxY)), message: "", controller: self)
        
    }
    func webViewDidFinishLoad(_ webView: UIWebView) {
       customeDotLoaderRemove()
    }
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
       customeDotLoaderRemove()
    }
}
