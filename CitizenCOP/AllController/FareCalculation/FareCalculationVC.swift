//
//  FareCalculationVC.swift
//  CitizenCOP
//
//  Created by Navin Patidar on 6/19/19.
//  Copyright © 2019 CitizenCop. All rights reserved.
//

import UIKit
import CoreLocation

class FareCalculationVC: UIViewController {
    
    // MARK: - --------------IBOutlet
    // MARK: -
    
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var btnCalculate: UIButton!
    @IBOutlet weak var btnCalculateOffline: UIButton!
    @IBOutlet weak var btnDayFare: UIButton!
    @IBOutlet weak var btnNightFare: UIButton!

    @IBOutlet weak var btnSource: UIButton!
    @IBOutlet weak var btnDestination: UIButton!
    @IBOutlet weak var btnType: UIButton!
   
    @IBOutlet weak var lblSource: UILabel!
    @IBOutlet weak var lblDestination: UILabel!
    @IBOutlet weak var lblType: UILabel!
    
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblDistenceKM: UILabel!

    var textField: UITextField?

    var LatSource = "0.0"
    var LongSource = "0.0"
    var LatDestination = "0.0"
    var LongDestination = "0.0"
    var index = 0
    var aryVehicleType = NSMutableArray()
    var dictFareData = NSMutableDictionary()
    var nightFare = Bool()
    // MARK: - ------------Life Cycle
    // MARK: -
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryTheamColor)
        btnCalculate.layer.cornerRadius = 5.0
        btnCalculate.backgroundColor = hexStringToUIColor(hex: primaryDark)
        btnCalculateOffline.layer.cornerRadius = 5.0
        btnCalculateOffline.backgroundColor = hexStringToUIColor(hex: primaryDark)
        lblType.textColor = .lightGray
        lblSource.textColor = .lightGray
        lblDestination.textColor = .lightGray
        nightFare = false
      
        lblAmount.text = "Amount (\u{20B9}) :"
        lblDistenceKM.text = "Travel Distance in KM :"
        self.callVehicleSearchAPI()
    }
    
    // MARK: - ---------------IBAction
    // MARK: -
    @IBAction func actiononBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func actiononHelp(_ sender: UIButton) {
        let testController = mainStoryboard.instantiateViewController(withIdentifier: "HelpVC")as! HelpVC
        testController.strMessage = alertYourFareCalculation
        self.navigationController?.pushViewController(testController, animated: true)
        
    }
    @IBAction func actiononDayFare(_ sender: UIButton) {
        btnDayFare.setImage(UIImage(named: "RadioButton-Selected"), for: .normal)
        btnNightFare.setImage(UIImage(named: "RadioButton-Unselected"), for: .normal)
        nightFare = false
    }
    @IBAction func actiononNightFare(_ sender: UIButton) {
        btnNightFare.setImage(UIImage(named: "RadioButton-Selected"), for: .normal)
        btnDayFare.setImage(UIImage(named: "RadioButton-Unselected"), for: .normal)
        nightFare = true

    }
    
    @IBAction func actiononCalculate(_ sender: UIButton) {
        if(validation()){
            let coordinate₀ = CLLocation(latitude: Double((self.LatSource as NSString).doubleValue), longitude: Double((self.LongSource as NSString).doubleValue))
            let coordinate₁ = CLLocation(latitude: Double((self.LatDestination as NSString).doubleValue), longitude: Double((self.LongDestination as NSString).doubleValue))
            let distanceInMeters = coordinate₀.distance(from: coordinate₁)
            
            let dKm = "\(distanceInMeters/1000)"
            self.showOfflineFare(distence:((dKm)as NSString).floatValue)

        }
    }
    
    @IBAction func actiononCalculatOffline(_ sender: UIButton) {
        
        if(lblType.tag  ==  0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_FareType, viewcontrol: self)

        }else{
            let alert = UIAlertController(title: "Calculate Travel Fare", message: "Enter distance between souce and destination.", preferredStyle: UIAlertController.Style.alert)
            alert.addTextField(configurationHandler: configurationTextField)
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler:nil))
            alert.addAction(UIAlertAction(title: "CALCULATE", style: .default, handler:{ (UIAlertAction) in
                if(self.textField?.text?.count != 0){
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                        self.showOfflineFare(distence:((self.textField!.text!)as NSString).floatValue)
                    })
                }else{
                    FTIndicator.showToastMessage("Please enter distance between souce and destination.")
                }
            }))
            self.present(alert, animated: true, completion: nil)
        }
        
  
    }
    
    func configurationTextField(textField: UITextField!) {
        if (textField) != nil {
            self.textField = textField!
            self.textField?.placeholder = "Enter distence";
            self.textField?.borderStyle = .roundedRect
            self.textField?.keyboardType = UIKeyboardType.numberPad
            self.textField?.delegate = self
        }
    }
    
    @IBAction func actiononType(_ sender: UIButton) {
        if(self.aryVehicleType.count != 0){
           self.PopViewWithArray(tag: 9, aryData: self.aryVehicleType, strTitle: "Select")
        }
    }
    
    @IBAction func actiononSource(_ sender: UIButton) {
        index = 0
        
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "AddressGetFromMapVC")as! AddressGetFromMapVC
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .coverVertical
        vc.delegate = self
        self.present(vc, animated: true, completion: nil)
        
       
    }
    @IBAction func actiononDestination(_ sender: UIButton) {
        index = 1
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "AddressGetFromMapVC")as! AddressGetFromMapVC
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .coverVertical
        vc.delegate = self
        self.present(vc, animated: true, completion: nil)
        
    }
    

    
    
    @objc func showOfflineFare(distence : Float) {
        print(dictFareData)
    
        let strFareAfterOneKmPerKm = ("\(dictFareData.value(forKey: "FareAfterOneKmPerKm")!)" as NSString).floatValue
        let strMinimumFare = ("\(dictFareData.value(forKey: "MinimumFare")!)" as NSString).floatValue
        let strNightCharge = ("\(dictFareData.value(forKey: "NightCharge")!)" as NSString).floatValue
        let strMinimumFareKm = ("\(dictFareData.value(forKey: "MinimumFareKm")!)" as NSString).floatValue
        let strAdditionalMeterClot = ("\(dictFareData.value(forKey: "AdditionalMeterClot")!)" as NSString).floatValue
        let strAdditionalCharge = ("\(dictFareData.value(forKey: "AdditionalCharge")!)" as NSString).floatValue

      
        
         var str_Fare = strMinimumFare

       if(distence > strMinimumFareKm){
        
            str_Fare = ((distence-strMinimumFareKm)/strAdditionalMeterClot)*1000*strAdditionalCharge+strMinimumFare
            lblAmount.text = String(format: "Amount (\u{20B9}) : %.2f", str_Fare)
            lblDistenceKM.text = String(format: "Travel Distance in KM : %.2f", distence)
       }
        
         if (distence <= strMinimumFareKm && distence > 0){
            str_Fare = strMinimumFare
            lblAmount.text = String(format: "Amount (\u{20B9}) : %.2f", str_Fare)
            lblDistenceKM.text = String(format: "Travel Distance in KM : %.2f", distence)
        }
      
        if(nightFare)
        {
            str_Fare = str_Fare + (str_Fare * strNightCharge) / 100;
            lblAmount.text = String(format: "Amount (\u{20B9}) : %.2f", str_Fare)
            lblDistenceKM.text = String(format: "Travel Distance in KM : %.2f", distence)
        }

    }
    
    func validation() -> Bool {
        if(LatSource == "0.0"){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_FareSource, viewcontrol: self)
            return false
        }else if (LongDestination == "0.0"){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_FareDestination, viewcontrol: self)
            return false

        }else if (lblType.tag  ==  0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_FareType, viewcontrol: self)
            return false
        }
        return true
    }
    
    //MARK: Other Function's
    func PopViewWithArray(tag: Int , aryData : NSMutableArray , strTitle: String)  {
        let vc: SelectionClass = mainStoryboard.instantiateViewController(withIdentifier: "SelectionClass") as! SelectionClass
        vc.strTitle = strTitle
        vc.strTag = tag
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .coverVertical
        vc.delegate = self
        vc.aryList = self.aryVehicleType
        self.present(vc, animated: true, completion: {})
        }
    
    // MARK: - ---------------API CAlling
    // MARK: -
    func callVehicleSearchAPI() {
        if !isInternetAvailable() {
            self.aryVehicleType = NSMutableArray()
            self.aryVehicleType = getDataFromCoreDataBase(strEntity: "FareVehicleType", strkey: "fareVehicleType")
            
        }else{
            customDotLoaderShowOnFull(message: "", controller: self)
            let strGetFare = API_GetFare + "&cityid=\(((dictCityData.value(forKey: "CityData")as! NSArray).object(at: 0)as! NSDictionary).value(forKey: "city_id")!)"
            customDotLoaderShowOnFull(message: "", controller: self)

            
            WebServiceClass.callAPIBYPOST(parameter: NSDictionary(), url: strGetFare) { (responce, status) in
                customeDotLoaderRemove()
                if status == "Suceess"{
                    
                    self.aryVehicleType = NSMutableArray()
                    self.aryVehicleType = (responce.value(forKey: "data")as! NSArray).mutableCopy()as! NSMutableArray
                    deleteAllRecords(strEntity:"FareVehicleType")
                    saveDataInLocalArray(strEntity: "FareVehicleType", strKey: "fareVehicleType", data: self.aryVehicleType)
                    
                    if(self.aryVehicleType.count != 0){
                        self.dictFareData = NSMutableDictionary()
                        self.dictFareData = (self.aryVehicleType.object(at: 0)as! NSDictionary).mutableCopy()as! NSMutableDictionary
                        self.lblType.text = "\(self.dictFareData.value(forKey: "VehicleType")!)"
                        self.lblType.tag = Int("\(self.dictFareData.value(forKey: "FareId")!)")!
                        self.lblType.textColor = hexStringToUIColor(hex: primaryDark)
                    }
                    
                   
                    
                }
                else{
                    
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                    
                }
            }
            
            
        }
    }
}



//MARK:-
//MARK:- ---------AddressFromMapScreenDelegate------

extension FareCalculationVC: AddressFromMapScreenDelegate {
    func GetAddressFromMapScreen(dictData: NSDictionary, tag: Int) {
        
        if(index == 0){
            self.lblSource.text = (dictData.value(forKey: "address")as! String)
            LatSource = (dictData.value(forKey: "lat")as! String)
            LongSource = (dictData.value(forKey: "long")as! String)
            self.lblSource.textColor = hexStringToUIColor(hex: primaryDark)
        }else{
            self.lblDestination.text = (dictData.value(forKey: "address")as! String)
            LatDestination = (dictData.value(forKey: "lat")as! String)
            LongDestination = (dictData.value(forKey: "long")as! String)
            self.lblDestination.textColor = hexStringToUIColor(hex: primaryDark)
        }
      
    }
    
    
}

//MARK:-
//MARK:- ---------PopUpDelegate

extension FareCalculationVC : PopUpDelegate
{
    func getDataFromPopupDelegate(dictData: NSDictionary, tag: Int) {
        if(tag == 9){
            self.dictFareData = NSMutableDictionary()
            self.dictFareData = dictData.mutableCopy()as! NSMutableDictionary
            self.lblType.text = "\(dictData.value(forKey: "VehicleType")!)"
            self.lblType.tag = Int("\(dictData.value(forKey: "FareId")!)")!
            self.lblType.textColor = hexStringToUIColor(hex: primaryDark)
        }
    }
}
//MARK:-
//MARK:- ---------TextFiledDelegate

extension FareCalculationVC : UITextFieldDelegate
{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if ( textField == self.textField)
        {
            return txtFiledValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 2)
        }
            return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
}
