//
//  SplashDynamicVC.swift
//  CitizenCOP
//
//  Created by Sourabh Khare  on 4/23/24.
//

import UIKit

protocol citySelectionBack : Any {
    func backPop(isBool : Bool)
}

class SplashDynamicVC: UIViewController {
    
    @IBOutlet weak var imgView: UIImageView!
    
    var SplashVersionMsg = String()
    var delegate : citySelectionBack?
    var time: Double?
    var timer: Timer?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let url = "https://m.citizencop.org/images/\(SplashVersionMsg)"
        self.imgView.setImageWith(URL(string: url), placeholderImage: UIImage(named: "NoImage"), options: SDWebImageOptions(rawValue: 1), completed: { (image, error, type, url) in
            }, usingActivityIndicatorStyle: .gray)
       
        let when = DispatchTime.now() + (time ?? 0.0)
        DispatchQueue.main.asyncAfter(deadline: when){
            print("Hello")
            self.delegate?.backPop(isBool: true)
        }
    }
    
}
