//
//  imageShowViewController.swift
//  CitizenCOP
//
//  Created by Sourabh Khare  on 4/25/24.
//

import UIKit

class imageShowViewController: UIViewController {
    
    @IBOutlet weak var imageVIew: UIImageView!
    @IBOutlet weak var viewHeader: UIView!
    
    var qrcodeimage = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryTheamColor)
        imageVIew.setImageWith(URL(string: qrcodeimage), placeholderImage: UIImage(named: "NoImage"), options: SDWebImageOptions(rawValue: 1), completed: { (image, error, type, url) in
        }, usingActivityIndicatorStyle: .gray)
    }
    

    @IBAction func actionOnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: false)
    }
    

}
