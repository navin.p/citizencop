//
//  HelpMeTimerVC.swift
//  CitizenCOP
//
//  Created by Navin Patidar on 6/17/19.
//  Copyright © 2019 CitizenCop. All rights reserved.
//

import UIKit
import MessageUI
import CoreLocation
import UserNotifications

class HelpMeTimerVC: UIViewController {
    // MARK: - --------------IBOutlet
    // MARK: -
    
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var picker: UIDatePicker!
    @IBOutlet weak var lblTimer: UILabel!
    @IBOutlet weak var btnStartTimer: UIButton!
    @IBOutlet weak var btnResetTimer: UIButton!
    
    var remainingCounts = Int()
    var notiTitle = String()

    // MARK: - -------------LifeCycle
    // MARK: -
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 13.4, *) {
            picker.preferredDatePickerStyle = .wheels
        } else {
            // Fallback on earlier versions
        }
        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryTheamColor)
        lblTimer.backgroundColor = hexStringToUIColor(hex: primaryTheamColor)
        btnStartTimer.backgroundColor = hexStringToUIColor(hex: primaryDark)
        btnResetTimer.backgroundColor  = hexStringToUIColor(hex: primaryDark)
        btnResetTimer.layer.cornerRadius = 6.0
        btnStartTimer.layer.cornerRadius = 6.0
        lblTimer.layer.cornerRadius = 10.0
        lblTimer.clipsToBounds = true
        btnResetTimer.isUserInteractionEnabled = false
        btnResetTimer.setTitleColor(UIColor.lightGray, for: .normal)
        btnStartTimer.isUserInteractionEnabled = true
        btnStartTimer.setTitleColor(UIColor.white, for: .normal)
        if(nsud.value(forKey: "CitizenCop_HelpMeTimer") != nil){
            if(nsud.value(forKey: "CitizenCop_HelpMeTimer") is Date){
                remainingCounts = Int((nsud.value(forKey: "CitizenCop_HelpMeTimer")as! Date).timeIntervalSinceNow)
                if remainingCounts != 0 && !(remainingCounts < 0) {
                    timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.counterDown), userInfo: nil, repeats: true)
                }
            }
        }
        nsud.set(false, forKey: "HelpMeTimerNoti")
        picker.layer.cornerRadius = 20.0
        if(notiTitle == "HelpMeTimerNoti"){
            callAPIForSendMessage()
        }
    }
    // MARK: - --------------ScheduleNotification
    // MARK: -
    func scheduleNotification(strMessage : String , timeInterval : Int) {
        //Compose New Notificaion
        let content = UNMutableNotificationContent()
        content.sound = UNNotificationSound.default
        content.title = "HelpMe"
        content.body = strMessage.html2String
        content.badge = 0
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: TimeInterval(timeInterval), repeats: false)
        let identifier = "\(uniqueString())"
        let request = UNNotificationRequest(identifier: identifier, content: content, trigger: trigger)
        appDelegate.center.delegate = appDelegate
        appDelegate.center.add(request) { (error) in
            if let error = error {
                print("Error \(error.localizedDescription)")
            }
        }
    }
    
    // MARK: - --------------IBAction
    // MARK: -
    @IBAction func actionOnBack(_ sender: UIButton) {
        if(notiTitle == "HelpMeTimerNoti"){
                  let testController = mainStoryboard.instantiateViewController(withIdentifier: "DashBoardVC")as! DashBoardVC
                          self.navigationController?.pushViewController(testController, animated: false)
                  
            
        }else{
            self.navigationController?.popViewController(animated: true)

        }
        
    }
    
    @IBAction func actionOnStartTimer(_ sender: Any) {
        if(lblTimer.text == "00:00:00" || lblTimer.text == "---HH:MM:SS---"){
           showAlertWithoutAnyAction(strtitle: alertMessage, strMessage:"Please set the time first." , viewcontrol: self)
        }else {

             nsud.set(picker?.date, forKey: "CitizenCop_HelpMeTimer")
            let elapsedTime = Int((nsud.value(forKey: "CitizenCop_HelpMeTimer")as! Date).timeIntervalSinceNow)
            scheduleNotification(strMessage: "Do you want to send Message?", timeInterval: elapsedTime)

            timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.counterDown), userInfo: nil, repeats: true)
        }
    }
    
    @IBAction func actionOnRestTimer(_ sender: Any) {
        btnResetTimer.isUserInteractionEnabled = false
        btnResetTimer.setTitleColor(UIColor.lightGray, for: .normal)
        btnStartTimer.isUserInteractionEnabled = true
        btnStartTimer.setTitleColor(UIColor.white, for: .normal)
        lblTimer.text = "00:00:00"
        picker!.date = Date()
        nsud.set(nil, forKey: "CitizenCop_HelpMeTimer")
        timer.invalidate()
    }
    
    @IBAction func dateChanged(_ sender: Any) {
        let picker = sender as? UIDatePicker
        let pickerDate = picker?.date
        let elapsedTime = pickerDate?.timeIntervalSinceNow
        let ti = Int(elapsedTime ?? 0)
        let seconds = ti % 60
        let minutes = (ti / 60) % 60
        let hours = ti / 3600
        lblTimer.text = String(format: "%02ld:%02ld:%02ld", hours, minutes, seconds)
        let string = lblTimer.text
        if (string! as NSString).range(of: "-").location == NSNotFound {
            lblTimer.text = String(format: "%02ld:%02ld:%02ld", hours, minutes, seconds)
        } else {
            lblTimer.text = "00:00:00"
            picker!.date = Date()
        }
    }
    
    @objc func counterDown() {
        if(nsud.value(forKey: "CitizenCop_HelpMeTimer") != nil){
            let elapsedTime = Int((nsud.value(forKey: "CitizenCop_HelpMeTimer")as! Date).timeIntervalSinceNow)
            let seconds = elapsedTime % 60
            let minutes = (elapsedTime / 60) % 60
            let hours = elapsedTime / 3600
            lblTimer.text = String(format: "%02ld:%02ld:%02ld", hours, minutes, seconds)
            if elapsedTime != 0 && !(elapsedTime < 0) {
                lblTimer.text = String(format: "%02ld:%02ld:%02ld", hours, minutes, seconds)
                btnStartTimer.isUserInteractionEnabled = false
                btnStartTimer.setTitleColor(UIColor.lightGray, for: .normal)
                btnResetTimer.isUserInteractionEnabled = true
                btnResetTimer.setTitleColor(UIColor.white, for: .normal)
                // nsud.set(remainingCounts, forKey: "CitizenCop_HelpMeTimer")
            } else {
                nsud.set(nil, forKey: "CitizenCop_HelpMeTimer")
                lblTimer.text = "00:00:00"
                picker.date = Date()
                timer.invalidate()
                btnResetTimer.isUserInteractionEnabled = false
                btnResetTimer.setTitleColor(UIColor.lightGray, for: .normal)
                btnStartTimer.isUserInteractionEnabled = true
                btnStartTimer.setTitleColor(UIColor.white, for: .normal)
               // callAPIForSendMessage()
            }
            print("\(lblTimer.text ?? "0")")
        }
       
    }
    
    func callAPIForSendMessage() {
        let strCityId = "\(((dictCityData.value(forKey: "CityData")as! NSArray).object(at: 0)as! NSDictionary).value(forKey: "city_id")!)"
        let msgBody = "\(strCityId) ^ Help Me ! My Location : \nmaps.google.com/maps?f=q&t=m&q=\(GlobleLat),\(GlobleLong)"
        if (MFMessageComposeViewController.canSendText()) {
            let messageVC = MFMessageComposeViewController()
            messageVC.body = msgBody;
            if nsud.value(forKey: "CitizenCOP_HelpContact") != nil {
                var aryContactList = NSMutableArray()
                aryContactList = (nsud.value(forKey: "CitizenCOP_HelpContact")as! NSArray).mutableCopy()as! NSMutableArray
                var strrecipients = ""
                for item in aryContactList{
                    strrecipients = strrecipients + "\(item as AnyObject),"
                }
                let strHelpNumber = "\(((dictCityData.value(forKey: "CityData")as! NSArray).object(at: 0)as! NSDictionary).value(forKey: "HelpNo")!)"
                messageVC.recipients = [strrecipients,strHelpNumber]
            }
            messageVC.messageComposeDelegate = self
            self.present(messageVC, animated: true, completion: nil)
        }else{
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertCalling, viewcontrol: self)
        }
    }
}
// MARK: - ----------------MFMessageComposeViewControllerDelegate
// MARK: -
extension HelpMeTimerVC : MFMessageComposeViewControllerDelegate{
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        switch (result) {
        case .cancelled:
            print("Message was cancelled")
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Message was cancelled", viewcontrol: self)
            dismiss(animated: true, completion: nil)
        case .failed:
            print("Message failed")
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Message failed", viewcontrol: self)
            dismiss(animated: true, completion: nil)
        case .sent:
            print("Message was sent")
            dismiss(animated: true, completion: nil)
        default:
            break
        }
    }
    
}
//// MARK: --------UNNotification Delegate Method----------
//
//extension HelpMeTimerVC : UNUserNotificationCenterDelegate{
//
//    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
//        //displaying the ios local notification when app is in foreground
//        completionHandler([.alert, .badge, .sound])
//        callAPIForSendMessage()
//        
//    }
//
//    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
//        callAPIForSendMessage()
//        print("recive user notification\(response)")
//       // showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: "", viewcontrol: DashBoardVC.init())
//    }
//}
