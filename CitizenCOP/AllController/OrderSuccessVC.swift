//
//  OrderSuccessVC.swift
//  CitizenCOP
//
//  Created by Navin Patidar on 4/15/20.
//  Copyright © 2020 CitizenCop. All rights reserved.
//

import UIKit

class OrderSuccessVC: UIViewController {
    @IBOutlet weak var btnok: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        btnok.layer.cornerRadius = 8.0

        // Do any additional setup after loading the view.
    }
    
    // MARK: - ----Button Actions

      @IBAction func actiononOK(_ sender: UIButton) {
        NotificationCenter.default.post(name: Notification.Name("Back"), object: nil)

        self.dismiss(animated: false) {
        }
      }
      
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
