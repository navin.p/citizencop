//
//  HelpVC.swift
//  CitizenCOP
//
//  Created by Navin Patidar on 5/17/19.
//  Copyright © 2019 CitizenCop. All rights reserved.
//

import UIKit

class HelpVC: UIViewController {
   
    // MARK: - ----IBOutlet
    @IBOutlet weak var view_ForHeader: UIView!
    @IBOutlet weak var txtView: UITextView!
    
    // MARK: - ----Variable
    var strMessage = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view_ForHeader.backgroundColor = hexStringToUIColor(hex: primaryTheamColor)
        txtView.text = strMessage
        txtView.isUserInteractionEnabled = false
    }
    // MARK: - ---------------IBAction
    // MARK: -
    @IBAction func actiononBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
     
        
    }
}
