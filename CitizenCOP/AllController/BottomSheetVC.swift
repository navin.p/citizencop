//
//  BottomSheetVC.swift
//  CitizenCOP
//
//  Created by Navin Patidar on 11/7/19.
//  Copyright © 2019 CitizenCop. All rights reserved.
//

import UIKit

class BottomSheetVC: UIViewController {
    
    
    var frame = CGRect()
    var strTitle = ""
    var strMessage = ""
    var strViewComeFrom = ""

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var viewContinue: CardView!
    @IBOutlet weak var btnClose: UIButton!

   @IBOutlet weak var constantWebView: NSLayoutConstraint!
    open weak var delegate:BottomSheetDelegate?

    //MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblTitle.text = strTitle
        
        lblTitle.textColor = hexStringToUIColor(hex: primaryTheamColor)
        viewContinue.backgroundColor = hexStringToUIColor(hex: primaryTheamColor)
       
    }
    override func viewWillAppear(_ animated: Bool) {
        constantWebView.constant = frame.origin.y + 100.0
          if(strViewComeFrom == "CitySection"){
                     if(dictCityData.value(forKey: "CityData")as? NSArray ?? NSArray()).count != 0{
                      let isSplashVersionMsg = "\(((dictCityData.value(forKey: "CityData")as! NSArray).object(at: 0)as! NSDictionary).value(forKey: "SplashVersionMsg")!)"
                      self.webView.loadHTMLString(isSplashVersionMsg, baseURL: nil)
              }
              btnClose.isHidden = true
              viewContinue.isHidden = false
          }
          else if(strViewComeFrom == "CitySectionInfo"){
              
              if(dictCityData.value(forKey: "CityData")as? NSArray ?? NSArray()).count != 0{
                  let isSplashVersionMsg = "\(((dictCityData.value(forKey: "CityData")as! NSArray).object(at: 0)as! NSDictionary).value(forKey: "SplashVersionMsg")!)"
                  self.webView.loadHTMLString(isSplashVersionMsg, baseURL: nil)
              }
              viewContinue.isHidden = true
          }
          
          else{
              
              viewContinue.isHidden = true
              self.webView.loadHTMLString(strMessage, baseURL: nil)
          }
        UIView.animate(withDuration: 0.3, animations: { [weak self] in
            self!.view.frame = self!.frame
        })
        
      
        
    }
    
    //MARK: IBAction

     @IBAction func actionOnClose(_ sender: UIButton) {
     self.delegate?.getDataFromBottomSheetDelegate()
        UIView.animate(withDuration: 0.6, animations: { [weak self] in
            self!.willMove(toParent: nil)
            self!.removeFromParent()
            self!.view.removeFromSuperview()
        })
    }

    @IBAction func actionOnContinue(_ sender: UIButton) {
       self.delegate?.getDataFromBottomSheetDelegate()
        let vc: DynemicSplashVC = mainStoryboard.instantiateViewController(withIdentifier: "DynemicSplashVC") as! DynemicSplashVC
        self.navigationController?.pushViewController(vc, animated: false)
    }
}
//MARK: -------Protocol

protocol BottomSheetDelegate : class{
    func getDataFromBottomSheetDelegate()
}
