//
//  SearchOfficialVC.swift
//  CitizenCOP
//
//  Created by Navin Patidar on 7/4/19.
//  Copyright © 2019 CitizenCop. All rights reserved.
//

import UIKit

class SearchOfficialVC: UIViewController {
  
    // MARK: - --------------IBOutlet
    // MARK: -
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var txtEmpCode: ACFloatingTextfield!
    @IBOutlet weak var txtEmpName: ACFloatingTextfield!
    @IBOutlet weak var btnCategoryTitle: UIButton!
    @IBOutlet weak var lblTitle: UILabel!

    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblCode: UILabel!
    @IBOutlet weak var lblContactNumber: UILabel!
    @IBOutlet weak var lblDOB: UILabel!
    @IBOutlet weak var lblBG: UILabel!
    @IBOutlet weak var lblDesignation: UILabel!
    @IBOutlet weak var lblPosting: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblReportingOfficerName: UILabel!
    @IBOutlet weak var lblReportingOfficerNumber: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var tv: UITableView!

    
    
    var strlblTittle = String()
    var aryCategory = NSMutableArray()
    var dictSearchData = NSMutableDictionary()

  
    // MARK: - ---------------LifeCycle
    // MARK: -
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblTitle.text = strlblTittle
        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryTheamColor)
        btnSearch.backgroundColor = hexStringToUIColor(hex: primaryDark)
        if(dictCityData.value(forKey: "CityData")as! NSArray).count != 0{
            let strCityId = "\(((dictCityData.value(forKey: "CityData")as! NSArray).object(at: 0)as! NSDictionary).value(forKey: "city_id")!)"
            callOfficialsSearchAPI(strCityID: strCityId)
        }
        btnSearch.layer.cornerRadius = 8.0
        btnSearch.backgroundColor = hexStringToUIColor(hex: primaryDark)
        tv.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        imgProfile.layer.cornerRadius = 10.0
        imgProfile.layer.borderColor = UIColor.groupTableViewBackground.cgColor
        imgProfile.layer.borderWidth = 1.0
        
    }

    // MARK: - ---------------IBAction
    // MARK: -
    @IBAction func actiononBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actiononHelp(_ sender: UIButton) {
        let testController = mainStoryboard.instantiateViewController(withIdentifier: "HelpVC")as! HelpVC
        testController.strMessage = alertOfficialsearch
        self.navigationController?.pushViewController(testController, animated: true)
    }
    
    @IBAction func actionOnSearch(_ sender: Any) {
        if(btnCategoryTitle.tag == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please Select Category!", viewcontrol: self)
        }else if(txtEmpName.text?.count == 0 && txtEmpCode.text?.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter employee name/ employee code!", viewcontrol: self)
        }else {
            if(dictCityData.value(forKey: "CityData")as! NSArray).count != 0{
                let strCityId = "\(((dictCityData.value(forKey: "CityData")as! NSArray).object(at: 0)as! NSDictionary).value(forKey: "city_id")!)"
                let AllSearchOfficials :String = "\(API_GetAllSearchOfficials)&cityid=\(strCityId)&EmployeeCode=\(txtEmpCode.text!)&Name=\(txtEmpName.text!)&PVCategoryId=\(btnCategoryTitle.tag)"
                getOfficialsSearchAPI(urlName: AllSearchOfficials)
                
            }
        }
        
       
    }
    
    @IBAction func actionOnDropDown(_ sender: Any) {
        if(self.aryCategory.count != 0){
            self.PopViewWithArray(tag: 10, aryData: self.aryCategory, strTitle: "Select Category")
        }else{
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Category not found!", viewcontrol: self)
        }

    }
    // MARK: - ---------------API CAlling
    // MARK: -
    func getOfficialsSearchAPI(urlName: String) {
        self.view.endEditing(true)
        if !isInternetAvailable() {
            FTIndicator.showToastMessage(alertInternet)
        }else{
            customDotLoaderShowOnButton(btn: btnSearch, view: self.view, controller: self)
            WebServiceClass.callAPIBYPOST(parameter: NSDictionary(), url: urlName) { (responce, status) in
                customeDotLoaderRemove()
                if status == "Suceess"{
                    print(responce)
                    let dictData = (responce.value(forKey: "data")as! NSDictionary)
                    let status = "\(dictData.value(forKey: "Result")!)"
                    if(status == "True"){
                        self.dictSearchData = NSMutableDictionary()
                        let aryTemp = (dictData.value(forKey: "PoliceVerification")as! NSArray)
                        self.dictSearchData = (aryTemp.object(at: 0)as! NSDictionary).mutableCopy()as! NSMutableDictionary
                        self.setDataOnView(dict: self.dictSearchData)

                    }else{
                        self.tv.isHidden = true

                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: dictData.value(forKey: "Message") as! String, viewcontrol: self)
                    }
                }
                else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            }
            
            
        }
    }
    
    
    func callOfficialsSearchAPI(strCityID: String) {
        if !isInternetAvailable() {
            FTIndicator.showToastMessage(alertInternet)
        }else{
            customDotLoaderShowOnButton(btn: btnCategoryTitle, view: btnCategoryTitle.superview!, controller: self)

            WebServiceClass.callAPIBYPOST(parameter: NSDictionary(), url: API_SearchOfficialsCategory + strCityID) { (responce, status) in
                customeDotLoaderRemove()
                if status == "Suceess"{
                    self.aryCategory = NSMutableArray()
                    self.aryCategory = (responce.value(forKey: "data")as! NSArray).mutableCopy()as! NSMutableArray
                }
                else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            }
            
            
        }
    }
    // MARK: - ---------------Other Function's
    // MARK: -
    
    func setDataOnView(dict : NSMutableDictionary) {
        self.tv.isHidden = false

        lblName.text = "\(dict.value(forKey: "FirstName")!) \(dict.value(forKey: "LastName")!)"
        lblCode.text = "EmployeeCode-\(dict.value(forKey: "EmployeeCode")!)"
        lblContactNumber.text = "\(dict.value(forKey: "ContactNo")!)"
        lblDOB.text = "DOB: \(dict.value(forKey: "DOB")!)"
        lblBG.text = "BloodGroup: \(dict.value(forKey: "BloodGroup")!)"
        lblAddress.text = "Address: \(dict.value(forKey: "Address")!)"
        lblDesignation.text = "Designation: \(dict.value(forKey: "Designation")!)"
        lblPosting.text = "Current Posting: \(dict.value(forKey: "CurrentPosting")!)"
        lblReportingOfficerName.text = "Reporting Officer Name: \(dict.value(forKey: "ReportingOfficerName")!)"
        lblReportingOfficerNumber.text = "Reporting Officer Contact No.: \(dict.value(forKey: "ReportingOfficerContactNo")!)"
        var imgUrl = "\(BaseURL2)whitelistimage/\(dict.value(forKey: "Photo")!)"
    }
    
    
    func PopViewWithArray(tag: Int , aryData : NSMutableArray , strTitle: String)  {
        let vc: SelectionClass = mainStoryboard.instantiateViewController(withIdentifier: "SelectionClass") as! SelectionClass
        vc.strTitle = strTitle
        vc.strTag = tag
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .coverVertical
        vc.delegate = self
        vc.aryList = aryData
        self.present(vc, animated: true, completion: {})
    }
}

//MARK:-
//MARK:- ---------PopUpDelegate

extension SearchOfficialVC : PopUpDelegate
{
    func getDataFromPopupDelegate(dictData: NSDictionary, tag: Int) {
        btnCategoryTitle.setTitle("\(dictData.value(forKey: "PVCategoryName")!)", for: .normal)
        btnCategoryTitle.tag = Int("\(dictData.value(forKey:"PVCategoryId")!)")!
    }
}

//MARK:-
//MARK:- ---------UITextFieldDelegate

extension SearchOfficialVC : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if ( textField == txtEmpName)
        {
            return txtFiledValidation(textField: textField, string: string, returnOnly: "ALL", limitValue: 45)
        }
        if ( textField == txtEmpCode)
        {
            return txtFiledValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 45)
        }
            
        else
        {
            return true
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
}
