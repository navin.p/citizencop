//
//  NearByPlaceVC.swift
//  CitizenCOP
//
//  Created by Navin Patidar on 5/22/19.
//  Copyright © 2019 CitizenCop. All rights reserved.
//

import UIKit
import CoreLocation

class NearByPlaceVC: UIViewController {
   
    // MARK: - ----IBOutlet
    @IBOutlet weak var tvlist: UITableView!
    @IBOutlet weak var view_ForHeader: UIView!
    // MARK: - ----Variable
    var ary_NearData = NSMutableArray()
  
    override func viewDidLoad() {
        super.viewDidLoad()
        view_ForHeader.backgroundColor = hexStringToUIColor(hex: primaryTheamColor)

        print(ary_NearData)
        
        
        ary_NearData = [["title":"Police","image":"police"],["title":"Hospital","image":"hospital"],["title":"Doctor","image":"doctor"],["title":"Atm","image":"bank_atm"],["title":"Bank","image":"bank"],["title":"Gas_station","image":"petrol"],["title":"Busstand","image":"busstand"],["title":"Restaurants","image":"resturant"],["title":"Railwaystation","image":"railway"],["title":"Pharmacy","image":"pharmacy"],["title":"Carrepair","image":"car_service"],["title":"Cafe","image":"cafe"]]
        self.tvlist.reloadData()
    }
    
    // MARK: - --------------IBAction
    // MARK: -
    @IBAction func actionOnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    } 

    
}
// MARK: - ----------------UITableViewDelegate
// MARK: -

extension NearByPlaceVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ary_NearData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tvlist.dequeueReusableCell(withIdentifier: "NearPlaceCell", for: indexPath as IndexPath) as! NearPlaceCell
                let dict = ary_NearData.object(at: indexPath.row)as! NSDictionary
        cell.lblTitle.text = dict["title"]as? String
        cell.img.image = UIImage(named: "\(dict["image"]!)")
      
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 103
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let dict = ary_NearData.object(at: indexPath.row)as! NSDictionary
        let url = "http://maps.apple.com/maps?q=\(dict["title"]as! String)"
        UIApplication.shared.openURL(URL(string:url)!)

        
    }
 
}


// MARK: - ----------------NearPlaceCell
// MARK: -
class NearPlaceCell: UITableViewCell {
    //DashBoard
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var img: UIImageView!

    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}
