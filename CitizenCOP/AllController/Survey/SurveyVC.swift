//
//  SurveyVC.swift
//  CitizenCOP
//
//  Created by Navin Patidar on 9/19/19.
//  Copyright © 2019 CitizenCop. All rights reserved.
//

import UIKit

class SurveyVC: UIViewController {
    
    // MARK: - --------------IBOutlet
    // MARK: -
    
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var tvForList: UITableView!
    @IBOutlet weak var btnSubmit: UIButton!

    var strSurveyID = String()
    var arySurvey = NSMutableArray()
    // MARK: - --------------LifeCycle
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        tvForList.tableFooterView = UIView()
        tvForList.estimatedRowHeight = 95.0
        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryTheamColor)
        callSurveyDataAPI(strSurveyID: strSurveyID)
        
        btnSubmit.layer.cornerRadius = 8.0
        btnSubmit.backgroundColor = hexStringToUIColor(hex: primaryDark)
        // Do any additional setup after loading the view.
    }
    
    // MARK: - ---------------IBAction
    // MARK: -
    @IBAction func actiononBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func actionOnSubmit(_ sender: UIButton) {
      callSurveySaveDataAPI()

    }
    // MARK: - ---------------genrateDynemicView
    // MARK: -
    func genrateDynemicView( aryData : NSMutableArray)  {
    
    for view in self.tvForList.subviews {
    view.removeFromSuperview()
    }
    var yAxix = 20.0
    
    for (index, item) in aryData.enumerated() {
    let dict = (item as AnyObject)as! NSDictionary
    let dictData = dict.mutableCopy()as! NSMutableDictionary
    let strGroupName = dictData.value(forKey: "GroupName")as! String
    let strlblQuestion = dictData.value(forKey: "title")as! String
    let strType = dictData.value(forKey: "type")as! String
    let strOption = dictData.value(forKey: "lables")as! String
    let strAnswer = dictData.value(forKey: "value")as! String
    let strIsYesNo = "\(dictData.value(forKey: "IsYesNo")!)"
    //MARK:Radio
    
    if(strType == "standard-radio" || strType == "radio"){
        
        //GropName
        let lblGroupName = UILabel()
         lblGroupName.numberOfLines = 0
         lblGroupName.font = UIFont.boldSystemFont(ofSize: 18)
         lblGroupName.text = strGroupName
         lblGroupName.frame = CGRect(x: 10, y: Int(yAxix), width: Int(self.tvForList.frame.width - 20), height:  Int(estimatedHeightOfLabel(text: lblGroupName.text!, view: self.view)))
        yAxix = Double(CGFloat(yAxix) + lblGroupName.frame.size.height  + 10)
         self.tvForList.addSubview(lblGroupName)
       
        //QuestionName

         let lblQuestion = UILabel()
         lblQuestion.numberOfLines = 0
         lblQuestion.font = UIFont.systemFont(ofSize: 18)
        lblQuestion.text = "Question:- " + strlblQuestion
        lblQuestion.frame = CGRect(x: 10, y: Int(yAxix), width: Int(self.tvForList.frame.width - 20), height:  Int(estimatedHeightOfLabel(text: lblQuestion.text!, view: self.view)))
        yAxix = Double(CGFloat(yAxix) + lblQuestion.frame.size.height  + 10)
        self.tvForList.addSubview(lblQuestion)
        
        
        //Opetion
        let aryOption = strOption.split(separator: ",")
        for item in aryOption{
            let btnradio = UIButton()
            btnradio.setTitleColor(UIColor.clear, for: .normal)
            btnradio.frame = CGRect(x: 10, y: Int(yAxix), width: 30, height: 30)
            btnradio.setTitle(((item as AnyObject)as! String), for: .normal)

            if(strAnswer == ""){
                btnradio.setImage(UIImage(named: "RadioButton-Unselected"), for: .normal)
            }else{
                if("\((btnradio.titleLabel?.text!)!)" == strAnswer){
                    btnradio.setImage(UIImage(named: "RadioButton-Selected"), for: .normal)
                }else{
                    btnradio.setImage(UIImage(named: "RadioButton-Unselected"), for: .normal)
                }
            }
            btnradio.tag = index
            btnradio.addTarget(self, action: #selector(actionOnRadioButto), for: .touchUpInside)


            let lblOption = UILabel()
            lblOption.text = ((item as AnyObject)as! String)
            lblOption.font = UIFont.systemFont(ofSize: 18)
            lblOption.numberOfLines = 0
            lblOption.frame = CGRect(x: 40, y: Int(yAxix), width: Int(self.tvForList.frame.width) - 60, height:  Int(estimatedHeightOfLabel(text: lblOption.text!, view: self.view)))
           
            self.tvForList.addSubview(btnradio)
            self.tvForList.addSubview(lblOption)

            yAxix = Double(CGFloat(yAxix) +  lblOption.frame.height  + 10)
        }
        
        
        self.tvForList.addSubview(underlineCreat(ract: CGRect(x: 0.0, y: yAxix, width:  Double(self.view.frame.width ), height: 1.0)))
        yAxix = Double(CGFloat(yAxix) +  5.0)
    }
    
        //MARK:TextBox
        if(strType == "textarea" || strType == "TextBox" || strType == "textbox"){
            //GropName
            let lblGroupName = UILabel()
            lblGroupName.numberOfLines = 0
            lblGroupName.font = UIFont.boldSystemFont(ofSize: 18)
            lblGroupName.text = strGroupName
            lblGroupName.frame = CGRect(x: 10, y: Int(yAxix), width: Int(self.tvForList.frame.width - 20), height:  Int(estimatedHeightOfLabel(text: lblGroupName.text!, view: self.view)))
            yAxix = Double(CGFloat(yAxix) + lblGroupName.frame.size.height  + 10)
            self.tvForList.addSubview(lblGroupName)
            
            //QuestionName
            let lblQuestion = UILabel()
            lblQuestion.numberOfLines = 0
            lblQuestion.font = UIFont.systemFont(ofSize: 18)
            lblQuestion.text = "Question:- " + strlblQuestion
            lblQuestion.frame = CGRect(x: 10, y: Int(yAxix), width: Int(self.tvForList.frame.width - 20), height:  Int(estimatedHeightOfLabel(text: lblQuestion.text!, view: self.view)))
            yAxix = Double(CGFloat(yAxix) + lblQuestion.frame.size.height  + 10)
            self.tvForList.addSubview(lblQuestion)
            
            //Option
           let txtview = UITextView()
           txtview.text = strAnswer
            txtview.layer.cornerRadius = 8.0
            txtview.layer.borderWidth = 1.0
            txtview.layer.borderColor = UIColor.lightGray.cgColor
            txtview.frame = CGRect(x: 10, y: Int(yAxix), width: Int(self.view.frame.width - 20), height: 100)
            txtview.delegate = self
            txtview.tag = index
            txtview.font = UIFont.systemFont(ofSize: 18)
            yAxix = Double(CGFloat(yAxix) + 100 + 10)

            self.tvForList.addSubview(txtview)
            self.tvForList.addSubview(underlineCreat(ract: CGRect(x: 0.0, y: yAxix, width:  Double(self.view.frame.width ), height: 1.0)))
            yAxix = Double(CGFloat(yAxix) +  5.0)
    }
        
        
        if(strType == "checkbox"){
            //GropName
            let lblGroupName = UILabel()
            lblGroupName.numberOfLines = 0
            lblGroupName.font = UIFont.boldSystemFont(ofSize: 18)
            lblGroupName.text = strGroupName
            lblGroupName.frame = CGRect(x: 10, y: Int(yAxix), width: Int(self.tvForList.frame.width - 20), height:  Int(estimatedHeightOfLabel(text: lblGroupName.text!, view: self.view)))
            yAxix = Double(CGFloat(yAxix) + lblGroupName.frame.size.height  + 10)
            self.tvForList.addSubview(lblGroupName)
            
            //QuestionName
            let lblQuestion = UILabel()
            lblQuestion.numberOfLines = 0
            lblQuestion.font = UIFont.systemFont(ofSize: 18)
            lblQuestion.text = "Question:- " + strlblQuestion
            
            lblQuestion.frame = CGRect(x: 10, y: Int(yAxix), width: Int(self.tvForList.frame.width - 20), height:  Int(estimatedHeightOfLabel(text: lblQuestion.text!, view: self.view)))
            yAxix = Double(CGFloat(yAxix) + lblQuestion.frame.size.height  + 10)
            self.tvForList.addSubview(lblQuestion)
            
            //Opetion
            let aryOption = strOption.split(separator: ",")
            let aryAnswer = strAnswer.split(separator: ",")

            for item in aryOption{
                let btnCheckBox = UIButton()
                btnCheckBox.setTitleColor(UIColor.clear, for: .normal)
                btnCheckBox.frame = CGRect(x: 10, y: Int(yAxix), width: 30, height: 30)
                btnCheckBox.setTitle(((item as AnyObject)as! String), for: .normal)
                btnCheckBox.setImage(UIImage(named: "CheckBox-Unselected"), for: .normal)

                if(strAnswer == ""){
                    btnCheckBox.setImage(UIImage(named: "CheckBox-Unselected"), for: .normal)
                }else{
                    for item in aryAnswer{
                        if ("\((btnCheckBox.titleLabel?.text!)!)" ==  (item as AnyObject) as! String){
                            btnCheckBox.setImage(UIImage(named: "CheckBox-Selected"), for: .normal)
                        }
                    }
                 
                }
                btnCheckBox.tag = index
                btnCheckBox.addTarget(self, action: #selector(actionOnCheckButton), for: .touchUpInside)
                
                
                let lblOption = UILabel()
                lblOption.text = ((item as AnyObject)as! String)
                lblOption.font = UIFont.systemFont(ofSize: 18)
                lblOption.numberOfLines = 0
                lblOption.frame = CGRect(x: 40, y: Int(yAxix), width: Int(self.tvForList.frame.width) - 60, height:  Int(estimatedHeightOfLabel(text: lblOption.text!, view: self.view)))
                
                self.tvForList.addSubview(btnCheckBox)
                self.tvForList.addSubview(lblOption)
                
                yAxix = Double(CGFloat(yAxix) +  lblOption.frame.height  + 10)
            
            
        }
            self.tvForList.addSubview(underlineCreat(ract: CGRect(x: 0.0, y: yAxix, width:  Double(self.view.frame.width ), height: 1.0)))
            yAxix = Double(CGFloat(yAxix) +  5.0)
        }
        
        
        //MARK:Slider / YES NO
        if(strType == "none"){
            
            //GropName
            let lblGroupName = UILabel()
            lblGroupName.numberOfLines = 0
            lblGroupName.font = UIFont.boldSystemFont(ofSize: 18)
            lblGroupName.text = strGroupName
            lblGroupName.frame = CGRect(x: 10, y: Int(yAxix), width: Int(self.tvForList.frame.width - 20), height:  Int(estimatedHeightOfLabel(text: lblGroupName.text!, view: self.view)))
            yAxix = Double(CGFloat(yAxix) + lblGroupName.frame.size.height  + 10)
            self.tvForList.addSubview(lblGroupName)
            
            //QuestionName
            let lblQuestion = UILabel()
            lblQuestion.numberOfLines = 0
            lblQuestion.font = UIFont.systemFont(ofSize: 18)
            if(strIsYesNo == "0"){ //
                lblQuestion.text = "Question:- " + strlblQuestion + "\n\nValue : \((strAnswer as NSString).floatValue)"
            }else{
               lblQuestion.text = "Question:- " + strlblQuestion
            }
            
            lblQuestion.frame = CGRect(x: 10, y: Int(yAxix), width: Int(self.tvForList.frame.width - 20), height:  Int(estimatedHeightOfLabel(text: lblQuestion.text!, view: self.view)))
            yAxix = Double(CGFloat(yAxix) + lblQuestion.frame.size.height  + 10)
            self.tvForList.addSubview(lblQuestion)
            
            //Option
            
            if(strIsYesNo == "0"){ //Slider
                
                let sliderRange = UISlider()
                sliderRange.frame = CGRect(x: 10.0, y: CGFloat(yAxix), width: self.view.frame.width - 20 , height: 30 )
                sliderRange.thumbTintColor = hexStringToUIColor(hex: primaryTheamColor)
                sliderRange.minimumTrackTintColor = hexStringToUIColor(hex: primaryDark)
                sliderRange.maximumTrackTintColor = UIColor.lightGray
                yAxix = Double(CGFloat(yAxix) + sliderRange.frame.size.height  + 20)
                sliderRange.maximumValue = 10.0
                sliderRange.minimumValue = 0.0
                sliderRange.tag = index
                sliderRange.addTarget(self, action: #selector(pressSliderChange(_:)), for: .valueChanged)
                if(strAnswer == ""){
                     sliderRange.value = 0
                }else{
                    sliderRange.value = (strAnswer as NSString).floatValue
                }
                self.tvForList.addSubview(sliderRange)

            }else{ // Yes No Button
                 var aryTemp = NSMutableArray()
                aryTemp.add("Yes")
                aryTemp.add("No")
                aryTemp.add("N/A")
                for item in aryTemp{
                    let btnradio = UIButton()
                    btnradio.setTitleColor(UIColor.clear, for: .normal)
                    btnradio.frame = CGRect(x: 10, y: Int(yAxix), width: 30, height: 30)
                    btnradio.setTitle(((item as AnyObject)as! String), for: .normal)
                    
                    if(strAnswer == ""){
                        btnradio.setImage(UIImage(named: "RadioButton-Unselected"), for: .normal)
                    }else{
                        if("\((btnradio.titleLabel?.text!)!)" == strAnswer){
                            btnradio.setImage(UIImage(named: "RadioButton-Selected"), for: .normal)
                        }else{
                            btnradio.setImage(UIImage(named: "RadioButton-Unselected"), for: .normal)
                        }
                    }
                    btnradio.tag = index
                    btnradio.addTarget(self, action: #selector(actionOnRadioButto), for: .touchUpInside)
                    let lblOption = UILabel()
                    lblOption.text = ((item as AnyObject)as! String)
                    lblOption.font = UIFont.systemFont(ofSize: 18)
                    lblOption.numberOfLines = 0
                    lblOption.frame = CGRect(x: 40, y: Int(yAxix), width: Int(self.tvForList.frame.width) - 60, height:  Int(estimatedHeightOfLabel(text: lblOption.text!, view: self.view)))
                    
                    self.tvForList.addSubview(btnradio)
                    self.tvForList.addSubview(lblOption)
                    yAxix = Double(CGFloat(yAxix) +  lblOption.frame.height  + 10)

                }
                
            }
            self.tvForList.addSubview(underlineCreat(ract: CGRect(x: 0.0, y: yAxix, width:  Double(self.view.frame.width ), height: 1.0)))
            yAxix = Double(CGFloat(yAxix) +  5.0)
        }
      
   }
        DispatchQueue.main.async {
            self.tvForList.contentSize = CGSize(width: self.view.frame.size.width, height: CGFloat(yAxix + 100.0))
            
        }
  }
    // MARK: - --------------All Custome Action
    // MARK: -
    @objc func actionOnRadioButto(sender: UIButton) {
        
        let dict = arySurvey.object(at: sender.tag)as! NSDictionary
        let dictData = dict.mutableCopy()as! NSMutableDictionary
        dictData.setValue("\((sender.titleLabel?.text)!)", forKey: "value")
        arySurvey.replaceObject(at: sender.tag, with: dictData)
        genrateDynemicView(aryData: arySurvey)
        
    }
    @objc func actionOnCheckButton(sender: UIButton) {
        
        let dict = arySurvey.object(at: sender.tag)as! NSDictionary
        let dictData = dict.mutableCopy()as! NSMutableDictionary
        var answer = (dictData.value(forKey: "value")as! NSString)
        let strCheck = "\(String(describing: (sender.titleLabel?.text!)!)),"
        if answer.contains(strCheck) {
            let newString = answer.replacingOccurrences(of: "\(strCheck)", with:"")
            dictData.setValue(newString, forKey: "value")
        }else{
            answer = "\(answer)\(strCheck)" as NSString
            dictData.setValue(answer, forKey: "value")
            
        }
        
        arySurvey.replaceObject(at: sender.tag, with: dictData)
        genrateDynemicView(aryData: arySurvey)
        
    }
    @objc func pressSliderChange(_ slider: UISlider) {
        
        let dict = arySurvey.object(at: slider.tag)as! NSDictionary
        let dictData = dict.mutableCopy()as! NSMutableDictionary
        let roundedStepValue = round(slider.value / 0.5) * 0.5
        slider.value = roundedStepValue
        let twoDecimalPlaces = String(format: "%.1f", slider.value)
        dictData.setValue(twoDecimalPlaces, forKey: "value")
        arySurvey.replaceObject(at: slider.tag, with: dictData)
        genrateDynemicView(aryData: arySurvey)
    }
    
    func underlineCreat(ract:CGRect) -> UILabel {
        let lbl = UILabel()
        lbl.frame = ract
        lbl.backgroundColor = UIColor.lightGray
        return lbl
    }
    
    
    
    // MARK: - --------------API
    // MARK: -
    func callSurveyDataAPI(strSurveyID : String) {
        if !isInternetAvailable() {
            FTIndicator.showToastMessage(alertInternet)
        }else{
            let URL = API_ESurvey + strSurveyID
           // let URL = "http://www.gogetquality.com/survey/GoHandler.ashx?key=questionList&aid=4375172"
            print(URL)
            
            let loading = DPBasicLoading(table: self.tvForList, fontName: "HelveticaNeue")
            loading.startLoading(text: "Loading...")
            
            WebServiceClass.callAPIBYPOST(parameter: NSDictionary(), url: URL) { (responce, status) in
                print(responce)
                loading.endLoading()

                if status == "Suceess"{
                    let data = (responce.value(forKey: "data")as! NSDictionary).mutableCopy()as! NSMutableDictionary
                    if("\(data.value(forKey: "status")!)" == "New"){
                        self.arySurvey = NSMutableArray()
                        self.arySurvey = (data.value(forKey: "qList")as! NSArray).mutableCopy()as! NSMutableArray
                        self.genrateDynemicView(aryData: self.arySurvey)
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)

                    }
                }
                else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            }
            
        }
    }
    func callSurveySaveDataAPI() {
        if !isInternetAvailable() {
            FTIndicator.showToastMessage(alertInternet)
        }else{
            var json = Data()
            var jsonString = NSString()
            if(arySurvey.count == 0){
                jsonString = ""
            }else{
                if JSONSerialization.isValidJSONObject(arySurvey) {
                    json = try! JSONSerialization.data(withJSONObject: arySurvey, options: .prettyPrinted)
                    jsonString = String(data: json, encoding: .utf8)! as NSString
                    print("UpdateLeadinfo JSON: \(jsonString)")
                }else{
                    jsonString = ""
                }
            }
            let URL = "http://m.citizencop.org/Esurvey.ashx?Key=insertAuditNew&audit=\(jsonString)&Aid=\(strSurveyID)&status=Done&findings=&CompanyKey="
            print(URL)
            customDotLoaderShowOnButton(btn: btnSubmit, view: self.view, controller: self)

            WebServiceClass.callAPIBYPOST_ResponceInString(parameter: NSDictionary(), url: URL) { (responce, status) in
                print(responce)
                customeDotLoaderRemove()
                if status == "Suceess"{
                    
                    let alert = UIAlertController(title: alertInfo, message: "Survey submitted successfully.", preferredStyle: UIAlertController.Style.alert)
                    
                    // add the actions (buttons)
                    alert.addAction(UIAlertAction (title: "OK", style: .default, handler: { (nil) in
                        self.navigationController?.popViewController(animated: true)
                    }))
                    self.present(alert, animated: true, completion: nil)
                                   
                }
                else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            }
            
        }
    }
}
//MARK:
//MARK: -----------Text View Delegate Method----------

extension SurveyVC: UITextViewDelegate {
    
    func textViewDidEndEditing(_ textView: UITextView) {
        let dict = arySurvey.object(at:textView.tag)as! NSDictionary
        let dictData = dict.mutableCopy()as! NSMutableDictionary
        dictData.setValue(textView.text, forKey: "value")
        arySurvey.replaceObject(at: textView.tag, with: dictData)
        self.genrateDynemicView(aryData: arySurvey)
    }
    
}
