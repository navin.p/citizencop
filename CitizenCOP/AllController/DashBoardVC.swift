//
//  DashBoardVC.swift
//  CitizenCOP
//
//  Created by Navin Patidar on 5/17/19.
//  Copyright © 2019 CitizenCop. All rights reserved.
//

import UIKit
import SWXMLHash
import StringExtensionHTML
import AEXML
import SafariServices
import UserNotifications
import Firebase
import FirebaseMessaging
import CoreLocation
import AVFoundation
import AVKit
import Alamofire
import Photos
import CoreLocation
import UserNotifications
import Contacts
class DashBoardVC: UIViewController {
    
    
    
    @IBOutlet weak var collection: UICollectionView!
    @IBOutlet weak var lblCityName: UILabel!
    @IBOutlet weak var view_ForHeader: UIView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var btnLightVersion: UIButton!
    @IBOutlet weak var viewForAdvertisement: CPImageSlider!
    @IBOutlet weak var viewAdvertiseContain: UIView!
    @IBOutlet weak var btnIsPledge: UIButton!
    @IBOutlet weak var imgPledge: UIImageView!
    @IBOutlet weak var btnIsLightFull: UIButton!
    
    // MARK: - ----Variable
    // MARK:
    var ary_CollectionData = NSMutableArray()
    var refresher:UIRefreshControl!
    var ary_Advertisement = NSMutableArray()
    var ary_IsPledge = NSMutableArray()
    var dictNotiData = NSMutableDictionary()
    var alertStatus = String()
    
    var locationManager = CLLocationManager()
    var btnBackgroundView = UIButton()
    
    // MARK: - ----Life Cycle
    // MARK:
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(self.appMovedToBackground), name: NSNotification.Name(rawValue: "HelpMeNotif"), object: nil)
        
        
        
        if(nsud.value(forKey: "HelpMeTimerNoti") != nil){
            if(nsud.value(forKey: "HelpMeTimerNoti")as! Bool){
                nsud.set(false, forKey: "HelpMeTimerNoti")
                let testController = mainStoryboard.instantiateViewController(withIdentifier: "HelpMeTimerVC")as! HelpMeTimerVC
                testController.notiTitle = "HelpMeTimerNoti"
                self.navigationController?.pushViewController(testController, animated: true)
            }
        }
        
        
        if(nsud.value(forKey: "CitizenCOP_MySafeZone") == nil) || (nsud.value(forKey: "CitiZenCOP_JoinedGroupLocation") == nil){
            locationManager.stopUpdatingLocation()
        }
        
        
        
        if #available(iOS 13.0, *) {
            let statusBar1 =  UIView()
            statusBar1.frame = (UIApplication.shared.keyWindow?.windowScene?.statusBarManager!.statusBarFrame)!
            statusBar1.backgroundColor = hexStringToUIColor(hex: primaryTheamColor)
            UIApplication.shared.keyWindow?.addSubview(statusBar1)
            
        } else {
            let statusBar1: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
            statusBar1.backgroundColor = hexStringToUIColor(hex: primaryTheamColor)
        }
        
        if let uuid = UIDevice.current.identifierForVendor?.uuidString {
            DeviceID = "\(uuid)"
            //DeviceID = "60CB34F0-EA9E-4E1D-B7EC-910CEBBFE094"
            print("Device ID-----------\(DeviceID)")
        }
        self.becomeFirstResponder()
        if(nsud.value(forKey: "CitizenCOP_FCM_Token") != nil){
            PushToken = "\(nsud.value(forKey: "CitizenCOP_FCM_Token")!)"
            // PushToken = "60CB34F0-EA9E-4E1D-B7EC-910CEBBFE094"
            
        }else{
            PushToken = ""
        }
        
        view_ForHeader.backgroundColor = hexStringToUIColor(hex: primaryTheamColor)
        viewAdvertiseContain.backgroundColor = hexStringToUIColor(hex: primaryTheamColor)
        self.refresher = UIRefreshControl()
        self.refresher.tintColor = hexStringToUIColor(hex: primaryTheamColor)
        self.collection!.addSubview(refresher)
        
        dictCityData = NSMutableDictionary()
        dictCityData = (getDataFromCoreDataBaseDict(strEntity: "DashBoardCityData", strkey: "dashBoardCityData")).mutableCopy()as! NSMutableDictionary
        
        if(dictCityData.value(forKey: "CityData")as! NSArray).count != 0{
            lblCityName.text = "  \(((dictCityData.value(forKey: "CityData")as! NSArray).object(at: 0)as! NSDictionary).value(forKey: "city_name")!)"
        }
        
        //MARK: IsLiteVersion
        btnLightVersion.isHidden = false
        if(dictCityData.value(forKey: "CityData")as! NSArray).count != 0{
            let isLiteVersion = "\(((dictCityData.value(forKey: "CityData")as! NSArray).object(at: 0)as! NSDictionary).value(forKey: "IsLiteVersion")!)"
            isLiteVersion == "1" ? btnIsLightFull.setTitle("Lite Version", for: .normal) : btnIsLightFull.setTitle("Full Version", for: .normal)
        }
        
        
        //MARK:Get Token
        let center1 = UNUserNotificationCenter.current()
        center1.delegate = self
        center1.getNotificationSettings(completionHandler: { settings in
            switch settings.authorizationStatus {
            case .authorized, .provisional:
                print("authorized")
                DispatchQueue.main.async {
                    UIApplication.shared.registerForRemoteNotifications()
                    //                    if let token = InstanceID.instanceID().token() {
                    //                        nsud.setValue("\(token)", forKey: "CitizenCOP_FCM_Token")
                    //                        nsud.synchronize()
                    //                        print("FCM_Token----------------- : \(String(describing: token))")
                    //                    }
                }
            case .denied:
                break
            case .notDetermined:
                break
            @unknown default:
                break
            }
        })
        
        //MARK:Check ADMIN Show HIde
        
        if(nsud.value(forKey: "CItizenCOP_OneDay") != nil){
            let dateFormatter : DateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd"
            let date = Date()
            let dateString = dateFormatter.string(from: date)
            
            if(dateString != "\(nsud.value(forKey: "CItizenCOP_OneDay")!)"){
                let strCityId = "\(((dictCityData.value(forKey: "CityData")as! NSArray).object(at: 0)as! NSDictionary).value(forKey: "city_id")!)"
                self.callGetAdmin_NumberOrCardSHowHideAPI(strCityID: strCityId)
            }
            
        }else{
            let strCityId = "\(((dictCityData.value(forKey: "CityData")as! NSArray).object(at: 0)as! NSDictionary).value(forKey: "city_id")!)"
            self.callGetAdmin_NumberOrCardSHowHideAPI(strCityID: strCityId)
        }
        
        if(DeviceID.count != 0 && PushToken.count != 0){
            addDevice()
        }
        
        
        
        
        
        self.ary_CollectionData = NSMutableArray()
        
        
    }
    override func motionEnded(_ motion: UIEvent.EventSubtype, with event: UIEvent?) {
        if((nsud.value(forKey: "CitiZenCOP_ShakeHelp")) != nil){
            if(nsud.value(forKey: "CitiZenCOP_ShakeHelp")as! Bool){
                if motion == .motionShake {
                    if nsud.value(forKey: "CitizenCOP_HelpContact") != nil {
                        let aryContactList = (nsud.value(forKey: "CitizenCOP_HelpContact")as! NSArray).mutableCopy()as! NSMutableArray
                        if(aryContactList.count != 0){
                            let testController = mainStoryboard.instantiateViewController(withIdentifier: "HelpMeVC")as! HelpMeVC
                            self.navigationController?.pushViewController(testController, animated: true)
                        }else{
                            let testController = mainStoryboard.instantiateViewController(withIdentifier: "AddContactVC")as! AddContactVC
                            self.navigationController?.pushViewController(testController, animated: true)
                        }
                        
                    }else{
                        let testController = mainStoryboard.instantiateViewController(withIdentifier: "AddContactVC")as! AddContactVC
                        self.navigationController?.pushViewController(testController, animated: true)
                    }
                }
            }
        }
        
    }
    
    override var canBecomeFirstResponder: Bool {
        get {
            return true
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //MARK:RereshCityData
        getDashBoardDataAPI()
        
        
        super.viewWillAppear(true)
        self.btnBackgroundView.removeFromSuperview()
        self.ary_CollectionData = NSMutableArray()
        self.collection.reloadData()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            
            if(dictCityData.value(forKey: "MobileIcon")as! NSArray).count != 0{
                self.ary_CollectionData = NSMutableArray()
                self.ary_CollectionData = (dictCityData.value(forKey: "MobileIcon")as! NSArray).mutableCopy()as! NSMutableArray
            }
            
            
            //MARK: CollectionViewPageHorizontalLayout
            let pageHorizontalLayout = KSTCollectionViewPageHorizontalLayout();
            pageHorizontalLayout.lineSpacing = 0;
            pageHorizontalLayout.interitemSpacing = 0;
            pageHorizontalLayout.itemSize = CGSize(width: (self.collection.frame.size.width)  / 3, height:(self.collection.frame.size.height)  / 3);
            self.collection.collectionViewLayout = pageHorizontalLayout;
            //   self.collection!.contentInset = UIEdgeInsets(top: 0, left: 0, bottom:0, right: 0)
            self.collection.dataSource = self
            self.collection.delegate = self
            self.collection.reloadData()
            self.collection.isPagingEnabled = true
            self.pageControl.numberOfPages = self.ary_CollectionData.count/9
            if(self.ary_CollectionData.count % 9 != 0){
                self.pageControl.numberOfPages = self.pageControl.numberOfPages + 1
            }
            
        }
        
        //MARK:Advertisment
        addAdvertisment()
        
        //MARK: For Close Group Location & Traking Device
        getCurrentLocation()
        
        //MARK: Sync Report Data
        SyncReportDataVC().getReportData()
        
        //MARK:Notification Recive
        checkNotification()
        
        //MARK:Permission
        
        if(nsud.value(forKey: "CitiZenCOP_Permission") == nil){
            let testController = mainStoryboard.instantiateViewController(withIdentifier: "PermissionVC")as! PermissionVC
            testController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            // self.present(testController, animated: false) {}
            
            
            CameraAndGellaryPermission()
            contactPermission()
            AudioSessionPermission()
            NotificationPermission()
        }
        
        //MARK: Pledge
        imgPledge.layer.cornerRadius = imgPledge.frame.width/2
        imgPledge.clipsToBounds = true
        imgPledge.isHidden = true
        btnIsPledge.isHidden = true
        if(PledgeCheckForCity()){
            let dict  = ary_IsPledge.object(at: 0)as! NSDictionary
            if("\(dict.value(forKey: "Status")!)" == "1"){
                if(nsud.value(forKey: "CitiZenCOP_PledgeStatus") != nil){
                    let strPledgeStatus = nsud.value(forKey: "CitiZenCOP_PledgeStatus") as! String
                    if (strPledgeStatus == "AGREE"){
                        
                        let dateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "dd-MM-yyyy"
                        
                        let FromDate = "\(dict.value(forKey: "FromDate")!)"
                        let ToDate = "\(dict.value(forKey: "ToDate")!)"
                        let currentDate = dateFormatter.string(from: Date())
                        
                        let from_date = dateFormatter.date(from: FromDate)
                        let to_date = dateFormatter.date(from: ToDate)
                        let current_date = dateFormatter.date(from: currentDate)
                        
                        if(current_date! >= from_date! && current_date! <= to_date!){
                            let urlImage = "\(BaseURL2)PledgeIcon/\(dict.value(forKey: "PledgeIcon")!)"
                            imgPledge.setImageWith(URL(string: urlImage), placeholderImage: UIImage(named: ""), options: SDWebImageOptions(rawValue: 1), completed: { (image, error, type, url) in
                                //print(url ?? 0)
                            }, usingActivityIndicatorStyle: .gray)
                            imgPledge.isHidden = false
                            btnIsPledge.isHidden = false
                        }else{
                            imgPledge.isHidden = true
                            btnIsPledge.isHidden = true
                        }
                    }else {
                        imgPledge.isHidden = true
                        btnIsPledge.isHidden = true
                    }
                }
            }
        }
        
        
        
        
        
    }
    
    override func viewWillLayoutSubviews() {
        
    }
    
    @objc func appMovedToBackground() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            let testController = mainStoryboard.instantiateViewController(withIdentifier: "HelpMeTimerVC")as! HelpMeTimerVC
            testController.notiTitle = "HelpMeTimerNoti"
            self.navigationController?.pushViewController(testController, animated: false)
            
        }
    }
    
    //MARK:
    //MARK: checkUpdate
    func checkUpdate() {
        let date = Date()
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd"
        let dateString = df.string(from: date)
        
        nsud.set("\(dateString)", forKey: "VersionCheckDate")
        VersionCheck.shared.checkAppStore() { isNew, version , Oldversion , message in
            // print("Old version = \(Oldversion!)")
            // print("New version = \(version)")
            
            if(isNew != nil){
                if(isNew!){
                    let alert = UIAlertController(title: "New Version Available", message: message, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Update", style: .default, handler: { action in
                        guard let url = URL(string: app_URL) else { return }
                        UIApplication.shared.open(url)
                        
                    }))
                    alert.addAction(UIAlertAction(title: "Skip", style: .default, handler: { action in
                        
                    }))
                    self.present(alert, animated: true, completion: nil)
                }
                
            }
        }
    }
    
    // MARK: - --------------Pull Refresh
    // MARK: -
    func RefreshloadData() {
        
        if(nsud.value(forKey: "VersionCheckDate") != nil){
            let dateStr = "\(nsud.value(forKey: "VersionCheckDate")!)"
            let date = Date()
            let df = DateFormatter()
            df.dateFormat = "yyyy-MM-dd"
            let dateString = df.string(from: date)
            if(dateStr != dateString){
                self.checkUpdate()
            }
        }else{
            self.checkUpdate()
        }
        
        //        if("\(dictCityData.value(forKey: "IsAppUpdate")!)" == "True"){
        //
        //        }
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd"
        let day  = dateFormatter.string(from: date)
        
        if((nsud.value(forKey: "CItizenCOP_PledgeUpdateDate")) != nil){
            if(Int(day) != Int((nsud.value(forKey: "CItizenCOP_PledgeUpdateDate")!)as! String)){
                self.ShowPledgeController()
            }
            else {
                print("Kal Call Hoga-------------")
            }
        }else{
            self.ShowPledgeController()
        }
        imgPledge.layer.cornerRadius = imgPledge.frame.width/2
        imgPledge.clipsToBounds = true
        imgPledge.isHidden = true
        btnIsPledge.isHidden = true
        if(PledgeCheckForCity()){
            let dict  = ary_IsPledge.object(at: 0)as! NSDictionary
            if("\(dict.value(forKey: "Status")!)" == "1"){
                if(nsud.value(forKey: "CitiZenCOP_PledgeStatus") != nil){
                    let strPledgeStatus = nsud.value(forKey: "CitiZenCOP_PledgeStatus") as! String
                    if (strPledgeStatus == "AGREE"){
                        
                        let dateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "dd-MM-yyyy"
                        
                        let FromDate = "\(dict.value(forKey: "FromDate")!)"
                        let ToDate = "\(dict.value(forKey: "ToDate")!)"
                        let currentDate = dateFormatter.string(from: Date())
                        
                        let from_date = dateFormatter.date(from: FromDate)
                        let to_date = dateFormatter.date(from: ToDate)
                        let current_date = dateFormatter.date(from: currentDate)
                        
                        if(current_date! >= from_date! && current_date! <= to_date!){
                            let urlImage = "\(BaseURL2)PledgeIcon/\(dict.value(forKey: "PledgeIcon")!)"
                            imgPledge.setImageWith(URL(string: urlImage), placeholderImage: UIImage(named: ""), options: SDWebImageOptions(rawValue: 1), completed: { (image, error, type, url) in
                                //print(url ?? 0)
                            }, usingActivityIndicatorStyle: .gray)
                            imgPledge.isHidden = false
                            btnIsPledge.isHidden = false
                        }else{
                            imgPledge.isHidden = true
                            btnIsPledge.isHidden = true
                        }
                    }else {
                        imgPledge.isHidden = true
                        btnIsPledge.isHidden = true
                    }
                }
            }
        }
        
        
    }
    
    
    func ShowPledgeController() {
        //Pledge Check
        
        if(PledgeCheckForCity()){
            if(nsud.value(forKey: "CitiZenCOP_PledgeStatus") != nil){
                let dict  = ary_IsPledge.object(at: 0)as! NSDictionary
                
                let strPledgeStatus = nsud.value(forKey: "CitiZenCOP_PledgeStatus") as! String
                if (strPledgeStatus == "AGREE"){
                    let urlImage = "\(BaseURL2)PledgeIcon/\(dict.value(forKey: "PledgeIcon")!)"
                    imgPledge.setImageWith(URL(string: urlImage), placeholderImage: UIImage(named: ""), options: SDWebImageOptions(rawValue: 1), completed: { (image, error, type, url) in
                        //  print(url ?? 0)
                    }, usingActivityIndicatorStyle: .gray)
                }else if (strPledgeStatus == "REMINDER"){
                    let dateFormatter : DateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "dd"
                    let dateString = dateFormatter.string(from: Date())
                    if(dateString != "\(nsud.value(forKey: "REMINDER_Date")!)"){
                        
                        let child = mainStoryboard.instantiateViewController(withIdentifier: "PledgeVC")as! PledgeVC
                        child.delegate = self
                        btnBackgroundView.alpha = 0.5
                        btnBackgroundView.frame = self.view.frame
                        btnBackgroundView.backgroundColor = UIColor.black
                        self.view.addSubview(btnBackgroundView)
                        child.aryPledgeData = self.ary_IsPledge
                        child.frame = CGRect(x: 0, y: view_ForHeader.frame.maxY, width: (self.view.frame.width), height: (self.view.frame.height))
                        child.view.frame = CGRect(x: 0, y: self.view.frame.maxY, width: (self.view.frame.width), height: (self.view.frame.height))
                        self.addChild(child)
                        self.view.addSubview(child.view)
                        child.didMove(toParent: self)
                    }
                    
                }
                
            }else{
                
                
                let child = mainStoryboard.instantiateViewController(withIdentifier: "PledgeVC")as! PledgeVC
                btnBackgroundView.alpha = 0.5
                child.delegate = self
                btnBackgroundView.frame = self.view.frame
                btnBackgroundView.backgroundColor = UIColor.black
                self.view.addSubview(btnBackgroundView)
                child.aryPledgeData = self.ary_IsPledge
                child.frame = CGRect(x: 0, y: view_ForHeader.frame.maxY, width: (self.view.frame.width), height: (self.view.frame.height))
                child.view.frame = CGRect(x: 0, y: self.view.frame.maxY, width: (self.view.frame.width), height: (self.view.frame.height))
                self.addChild(child)
                self.view.addSubview(child.view)
                child.didMove(toParent: self)
                
                
                //                let vc: PledgeVC = self.storyboard!.instantiateViewController(withIdentifier: "PledgeVC") as! PledgeVC
                //                vc.aryPledgeData = self.ary_IsPledge
                //                //                vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                //                //                vc.modalTransitionStyle = .coverVertical
                //                self.present(vc, animated: true, completion: {})
            }
        }
    }
    
    
    // MARK: - -------------Pledge Check
    // MARK: -
    func PledgeCheckForCity() -> Bool {
        if(dictCityData.value(forKey: "PledgeContent")as! NSArray).count != 0{
            ary_IsPledge = NSMutableArray()
            ary_IsPledge  = (dictCityData.value(forKey: "PledgeContent")as! NSArray).mutableCopy()as! NSMutableArray
            if (ary_IsPledge.count != 0){
                return true
                
            }else{
                return false
                //print("Pledge data not found for this City")
            }
        }else{
            return false
        }
    }
    // MARK: - -------------Notification Check
    // MARK: -
    func checkNotification() {
        if(dictNotiData.count != 0){
            let aps = (dictNotiData.value(forKey: "aps")as! NSDictionary)
            let alert = (aps.value(forKey: "alert")as! NSDictionary)
            let strTitle = (alert.value(forKey: "title")!)
            let strbody = (alert.value(forKey: "body")!)
            if(alertStatus == "YES"){
                showAlertWithoutAnyAction(strtitle: strTitle as! String, strMessage: strbody as! String, viewcontrol: self)
            }else{
                let strPushType = "\(dictNotiData.value(forKey: "PushType")!)"
                if(strPushType == "forNews"){
                    let testController = mainStoryboard.instantiateViewController(withIdentifier: "NewsVC")as! NewsVC
                    self.navigationController?.pushViewController(testController, animated: true)
                }else if (strPushType == "forSurvey"){
                    let testController = mainStoryboard.instantiateViewController(withIdentifier: "SurveyVC")as! SurveyVC
                    testController.strSurveyID = "1"
                    self.navigationController?.pushViewController(testController, animated: true)
                }else if (strPushType == "PersonalAlert"){
                    let alert = UIAlertController(title: strTitle as? String, message: strbody as? String, preferredStyle: UIAlertController.Style.alert)
                    
                    alert.addAction(UIAlertAction (title: "OK", style: .default, handler: { (nil) in
                    }))
                    
                    self.present(alert, animated: true, completion: nil)
                }
            }
            
            
            
            dictNotiData = NSMutableDictionary()
        }
    }
    // MARK: - -----------Current Location
    // MARK: -
    func getCurrentLocation() {
        self.locationManager.requestAlwaysAuthorization()
        
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
    }
    
    // MARK: - -------------AddAdvertisment
    // MARK: -
    func addAdvertisment() {
        ary_Advertisement = getDataFromCoreDataBase(strEntity: "AdvertiseMent", strkey: "advertiseMent")
        if(ary_Advertisement.count == 0){
            if(dictCityData.value(forKey: "CityData")as! NSArray).count != 0{
                let strCityId = "\(((dictCityData.value(forKey: "CityData")as! NSArray).object(at: 0)as! NSDictionary).value(forKey: "city_id")!)"
                self.callGetAddAPI(strCityID: strCityId)
                
            }
        }else{
            viewForAdvertisement.backgroundColor = hexStringToUIColor(hex: primaryTheamColor)
            viewForAdvertisement.images =  ary_Advertisement
            viewForAdvertisement.startAutoPlay()
            viewForAdvertisement.delegate = self
            viewForAdvertisement.autoSrcollEnabled = true
            viewForAdvertisement.enableArrowIndicator = false
            viewForAdvertisement.enablePageIndicator = false
            viewForAdvertisement.enableSwipe = true
        }
        
    }
    func addDevice() {
        if !isInternetAvailable() {
            //FTIndicator.showToastMessage(alertInternet)
        }else{
            if(dictCityData.value(forKey: "CityData")as! NSArray).count != 0{
                let strCityId = "\(((dictCityData.value(forKey: "CityData")as! NSArray).object(at: 0)as! NSDictionary).value(forKey: "city_id")!)"
                let strURL = API_Add_device + "&regid=\(PushToken)&imeiNo=\(DeviceID)&cityid=\(strCityId)"
                print(strURL)
                
                
                
                WebServiceClass.callAPIBYPOST_ResponceInString(parameter: NSDictionary(), url: strURL) { (responce, status) in
                    if status == "Suceess"{
                        print("addDevice------\(responce)")
                    }
                    else{
                    }
                }
            }
        }
    }
    
    // MARK: - --------------IBAction
    // MARK: -
    @IBAction func actionOnMenu(_ sender: UIButton) {
        viewForAdvertisement.stopAutoPlay()
        let vc: DrawerVC = self.storyboard!.instantiateViewController(withIdentifier: "DrawerVC") as! DrawerVC
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .coverVertical
        vc.handleDrawerView = self
        self.present(vc, animated: false, completion: {})
    }
    @IBAction func actionOnChangeCity(_ sender: UIButton) {
        viewForAdvertisement.stopAutoPlay()
        let testController = mainStoryboard.instantiateViewController(withIdentifier: "CitySelectionVC")as! CitySelectionVC
        testController.typeComeFrom = "ChangeCity"
        self.navigationController?.pushViewController(testController, animated: true)
    }
    @IBAction func actionOnLightVersion(_ sender: UIButton) {
        if(dictCityData.value(forKey: "CityData")as! NSArray).count != 0{
            
            let isLiteVersion = "\(((dictCityData.value(forKey: "CityData")as! NSArray).object(at: 0)as! NSDictionary).value(forKey: "IsLiteVersion")!)"
            let isLightVersionMessage = "\(((dictCityData.value(forKey: "CityData")as! NSArray).object(at: 0)as! NSDictionary).value(forKey: "LiteMsg")!)"
            let child = mainStoryboard.instantiateViewController(withIdentifier: "BottomSheetVC")as! BottomSheetVC
            child.delegate = self
            btnBackgroundView.alpha = 0.5
            btnBackgroundView.frame = self.view.frame
            btnBackgroundView.backgroundColor = UIColor.black
            self.view.addSubview(btnBackgroundView)
            child.strTitle = isLiteVersion == "1" ? "Lite Version" : "Full Version"
            child.strMessage = isLightVersionMessage
            child.frame = CGRect(x: 0, y: btnIsLightFull.frame.maxY, width: (self.view.frame.width), height: (self.view.frame.height))
            child.view.frame = CGRect(x: 0, y: self.view.frame.maxY, width: (self.view.frame.width), height: (self.view.frame.height))
            self.addChild(child)
            self.view.addSubview(child.view)
            child.didMove(toParent: self)
            
        }
    }
    
    @IBAction func actionOnIsPledge(_ sender: UIButton) {
        if !(imgPledge.isHidden){
            
            let child = mainStoryboard.instantiateViewController(withIdentifier: "PledgeVC")as! PledgeVC
            btnBackgroundView.alpha = 0.5
            child.delegate = self
            btnBackgroundView.frame = self.view.frame
            btnBackgroundView.backgroundColor = UIColor.black
            self.view.addSubview(btnBackgroundView)
            child.aryPledgeData = self.ary_IsPledge
            child.frame = CGRect(x: 0, y: view_ForHeader.frame.maxY, width: (self.view.frame.width), height: (self.view.frame.height))
            child.view.frame = CGRect(x: 0, y: self.view.frame.maxY, width: (self.view.frame.width), height: (self.view.frame.height))
            self.addChild(child)
            self.view.addSubview(child.view)
            child.didMove(toParent: self)
        }
        
    }
    @IBAction func actionOnNotification(_ sender: UIButton) {
        
        let testController = mainStoryboard.instantiateViewController(withIdentifier: "PushNotificationVC")as! PushNotificationVC
        testController.strComeFrom = "DashBoard"
        self.navigationController?.pushViewController(testController, animated: true)
        
    }
    
    //MARK: - ----------Permission
    func CameraAndGellaryPermission() {
        //Camera
        AVCaptureDevice.requestAccess(for: AVMediaType.video) { response in
            if response {
                //access granted
            } else {
                
            }
        }
        
        //Photos
        let photos = PHPhotoLibrary.authorizationStatus()
        if photos == .notDetermined {
            PHPhotoLibrary.requestAuthorization({status in
                if status == .authorized{
                    
                } else {
                    
                }
            })
        }
        
    }
    func contactPermission() {
        CNContactStore().requestAccess(for: .contacts) { (success, error) in
            if success && error == nil {
                
            }
        }
    }
    
    func AudioSessionPermission() {
        switch AVAudioSession.sharedInstance().recordPermission {
        case .granted:
            print("Permission granted")
        case .denied:
            print("Permission denied")
        case .undetermined:
            print("Request permission here")
            AVAudioSession.sharedInstance().requestRecordPermission({ granted in
                // Handle granted
            })
        @unknown default:
            print("Unknown case")
        }
    }
    func NotificationPermission()  {
        let options : UNAuthorizationOptions = [.alert, .sound, .badge, .carPlay]
        UNUserNotificationCenter.current().requestAuthorization(options: options) { (authorized, error) in
            if let err = error{
                print(err.localizedDescription)
            }
            guard authorized else {
                print("Authorization failed")
                DispatchQueue.main.async {
                    let settingsURL = URL(string: UIApplication.openSettingsURLString)
                    UIApplication.shared.open(settingsURL!, options: [:]) { (status) in
                    }
                    
                }
                return
            }
            
            DispatchQueue.main.async {
                UIApplication.shared.registerForRemoteNotifications()
            }
        }
        
    }
    // MARK: - ---------------API CAlling
    // MARK: -
    
    func callGetAdmin_NumberOrCardSHowHideAPI(strCityID : String) {
        if !isInternetAvailable() {
            FTIndicator.showToastMessage(alertInternet)
            
        }else{
            let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><CheckEmailImeiNoExists xmlns='http://m.citizencop.org/'><cityid>\(strCityID)</cityid><ImeiNo>\(DeviceID)</ImeiNo></CheckEmailImeiNoExists></soap12:Body></soap12:Envelope>"
            
            WebServiceClass.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: BaseURL, resultype: "CheckEmailImeiNoExistsResult", responcetype: "CheckEmailImeiNoExistsResponse") { (responce, status) in
                customeDotLoaderRemove()
                if status == "Suceess"{
                    print("----CheckEmailImeiNoExists\(responce)")
                    let dictData = responce.value(forKey: "data") as! NSDictionary
                    nsud.set("\(dictData.value(forKey: "Result")!)", forKey: "CItizenCOP_Admin")
                    nsud.synchronize()
                    let dateFormatter : DateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "dd"
                    let date = Date()
                    let dateString = dateFormatter.string(from: date)
                    nsud.set(dateString, forKey: "CItizenCOP_OneDay")
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            }
            
            
        }
    }
    func getDashBoardDataAPI() {
        if !isInternetAvailable() {
            //  FTIndicator.showToastMessage(alertInternet)
        }else{
            
            let strCityId = "\(((dictCityData.value(forKey: "CityData")as! NSArray).object(at: 0)as! NSDictionary).value(forKey: "city_id")!)"
            self.callGetAdmin_NumberOrCardSHowHideAPI(strCityID: strCityId)
            
            let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><getCityDynamicIconIPhone xmlns='http://m.citizencop.org/'><cityid>\(strCityId)</cityid><ImeiNo>\(DeviceID)</ImeiNo><DeviceType>\(PlatForm)</DeviceType></getCityDynamicIconIPhone></soap12:Body></soap12:Envelope>"
            
            WebServiceClass.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: BaseURL, resultype: "getCityDynamicIconIPhoneResult", responcetype: "getCityDynamicIconIPhoneResponse") { (responce, status) in
                customeDotLoaderRemove()
                if status == "Suceess"{
                    
                    
                    let dictDashboardData = (responce.value(forKey: "data")as! NSDictionary).mutableCopy()as! NSMutableDictionary
                    
                    deleteAllRecords(strEntity:"DashBoardCityData")
                    
                    saveDataInLocalDictionary(strEntity: "DashBoardCityData", strKey: "dashBoardCityData", data: dictDashboardData)
                    
                    
                    dictCityData = NSMutableDictionary()
                    dictCityData = (getDataFromCoreDataBaseDict(strEntity: "DashBoardCityData", strkey: "dashBoardCityData")).mutableCopy()as! NSMutableDictionary
                    
                    
                    self.RefreshloadData()
                    
                    
                }else{
                    //   showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            }
        }
    }
    
    
    
    func callGetAddAPI(strCityID : String) {
        if !isInternetAvailable() {
            FTIndicator.showToastMessage(alertInternet)
            
        }else{
            
            WebServiceClass.callAPIBYGET_XML(parameter: NSDictionary(), url: API_GetAddvertise + strCityID) { (responce, status) in
                self.refresher.endRefreshing()
                if status == "Suceess"{
                    self.ary_Advertisement = NSMutableArray()
                    let xmlData = (responce.value(forKey: "data")as! Data)
                    let xml1 = XMLHash.parse(xmlData)
                    
                    for elem in xml1["NewDataSet"]["Table1"].all {
                        let dict = NSMutableDictionary()
                        
                        dict.setValue("\(elem["advtid"].element!.text)", forKey: "advtid")
                        dict.setValue("\(elem["title"].element!.text)", forKey: "title")
                        dict.setValue("\(elem["url"].element!.text)", forKey: "url")
                        var strUrl = "\(elem["image"].element!.text)"
                        strUrl = strUrl.replacingOccurrences(of: "~", with: "")
                        dict.setValue(strUrl, forKey: "image")
                        
                        dict.setValue("\(elem["NationalStatus"].element!.text)", forKey: "NationalStatus")
                        dict.setValue("\(elem["CreatedDate"].element!.text)", forKey: "CreatedDate")
                        dict.setValue("\(elem["city_id"].element!.text)", forKey: "city_id")
                        self.ary_Advertisement.add(dict)
                    }
                    
                    deleteAllRecords(strEntity:"AdvertiseMent")
                    saveDataInLocalArray(strEntity: "AdvertiseMent", strKey: "advertiseMent", data: self.ary_Advertisement)
                    self.addAdvertisment()
                    
                }
                else{
                    
                    
                }
            }
            
            
        }
    }
    
}

// MARK: - ----------------UICollectionViewDelegate
// MARK: -

extension DashBoardVC : UICollectionViewDelegate ,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout , UIScrollViewDelegate{
    
    private func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.ary_CollectionData.count
    }
    //    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
    //        cell.alpha = 0.4
    //        cell.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
    //        UIView.animate(withDuration: 0.5) {
    //            cell.alpha = 1
    //            cell.transform = .identity
    //        }
    //
    //    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var identifire = ""
        DeviceType.IS_IPHONE_5 ? identifire = "DashBoardCell_iPhone5" : (identifire = "DashBoardCell")
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifire, for: indexPath as IndexPath) as! DashBoardCell
        let dict = ary_CollectionData.object(at: indexPath.row)as! NSDictionary
        cell.DashBoard_lbl_Title.text = "\(dict.value(forKey: "IconTitle")!)"
        let urlImage = "\(BaseURL2)MobileIcon/\(dict.value(forKey: "IconLogo")!)"
        
        cell.DashBoardCell_Image.setImageWith(URL(string: urlImage), placeholderImage: UIImage(named: ""), options: SDWebImageOptions(rawValue: 1), completed: { (image, error, type, url) in
            //   print(url ?? 0)
        }, usingActivityIndicatorStyle: .gray)
        if(dict.value(forKey: "EventName") is String){
            if("\(dict.value(forKey: "EventName")!)" .count != 0){
                cell.DashBoard_lbl_Title.text = "\(dict.value(forKey: "EventName")!)"
            }
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (self.collection.frame.size.width)  / 3, height:(self.collection.frame.size.height)  / 3)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        viewForAdvertisement.stopAutoPlay()
        
        let dict = ary_CollectionData.object(at: indexPath.row)as! NSDictionary
        
        let IconMasterId = "\(dict.value(forKey: "IconMasterId")!)"
        let strTitle = "\(dict.value(forKey: "IconTitle")!)"
        
        print("id ----- ------ ----- ")
        print(IconMasterId)
        
        
        
        print(strTitle)
        //Call Administration\r\n
        if(IconMasterId == "3"){
            let testController = mainStoryboard.instantiateViewController(withIdentifier: "CallAdministrationVC")as! CallAdministrationVC
            testController.strlblTittle = strTitle
            self.navigationController?.pushViewController(testController, animated: true)
            
        }
        //Emergency Calls\r\n
        else if(IconMasterId == "2034"){
            let testController = mainStoryboard.instantiateViewController(withIdentifier: "ReportTraficJamVC")as! ReportTraficJamVC
            testController.strlblTittle = strTitle
            
            self.navigationController?.pushViewController(testController, animated: true)
        }
        
        //Vehicle Search\r\n
        else if(IconMasterId == "7"){
            if !isInternetAvailable() {
                FTIndicator.showToastMessage(alertInternet)
            }else{
                let testController = mainStoryboard.instantiateViewController(withIdentifier: "VechicleSearchVC")as! VechicleSearchVC
                self.navigationController?.pushViewController(testController, animated: true)
            }
        }
        
        //Emergency Calls\r\n
        else if(IconMasterId == "15"){
            let testController = mainStoryboard.instantiateViewController(withIdentifier: "EmergencyCallVC")as! EmergencyCallVC
            self.navigationController?.pushViewController(testController, animated: true)
        }
        
        //My Safe Zone\r\n
        else if(IconMasterId == "4"){
            if nsud.value(forKey: "CitizenCOP_HelpContact") != nil {
                let aryContactList = (nsud.value(forKey: "CitizenCOP_HelpContact")as! NSArray).mutableCopy()as! NSMutableArray
                if(aryContactList.count != 0){
                    let testController = mainStoryboard.instantiateViewController(withIdentifier: "MySafeZoneVC")as! MySafeZoneVC
                    self.navigationController?.pushViewController(testController, animated: true)
                }else{
                    let testController = mainStoryboard.instantiateViewController(withIdentifier: "MySafeZoneVC")as! MySafeZoneVC
                    self.navigationController?.pushViewController(testController, animated: true)
                }
            }else{
                let testController = mainStoryboard.instantiateViewController(withIdentifier: "MySafeZoneVC")as! MySafeZoneVC
                self.navigationController?.pushViewController(testController, animated: true)
            }
        }
        
        
        //Help Me !!\r\n
        
        else if(IconMasterId == "5"){
            if nsud.value(forKey: "CitizenCOP_HelpContact") != nil {
                let aryContactList = (nsud.value(forKey: "CitizenCOP_HelpContact")as! NSArray).mutableCopy()as! NSMutableArray
                if(aryContactList.count != 0){
                    let testController = mainStoryboard.instantiateViewController(withIdentifier: "HelpMeVC")as! HelpMeVC
                    self.navigationController?.pushViewController(testController, animated: true)
                }else{
                    let testController = mainStoryboard.instantiateViewController(withIdentifier: "AddContactVC")as! AddContactVC
                    self.navigationController?.pushViewController(testController, animated: true)
                }
            }else{
                let testController = mainStoryboard.instantiateViewController(withIdentifier: "AddContactVC")as! AddContactVC
                self.navigationController?.pushViewController(testController, animated: true)
            }
        }
        
        //Near by Places\r\n
        else if(IconMasterId == "13"){
            let testController = mainStoryboard.instantiateViewController(withIdentifier: "NearByPlaceVC")as! NearByPlaceVC
            self.navigationController?.pushViewController(testController, animated: true)
        }
        //News Updates\r\n
        else if(IconMasterId == "10"){
            let testController = mainStoryboard.instantiateViewController(withIdentifier: "NewsVC")as! NewsVC
            self.navigationController?.pushViewController(testController, animated: true)
        }
        //Report an Incident\r\n
        else if(IconMasterId == "1"){
            let testController = mainStoryboard.instantiateViewController(withIdentifier: "ReportAnIncidentVC")as! ReportAnIncidentVC
            testController.strlblTittle = strTitle.html2String
            self.navigationController?.pushViewController(testController, animated: true)
        }
        
        //"Call Police\r\n
        else if(IconMasterId == "2"){
            let testController = mainStoryboard.instantiateViewController(withIdentifier: "CallPoliceVC")as! CallPoliceVC
            testController.strlblTittle = strTitle.html2String
            self.navigationController?.pushViewController(testController, animated: true)
        }
        
        //Report Lookup
        else if(IconMasterId == "18"){
            let testController = mainStoryboard.instantiateViewController(withIdentifier: "ReportLookupVC")as! ReportLookupVC
            self.navigationController?.pushViewController(testController, animated: true)
        }
        //I Need Help/Support
        else if(IconMasterId == "1034"){
            let testController = mainStoryboard.instantiateViewController(withIdentifier: "INeedHelpSupportVC")as! INeedHelpSupportVC
            testController.strlblTittle = strTitle
            self.navigationController?.pushViewController(testController, animated: true)
        }
        //Pathik
        else if(IconMasterId == "9"){
            let testController = mainStoryboard.instantiateViewController(withIdentifier: "TravelSafeVC")as! TravelSafeVC
            testController.strlblTittle = strTitle.html2String
            
            self.navigationController?.pushViewController(testController, animated: true)
        }
        //Your Reports
        else if(IconMasterId == "17"){
            let testController = mainStoryboard.instantiateViewController(withIdentifier: "ReportListVC")as! ReportListVC
            self.navigationController?.pushViewController(testController, animated: true)
        }
        //Report Lost Article
        else if(IconMasterId == "8"){
            let testController = mainStoryboard.instantiateViewController(withIdentifier: "ReportLostArticleVC")as! ReportLostArticleVC
            self.navigationController?.pushViewController(testController, animated: true)
        }
        
        //My Close Group
        else if(IconMasterId == "12"){
            let testController = mainStoryboard.instantiateViewController(withIdentifier: "CloseGroupVC")as! CloseGroupVC
            self.navigationController?.pushViewController(testController, animated: true)
        }
        
        //Aalamban
        else if(IconMasterId == "24"){
            let testController = mainStoryboard.instantiateViewController(withIdentifier: "HelpMeTimerVC")as! HelpMeTimerVC
            testController.notiTitle = "DashBoard"
            self.navigationController?.pushViewController(testController, animated: true)
        }
        //Fare Calculation
        
        else if(IconMasterId == "11"){
            let testController = mainStoryboard.instantiateViewController(withIdentifier: "FareCalculationVC")as! FareCalculationVC
            self.navigationController?.pushViewController(testController, animated: true)
        }
        
        //Towing Vehicle Search
        else if(IconMasterId == "6"){
            let testController = mainStoryboard.instantiateViewController(withIdentifier: "TowedvehiclesearchVC")as! TowedvehiclesearchVC
            self.navigationController?.pushViewController(testController, animated: true)
        }
        
        //Search Officials
        else if(IconMasterId == "19"){
            let testController = mainStoryboard.instantiateViewController(withIdentifier: "SearchOfficialVC")as! SearchOfficialVC
            testController.strlblTittle = strTitle.html2String
            self.navigationController?.pushViewController(testController, animated: true)
        }
        
        //Verification/Inform Police
        else if(IconMasterId == "14"){
            let testController = mainStoryboard.instantiateViewController(withIdentifier: "VerificationInformPolice")as! VerificationInformPolice
            self.navigationController?.pushViewController(testController, animated: true)
        }
        
        //Track My Location
        else if(IconMasterId == "16"){
            let testController = mainStoryboard.instantiateViewController(withIdentifier: "TrackMyLocationVC")as! TrackMyLocationVC
            self.navigationController?.pushViewController(testController, animated: true)
        }
        
        //Citizen Eye
        else if(IconMasterId == "28"){
            
            if((nsud.value(forKey: "CitizenCOP_CitizenEyeData")) != nil){
                if((nsud.value(forKey: "CitizenCOP_CitizenEyeData")as! NSDictionary).count != 0){
                    let testController = UIStoryboard.init(name: "CitizenEYE", bundle: nil).instantiateViewController(withIdentifier: "CameraHistroyVC")as! CameraHistroyVC
                    testController.strTitle = "History"
                    self.navigationController?.pushViewController(testController, animated: false)
                }else{
                    let testController = UIStoryboard.init(name: "CitizenEYE", bundle: nil).instantiateViewController(withIdentifier: "CitizenEye_LoginVC")as! CitizenEye_LoginVC
                    testController.strTitle = strTitle
                    
                    self.navigationController?.pushViewController(testController, animated: true)
                }
            }else{
                
                let testController = UIStoryboard.init(name: "CitizenEYE", bundle: nil).instantiateViewController(withIdentifier: "CitizenEye_LoginVC")as! CitizenEye_LoginVC
                testController.strTitle = strTitle
                
                self.navigationController?.pushViewController(testController, animated: true)
            }
            
        }
        
        //Find Traffic COP
        else if(IconMasterId == "29"){
            let testController = mainStoryboard.instantiateViewController(withIdentifier: "QRScannerController")as! QRScannerController
            testController.strTitle = strTitle
            testController.optionMenu = IconMasterId
            self.navigationController?.pushViewController(testController, animated: true)
        }
        
        //Know your Cop
        else if (IconMasterId == "3035"){
            let testController = mainStoryboard.instantiateViewController(withIdentifier: "QRScannerController")as! QRScannerController
            testController.strTitle = strTitle
            testController.isKnowYourCop = true
            testController.optionMenu = IconMasterId
            self.navigationController?.pushViewController(testController, animated: true)
        }
        
        //Dhara
        else if (IconMasterId == "3036"){
            let storyBoard : UIStoryboard = UIStoryboard(name: "Dhara", bundle:nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "DharaVC") as! DharaVC
            self.navigationController?.pushViewController(vc, animated: false)
        }
        
        //Online Food Order
        else if(IconMasterId == "33"){
            if(DeviceType.IS_IPAD){
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_CoomingSoon, viewcontrol: self)
                
            }else{
                let alert = UIAlertController(title: strTitle, message: "", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Order Now", style: UIAlertAction.Style.default, handler: { action in
                    let testController = mainStoryboard.instantiateViewController(withIdentifier: "OnlineRashanOrderVC")as! OnlineRashanOrderVC
                    testController.strTitle = strTitle
                    self.navigationController?.pushViewController(testController, animated: true)
                    
                }))
                alert.addAction(UIAlertAction(title: "View Order Detail", style: UIAlertAction.Style.default, handler: { action in
                    
                    let testController = mainStoryboard.instantiateViewController(withIdentifier: "OrderHistoryVC")as! OrderHistoryVC
                    testController.strTitle = strTitle
                    self.navigationController?.pushViewController(testController, animated: true)
                }))
                alert.addAction(UIAlertAction(title: "Close", style: UIAlertAction.Style.cancel, handler: { action in
                }))
                self.present(alert, animated: true, completion: nil)
            }
        }
        
        else if(dict.value(forKey: "EventName") is String){
            if("\(dict.value(forKey: "EventName")!)" .count != 0){
                let testController = mainStoryboard.instantiateViewController(withIdentifier: "WebViewVC")as! WebViewVC
                testController.strTitle = "\(dict.value(forKey: "EventName")!)"
                testController.strURL = "\(dict.value(forKey: "Link")!)"
                self.navigationController?.pushViewController(testController, animated: true)
                
            }else{
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_CoomingSoon, viewcontrol: self)
            }
        }else{
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alert_CoomingSoon, viewcontrol: self)
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let x = self.collection.contentOffset.x
        let w = self.collection.bounds.size.width
        let currentPage = Int(ceil(x/w))
        pageControl.currentPage = (currentPage)
    }
}

class DashBoardCell: UICollectionViewCell {
    //DashBoard
    @IBOutlet weak var DashBoardCell_Image: UIImageView!
    @IBOutlet weak var DashBoard_lbl_Title: UILabel!
}

extension DashBoardVC: DrawerScreenDelegate {
    func refreshDrawerScreen(strType: String, tag: Int) {
        if(strType == "Change City"){
            let testController = mainStoryboard.instantiateViewController(withIdentifier: "CitySelectionVC")as! CitySelectionVC
            testController.typeComeFrom = "ChangeCity"
            self.navigationController?.pushViewController(testController, animated: true)
        }else if(strType == "Share"){
            let text = app_ShareText
            let image = UIImage(named: "icon152")
            let myWebsite = NSURL(string:app_URL)
            let shareAll = [text , image! , myWebsite!] as [Any]
            let activityViewController = UIActivityViewController(activityItems: shareAll, applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = self.view
            self.present(activityViewController, animated: true, completion: nil)
            
        }else if(strType == "About"){
            let testController = mainStoryboard.instantiateViewController(withIdentifier: "WebViewVC")as! WebViewVC
            testController.strTitle = "About CitizenCOP"
            testController.strURL = AboutUsInfoWebView
            self.navigationController?.pushViewController(testController, animated: true)
        }else if(strType == "Developer Info"){
            let testController = mainStoryboard.instantiateViewController(withIdentifier: "WebViewVC")as! WebViewVC
            testController.strTitle = "Developer Info"
            testController.strURL = DeveloperInfoWebView
            self.navigationController?.pushViewController(testController, animated: true)
        } else if(strType == "Disclaimer"){
            let testController = mainStoryboard.instantiateViewController(withIdentifier: "WebViewVC")as! WebViewVC
            testController.strTitle = "Disclaimer"
            testController.strURL = DisclaimerInfoWebView
            self.navigationController?.pushViewController(testController, animated: true)
        } else if(strType == "Usage Tips"){
            let testController = mainStoryboard.instantiateViewController(withIdentifier: "WebViewVC")as! WebViewVC
            testController.strTitle = "Usage Tips"
            testController.strURL = UsageInfoWebView
            self.navigationController?.pushViewController(testController, animated: true)
        }else if(strType == "Permissions"){
            let testController = mainStoryboard.instantiateViewController(withIdentifier: "PermissionVC")as! PermissionVC
            testController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            self.present(testController, animated: false) {}
        }
        else if(strType == "CitizenCOP Video"){
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "PlayerVC") as! PlayerVC
            vc.strURl = "sh-tn53NhUc"
            self.present(vc, animated: true, completion: {})
        }
        
        
        else if(strType == "User Profile"){
            
            
            let vc: UserProfileVC = mainStoryboard.instantiateViewController(withIdentifier: "UserProfileVC") as! UserProfileVC
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.modalTransitionStyle = .coverVertical
            self.present(vc, animated: true) {
                
            }
            
        }
        
    }
    
}
// MARK: - ----------------CPSliderDelegate
// MARK: -
extension DashBoardVC  : CPSliderDelegate{
    func sliderImageIndex(slider: CPImageSlider, index: Int) {
        
    }
    
    
    func sliderImageTapped(slider: CPImageSlider, index: Int) {
        viewForAdvertisement.stopAutoPlay()
        
        let dictPlayer = ary_Advertisement.object(at: index)as! NSDictionary
        var urlImage = ""
        if("\(dictPlayer.value(forKey: "url")!)" != ""){
            urlImage = "\(dictPlayer.value(forKey: "url")!)"
            let safariVC = SFSafariViewController(url: NSURL(string: urlImage)! as URL)
            self.present(safariVC, animated: true, completion: nil)
        }
    }
}
// MARK: --------UNNotification/Messaging Delegate Method----------

extension DashBoardVC : UNUserNotificationCenterDelegate{
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        nsud.setValue("5437788560965635874098656heyfget87", forKey: "CitizenCOP_FCM_Token")
        nsud.synchronize()
    }
    
}
//MARK:-
//MARK:- ---------CLLocationManagerDelegate
extension DashBoardVC : CLLocationManagerDelegate{
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        
        //
        GlobleLat = "\(locValue.latitude)"
        GlobleLong = "\(locValue.longitude)"
        
        // For Group Location
        var strCloseGroupLocationStop = 0
        var strTrackingLocationStop = 0
        
        if(nsud.value(forKey: "CitiZenCOP_JoinedGroupLocation") != nil){
            if(nsud.value(forKey: "CitiZenCOP_JoinedGroupLocation")as! Bool){
                
                SettingVC().callUpdateMemberLocationAPI()
                strCloseGroupLocationStop = 1
            }
        }
        // For Track Location
        
        if(nsud.value(forKey: "CitizenCOP_TrackingStatus") != nil){
            if(nsud.value(forKey: "CitizenCOP_TrackingStatus")as! Bool){
                strTrackingLocationStop = 1
                
                guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
                print("locations = \(locValue.latitude) \(locValue.longitude)")
                if(nsud.value(forKey: "CitizenCOP_TrackingLat") == nil){
                    TrackMyLocationVC().callTrackingLocationAPI()
                    nsud.set(GlobleLat, forKey: "CitizenCOP_TrackingLat")
                    nsud.set(GlobleLong, forKey: "CitizenCOP_TrackingLong")
                    nsud.synchronize()
                }else{
                    let coordinate₀ = CLLocation(latitude: locValue.latitude, longitude: locValue.longitude)
                    let MomentaryLatitudeDesti = Double("\(nsud.value(forKey: "CitizenCOP_TrackingLat")!)")
                    let MomentaryLongitudeDesti = Double("\(nsud.value(forKey: "CitizenCOP_TrackingLong")!)")
                    let coordinate₁ = CLLocation(latitude:Double(MomentaryLatitudeDesti!), longitude: MomentaryLongitudeDesti!)
                    
                    let distanceInMeters = coordinate₀.distance(from: coordinate₁)
                    if(distanceInMeters > 10){
                        print("Tracking my Location.....")
                        GlobleLat = "\(locValue.latitude)"
                        GlobleLong = "\(locValue.longitude)"
                        Globlespeed = "\(manager.location!.speed)"
                        GlobleAcc = "\(manager.location!.horizontalAccuracy)"
                        GlobleAltitude = "\(manager.location!.altitude)"
                        TrackMyLocationVC().callTrackingLocationAPI()
                        nsud.set(GlobleLat, forKey: "CitizenCOP_TrackingLat")
                        nsud.set(GlobleLong, forKey: "CitizenCOP_TrackingLong")
                        nsud.synchronize()
                    }else{
                        print("Distence Error my Location.....")
                        
                    }
                }
            }
        }
        if(strTrackingLocationStop == 0 && strCloseGroupLocationStop == 0){
            if(nsud.value(forKey: "CitizenCOP_MySafeZone") == nil) || (nsud.value(forKey: "CitiZenCOP_JoinedGroupLocation") == nil){
                locationManager.stopUpdatingLocation()
            }
        }
    }
}



//MARK:-
//MARK:- ---------BottomSheetDelegate

extension DashBoardVC : BottomSheetDelegate
{
    func getDataFromBottomSheetDelegate() {
        self.viewWillAppear(true)
    }
    
}
//MARK:-
//MARK:- ---------BottomSheetDelegate

extension DashBoardVC : PledgeDelegate
{
    func getDataFromPledgeDelegate() {
        self.viewWillAppear(true)
        
    }
    
}
