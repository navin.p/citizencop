//
//  VechicleSearchVC.swift
//  CitizenCOP
//
//  Created by Navin Patidar on 5/21/19.
//  Copyright © 2019 CitizenCop. All rights reserved.
//

import UIKit
import SWXMLHash
import StringExtensionHTML
import AEXML

class VechicleSearchVC: UIViewController {
    
    // MARK: - --------------IBOutlet
    // MARK: -
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var tvlist: UITableView!
    @IBOutlet weak var txtNumber: UITextField!
    @IBOutlet weak var lblExample: UILabel!
    @IBOutlet weak var btnState: UIButton!
    @IBOutlet weak var btnType: UIButton!
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var btnTypeHeight: NSLayoutConstraint!
    @IBOutlet weak var viewType: UIView!

    
    // MARK: - --------------Variable
    // MARK: -
    var aryList = NSMutableArray()
    var aryState = NSMutableArray()
    var dictState = NSDictionary()
    // MARK: - --------------Life Cycle
    // MARK: -
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryTheamColor)
        tvlist.tableFooterView = UIView()
        tvlist.estimatedRowHeight = 80.0
        self.txtNumber.placeholder = "Enter Vehicle Number"
        self.lblExample.text = "EX- MP43ME9549"
        self.btnType.setTitle("Vehicle Number", for: .normal)
        self.btnType.tag = 1
        viewType.layer.borderColor = UIColor.lightGray.cgColor
        viewType.layer.borderWidth = 1.0
        self.aryState = getDataFromCoreDataBase(strEntity: "StateForVechileSearch", strkey: "stateForVechileSearch")
        if(self.aryState.count == 0){
           callStateForVehicleSearchAPI()
        }else{
            self.tvlist.reloadData()
        }
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        btnSearch.layer.cornerRadius = 8.0
        btnSearch.backgroundColor = hexStringToUIColor(hex: primaryDark)
    }
    
    // MARK: - ---------------IBAction
    // MARK: -
    @IBAction func actiononBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        
    }
    @IBAction func actiononReload(_ sender: UIButton) {
        callStateForVehicleSearchAPI()
        
    }
    @IBAction func actiononDropDown(_ sender: UIButton) {
        self.PopViewWithArray(tag: 4, aryData: self.aryState, strTitle: "Select State")
        
    }
    
    
    @IBAction func actiononSearch(_ sender: UIButton) {
        self.view.endEditing(true)
       if(btnState.tag == 28){ //Chhattisgarh
              if(txtNumber.text?.count == 0){
                  showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter number!", viewcontrol: self)
              } else{
                  
                  var strType = ""
                  var strResultType = ""
                  var strResponceType = ""
                  var strParameter = ""
                  
                      strType = "getVehicleDataByRegNumber"
                      strResultType = "getVehicleDataByRegNumberResult"
                      strResponceType = "getVehicleDataByRegNumberResponse"
                      strParameter = "Veh_Regn_No"
                 
                  
                  let TransportUrl = "\(dictState.value(forKey: "TransportUrl")!)"
                  // let SoapAction = "\(dictState.value(forKey: "SoapAction")!)"
                  let TargetNameSpace = "\(dictState.value(forKey: "TargetNameSpace")!)"
                  
                  let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><\(strType) xmlns='\(TargetNameSpace)'><\(strParameter)>\(txtNumber.text!)</\(strParameter)></\(strType)></soap12:Body></soap12:Envelope>"
                  callVechileSearchAPI(soapMessage: soapMessage, baseUrl: TransportUrl, resultType: strResultType, responceType: strResponceType)
              }
               }else{
        
                  if(btnState.tag == 0){
                            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please Select State!", viewcontrol: self)
                        } else if(txtNumber.text?.count == 0){
                            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter number!", viewcontrol: self)
                        } else{
                            
                            var strType = ""
                            var strResultType = ""
                            var strResponceType = ""
                            var strParameter = ""
                            
                            if(btnType.tag == 1){
                                strType = "getVehicleDataByRegNumber"
                                strResultType = "getVehicleDataByRegNumberResult"
                                strResponceType = "getVehicleDataByRegNumberResponse"
                                strParameter = "Veh_Regn_No"
                            }else if(btnType.tag == 2){
                                strType = "getVehicleDataByChassisNumber"
                                strResultType = "getVehicleDataByChassisNumberResult"
                                strResponceType = "getVehicleDataByChassisNumberResponse"
                                strParameter = "Veh_Chassis_No"
                            }else if(btnType.tag == 3){
                                strType = "getVehicleDataByEngineNumber"
                                strResultType = "getVehicleDataByEngineNumberResult"
                                strResponceType = "getVehicleDataByEngineNumberResponse"
                                strParameter = "Veh_Engine_No"
                            }
                            
                            let TransportUrl = "\(dictState.value(forKey: "TransportUrl")!)"
                            // let SoapAction = "\(dictState.value(forKey: "SoapAction")!)"
                            let TargetNameSpace = "\(dictState.value(forKey: "TargetNameSpace")!)"
                            
                            let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><\(strType) xmlns='\(TargetNameSpace)'><\(strParameter)>\(txtNumber.text!)</\(strParameter)></\(strType)></soap12:Body></soap12:Envelope>"
                            callVechileSearchAPI(soapMessage: soapMessage, baseUrl: TransportUrl, resultType: strResultType, responceType: strResponceType)
                        }
               }
        
      
        
        
    }
    
    
    
    
    
    
    @IBAction func actiononSelectType(_ sender: UIButton) {
        
        let alertController = UIAlertController(title: "Search Vehicle by", message: "Please Choose a type", preferredStyle: .actionSheet)
        let action1 = UIAlertAction(title: "Vehicle Number", style: .default) { (action) in
            print("Default is pressed.....")
            self.txtNumber.placeholder = "Enter Vehicle Number"
            self.lblExample.text = "EX- MP43ME9549"
            self.txtNumber.text=""
            self.aryList = NSMutableArray()
            self.tvlist.reloadData()
            self.btnType.setTitle("Vehicle Number", for: .normal)
            self.btnType.tag = 1
        }
        let action2 = UIAlertAction(title: "Chassis Number", style: .default) { (action) in
            print("Cancel is pressed......")
            self.txtNumber.placeholder = "Enter Chassis Number"
            self.lblExample.text = "EX- MD625MF51B1K56772"
            self.txtNumber.text=""
            self.aryList = NSMutableArray()
            self.tvlist.reloadData()
            self.btnType.setTitle("Chassis Number", for: .normal)
            self.btnType.tag = 2
            
        }
        let action3 = UIAlertAction(title: "Engine Number", style: .default) { (action) in
            self.txtNumber.placeholder = "Enter Engine Number"
            self.lblExample.text = "EX- DF5KB1054296"
            self.txtNumber.text=""
            self.aryList = NSMutableArray()
            self.tvlist.reloadData()
            self.btnType.setTitle("Engine Number", for: .normal)
            self.btnType.tag = 3
            
        }
        let action4 = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            print("Destructive is pressed....")
            
        }
        alertController.addAction(action1)
        alertController.addAction(action2)
        alertController.addAction(action3)
        alertController.addAction(action4)
        alertController.popoverPresentationController?.sourceRect = sender.frame
        alertController.popoverPresentationController?.sourceView = self.view
        self.present(alertController, animated: true, completion: nil)
        
        
    }
    
    // MARK: - ---------------API CAlling
    // MARK: -
    func callStateForVehicleSearchAPI() {
        if !isInternetAvailable() {
            FTIndicator.showToastMessage(alertInternet)
        }else{
            customDotLoaderShowOnFull(message: "", controller: self)
            
            WebServiceClass.callAPIBYPOST(parameter: NSDictionary(), url: API_GetState_VehicleSearch) { (responce, status) in
                customeDotLoaderRemove()
                if status == "Suceess"{
                    self.aryState = NSMutableArray()
                    self.aryState = (responce.value(forKey: "data")as! NSArray).mutableCopy()as! NSMutableArray
                    deleteAllRecords(strEntity:"StateForVechileSearch")
                    saveDataInLocalArray(strEntity: "StateForVechileSearch", strKey: "stateForVechileSearch", data: self.aryState)
                }
                else{
                    
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                    
                }
            }
            
            
        }
    }
    func callVechileSearchAPI(soapMessage : String , baseUrl : String , resultType : String , responceType : String)  {
        self.aryList = NSMutableArray()
         self.tvlist.reloadData()
        customDotLoaderShowOnButton(btn: btnSearch, view: self.view, controller: self)
        
        WebServiceClass.callVehicleDataParseAPI(soapMessage: soapMessage, url: baseUrl, resultype: resultType, responcetype: responceType) { (responce, status) in
            customeDotLoaderRemove()
            
           // print(responce)
            if status == "Suceess"{
                self.aryList = NSMutableArray()
                self.aryList = (responce.value(forKey: "data")as! NSMutableArray)
                if(self.aryList.count == 0){
                    
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertDataNotFound, viewcontrol: self)
                }else{
                    
                    
                    if(nsud.value(forKey: "CItizenCOP_Admin") != nil){
                        if("\(nsud.value(forKey: "CItizenCOP_Admin")!)" != "True")
                        {
                           self.aryList.removeLastObject()
                        }
                    }
                    self.tvlist.reloadData()
                }
                
            }else{
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
            }
        }
    }
    
    // MARK: - ---------------Other Function's
    // MARK: -
    func PopViewWithArray(tag: Int , aryData : NSMutableArray , strTitle: String)  {
        let vc: SelectionClass = mainStoryboard.instantiateViewController(withIdentifier: "SelectionClass") as! SelectionClass
        vc.strTitle = strTitle
        vc.strTag = tag
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .coverVertical
        vc.delegate = self
        vc.aryList = aryData
        self.present(vc, animated: true, completion: {})
    }
    
}
//MARK:-
//MARK:- ---------PopUpDelegate

extension VechicleSearchVC : PopUpDelegate
{
    func getDataFromPopupDelegate(dictData: NSDictionary, tag: Int) {
        btnState.setTitle("\(dictData.value(forKey: "state_name")!)", for: .normal)
        btnState.tag = Int("\(dictData.value(forKey: "state_id")!)")!
        if(btnState.tag == 28){ //Chhattisgarh
            btnTypeHeight.constant = 0.0
        }else{
           // btnTypeHeight.constant = 40.0
        }
        dictState = NSDictionary()
        dictState = dictData
    }
}

//MARK:-
//MARK:- ---------UITextFieldDelegate

extension VechicleSearchVC : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if ( textField == txtNumber)
        {
            return txtFiledValidation(textField: textField, string: string, returnOnly: "ALL", limitValue: 25)
        }
            
        else
        {
            return true
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
}


// MARK: - ----------------UITableViewDelegate
// MARK: -

extension VechicleSearchVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return aryList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tvlist.dequeueReusableCell(withIdentifier: "VehicleSearchCell", for: indexPath as IndexPath) as! VehicleSearchCell
        
        
        switch indexPath.row {
        case 0:
            
            cell.lblTitle.text = "Owner Name"
            cell.lblDetail.text = (aryList.object(at: indexPath.row)as! String)
            
        case 1:
            
            cell.lblTitle.text = "Father Name"
            cell.lblDetail.text = (aryList.object(at: indexPath.row)as! String)
            
        case 2:
            
            cell.lblTitle.text = "Address"
            cell.lblDetail.text = (aryList.object(at: indexPath.row)as! String)
            
        case 3:
            
            cell.lblTitle.text = "Vehicle Type"
            cell.lblDetail.text = (aryList.object(at: indexPath.row)as! String)
            
        case 4:
            
            cell.lblTitle.text = "Manufacturer"
            cell.lblDetail.text = (aryList.object(at: indexPath.row)as! String)
            
        case 5:
            
            cell.lblTitle.text = "Model"
            cell.lblDetail.text = (aryList.object(at: indexPath.row)as! String)
            
        case 6:
            
            cell.lblTitle.text = "Manufacture year"
            cell.lblDetail.text = (aryList.object(at: indexPath.row)as! String)
            
        case 7:
            
            cell.lblTitle.text = "Color"
            cell.lblDetail.text = (aryList.object(at: indexPath.row)as! String)
            
        case 8:
            
            cell.lblTitle.text = "Chasis No"
            cell.lblDetail.text = (aryList.object(at: indexPath.row)as! String)
            
        case 9:
            
            cell.lblTitle.text = "Engine No"
            cell.lblDetail.text = (aryList.object(at: indexPath.row)as! String)
            
        case 10:
            
            cell.lblTitle.text = "Mobile No"
            cell.lblDetail.text = (aryList.object(at: indexPath.row)as! String)
            
        default:
            
            break
        }
        
        return cell
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if(aryList.count != 0){
            
        }
        
    }
}

// MARK: - ----------------VehicleSearchCell
// MARK: -
class VehicleSearchCell: UITableViewCell {
    //VehicleSearchCell
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDetail: UILabel!
    
    var attrs = [
        NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14.0),
        NSAttributedString.Key.foregroundColor : hexStringToUIColor(hex: "287AFF"),
        NSAttributedString.Key.underlineStyle : 1] as [NSAttributedString.Key : Any] as [NSAttributedString.Key : Any]
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}
