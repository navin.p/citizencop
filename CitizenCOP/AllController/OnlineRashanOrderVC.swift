//
//  OnlineRashanOrderVC.swift
//  CitizenCOP
//
//  Created by Navin Patidar on 4/14/20.
//  Copyright © 2020 CitizenCop. All rights reserved.
//

import UIKit

class OnlineRashanOrderVC: UIViewController {
    
    // MARK: - ----IBOutlet
    @IBOutlet weak var view_ForHeader: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var imgCategory: UIImageView!
    @IBOutlet weak var heightCategory: NSLayoutConstraint!
    
    @IBOutlet weak var heightVIewQuantity: NSLayoutConstraint!
    
    @IBOutlet weak var btnOrder: UIButton!
    
    @IBOutlet weak var lblQuantity: UILabel!
    
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblDetail: UILabel!
    
    @IBOutlet weak var lblMinimumNote: UILabel!
    @IBOutlet weak var txtCategory: UITextField!
    @IBOutlet weak var stepper: UIStepper!
    
    
    // MARK:
    // MARK: - ----Variable
    var strTitle = String()
    var aryCategory = NSMutableArray()
    var strPrice = ""
    var minValue = 0
    var MaxValue = 0
    
    // MARK:
    // MARK: - ----Lifew Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        lblTitle.text = strTitle
        view_ForHeader.backgroundColor = hexStringToUIColor(hex: primaryTheamColor)
        heightCategory.constant = 0.0
        heightVIewQuantity.constant = 0.0
        self.callFoodCategoryAPI(sender: 0)
        txtCategory.layer.cornerRadius = 8.0
        btnOrder.layer.cornerRadius = 8.0
        lblAmount.layer.cornerRadius = 6.0
        lblMinimumNote.layer.cornerRadius = 6.0
        
        txtCategory.layer.borderColor = UIColor.lightGray.cgColor
        txtCategory.layer.borderWidth = 1.0
        btnOrder.backgroundColor = hexStringToUIColor(hex: primaryDark)
        btnOrder.isHidden = true
    }
    // MARK: - ---------------IBAction
    // MARK: -
    @IBAction func actiononBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionOnorder(_ sender: Any) {
        if(lblAmount.text != "" && lblAmount.text != "-"){
            let testController = mainStoryboard.instantiateViewController(withIdentifier: "OrderDetailVC")as! OrderDetailVC
            testController.strTitle = strTitle
            testController.strImage = imgCategory.image!
            let dict = NSMutableDictionary()
            dict.setValue(lblQuantity.text, forKey: "QTY")
            dict.setValue(lblAmount.text, forKey: "AMT")
            dict.setValue(lblDetail.text, forKey: "Detail")
            dict.setValue(txtCategory.text, forKey: "Category")
            testController.DictData = dict
            self.navigationController?.pushViewController(testController, animated: true)
        }
    }
    
    @IBAction func actionOnQuantity(_ sender: UIStepper) {
        let strQTY = Int("\(sender.value)".replacingOccurrences(of: ".0", with: ""))
        lblQuantity.text = "\(strQTY!)"
        let strPrie = Int("\(String(describing: strPrice))")
        lblAmount.text = "\u{20B9} \(strQTY! * strPrie!)"
    }
    
    @IBAction func actionOnSelect(_ sender: Any) {
        
        if(aryCategory.count != 0){
            self.PopViewWithArray(tag: 13, aryData: self.aryCategory, strTitle: "Select Category")
            
        }else{
            self.callFoodCategoryAPI(sender: 1)
        }
        
    }
    
    
    //MARK: Other Function's
    func PopViewWithArray(tag: Int , aryData : NSMutableArray , strTitle: String)  {
        let vc: SelectionClass = mainStoryboard.instantiateViewController(withIdentifier: "SelectionClass") as! SelectionClass
        vc.strTitle = strTitle
        vc.strTag = tag
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .coverVertical
        vc.delegate = self
        vc.aryList = aryData
        self.present(vc, animated: true, completion: {})
    }
    //  MARK: API CAlling
    
    func callFoodCategoryAPI(sender : Int) {
        if !isInternetAvailable() {
            FTIndicator.showToastMessage(alertInternet)
        }else{
            let loader = loader_Show(controller: self, strMessage: "", title: "Please wait...", style: .alert)
            let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><GetGroceryCategoryNew xmlns='http://m.citizencop.org/'></GetGroceryCategoryNew></soap12:Body></soap12:Envelope>"
            
            WebServiceClass.callSOAP_APIBY_GETHELP(soapMessage: soapMessage, url: BaseURL3, resultype: "GetGroceryCategoryNewResult", responcetype: "GetGroceryCategoryNewResponse") { (responce, status) in
                loader.dismiss(animated: false, completion: nil)
                if status == "Suceess"{
                    let dictTemp = (responce.value(forKey: "data")as! NSDictionary).mutableCopy()as! NSMutableDictionary
                    if("\(dictTemp.value(forKey: "Result")!)" == "True"){
                        self.aryCategory = NSMutableArray()
                        self.aryCategory = (dictTemp.value(forKey: "Category")as! NSArray).mutableCopy()as! NSMutableArray
                        if(sender == 1){
                            self.PopViewWithArray(tag: 13, aryData: self.aryCategory, strTitle: "Select Category")
                        }
                    }else{
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                        
                    }
                    
                }else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
                }
            }
        }
    }
}


//MARK:-
//MARK:- ---------PopUpDelegate

extension OnlineRashanOrderVC : PopUpDelegate
{
    func getDataFromPopupDelegate(dictData: NSDictionary, tag: Int) {
        
        let strcategory = "\(dictData.value(forKey: "Category")!)"
        let strStatus = "\(dictData.value(forKey: "IsDisabled")!)"
        let strMessage = "\(dictData.value(forKey: "Description")!)"
        if(strStatus == "True"){
            if(strcategory == "Kirana Mang Parchi"){
                       txtCategory.text = strcategory
                       let testController = mainStoryboard.instantiateViewController(withIdentifier: "KiranaMangParchiVC")as! KiranaMangParchiVC
                       testController.strTitle = strcategory
                       self.navigationController?.pushViewController(testController, animated: true)
            }else{
                txtCategory.text = strcategory
                btnOrder.isHidden = false
                strPrice = "\(dictData.value(forKey: "Price")!)"
                let strimg = "\(dictData.value(forKey: "RashanImage")!)"
                let strMinValue = "\(dictData.value(forKey: "MinLimit")!)" == "" ? "0" : "\(dictData.value(forKey: "MinLimit")!)"
                let strMaxValue = "\(dictData.value(forKey: "MaxLimit")!)" == "" ? "0" : "\(dictData.value(forKey: "MaxLimit")!)"
                
                imgCategory.setImageWith(URL(string: strimg), placeholderImage: UIImage(named: ""), options: SDWebImageOptions(rawValue: 1), completed: { (image, error, type, url) in
                }, usingActivityIndicatorStyle: .gray)
                heightCategory.constant = 150.0
                heightVIewQuantity.constant = 110.0
                lblMinimumNote.text = "*न्यूनतम आर्डर संख्या \(strMinValue) बैग।*"
                lblDetail.text = "\(dictData.value(forKey: "Description")!)"
                stepper.maximumValue = (strMaxValue as NSString).doubleValue
                stepper.minimumValue = (strMinValue as NSString).doubleValue
                lblQuantity.text = "\(strMinValue)"
                let strPrie = Int("\(String(describing: strPrice))")
                lblAmount.text = "\u{20B9} \(Int(strMinValue)! * strPrie!)"
            }
        }else{
            let strimg = "\(dictData.value(forKey: "RashanImage")!)"
            txtCategory.text = strcategory
            imgCategory.setImageWith(URL(string: strimg), placeholderImage: UIImage(named: ""), options: SDWebImageOptions(rawValue: 1), completed: { (image, error, type, url) in
            }, usingActivityIndicatorStyle: .gray)
            heightCategory.constant = 150.0
            
            let alert = UIAlertController(title: alertMessage, message: strMessage, preferredStyle: .alert)
                                alert.addAction(UIAlertAction(title: "ok", style: .default, handler: { action in
            }))
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
              self.present(alert, animated: true, completion: nil)
            }
        }
       
    }
}
