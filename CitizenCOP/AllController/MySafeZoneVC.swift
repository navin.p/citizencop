//
//  MySafeZoneVC.swift
//  CitizenCOP
//
//  Created by Navin Patidar on 10/23/19.
//  Copyright © 2019 CitizenCop. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
class MySafeZoneVC: UIViewController , UIGestureRecognizerDelegate{
    // MARK: - --------------IBOutlet
    // MARK: -
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var lblRadius: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet var viewSelectRadius: UIView!
    @IBOutlet weak var picker: UIPickerView!
    @IBOutlet weak var btnErase: UIButton!
    
    
    
    var strLatSafeZone = 0.0
    var strLongSafeZone = 0.0
    var strAddress = ""
    var radius = 3000.0
    var dictZoneData = NSMutableDictionary()
    var arrayList = [String]()
    
    // MARK: - --------------Variable
    // MARK: -
    var locationManager: CLLocationManager = CLLocationManager()
    
    
    // MARK: - --------------Life Cycle
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        arrayList = ["2 KM","4 KM","6 KM","8 KM","10 KM","12 KM","14 KM","16 KM","18 KM","20 KM","22 KM","24 KM","26 KM","28 KM","30 KM"]
        
        btnSave.backgroundColor = hexStringToUIColor(hex: primaryDark)
        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryTheamColor)
        setMapview()
        if(nsud.value(forKey: "CitizenCOP_MySafeZone") != nil){
            dictZoneData = (nsud.value(forKey: "CitizenCOP_MySafeZone")as! NSDictionary).mutableCopy()as! NSMutableDictionary
            
            strLatSafeZone = dictZoneData.value(forKey: "ZoneLat")as! Double
            strLongSafeZone = dictZoneData.value(forKey: "ZoneLong")as! Double
            radius = dictZoneData.value(forKey: "ZoneRadius")as! Double
            
            
            lblRadius.text = "Radius = \(radius/1000) KM"
            btnErase.isHidden = false
            
            let MomentaryLatitudeDesti = Double(strLatSafeZone)
            let MomentaryLongitudeDesti = Double(strLongSafeZone)
            let coordinate₁ = CLLocation(latitude: MomentaryLatitudeDesti, longitude: MomentaryLongitudeDesti)
            self.addRadiusCircle(location: coordinate₁, radius: radius)
            btnSave.setTitle("UPDATE", for: .normal)
        }else{
            btnErase.isHidden = true
            btnSave.setTitle("SAVE", for: .normal)
            
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        isAuthorizedtoGetUserLocation()
        getCurrentLocation()
        
        
    }
    // MARK: - ---------------EXTRA
    // MARK: -
    
    func getCurrentLocation()
    {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                showLocationAccessAlertAction(viewcontrol: self)
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
                locationManager.startUpdatingLocation()
            @unknown default:
                break
            }
        } else {
            print("Location services are not enabled")
        }
    }
    
    
    func requestLocationAccess() {
        let status = CLLocationManager.authorizationStatus()
        
        switch status {
        case .authorizedAlways, .authorizedWhenInUse:
            return
            
        case .denied, .restricted:
            print("location access denied")
            
        default:
            locationManager.requestWhenInUseAuthorization()
        }
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        if(nsud.value(forKey: "CitizenCOP_MySafeZone") == nil) || (nsud.value(forKey: "CitiZenCOP_JoinedGroupLocation") == nil){
            locationManager.stopUpdatingLocation()
        }
        print("Location Stop")
    }
    func addRadiusCircle(location: CLLocation ,radius : Double){
        getAddressFromLatLon(pdblLatitude: "\(location.coordinate.latitude)", withLongitude: "\(location.coordinate.longitude)")
        self.mapView.removeOverlays(mapView.overlays)
        self.mapView.removeAnnotations(mapView.annotations)
        self.mapView.delegate = self
        let circle = MKCircle(center: location.coordinate, radius: CLLocationDistance(radius))
        self.mapView.addOverlay(circle)
        let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
        
        //set region on the map
        mapView.setRegion(region, animated: true)
        let newPin = MKPointAnnotation()
        
        newPin.coordinate = location.coordinate
        mapView.addAnnotation(newPin)
        self.mapView.showAnnotations(mapView.annotations, animated: true)
        let viewRegion = MKCoordinateRegion(center: location.coordinate, latitudinalMeters: radius*2, longitudinalMeters: radius*2)
        self.mapView.setRegion(viewRegion, animated: true)//
        
        
    }
    
    func isAuthorizedtoGetUserLocation() {
        if CLLocationManager.authorizationStatus() != .authorizedWhenInUse     {
            locationManager.requestWhenInUseAuthorization()
        }
    }
    func setMapview(){
        let lpgr = UILongPressGestureRecognizer(target: self, action: #selector(self.handleLongPress(gestureReconizer:)))
        lpgr.minimumPressDuration = 0.5
        lpgr.delaysTouchesBegan = true
        lpgr.delegate = self
        self.mapView.addGestureRecognizer(lpgr)
    }
    // MARK: - ---------------IBAction
    // MARK: -
    
    @IBAction func actionOnPickerOk(_ sender: UIButton) {
        viewSelectRadius.removeFromSuperview()
        
    }
    
    @IBAction func actionOnPickerClose(_ sender: UIButton) {
        viewSelectRadius.removeFromSuperview()
        
        
    }
    @IBAction func actionEdit(_ sender: UIButton) {
        self.viewSelectRadius.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        self.view.addSubview(self.viewSelectRadius)
    }
    @IBAction func actionErase(_ sender: UIButton) {
        nsud.set(nil, forKey: "CitizenCOP_MySafeZone")
        strLatSafeZone = 0.0
        strLongSafeZone = 0.0
        strAddress = ""
        radius = 3000.0
        locationManager.startUpdatingLocation()
        btnSave.setTitle("SAVE", for: .normal)
        btnErase.isHidden = true
        
    }
    @IBAction func actiononBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actiononSave(_ sender: UIButton) {
        btnErase.isHidden = false
        let dictSafeZoneData = NSMutableDictionary()
        dictSafeZoneData.setValue(strLatSafeZone, forKey: "ZoneLat")
        dictSafeZoneData.setValue(strLongSafeZone, forKey: "ZoneLong")
        dictSafeZoneData.setValue(radius, forKey: "ZoneRadius")
        dictSafeZoneData.setValue(strAddress, forKey: "ZoneAddress")
        nsud.setValue(dictSafeZoneData, forKey: "CitizenCOP_MySafeZone")
        showAlertWithoutAnyAction(strtitle: "", strMessage: "Your safezone marked successfully.", viewcontrol: self)
        btnSave.setTitle("UPDATE", for: .normal)
        
    }
    @IBAction func action_Help(_ sender: Any) {
        self.view.endEditing(true)
        let testController = mainStoryboard.instantiateViewController(withIdentifier: "HelpVC")as! HelpVC
        testController.strMessage = alertMYSafeZoneHelp
        self.navigationController?.pushViewController(testController, animated: true)
    }
    
    @objc func handleLongPress(gestureReconizer: UILongPressGestureRecognizer) {
        if gestureReconizer.state != UIGestureRecognizer.State.ended {
            let touchLocation = gestureReconizer.location(in: mapView)
            let locationCoordinate = mapView.convert(touchLocation,toCoordinateFrom: mapView)
            print("Tapped at lat: \(locationCoordinate.latitude) long: \(locationCoordinate.longitude)")
            strLatSafeZone = Double(locationCoordinate.latitude)
            strLongSafeZone = Double(locationCoordinate.longitude)
            let coordinate₁ = CLLocation(latitude: strLatSafeZone, longitude: strLongSafeZone)
            self.addRadiusCircle(location: coordinate₁, radius: radius)
            return
        }
        if gestureReconizer.state != UIGestureRecognizer.State.began {
            return
        }
    }
    
    func getAddressFromLatLon(pdblLatitude: String, withLongitude pdblLongitude: String) {
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        let lat: Double = Double("\(pdblLatitude)")!
        //21.228124
        let lon: Double = Double("\(pdblLongitude)")!
        //72.833770
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = lat
        center.longitude = lon
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                if (error != nil)
                {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                    return
                }
                let pm = placemarks! as [CLPlacemark]
                if pm.count > 0 {
                    let pm = placemarks![0]
                    
                    var addressString : String = ""
                    if pm.subLocality != nil {
                        addressString = addressString + pm.subLocality! + ", "
                    }
                    if pm.thoroughfare != nil {
                        addressString = addressString + pm.thoroughfare! + ", "
                    }
                    if pm.locality != nil {
                        addressString = addressString + pm.locality! + ", "
                    }
                    if pm.country != nil {
                        addressString = addressString + pm.country! + ", "
                    }
                    if pm.postalCode != nil {
                        addressString = addressString + pm.postalCode! + " "
                    }
                    self.lblAddress.text = addressString
                    //                       self.strLat = "\(pdblLatitude)"
                    //                       self.strLong = "\(pdblLongitude)"
                    print(addressString)
                }
        })
    }
}


// MARK: - ---------------MKMapViewDelegate
// MARK: -

extension MySafeZoneVC: MKMapViewDelegate ,CLLocationManagerDelegate  {
    
    /*  func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView?
     {
     if !(annotation is MKPointAnnotation) {
     return nil
     }
     let annotationIdentifier = "AnnotationIdentifier"
     var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: annotationIdentifier)
     if annotationView == nil {
     annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: annotationIdentifier)
     annotationView!.canShowCallout = true
     }
     else {
     annotationView!.annotation = annotation
     }
     print("\(String(describing: annotation.subtitle!))")
     annotationView!.image = UIImage(named: "help-me-blank")
     return annotationView
     
     }*/
    
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        if overlay is MKCircle {
            let circle = MKCircleRenderer(overlay: overlay)
            circle.strokeColor = hexStringToUIColor(hex: primaryDark)
            circle.fillColor = UIColor(red: 255, green: 0, blue: 0, alpha: 0.1)
            circle.lineWidth = 1
            return circle
        } else {
            return MKPolylineRenderer()
        }
    }
    
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if(strLatSafeZone == 0.0){
            radius = 3000.0
            strLatSafeZone = Double(locations.last!.coordinate.latitude)
            strLongSafeZone = Double(locations.last!.coordinate.longitude)
            self.addRadiusCircle(location: locations.last!, radius: radius)
        }
        self.locationManager.stopUpdatingLocation()
    }
}




//MARK: - Pickerview method
extension MySafeZoneVC: UIPickerViewDelegate , UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrayList.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arrayList[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        var strValue = arrayList[row]
        strValue = strValue.replacingOccurrences(of: " KM", with: "")
        radius = Double(Int(strValue)! * 1000)
        let coordinate₁ = CLLocation(latitude: strLatSafeZone, longitude: strLongSafeZone)
        self.addRadiusCircle(location: coordinate₁, radius: radius)
        lblRadius.text = "Radius = \(radius/1000) KM"
    }
}
