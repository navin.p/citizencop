//
//  DharaVC.swift
//  CitizenCOP
//
//  Created by Sourabh Khare  on 7/3/24.
//

import UIKit

class DharaVC: UIViewController {

    @IBOutlet weak var topBar: UIView!
    @IBOutlet weak var imgView1: UIImageView!
    @IBOutlet weak var imgVIew2: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.intialSetup()
    }
    
    func intialSetup(){
        topBar.backgroundColor = hexStringToUIColor(hex: primaryTheamColor)
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(cellTappedMethod(_:)))
        imgView1.isUserInteractionEnabled = true
        imgView1.addGestureRecognizer(tapGestureRecognizer)
        let tapGestureRecognizer1 = UITapGestureRecognizer(target: self, action: #selector(cellTappedMethod1(_:)))
        imgVIew2.isUserInteractionEnabled = true
        imgVIew2.addGestureRecognizer(tapGestureRecognizer1)
    }
    
    @objc func cellTappedMethod(_ sender:AnyObject){
        let storyBoard : UIStoryboard = UIStoryboard(name: "Dhara", bundle:nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "DharaListVC") as! DharaListVC
        vc.strSectionType = "IPC"
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    @objc func cellTappedMethod1(_ sender:AnyObject){
         print("you tap imgVIew2 number: \(sender.view.tag)")
        let storyBoard : UIStoryboard = UIStoryboard(name: "Dhara", bundle:nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "DharaListVC") as! DharaListVC
        vc.strSectionType = "CRPC"
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    @IBAction func actionOnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: false)
    }
    
}
