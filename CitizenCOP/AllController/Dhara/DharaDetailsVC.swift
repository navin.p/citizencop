//
//  DharaDetailsVC.swift
//  CitizenCOP
//
//  Created by Sourabh Khare  on 7/4/24.
//

import UIKit

class DharaDetailsVC: UIViewController {
    
    @IBOutlet weak var tblViiew: UITableView!
    @IBOutlet weak var topBar: UIView!
    
    var arrayList = NSMutableArray()
    var isHindi = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        topBar.backgroundColor = hexStringToUIColor(hex: primaryTheamColor)
        self.tblViiew.delegate = self
        self.tblViiew.dataSource  = self
    }
    
    
    @IBAction func actionOnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func actionOnTranslate(_ sender: UIButton) {
        if isHindi == false {
            isHindi = true
        }else{
            isHindi = false
        }
        tblViiew.reloadData()
    }
    
}

extension DharaDetailsVC : UITableViewDataSource , UITableViewDelegate {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DharaDetailsCell", for: indexPath) as? DharaDetailsCell
        let dict = removeNullFromDict(dict: (arrayList.object(at: indexPath.row)as! NSDictionary).mutableCopy() as! NSMutableDictionary)
        if isHindi == false {
            cell?.lblDhara.text = "  Old Dhara - \(dict.value(forKey: "OldSection") ?? "")"
            cell?.lblDharaNew.text = "  New Dhara - \(dict.value(forKey: "NewSection") ?? "")"
            cell?.lblDharaOldName.text = "\(dict.value(forKey: "OldSectionDescription") ?? "")"
            cell?.lblDharaNewName.text = "\(dict.value(forKey: "NewSectionDescription") ?? "")"
        }else{
            cell?.lblDhara.text = "  धारा पहले - \(dict.value(forKey: "OldSection") ?? "")"
            cell?.lblDharaNew.text = "  धारा अब  - \(dict.value(forKey: "NewSection") ?? "")"
            cell?.lblDharaOldName.text = "\(dict.value(forKey: "OldSectionDescriptionHindi") ?? "")"
            cell?.lblDharaNewName.text = "\(dict.value(forKey: "NewSectionDescriptionHindi") ?? "")"
        }
       
        return cell ?? UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
}

class DharaDetailsCell : UITableViewCell {
    
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var lblDhara: UILabel!
    @IBOutlet weak var lblDharaOldName: UILabel!
    @IBOutlet weak var lblDharaNew: UILabel!
    @IBOutlet weak var lblDharaNewName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        bgView.layer.cornerRadius = 10
        bgView.layer.borderWidth = 0.5
        bgView.layer.borderColor = UIColor.red.cgColor
        
        lblDhara.backgroundColor = hexStringToUIColor(hex: primaryTheamColor)
        lblDharaNew.backgroundColor = hexStringToUIColor(hex: primaryTheamColor)
        lblDhara.layer.cornerRadius = 5
        lblDharaNew.layer.cornerRadius = 5
        
        lblDhara.layer.masksToBounds = true
        lblDharaNew.layer.masksToBounds = true
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
}
