//
//  DharaListVC.swift
//  CitizenCOP
//
//  Created by Sourabh Khare  on 7/3/24.
//

import UIKit


class DharaListVC: UIViewController {
    
    @IBOutlet weak var txtKeyWord: UITextField!
    @IBOutlet weak var txtDhara: UITextField!
    @IBOutlet weak var topBar: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    
    var arryDharaList = NSMutableArray()
    var strSectionType = ""
    var strOldSecation = ""
    var arrDharaListTemp = NSMutableArray()
    var arrFilterList = NSMutableArray()
    var dictFilter = NSDictionary()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        topBar.backgroundColor = hexStringToUIColor(hex: primaryTheamColor)
        self.getDharaListApi()
        self.lblTitle.text = strSectionType
    }
    
    func getDharaListApi(){
        
        let dictSendData = NSMutableDictionary()
        
        dictSendData.setValue("0", forKey: "SectionId")
        dictSendData.setValue("0", forKey: "StateId")
        dictSendData.setValue("0", forKey: "CityId")
        dictSendData.setValue("", forKey: "Crime")
        dictSendData.setValue("", forKey: "CrimeHindi")
        dictSendData.setValue("", forKey: "OldSection")
        dictSendData.setValue("", forKey: "OldSectionHindi")
        dictSendData.setValue("", forKey: "NewSection")
        dictSendData.setValue("", forKey: "NewSectionHindi")
        dictSendData.setValue("", forKey: "OldSectionDescription")
        dictSendData.setValue("", forKey: "OldSectionDescriptionHindi")
        dictSendData.setValue("", forKey: "NewSectionDescription")
        dictSendData.setValue("", forKey: "NewSectionDescriptionHindi")
        dictSendData.setValue("", forKey: "IsActive")
        dictSendData.setValue(strSectionType, forKey: "SectionType")
        
        var json = Data()
        var jsonString = NSString()
        if JSONSerialization.isValidJSONObject(dictSendData) {
            json = try! JSONSerialization.data(withJSONObject: dictSendData, options: .prettyPrinted)
            jsonString = String(data: json, encoding: .utf8)! as NSString
        }
        
        let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><GetSectionDetail xmlns='\(BaseHostURL)/'><SectionSearchMdl>\(jsonString)</SectionSearchMdl></GetSectionDetail></soap12:Body></soap12:Envelope>"
        
        customDotLoaderShowOnFull(message: "", controller: self)
    
        WebServiceClass.callSOAP_APIBY_POST(soapMessage: soapMessage, url: BaseURLCitizen, resultype: "GetSectionDetailResult", responcetype: "GetSectionDetailResponse") { (responce, status) in
           customeDotLoaderRemove()
            if status == "Suceess"{
                self.arryDharaList = NSMutableArray()
                self.arryDharaList = (responce.value(forKey: "data")as! NSArray).mutableCopy()as! NSMutableArray
                print(self.arryDharaList)
            }else{
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
            }
        }
    }
    
    func getDharaListTempApi(){
        
        let dictSendData = NSMutableDictionary()
        
        dictSendData.setValue("0", forKey: "SectionId")
        dictSendData.setValue("0", forKey: "StateId")
        dictSendData.setValue("0", forKey: "CityId")
        dictSendData.setValue("", forKey: "Crime")
        dictSendData.setValue("", forKey: "CrimeHindi")
        dictSendData.setValue(strOldSecation, forKey: "OldSection")
        dictSendData.setValue("", forKey: "OldSectionHindi")
        dictSendData.setValue("", forKey: "NewSection")
        dictSendData.setValue("", forKey: "NewSectionHindi")
        dictSendData.setValue("", forKey: "OldSectionDescription")
        dictSendData.setValue("", forKey: "OldSectionDescriptionHindi")
        dictSendData.setValue("", forKey: "NewSectionDescription")
        dictSendData.setValue("", forKey: "NewSectionDescriptionHindi")
        dictSendData.setValue("", forKey: "IsActive")
        dictSendData.setValue(strSectionType, forKey: "SectionType")
        
        var json = Data()
        var jsonString = NSString()
        if JSONSerialization.isValidJSONObject(dictSendData) {
            json = try! JSONSerialization.data(withJSONObject: dictSendData, options: .prettyPrinted)
            jsonString = String(data: json, encoding: .utf8)! as NSString
        }
        
        let soapMessage = "<?xml version='1.0' encoding='utf-8'?><soap12:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap12='http://www.w3.org/2003/05/soap-envelope'><soap12:Body><GetSectionDetail xmlns='\(BaseHostURL)/'><SectionSearchMdl>\(jsonString)</SectionSearchMdl></GetSectionDetail></soap12:Body></soap12:Envelope>"
        
        customDotLoaderShowOnFull(message: "", controller: self)
    
        WebServiceClass.callSOAP_APIBY_POST(soapMessage: soapMessage, url: BaseURLCitizen, resultype: "GetSectionDetailResult", responcetype: "GetSectionDetailResponse") { (responce, status) in
           customeDotLoaderRemove()
            if status == "Suceess"{
                self.arrDharaListTemp = NSMutableArray()
                self.arrDharaListTemp = (responce.value(forKey: "data")as! NSArray).mutableCopy()as! NSMutableArray
                DispatchQueue.main.async {
                    let storyBoard : UIStoryboard = UIStoryboard(name: "Dhara", bundle:nil)
                    let vc = storyBoard.instantiateViewController(withIdentifier: "DharaDetailsVC") as! DharaDetailsVC
                    vc.arrayList = self.arrDharaListTemp
                    self.navigationController?.pushViewController(vc, animated: false)
                }
                
            }else{
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertSomeError, viewcontrol: self)
            }
        }
    }
    
    //MARK: Other Function's
    func PopViewWithArray(tag: Int , aryData : NSMutableArray , strTitle: String)  {
        let vc: SelectionClass = mainStoryboard.instantiateViewController(withIdentifier: "SelectionClass") as! SelectionClass
        vc.strTitle = strTitle
        vc.strTag = tag
        vc.aryList = aryData
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .coverVertical
        vc.isClearBtnEnable = true
        vc.delegate = self
        self.present(vc, animated: true, completion: {})
    }
    

    @IBAction func actionOnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func actionOnDropDown(_ sender: UIButton) {
        self.strOldSecation = ""
        self.PopViewWithArray(tag: 21, aryData: arryDharaList, strTitle: "Dhara")
    }
    
    @IBAction func actionOnSubmit(_ sender: UIButton) {
        arrFilterList = []
        if txtKeyWord.text?.count == 0 {
            self.getDharaListTempApi()
        }else if txtKeyWord.text!.count > 0 && txtDhara.text!.count > 0 {
            
            let dict = dictFilter
            
            if "\(dict.value(forKey: "OldSectionDescription") ?? "")".lowercased().contains(txtKeyWord.text?.lowercased() ?? "") || "\(dict.value(forKey: "NewSectionDescription") ?? "")".lowercased().contains(txtKeyWord.text?.lowercased() ?? "") || "\(dict.value(forKey: "OldSectionDescriptionHindi") ?? "")".lowercased().contains(txtKeyWord.text?.lowercased() ?? "") || "\(dict.value(forKey: "NewSectionDescriptionHindi") ?? "")".lowercased().contains(txtKeyWord.text?.lowercased() ?? "") || "\(dict.value(forKey: "OldSection") ?? "")".lowercased().contains(txtKeyWord.text?.lowercased() ?? "") || "\(dict.value(forKey: "NewSection") ?? "")".lowercased().contains(txtKeyWord.text?.lowercased() ?? "") {
                arrFilterList.add(dict)
            }
            
            if arrFilterList.count > 0 {
                DispatchQueue.main.async {
                    let storyBoard : UIStoryboard = UIStoryboard(name: "Dhara", bundle:nil)
                    let vc = storyBoard.instantiateViewController(withIdentifier: "DharaDetailsVC") as! DharaDetailsVC
                    vc.arrayList = self.arrFilterList
                    self.navigationController?.pushViewController(vc, animated: false)
                }
            }else{
                showAlertWithoutAnyAction(strtitle: alertInfo, strMessage: "No Result Found!", viewcontrol: self)
            }
            
        } else{
            for i in self.arryDharaList {
                let dict = i as? NSDictionary ?? NSDictionary()
                if "\(dict.value(forKey: "OldSectionDescription") ?? "")".lowercased().contains(txtKeyWord.text?.lowercased() ?? "") || "\(dict.value(forKey: "NewSectionDescription") ?? "")".lowercased().contains(txtKeyWord.text?.lowercased() ?? "") || "\(dict.value(forKey: "OldSectionDescriptionHindi") ?? "")".lowercased().contains(txtKeyWord.text?.lowercased() ?? "") || "\(dict.value(forKey: "NewSectionDescriptionHindi") ?? "")".lowercased().contains(txtKeyWord.text?.lowercased() ?? "") || "\(dict.value(forKey: "OldSection") ?? "")".lowercased().contains(txtKeyWord.text?.lowercased() ?? "") || "\(dict.value(forKey: "NewSection") ?? "")".lowercased().contains(txtKeyWord.text?.lowercased() ?? "") {
                    arrFilterList.add(dict)
                }
            }
            DispatchQueue.main.async {
                let storyBoard : UIStoryboard = UIStoryboard(name: "Dhara", bundle:nil)
                let vc = storyBoard.instantiateViewController(withIdentifier: "DharaDetailsVC") as! DharaDetailsVC
                vc.arrayList = self.arrFilterList
                self.navigationController?.pushViewController(vc, animated: false)
            }
        }
    }
}

extension DharaListVC : PopUpDelegate {
    
    func getDataFromPopupDelegate(dictData: NSDictionary, tag: Int) {
        self.dictFilter = dictData
        txtDhara.text = "\(dictData.value(forKey: "OldSection") ?? "")"
        strOldSecation = "\(dictData.value(forKey: "OldSection") ?? "")"
    }
    
    
}
