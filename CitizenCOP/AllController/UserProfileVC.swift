//
//  UserProfileVC.swift
//  CitizenCOP
//
//  Created by Navin Patidar on 9/25/19.
//  Copyright © 2019 CitizenCop. All rights reserved.
//

import UIKit
import SWXMLHash
import StringExtensionHTML
import AEXML


class UserProfileVC: UIViewController {
    // MARK: - --------------IBOutlet
    // MARK: -
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblBloodGroup: UILabel!
    @IBOutlet weak var lblDOB: UILabel!
    
    @IBOutlet weak var lblAddressPosting: UILabel!
    @IBOutlet weak var lblAuthorizedPerson: UILabel!
    @IBOutlet weak var imgSign: UIImageView!
    
    @IBOutlet weak var lblCityPoliceName: UILabel!
    @IBOutlet weak var imgBG: UIImageView!
    
    // MARK: - --------------Varible
    // MARK: -
    
    var dictUserProfileData = NSMutableDictionary()
    
    
    // MARK: - --------------Life Cycle
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        callGetUserDataAPI()
        imgBG.layer.cornerRadius = 10.0
    }
    
    
    
    
    // MARK: - ---------------IBAction
    // MARK: -
    @IBAction func actiononBack(_ sender: UIButton) {
        self.dismiss(animated: true) {
            
        }
    }
    
    // MARK: - ---------------IBAction
    // MARK: -
    func setUpCardData(dict : NSMutableDictionary) {
        lblName.text = "\(dict.value(forKey: "Name")!)"
        lblDOB.text = "DOB: \(dict.value(forKey: "Dob")!)"
        lblBloodGroup.text = "Blood Group: \(dict.value(forKey: "BloodGroup")!)"
        lblAddressPosting.text = "Address: \(dict.value(forKey: "Address")!)"
        lblAuthorizedPerson.text = "(Authorized Signatory)\n\(dict.value(forKey: "NameAuthority")!)\n\(dict.value(forKey: "Designation")!)"
        lblCityPoliceName.text = "\(dict.value(forKey: "CityName")!)"
        
        
        let urlPhotoPath = "\(BaseURL2)CardHolderImage/\(dict.value(forKey: "PhotoPath")!)"
        imgProfile.setImageWith(URL(string: urlPhotoPath), placeholderImage: UIImage(named: "CloseGroupMember"), options: SDWebImageOptions(rawValue: 1), completed: { (image, error, type, url) in
            //print(url ?? 0)
        }, usingActivityIndicatorStyle: .gray)
        
        imgProfile.layer.cornerRadius = 10.0
        imgProfile.layer.borderColor = hexStringToUIColor(hex: primaryTheamColor).cgColor
        imgProfile.layer.borderWidth = 1.0
        
        let urlSignatureImage = "\(BaseURL2)SignatureImage/\(dict.value(forKey: "SignaturePath")!)"
        imgSign.setImageWith(URL(string: urlSignatureImage), placeholderImage: UIImage(named: ""), options: SDWebImageOptions(rawValue: 1), completed: { (image, error, type, url) in
            //print(url ?? 0)
        }, usingActivityIndicatorStyle: .gray)
        
    }
    
    // MARK: - ---------------GetUserData
    // MARK: -
    
    
    func callGetUserDataAPI() {
        if !isInternetAvailable() {
            FTIndicator.showToastMessage(alertInternet)
        }else{
            let strImeiNo = "&imeiNo=\(DeviceID)"
            let strcity_id = "&city_id=\(((dictCityData.value(forKey: "CityData")as! NSArray).object(at: 0)as! NSDictionary).value(forKey: "city_id")!)"
            let strURL = API_CardDetail + strImeiNo + strcity_id
            WebServiceClass.callAPIBYGET_XML(parameter: NSDictionary(), url: strURL) { (responce, status) in
                customeDotLoaderRemove()
                //self.refresher.endRefreshing()
                print(responce)
                if status == "Suceess"{
                    
                    let xmlData = (responce.value(forKey: "data")as! Data)
                    let xml1 = XMLHash.parse(xmlData)
                    if(xml1.all.count != 0){
                        for elem in xml1["NewDataSet"]["Table1"].all {
                            let dict = NSMutableDictionary()
                            dict.setValue("\(elem["CardHolderId"].element!.text)", forKey: "CardHolderId")
                            dict.setValue("\(elem["imeiNo"].element!.text)", forKey: "imeiNo")
                            dict.setValue("\(elem["Name"].element!.text)", forKey: "Name")
                            dict.setValue("\(elem["Address"].element!.text)", forKey: "Address")
                            dict.setValue("\(elem["Dob"].element!.text)", forKey: "Dob")
                            dict.setValue("\(elem["BloodGroup"].element!.text)", forKey: "BloodGroup")
                            dict.setValue("\(elem["PhotoPath"].element!.text)", forKey: "PhotoPath")
                            dict.setValue("\(elem["CityName"].element!.text)", forKey: "CityName")
                            dict.setValue("\(elem["IsActive"].element!.text)", forKey: "IsActive")
                            dict.setValue("\(elem["SignatureId"].element!.text)", forKey: "SignatureId")
                            dict.setValue("\(elem["city_id"].element!.text)", forKey: "city_id")
                            dict.setValue("\(elem["CreatedBy"].element!.text)", forKey: "CreatedBy")
                            dict.setValue("\(elem["CreatedDate"].element!.text)", forKey: "CreatedDate")
                            dict.setValue("\(elem["NameAuthority"].element!.text)", forKey: "NameAuthority")
                            
                            dict.setValue("\(elem["Designation"].element!.text)", forKey: "Designation")
                            
                            dict.setValue("\(elem["SignaturePath"].element!.text)", forKey: "SignaturePath")
                            dict.setValue("\(elem["PinCode"].element!.text)", forKey: "PinCode")
                            dict.setValue("\(elem["StateName"].element!.text)", forKey: "StateName")
                            self.dictUserProfileData = dict
                            self.setUpCardData(dict: self.dictUserProfileData)
                        }
                        
                    }else{
                        
                        let alert = UIAlertController(title: alertMessage, message:alertUserDataNotFound, preferredStyle: UIAlertController.Style.alert)
                        
                        // add the actions (buttons)
                        alert.addAction(UIAlertAction (title: "OK", style: .default, handler: { (nil) in
                            self.dismiss(animated: true) {    
                            }
                        }))
                        self.present(alert, animated: true, completion: nil)
                    }
                    
                    
                }
                else{
                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertComplaintNoNotFound, viewcontrol: self)
                }
            }
        }
    }
    
}
