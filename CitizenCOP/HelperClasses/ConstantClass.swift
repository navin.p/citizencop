//
//  ConstantClass.swift
//  CitizenCOP
//
//  Created by Navin Patidar on 5/17/19.
//  Copyright © 2019 CitizenCop. All rights reserved.
//

import UIKit
import CoreLocation


//MARK:
//MARK: PAYtm

//let GENERATE_CHECKSUM = "http://m.citizencop.org/corona/Checksumcsharp/GenerateChecksum.aspx"
//let VERIFY_CHECKSUM = " http://gcapi.citizencop.org/Checksumcsharp/VerifyChecksum.aspx"
//let MerchantID = "Citize73156154292382"
//let Website = "APPSTAGING"
//let IndustryID = "Retail"
//let ChannelID = "WAP"
//let Transaction_Status  = "https://securegw-stage.paytm.in/order/status"
//let Production_server = "https://securegw-stage.paytm.in/theia/processTransaction"//
//let type_Production_Staging = "Staging"
//let callBackURL = "https://securegw-stage.paytm.in/theia/paytmCallback?ORDER_ID="

//Production
let Transaction_Status  = "https://securegw-stage.paytm.in/order/status"
let GENERATE_CHECKSUM = "http://m.citizencop.org/corona/Checksumcsharp/GenerateChecksumProduction.aspx"
let VERIFY_CHECKSUM = "http://gcapi.citizencop.org/Checksumcsharp/VerifyChecksumProduction.aspx"
let MerchantID = "Citize31131848116084"
let Website = "APPPROD"
let IndustryID = "Retail105"
let ChannelID = "WAP"
let type_Production_Staging = "Production"
let callBackURL = "https://securegw-stage.paytm.in/theia/paytmCallback?ORDER_ID="



//MARK: Primary Uses
var nsud = UserDefaults.standard
let appDelegate = UIApplication.shared.delegate as! AppDelegate
var mainStoryboard : UIStoryboard = UIStoryboard()
var storyboard_name = String()
var GlobleLat = "0.0"
var GlobleLong = "0.0"
var Globlespeed = "0.0"
var GlobleAcc = "0.0"
var GlobleAltitude = "0.0"
var GloblelocationManager = CLLocationManager()
var GlobleTimer = Timer()
var timer = Timer()
var DeviceNumberOffice1 = "9111138857"
var DeviceNumberOffice2 = "9770242375"



//MARK: Globle Variable
var dictCityData = NSMutableDictionary()
var DeviceID = ""
var PushToken = String()
var kMapsAPIKey = "AIzaSyCijU3QtvHL_cruZgyfbNmuErAvVzi_a1A"
var PlatForm = "IPhone"

//MARK: Alert Images
var strInternetImage = UIImage(named:"no-wifi")!
var strDataNotFoundImage = UIImage(named:"notfound")!

//MARK: Primary Color---
var primaryTheamColor = "EC2421"
var primaryDark = "354750"

//MARK: Version Information
var app_Version : String = "6.7.0"
var app_VersionDate : String = "20/May/2024"
var app_VersionSupport : String = "Requires iOS 10.0 or later.Compatible with iPhone&iPad."
var app_URL : String = "https://itunes.apple.com/us/app/citizen-cop/id631959732"
var app_MessageUpdate : String = "Thank you for using CitizenCOP.This release brings bug fixes that improve our product to help you."

var app_ShareText : String = "The Personal Safety Tool for Citizens #CitizenCOP"

//MARK: Common Alert
var AppName = "CitizenCOP"
var alertMessage = "Alert!"
var alertInfo = "Information!"
var alertInternet = "No Internet Connection, try later!"
var alertDataNotFound = "Data is not Available!"
var alertUserDataNotFound = "User Profile is not Available!"
var alertSomeError = "Somthing went wrong please try again!"
var alertCalling = "Your device doesn't support this feature."
var alertLogout = "Are you sure want to logout ?"
var alertRemove = "Are you sure want to remove?"
var alertDelete = "Are you sure want to delete?"
var Alert_Gallery = "Gallery"
var Alert_Camera = "Camera"
var Alert_Preview = "Preview"
var alert_Email = "Email address required!"
var alert_InvalidEmail = "Email address invalid!"
var alert_Mobile_limit = "Mobile number invalid!"
var alert_Mobile = "Mobile number required!"
var alert_Name = "Name is required!"
var alert_Contact = "Contact number is required!"
var alert_Contactinvalid = "Contact number is invalid!"
var alert_ownerName = "Owner Name is required!"
var alert_ShopName = "Shop Name is required!"
var alertCountry = "Country name required!"
var alertState = "State name required!"
var alertCity = "City name required!"

var alertCountryFirst = "Please select country name first!"
var alertStateFirst = "Please select state name first!"
var alertCityFirst = "Please select city name first!"
var alertReachedMax = "Max Limit Exceeded!"

var alertSelectCategory = "Please select category first!"
var alertSelectSubCategory = "Please select sub category first!"

var alertDescription = "Description required!"
var alert_ComplaintNo = "Please enter complaint number!"
var alert_ComplaintNew = "Complaint under review. Please check status shortly"
var alertComplaintNoNotFound = "Complaint number did not match. Please enter a valid Complaint number."
var alert_TravelSafeNo = "Please enter number!"
var alert_Source = "Source Address required!"
var alert_Destination = "Destination Address required!"

var alert_Feedback = "Feedback required!"
var alert_FeedbackSubmit = "Your Feedback Submitted Successfully."
var alert_ReportSubmit = "Your Report Submitted Successfully."
var alert_AddMobileNumber = "Contact number added successfully."


var alert_ReportComplain_Name = "Name required!"
var alert_ReportComplain_FName = "Father's Name required!"
var alert_ReportComplain_Address = "Address required!"
var alert_ReportComplain_Date = "Date required!"
var alert_ReportComplain_Time = "Time required!"
var alert_ReportComplain_Location = "Location required!"
var alert_ReportComplain_ContactNumberinvalid = "Mobile number invalid!"
var alert_ReportComplain_ContactNumber = "Mobile number required!"
var alert_ReportComplain_EmailReEnter = "Please re-enter email ID!"
var alert_ReportComplain_policestationID = "police station id required!!"
var alert_ReportComplain_description = "Please enter description!"

var alert_CloseGroupName = "Group Name required!"
var alert_CloseGroupKey = "Group Key required!"
var alert_CloseNickName = "Nick Name required!"

var alert_FareSource = "Source address required!"
var alert_FareDestination = "Destination address required!"
var alert_FareType = "Vechicle type required!"
var alert_Vechiclenumber = "Vechicle number required!"
var alert_DeviceKey = "Please Enter Device Key."

var alert_CoomingSoon = "Cooming soon in your city...."
var alert_TrafficDuty = "Chart is not Available!"
 var Alert_OTP  = "OTP Required!"
var Alert_PasswordLimit  = "Password should be minimum six digits!"


var alertLightVersion = "Thank you for using CitizenCOP App.\nThis is the Lite version and has almost all the features as in Full version.The Full version is also free and automatically loads up for the cities where the App has support from Local enforcement agencies.!"

var alertHelpSOS = "This option lets you send instant SMS to your friends, family and near ones at the time of any emergency or trouble. To use this option follow these simple steps.\n\n1)Add contacts of your choice. You have a maximum limit of adding four contacts.\n2)Push 'Send SMS' button to send auto generated SMS with your location.\n3)Clicking 'Send SMS' button will ask you to share your location. Click 'Allow' to send your location with the message.\n\nYou can also send Help SMS by enabling 'Shake To Send Help Me' button in 'Settings' option. The option automatically takes you to the 'Help Me' page by simply shaking the device after opening the application."
var alertEmergencyHelp = "Here is a list of all Emergency Numbers of your city."

var alertReportincidenceHelp = "To report any witnessed incidence, capture images, videos or audio of the incidence and click on 'Send Information' button to send the information to police control room.\nYou can even select multiple images by 'Gallery' option displayed here.\nOnce you click on the 'Send Information' button, the application will ask you to share your location, by clicking 'Allow' the application sends your location through GPS."

var alertCallPoliceHelp =  "Here is a list of all important  contact of police officials. To find contact number according to your current location, click the top most - My Location button.\n\nOnce you click this button, it will ask you to enable GPS. Clicking  yes option will take you to GPS settings page of your mobile, click use GPS satellites and find contact details of the nearest police station as per your current location."
var alertCallAdminstrativeHelp =  "Here is a list of all important contacts of Adminstrative officers."
var alertNewsHelp =  "You can check here for important news updates and notification that are published by the police department for general public. It also gives latest news on traffic updates and general notifications from police."
var alertReportLookUpHelp = "Enter complaint number in the space provided and click search button to know the status of your complaint."
var alertTravelSafeHelp = "This feature is to track the authenticity whether the Cab, Auto Rickshaw Driver, Guard are legal and you are in safe hands. You can type in their Identity number and get access to their details and can send the Text Message along with the location."
var alertYourReportHelp = "Here is the list of the complaints that you have reported through the application.\nClick on the desired complaint to proceed further."
var alertYourReportDetailHelp = "Here is the detailed complaint view selected from the previous list.The details include complaint number, date, time and location.You can view the status of your complaint by clicking the status button."
var alertYourReportFeedbackHelp = "Here is the detailed list of previous feedback regarding your complaint."
var alertYourFareCalculation = "Enter your source and destination from map and click on calculate button to calculate Auto rickshaw fare in your city."
var alertTowedvehicle = "You can search for your vehicle details in case it has been Towed by traffic Police. Just enter vehicle number & date and click on SEARCH  button to get the information."

var alertOfficialsearch = "Ensure the identity of police or other  officials with CitizenCOP. To know information about any official, enter their name or employee code and tap Search."

var alertTrackME = "Get a device key by registering at www.gpsnow.in and enter your device key here."


var alertjoinedthegroup = "You have successfully joined the group."
var alertCreatThegroup = "You have successfully creat the group."
var alertRemoveThegroup = "Group renamed Successfully."
var alertlocationsharingservices = "Your location sharing services"
var alertGroupmemberremoved = "Group member removed successfully."
var alertGroupMemberNotFound = "Group member is not Available!"

var alertVerificationInformPoliceHelp = "Verification/Inform police allows users to enter his personal details including name, address, contact number, Email id  and submit them to concerned authority. This feature is  useful for domestic workers, tenants and others. \n\nInformation can be submitted under any of the following categories :- \n\n- Details of domestic servant or helper \n\n- Information of tenants \n\n- Details for character verification Information can be submitted under any of the following categories :- \n\n- Details of domestic servant or helper\n- Information of tenants\n\n- Details for character verification"
var alert_DataSubmite = "Data Submitted Successfully."

var alert_trackingstoped = "Your tracking stopped successfully."
var alert_trackingstart = "Your tracking started successfully."
var alertMYSafeZoneHelp = "Create Geo-Fencing\n\n1.This feature lets fencing of the device for a particular  geographical boundary. By fencing a device you can set a geographical limit for it, and if the device crosses the fenced boundaries a message for its informations sent to the added contacts in the application.\n2. To create  a fence, long press on the desired  point on the Map displayed.You can use Erase options to modify the fencing as per earned.\n3. Click Save to save the fencing created."


//MARK:
//MARK: Google Key
var googleKey = "AIzaSyBegWih-wCmW44PaaIhUYWE7eVtC8hJBX8"


//MARK:
//MARK: ScreenSize&DeviceType
struct ScreenSize{
    static let SCREEN_WIDTH = UIScreen.main.bounds.size.width;
    static let SCREEN_HEIGHT = UIScreen.main.bounds.size.height;
    static let SCREEN_MAX_LENGTH  = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT);
    static let SCREEN_MIN_LENGTH = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT);
}
struct DeviceType{
    
    static let IS_IPHONE_4_OR_LESS  = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH < 480.0
    static let IS_IPHONE_5 = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
    static let IS_IPHONE_6  = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
    static let IS_IPHONE_6P = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
    static let IS_IPHONE_X = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH >= 812.0
    static let IS_IPHONE_XR_XS_MAX = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 896.0
    static let IS_IPAD = UIDevice.current.userInterfaceIdiom == .pad && ScreenSize.SCREEN_MAX_LENGTH >= 1024.0
}
func changeStringDateToGivenFormatWithoutT(strDate: String , strRequiredFormat: String) -> String

{

    let strTimeIn  = strDate

    

    var dateFormatter = DateFormatter()

    dateFormatter.timeZone = NSTimeZone.local

    dateFormatter.dateFormat = "MM/dd/yyyy hh:mm:ss a"

    var date1 = Date()

    var strDateFinal = String()

    

    if dateFormatter.date(from: strTimeIn) != nil

    {

        date1 = dateFormatter.date(from: strTimeIn)!

        dateFormatter.dateFormat = strRequiredFormat

        strDateFinal = dateFormatter.string(from: date1)

        return strDateFinal

    }

    else

    {

        dateFormatter = DateFormatter()

        dateFormatter.timeZone = NSTimeZone.local

        dateFormatter.dateFormat = "MM/dd/yyyy hh:mm a"

    }

    

    if dateFormatter.date(from: strTimeIn) != nil

    {

        date1 = Date()

        strDateFinal = String()

        date1 = dateFormatter.date(from: strTimeIn)!

        dateFormatter.dateFormat = strRequiredFormat

        strDateFinal = dateFormatter.string(from: date1)

        return strDateFinal

    }

    else

    {

        dateFormatter = DateFormatter()

        dateFormatter.timeZone = NSTimeZone.local

        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"

        

    }

    

    

    if dateFormatter.date(from: strTimeIn) != nil

    {

        date1 = Date()

        strDateFinal = String()

        date1 = dateFormatter.date(from: strTimeIn)!

        dateFormatter.dateFormat = strRequiredFormat

        strDateFinal = dateFormatter.string(from: date1)

        return strDateFinal

    }

    else

    {

        dateFormatter = DateFormatter()

        dateFormatter.timeZone = NSTimeZone.local

        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"

    }

    

    

    if dateFormatter.date(from: strTimeIn) != nil

    {

        date1 = Date()

        strDateFinal = String()

        date1 = dateFormatter.date(from: strTimeIn)!

        dateFormatter.dateFormat = strRequiredFormat

        strDateFinal = dateFormatter.string(from: date1)

        return strDateFinal

    }

    else

    {

        

        dateFormatter = DateFormatter()

        dateFormatter.timeZone = NSTimeZone.local

        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"

    }

    

    

    if dateFormatter.date(from: strTimeIn) != nil

    {

        date1 = Date()

        strDateFinal = String()

        date1 = dateFormatter.date(from: strTimeIn)!

        dateFormatter.dateFormat = strRequiredFormat

        strDateFinal = dateFormatter.string(from: date1)

        return strDateFinal

    }

    else

    {

        dateFormatter = DateFormatter()

        dateFormatter.timeZone = NSTimeZone.local

        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SS"

    }

    

    

    if dateFormatter.date(from: strTimeIn) != nil

    {

        date1 = Date()

        strDateFinal = String()

        date1 = dateFormatter.date(from: strTimeIn)!

        dateFormatter.dateFormat = strRequiredFormat

        strDateFinal = dateFormatter.string(from: date1)

        return strDateFinal

    }

    else

    {

        dateFormatter = DateFormatter()

        dateFormatter.timeZone = NSTimeZone.local

        dateFormatter.dateFormat = "MM/dd/yyyy HH:mm:ss"

    }

    

    if dateFormatter.date(from: strTimeIn) != nil

    {

        date1 = Date()

        strDateFinal = String()

        date1 = dateFormatter.date(from: strTimeIn)!

        dateFormatter.dateFormat = strRequiredFormat

        strDateFinal = dateFormatter.string(from: date1)

        return strDateFinal

    }

    else

    {

        dateFormatter = DateFormatter()

        dateFormatter.timeZone = NSTimeZone.local

        dateFormatter.dateFormat = "MM/dd/yyyy"

    }

    if dateFormatter.date(from: strTimeIn) != nil

    {

        date1 = Date()

        strDateFinal = String()

        date1 = dateFormatter.date(from: strTimeIn)!

        dateFormatter.dateFormat = strRequiredFormat

        strDateFinal = dateFormatter.string(from: date1)

        return strDateFinal

    }

    else

    {

        dateFormatter = DateFormatter()

        dateFormatter.timeZone = NSTimeZone.local

        dateFormatter.dateFormat = "MM/dd/yyyy HH:mm"

        //MM/dd/yyyy HH:mm

    }

    

    if dateFormatter.date(from: strTimeIn) != nil

    {

        date1 = Date()

        strDateFinal = String()

        date1 = dateFormatter.date(from: strTimeIn)!

        dateFormatter.dateFormat = strRequiredFormat

        strDateFinal = dateFormatter.string(from: date1)

        return strDateFinal

    }

    else

    {

        dateFormatter = DateFormatter()

        dateFormatter.timeZone = NSTimeZone.local

        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"

        //MM/dd/yyyy HH:mm

    }

    

    if dateFormatter.date(from: strTimeIn) != nil

    {

        date1 = Date()

        strDateFinal = String()

        date1 = dateFormatter.date(from: strTimeIn)!

        dateFormatter.dateFormat = strRequiredFormat

        strDateFinal = dateFormatter.string(from: date1)

        return strDateFinal

    }

    

    return ""

    

}
