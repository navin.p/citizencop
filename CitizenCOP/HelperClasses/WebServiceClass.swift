//
//  WebServiceClass.swift
//  CitizenCOP
//
//  Created by Navin Patidar on 5/17/19.
//  Copyright © 2019 CitizenCop. All rights reserved.
//

import UIKit

import Alamofire
import SWXMLHash
import StringExtensionHTML
import AEXML

var BaseURLTrackLocationKeyREGISTERED :String = "http://www.gpsnow.in/tracking/Login.ashx"



var BaseURLinstagram :String = "https://www.instagram.com/citizencoporg/"
var BaseURLtwitter :String = "https://twitter.com/citizencoporg/"
var BaseURLfacebook :String = "https://www.facebook.com/Citizencop/"
var DeveloperInfoWebView :String = "http://www.citizencop.org/?page_id=1035"
var DisclaimerInfoWebView :String = "http://www.citizencop.org/?page_id=1042"
var AboutUsInfoWebView :String = "http://www.citizencop.org/?page_id=1039"
var UsageInfoWebView :String = "http://www.citizencop.org/?page_id=1054"



var BaseURL :String = "http://m.citizencop.org/citizencop.asmx"
var BaseURL2 :String = "http://m.citizencop.org/"
var BaseURL3 :String = "http://m.citizencop.org/coronawebservice.asmx"

var BaseURL4 :String = "http://mp.citizencop.org/PatrollingWebService.asmx"




var BaseHostURL :String = ""


var API_UploadCameraImage :String = "\(BaseHostURL)/citizenEyeUploadImage.ashx"


var BaseURLCitizenEyestation :String = "\(BaseHostURL)/citizencop.asmx"
var BaseURLCitizenArea :String = "\(BaseHostURL)/citizenEye.asmx"

var BaseURLCitizenEye :String = "\(BaseHostURL)/citizenEye.asmx"
var BaseURLCitizen :String = "\(BaseHostURL)/citizencop.asmx"
var BaseURLTrafficChart :String = "http://preventive.indorepolice.in/trafficattendancewebservice.asmx"
var BaseURLTrackLocationUpdate :String = "\(BaseHostURL)/GPSNowWebService.asmx"

var API_PledgeAgree :String = "\(BaseURL2)CitizenCopAllHandler.ashx?key=Save_NewPledge"
var API_GetState_VehicleSearch :String = "\(BaseURL2)CitizenCopAllHandler.ashx?key=getState_Json_VehicleSearch"
var API_GetLostTheftArticle :String = "\(BaseURL2)CitizenCopAllHandler.ashx?key=getLostTheftArticle"
var API_GroupAvailable :String = "\(BaseURL2)CitizenCopAllHandler.ashx?key=CheckGroupAvailable&ImeiNo="
var API_CreatGroup :String = "\(BaseURL2)CitizenCopAllHandler.ashx?Key=InsertAdminCloseGroup_ForMultiple"

var API_DeleteGroup :String = "\(BaseURL2)CitizenCopAllHandler.ashx?Key=DeleteGroup_ForMultiple"

var API_JoinedGroupList :String = "\(BaseURL2)CitizenCopAllHandler.ashx?Key=GetMemberConnectedWhichAdmin_ForMultiple&ImeiNo="

var API_JoinedGroup :String = "\(BaseURL2)CitizenCopAllHandler.ashx?Key=InsertMembersCloseGroup_ForMultiple"
var API_DeleteJoinedGroup :String = "\(BaseURL2)CitizenCopAllHandler.ashx?key=InsertMembersCloseGroup_ForMultiple"
var API_RenameGroupNameCloseGroup :String = "\(BaseURL2)CitizenCopAllHandler.ashx?key=RenameGroupNameCloseGroup_ForMultiple"
var API_GetCloseGroupMembers :String = "\(BaseURL2)CitizenCopAllHandler.ashx?key=GetCloseGroupMembers_ForMultiple"
var API_GetFare :String = "\(BaseURL2)CitizenCopAllHandler.ashx?key=getFare"
var API_DeleteMemberCloseGroup :String = "\(BaseURL2)CitizenCopAllHandler.ashx?key=DeleteMemberCloseGroup_ForMultiple"
var API_Add_device :String = "\(BaseURL2)CitizenCopAllHandler.ashx?key=deviceReg"
var API_UpdateOnOffSetting :String = "\(BaseURL2)CitizenCopAllHandler.ashx?key=UpdateOnOffSetting_ForMultiple"

var API_UpdateMemberLocation :String = "\(BaseURL2)CitizenCopAllHandler.ashx?key=UpdateMemberLocationCloseGroup_ForMultiple"
var API_SubmitInformationCategory :String = "\(BaseURL2)CitizenCopAllHandler.ashx?&key=SubmitInformationCategory&"
var API_ESurvey :String = "\(BaseURL2)ESurvey.ashx?key=questionList&aid="
var API_CardDetail :String = "\(BaseURL2)CitizenCopAllHandler.ashx?key=CardDetail&"


var API_CrainVehicle :String = "\(BaseHostURL)/CrainVehicleHandler.ashx?"
var API_GetAddvertise :String = "\(BaseHostURL)/CitizenCopAllHandler.ashx?key=getadd&cityid="
var API_InsertComplaintFeedback :String = "\(BaseHostURL)/CitizenCopAllHandler.ashx?key=InsertComplaintFeedback&Feedback="
var API_getEmergencyNo :String = "\(BaseHostURL)/CitizenCopAllHandler.ashx?key=getEmergencyNo&cityid="
var API_ReportCategoryBy :String = "\(BaseHostURL)/citizencopallhandler.ashx?Key=CategoryByCity&cityid="
var API_ReportSubCategoryBy :String = "\(BaseHostURL)/citizenCopAllHandler.ashx?key=SubCategory&CategoryId="
var API_ContactListAdminstrator :String = "\(BaseHostURL)/CitizenCopAllHandler.ashx?key=getContactListAdminstrator&cityid="
var API_CallPolice :String = "\(BaseHostURL)/CitizenCopAllHandler.ashx?key=getContactList&cityid="
var API_News :String = "\(BaseHostURL)/CitizenCopAllHandler.ashx?key=news&cityid="
var API_ReportLookUp :String = "\(BaseHostURL)/CitizenCopAllHandler.ashx?key=getComplaintCommentLookup&cityid="
var API_UploadReportImage :String = "\(BaseHostURL)/CitizenCopUploadImage.ashx"
var API_UploadReportVideo :String = "\(BaseHostURL)/CitizenCopUploadVideo.ashx"
var API_UploadReportAUDIO :String = "\(BaseHostURL)/CitizenCopUploadAudio.ashx"
var API_TravelSafe :String = "\(BaseHostURL)/CitizenCopAllHandler.ashx?key=GetWhiteListByBadgeNo_VehicleNo&cityid="
var API_TravelSafeImgDownload :String = "\(BaseHostURL)/WhiteListImage/"
var API_SearchLostArticle :String = "\(BaseHostURL)/CitizenCopAllHandler.ashx?key=getLostTheftMobileArticleDetail_CompNo_ImeiNo"
var API_Insertlosttheftmobile :String = "\(BaseHostURL)/CitizenCopAllHandler.ashx?key=insertlosttheftmobile_multiplearticle"

var API_SearchOfficialsCategory :String = "\(BaseHostURL)/CitizenCopAllHandler.ashx?&key=SearchOfficialsCategory&cityid="

var API_GetAllSearchOfficials :String = "\(BaseHostURL)/CitizenCopAllHandler.ashx?&key=GetAllSearchOfficials&"
var API_SubmitInformationData :String = "\(BaseHostURL)/CitizenCopAllHandler.ashx?Key=SubmitInformationData&"


class WebServiceClass: NSObject , NSURLConnectionDelegate,XMLParserDelegate {
    
    
    
    //MARK: REST API Calling With XML
    
    class func callAPIBYGET_XML(parameter:NSDictionary,url:String,OnResultBlock: @escaping (_ dict: NSDictionary,_ status:String) -> Void) {
        let strUrlwithString = url.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        AF.request(strUrlwithString!, method: .get, parameters: parameter as? Parameters, encoding: URLEncoding.default, headers: nil).responseJSON { (response:AFDataResponse<Any>) in
            if(response.data?.count != 0){
                
                let dictdata = NSMutableDictionary()
                dictdata.setValue(response.data!, forKey: "data")
                OnResultBlock((dictdata) ,"Suceess")
            }else{
                let dic = NSMutableDictionary.init()
                dic .setValue("\(alertSomeError) ", forKey: "message")
                OnResultBlock(dic,"failure")
            }
        }
    }
    
    class func callAPIBYPost_XML(parameter:NSDictionary,url:String,OnResultBlock: @escaping (_ dict: NSDictionary,_ status:String) -> Void) {
        let strUrlwithString = url.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        AF.request(strUrlwithString!, method: .post, parameters: parameter as? Parameters, encoding: URLEncoding.default, headers: nil).responseJSON { (response:AFDataResponse<Any>) in
            if(response.data?.count != 0){
                
                let dictdata = NSMutableDictionary()
                dictdata.setValue(response.data!, forKey: "data")
                OnResultBlock((dictdata) ,"Suceess")
            }else{
                let dic = NSMutableDictionary.init()
                dic .setValue("\(alertSomeError) ", forKey: "message")
                OnResultBlock(dic,"failure")
            }
        }
    }
    //MARK: REST API Calling With Json
    
    
    class func callAPIBYGET(parameter:NSDictionary,url:String,OnResultBlock: @escaping (_ dict: NSDictionary,_ status:String) -> Void) {
        let strUrlwithString = url.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        AF.request(strUrlwithString!, method: .get, parameters: parameter as? Parameters, encoding: URLEncoding.default, headers: nil).responseJSON { (response:AFDataResponse<Any>) in
            switch(response.result) {
            case .success(_):
                if let data = response.value
                {
                    let dictdata = NSMutableDictionary()
                    let status = (data as AnyObject).allKeys.contains(where: { (k) -> Bool in
                        "Result" == (k as AnyObject)as! String
                        
                        
                    })
                    if status{
                        dictdata.setValue(data, forKey: "data")
                        OnResultBlock((dictdata) ,"Suceess")
                    }
                    else {
                        let dic = NSMutableDictionary.init()
                        dic .setValue("\(alertSomeError) ", forKey: "message")
                        OnResultBlock(dic,"failure")
                    }
                }
                break
            case .failure(_):
                print(response.error ?? 0)
                let dic = NSMutableDictionary.init()
                dic .setValue("\(alertSomeError) ", forKey: "message")
                OnResultBlock(dic,"failure")
                break
            }
        }
    }
    
    class func callAPIBYPOST_string(parameter:NSDictionary,url:String,OnResultBlock: @escaping (_ dict: NSDictionary,_ status:String) -> Void) {
        let strUrlwithString = url.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        AF.request(strUrlwithString!, method: .post, parameters: parameter as? Parameters, encoding: URLEncoding.default, headers: nil).responseString { response in
            switch(response.result)   {
            case .success(_):
                if let data = response.value
                {
                    // print(data)
                    let dictdata = NSMutableDictionary()
                    dictdata.setValue(data, forKey: "data")
                    OnResultBlock((dictdata) ,"Suceess")
                }
                break
            case .failure(_):
                print(response.error ?? 0)
                let dic = NSMutableDictionary.init()
                dic .setValue("Connection Time Out ", forKey: "message")
                OnResultBlock(dic,"failure")
                break
            }
        }
        
        
    }
    class func callAPIBYPOSTForCheckSum(parameter:NSDictionary,url:String,OnResultBlock: @escaping (_ dict: NSDictionary,_ status:String) -> Void) {
        AF.request(url, method: .post, parameters: parameter as? Parameters, encoding: URLEncoding.default, headers: nil).responseJSON { (response:AFDataResponse<Any>) in
               switch(response.result) {
               case .success(_):
                   if let data = response.value
                   {
                       // print(data)
                       let dictdata = NSMutableDictionary()
                       dictdata.setValue(data, forKey: "data")
                       OnResultBlock((dictdata) ,"success")
                   }
                   break
               case .failure(_):
                   print(response.error ?? 0)
                   let dic = NSMutableDictionary.init()
                   dic .setValue("Connection Time Out ", forKey: "message")
                   OnResultBlock(dic,"failure")
                   break
               }
           }
       }
    class func callAPIBYPOST(parameter:NSDictionary,url:String,OnResultBlock: @escaping (_ dict: NSDictionary,_ status:String) -> Void) {
        let strUrlwithString = url.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        
        AF.request(strUrlwithString!, method: .post, parameters: parameter as? Parameters, encoding: URLEncoding.default, headers: nil).responseJSON { (response:AFDataResponse<Any>) in
            switch(response.result) {
            case .success(_):
                if let data = response.value
                {
                    let dictdata = NSMutableDictionary()
                    dictdata.setValue(data, forKey: "data")
                    OnResultBlock((dictdata) ,"Suceess")
                }
                break
            case .failure(_):
                print(response.error ?? 0)
                let dic = NSMutableDictionary.init()
                dic .setValue("Connection Time Out ", forKey: "message")
                OnResultBlock(dic,"failure")
                break
            }
        }
    }
    
    class func callAPIBYPOST_ResponceInString(parameter:NSDictionary,url:String,OnResultBlock: @escaping (_ dict: NSDictionary,_ status:String) -> Void) {
        let strUrlwithString = url.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        
        AF.request(strUrlwithString!, method: .post, parameters: parameter as? Parameters, encoding: URLEncoding.default, headers: nil).responseString { (responce) in

            switch(responce.result) {
                        case .success(_):
                            if let data = responce.value
                            {
                                let dictdata = NSMutableDictionary()
                                dictdata.setValue(data, forKey: "data")
                                OnResultBlock((dictdata) ,"Suceess")
                            }
                            break
                        case .failure(_):
                            print(responce.error ?? 0)
                            let dic = NSMutableDictionary.init()
                            dic .setValue("Connection Time Out ", forKey: "message")
                            OnResultBlock(dic,"failure")
                            break
                        }


        }
        

    }
    
    //MARK: SOAP API Calling
    
    
    class func callSOAP_APIBY_POST(soapMessage:String,url:String,resultype:String , responcetype : String, OnResultBlock: @escaping (_ dict: NSDictionary,_ status:String) -> Void) {
        
        let url1 = URL(string: url)
        var theRequest = URLRequest(url: url1!)
        let msgLength = soapMessage.count
        theRequest.addValue("text/xml; charset=utf-8", forHTTPHeaderField: "Content-Type")
        theRequest.addValue(String(msgLength), forHTTPHeaderField: "Content-Length")
        theRequest.httpMethod = "POST"
        theRequest.httpBody = soapMessage.data(using: String.Encoding.utf8, allowLossyConversion: false) // or false
        AF.request(theRequest)
            .responseString { response in
                switch(response.result) {
                case .success(_):
                    if response.value != nil
                    {
                        if let xmlString = response.value {
                            
                            let xml = XMLHash.parse(xmlString)
                            let body =  xml["soap:Envelope"]["soap:Body"]
                            if let helpDeskElement = body[responcetype][resultype].element {
                                let getResult = helpDeskElement.text
                                let string = "\(getResult)"
                                let data = string.data(using: .utf8)!
                                do {
                                   
                                    if responcetype == "GetGroceryOrderResponse" {
                                        
                                        let json = try JSONSerialization.jsonObject(with: data, options: [])
                                        
                                        let dictdata = json as! NSDictionary
                                        
                                        if dictdata.count > 0 {
                                            
                                            if dictdata.value(forKey: "GroceryOrder") is NSArray {
                                                
                                                let dictdataLocal = NSMutableDictionary()
                                                let arrOrder = dictdata.value(forKey: "GroceryOrder") as! NSArray
                                                dictdataLocal.setValue(arrOrder, forKey: "data")
                                                OnResultBlock((dictdataLocal) ,"Suceess")
                                                
                                            } else {
                                                
                                                let dictdata = NSMutableDictionary()
                                                dictdata.setValue(dictdata, forKey: "data")
                                                OnResultBlock((dictdata) ,"failure")
                                                
                                            }
                                            
                                        } else {
                                            
                                            let dictdata = NSMutableDictionary()
                                            dictdata.setValue(dictdata, forKey: "data")
                                            OnResultBlock((dictdata) ,"failure")
                                            
                                        }

                                        
                                    } else if responcetype == "GetSectionDetailResponse" {
                                        
                                        let json = try JSONSerialization.jsonObject(with: data, options: [])
                                        
                                        let dictdata = json as! NSDictionary
                                        
                                        if dictdata.count > 0 {
                                            
                                            if dictdata.value(forKey: "SectionDetail") is NSArray {
                                                
                                                let dictdataLocal = NSMutableDictionary()
                                                let arrOrder = dictdata.value(forKey: "SectionDetail") as! NSArray
                                                dictdataLocal.setValue(arrOrder, forKey: "data")
                                                OnResultBlock((dictdataLocal) ,"Suceess")
                                                
                                            } else {
                                                
                                                let dictdata = NSMutableDictionary()
                                                dictdata.setValue(dictdata, forKey: "data")
                                                OnResultBlock((dictdata) ,"failure")
                                                
                                            }
                                            
                                        } else {
                                            
                                            let dictdata = NSMutableDictionary()
                                            dictdata.setValue(dictdata, forKey: "data")
                                            OnResultBlock((dictdata) ,"failure")
                                            
                                        }
                                        
                                    }else {
                                        
                                        if let jsonArray = try JSONSerialization.jsonObject(with: data, options : .allowFragments) as? [Dictionary<String,Any>]
                                        {
                                            let dictdata = NSMutableDictionary()
                                            dictdata.setValue(jsonArray, forKey: "data")
                                            OnResultBlock((dictdata) ,"Suceess")
                                            print(jsonArray) // use the json here
                                        } else {
                                            let dictdata = NSMutableDictionary()
                                            dictdata.setValue(dictdata, forKey: "data")
                                            OnResultBlock((dictdata) ,"failure")
                                        }
                                        
                                    }

                                } catch let error as NSError {
                                    let dictdata = NSMutableDictionary()
                                    dictdata.setValue(dictdata, forKey: "data")
                                    OnResultBlock((dictdata) ,"failure")
                                    print(error)
                                }
                            }else{
                                print(response.error ?? 0)
                                                   let dic = NSMutableDictionary.init()
                                                   dic .setValue("Connection Time Out ", forKey: "message")
                                                   OnResultBlock(dic,"failure")
                                                   break
                            }
                        }
                    }
                    break
                    
                case .failure(_):
                    print(response.error ?? 0)
                    let dic = NSMutableDictionary.init()
                    dic .setValue("Connection Time Out ", forKey: "message")
                    OnResultBlock(dic,"failure")
                    break
                }
        }
    }
    
    
    class func callSOAP_APIBY_GETHELP(soapMessage:String,url:String,resultype:String , responcetype : String, OnResultBlock: @escaping (_ dict: NSDictionary,_ status:String) -> Void) {
        
        let url1 = URL(string: url)
        var theRequest = URLRequest(url: url1!)
        let msgLength = soapMessage.count
        theRequest.addValue("text/xml; charset=utf-8", forHTTPHeaderField: "Content-Type")
        theRequest.addValue(String(msgLength), forHTTPHeaderField: "Content-Length")
        theRequest.httpMethod = "POST"
        theRequest.httpBody = soapMessage.data(using: String.Encoding.utf8, allowLossyConversion: false) // or false
        AF.request(theRequest)
            .responseString { response in
                switch(response.result) {
                case .success(_):
                    if response.value != nil
                    {
                        if let xmlString = response.value {
                            let xml = XMLHash.parse(xmlString)
                            let body =  xml["soap:Envelope"]["soap:Body"]
                            if let helpDeskElement = body["\(responcetype)"]["\(resultype)"].element {
                                let getResult = helpDeskElement.text
                                let dictdata = NSMutableDictionary()
                                print(convertJsonToDictionary(text:getResult ) ?? 0)
                                dictdata.setValue((convertJsonToDictionary(text:getResult ) ?? 0), forKey: "data")
                                OnResultBlock((dictdata) ,"Suceess")
                                break

                            }
                        }else{
                            let dic = NSMutableDictionary.init()
                            dic .setValue("Connection Time Out ", forKey: "message")
                            OnResultBlock(dic,"failure")
                            break

                        }
                    }
                    let dic = NSMutableDictionary.init()
                                       dic .setValue("Connection Time Out ", forKey: "message")
                                       OnResultBlock(dic,"failure")
                    break
                    
                case .failure(_):
                    print(response.error as Any)
                    let dic = NSMutableDictionary.init()
                    dic .setValue("Connection Time Out ", forKey: "message")
                    OnResultBlock(dic,"failure")
                    break
                }
        }
        
        
    }
    class func callAPIWithImage_New(parameter:NSDictionary,url:String,image:UIImage,fileName:String,withName:String,OnResultBlock: @escaping (_ dict: NSDictionary,_ status:String) -> Void){
        
                AF.upload(multipartFormData: { (multipartdata) in
        
                    let imageData = image.jpegData(compressionQuality:0.5)
                    multipartdata.append(imageData!, withName: withName, fileName: fileName, mimeType: "image/png")
                    for (key, value) in parameter {
                        multipartdata.append((value as! String).data(using: String.Encoding.utf8)!, withName: key as! String)
                    }
                    
                }, to: url).responseString(completionHandler: { response in
                    print(response.request ?? 0)  // original URL request
                     print(response.response ?? 0) // URL response
                     print(response.data ?? 0)     // server data
                     print(response.result)   // result of response serialization

                     if let JSON = response.value
                     {
                         if JSON == "OK" {
                             let dic = NSMutableDictionary.init()
                             dic.setValue("\(response.value ?? "")", forKey: "message")
                             OnResultBlock(dic,"Suceess")
                         }else{
                             let dic = NSMutableDictionary.init()
                             dic.setValue("FAILURE", forKey: "message")
                             OnResultBlock(dic,"failure")
                         }
                        
                         
                     }else{
                         let dic = NSMutableDictionary.init()
                         dic.setValue("FAILURE", forKey: "message")
                         OnResultBlock(dic,"failure")
                     }
                 })
        
        
        
        
}
    class func callAPIWithImage(parameter:NSDictionary,url:String,data:Data,fileName:String,withName:String,OnResultBlock: @escaping (_ dict: NSDictionary,_ status:String) -> Void){
        
        
        AF.upload(multipartFormData: { (multiPartFormData:MultipartFormData) in
            
            multiPartFormData.append(data, withName: withName, fileName: fileName, mimeType: "image/png")
            for (key, value) in parameter {
                multiPartFormData.append((value as! String).data(using: String.Encoding.utf8)!, withName: key as! String)
            }
       }, to: url).responseString { response in
           
           if let JSON = response.value
           {
               if JSON == "OK" {
                   let dic = NSMutableDictionary.init()
                   dic.setValue("\(response.value ?? "")", forKey: "message")
                   OnResultBlock(dic,"Suceess")
               }else{
                   let dic = NSMutableDictionary.init()
                   dic.setValue("FAILURE", forKey: "message")
                   OnResultBlock(dic,"failure")
               }
              
               
           }else{
               let dic = NSMutableDictionary.init()
               dic.setValue("FAILURE", forKey: "message")
               OnResultBlock(dic,"failure")
           }
       }

        
        
        
        //to: url) { (encodingResult:SessionManager.MultipartFormDataEncodingResult) in
//            switch (encodingResult){
//            case .success(request: let upload, streamingFromDisk: _, streamFileURL: _):
//                upload.responseJSON { response in
//                    //                        print(response.request)  // original URL request
//                    //                        print(response.response) // URL response
//                    //                        print(response.data)     // server data
//                    //                        print(response.result)   // result of response serialization
//                    let statusCode = response.response?.statusCode
//                    if statusCode == 200
//                    {
//                        let dic = NSMutableDictionary.init()
//                        dic.setValue("\(response.result)", forKey: "message")
//                        OnResultBlock(dic,"Suceess")
//                    }else{
//                        let dic = NSMutableDictionary.init()
//                        dic.setValue("FAILURE", forKey: "message")
//                        OnResultBlock(dic,"failure")
//                    }
//                }
//                
//            case .failure(let encodingError):
//                print(encodingError)
//                let dic = NSMutableDictionary.init()
//                dic.setValue("Connection Time Out", forKey: "message")
//                OnResultBlock(dic,"failure")
//            }
//        }
    }
    
    // MARK: - Get Vehicle Data ParseAPI
    
    class func callVehicleDataParseAPI(soapMessage:String,url:String,resultype:String , responcetype : String, OnResultBlock: @escaping (_ dict: NSDictionary,_ status:String) -> Void) {
        
        let url1 = URL(string: url)
        var theRequest = URLRequest(url: url1!)
        let msgLength = soapMessage.count
        theRequest.addValue("text/xml; charset=utf-8", forHTTPHeaderField: "Content-Type")
        theRequest.addValue(String(msgLength), forHTTPHeaderField: "Content-Length")
        theRequest.httpMethod = "POST"
        theRequest.httpBody = soapMessage.data(using: String.Encoding.utf8, allowLossyConversion: false) // or false
        AF.request(theRequest)
            .responseString { response in
                switch(response.result) {
                case .success(_):
                    if response.value != nil
                    {
                        if let xmlString = response.value {
                            let xml = XMLHash.parse(xmlString)
                            let body =  xml["soap:Envelope"]["soap:Body"]
                            
                            if let helpDeskElement = body[responcetype][resultype]["diffgr:diffgram"]["NewDataSet"]["RC_Detail"]["OWNER_NAME"].element {
                                var getResult = helpDeskElement.text
                                
                                let array = NSMutableArray()
                                
                                //dict.setValue(getResult, forKey: "OWNER_NAME")
                                array.add(getResult)
                                
                                var  helpDeskElement = body[responcetype][resultype]["diffgr:diffgram"]["NewDataSet"]["RC_Detail"]["FATHER_NAME"].element
                                getResult = helpDeskElement!.text
                                //dict.setValue(getResult, forKey: "FATHER_NAME")
                                array.add(getResult)
                                
                                helpDeskElement = body[responcetype][resultype]["diffgr:diffgram"]["NewDataSet"]["RC_Detail"]["OWNER_ADDRESS"].element
                                getResult = helpDeskElement!.text
                                //dict.setValue(getResult, forKey: "OWNER_ADDRESS")
                                array.add(getResult)
                                
                                helpDeskElement = body[responcetype][resultype]["diffgr:diffgram"]["NewDataSet"]["RC_Detail"]["VEHICLE_CLASS"].element
                                getResult = helpDeskElement!.text
                                //dict.setValue(getResult, forKey: "VEHICLE_CLASS")
                                array.add(getResult)
                                
                                helpDeskElement = body[responcetype][resultype]["diffgr:diffgram"]["NewDataSet"]["RC_Detail"]["MANUFACTURER_NAME"].element
                                getResult = helpDeskElement!.text
                                //dict.setValue(getResult, forKey: "MANUFACTURER_NAME")
                                array.add(getResult)
                                
                                helpDeskElement = body[responcetype][resultype]["diffgr:diffgram"]["NewDataSet"]["RC_Detail"]["MODEL"].element
                                getResult = helpDeskElement!.text
                                //dict.setValue(getResult, forKey: "MODEL")
                                array.add(getResult)
                                
                                helpDeskElement = body[responcetype][resultype]["diffgr:diffgram"]["NewDataSet"]["RC_Detail"]["MANUFACTURER_YEAR"].element
                                getResult = helpDeskElement!.text
                                //dict.setValue(getResult, forKey: "MANUFACTURER_YEAR")
                                array.add(getResult)
                                
                                helpDeskElement = body[responcetype][resultype]["diffgr:diffgram"]["NewDataSet"]["RC_Detail"]["COLOR"].element
                                getResult = helpDeskElement!.text
                                //dict.setValue(getResult, forKey: "COLOR")
                                array.add(getResult)
                                
                                helpDeskElement = body[responcetype][resultype]["diffgr:diffgram"]["NewDataSet"]["RC_Detail"]["CHASSIS_NO"].element
                                getResult = helpDeskElement!.text
                                //dict.setValue(getResult, forKey: "CHASSIS_NO")
                                array.add(getResult)
                                
                                helpDeskElement = body[responcetype][resultype]["diffgr:diffgram"]["NewDataSet"]["RC_Detail"]["ENGINE_NO"].element
                                getResult = helpDeskElement!.text
                                //dict.setValue(getResult, forKey: "ENGINE_NO")
                                array.add(getResult)
                              
                            
                                
                                if body[responcetype][resultype]["diffgr:diffgram"]["NewDataSet"]["RC_Detail"]["MOBILE_NUMBER"].element != nil{
                                    let helpDeskElement1 = body[responcetype][resultype]["diffgr:diffgram"]["NewDataSet"]["RC_Detail"]["MOBILE_NUMBER"].element!
                                    getResult = helpDeskElement1.text
                                    array.add(getResult)

                                }else{
                                    array.add("")
                                }
                                
                                
                                
                                let dictdata = NSMutableDictionary()
                                dictdata.setValue(array, forKey: "data")
                                OnResultBlock((dictdata) ,"Suceess")
                                
                            }else{
                                
                                let dictdata = NSMutableDictionary()
                                dictdata.setValue(dictdata, forKey: "data")
                                OnResultBlock((dictdata) ,"failure")
                                
                            }
                        }
                    }
                    break
                    
                case .failure(_):
                    print(response.error ?? 0)
                    let dic = NSMutableDictionary.init()
                    dic .setValue("Connection Time Out ", forKey: "message")
                    OnResultBlock(dic,"failure")
                    break
                }
        }
    }
    
    
    
    
    //    // MARK: - Convert String To Dictionary Function
    //    class func convertJsonStringToDictionary(text: String) -> [String: Any]? {
    //        if let data = text.replacingOccurrences(of: "\n", with: "").data(using: String.Encoding.utf8) {
    //            do {
    //                return try JSONSerialization.jsonObject(with: data, options: []) as? [String:AnyObject]
    //            } catch {
    //                print(error.localizedDescription)
    //            }
    //        }
    //        return nil
    //    }
    // MARK: - Convert String To Dictionary Function
    class func convertJsonToDictionary(text: String) -> [String: Any]? {
        if let data = text.replacingOccurrences(of: "\n", with: "").data(using: String.Encoding.utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: [.allowFragments]) as? [String:AnyObject]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
}


