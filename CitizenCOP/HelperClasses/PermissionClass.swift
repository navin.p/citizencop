//
//  PermissionClass.swift
//  TargetDemo
//
//  Created by Navin Patidar on 8/28/19.
//  Copyright © 2019 Navin Patidar. All rights reserved.
//

import UIKit
import Photos
import AVFoundation
import Contacts
import CoreLocation

public enum PermissionsStatus: Int {
    case disabled
    case enabled
    case checking
    case unavailable
    case denied
}

public enum PermissionsType: String {
    case calendar = "calendar"
    case reminders = "reminders"
    case contacts = "contacts"
    case bluetooth = "bluetooth"
    case location = "location"
    case notifications = "notifications"
    case microphone = "microphone"
    case motionFitness = "motion fitness"
    case camera = "Camera"
    case custom = "custom"
    case photoLibrary = "Photo library"
}

public enum PermissionsDetailKey: String {
   case NSCalendarsUsageDescription="Required to show your Apple Calendar"
   case NSContactsUsageDescription="This will allow access to your device address book to send messages and make calls."
   case NSRemindersUsageDescription="Required to show your Apple Reminders"
   case NSLocationAlwaysUsageDescription="The application uses your location details  to provide directions, nearby search results and get your location for emergency help."
   case NSLocationWhenInUseUsageDescription="The application uses your location details  to provide directions, nearby search results and get your location for emergency help. "
   case NSBluetoothPeripheralUsageDescription = "Required to connect with your cool device"
   case NSMicrophoneUsageDescription = "This will allow the application to access the device microphone."
   case  NSCameraUsageDescription = "This App uses Camera to upload image for checklist answers."
   case  NSMotionUsageDescription = "Required to monitor motion & fitness"
   case NSPhotoLibraryUsageDescription = "This App uses Gallery to upload image for checklist answers."
   case NSAppleMusicUsageDescription = "Required to access your Media Library"
    case NSNotificationUsageDescription = "Required to access your notification"

}

class PermissionClass: UIViewController {

      var status = PermissionsStatus.checking
     var mediaType = AVMediaType.video

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
   
    //MARK:- checkPermission

    
   public func checkPermission(type: PermissionsType) -> PermissionsStatus {

        switch type {
        case .photoLibrary:
            let authStatus = PHPhotoLibrary.authorizationStatus()
            switch authStatus {
            case .authorized: self.status =
                .enabled
            print(self.status)
            case .denied:
                self.status = .denied
                print(self.status)
                
            case .notDetermined:
                self.status = .disabled
                print(self.status)
                
            case .restricted:
                self.status = .unavailable
                print(self.status)
                
            @unknown default:
                self.status = .unavailable
            }
        case .camera:
                let authStatus = AVCaptureDevice.authorizationStatus(for: mediaType)
                switch authStatus {
                case .authorized:
                    self.status = .enabled
                case .denied:
                    self.status = .disabled
                case .notDetermined:
                    self.status = .disabled
                default:
                    self.status = .unavailable
                }

            break
        case .custom:
            break
        case .reminders:
            break
        case .contacts:
            let currentStatus = CNContactStore.authorizationStatus(for: .contacts)
            switch currentStatus{
            case .authorized:
                status = .enabled
            case .denied:
                status = .denied
            case .notDetermined:
                status = .disabled
            case .restricted:
                status = .unavailable
            @unknown default:
                status = .unavailable
            }
        case .bluetooth:
            break
        case .location:
            break
        case .notifications:
            break
        case .microphone:
            if AVAudioSession.sharedInstance().isInputAvailable {
                if #available(iOS 8.0, *) {
                    if AVAudioSession.sharedInstance().recordPermission == .granted {
                        self.status = .enabled
                    }else{
                        self.status = .disabled
                    }
                }else{
                    self.status = .enabled
                }
            }else{
                self.status = .disabled
            }
            break
        case .calendar:
            break
        case .motionFitness:
            break
       
        }
    return status
    }
        //MARK:- UpdatePermission

    public func UpdatePermission(type: PermissionsType ,oldStatus : PermissionsStatus){
        switch type {
        case .photoLibrary:
            if #available(iOS 8.0, *) {
                PHPhotoLibrary.requestAuthorization({ (status: PHAuthorizationStatus) in
                    if(status == .denied && oldStatus == .denied){
                        self.OpenSettingURL(type: PermissionsType.photoLibrary)
                    }
                    NotificationCenter.default.post(name: Notification.Name("CheckPermissions"), object: nil)
                })
            } else {
                // Photo Library Only available above iOS 8
            }
        case .camera:
            if #available(iOS 8.0, *) {
                let authStatus = AVCaptureDevice.authorizationStatus(for: mediaType)
                if authStatus == .denied {
                   OpenSettingURL(type: PermissionsType.camera)
                }else{
                    AVCaptureDevice.requestAccess(for: mediaType, completionHandler: { (result) in
                        NotificationCenter.default.post(name: Notification.Name("CheckPermissions"), object: nil)
                        
                    })
                }
            }else{
                
            }
            
            break
        case .custom:
            break
        case .reminders:
            break
        case .contacts:
            CNContactStore().requestAccess(for: .contacts) { (success, error) in
                if success && error == nil {
                    
                  self.status = .enabled
                } else {
                  self.status = .denied
                    if(self.status == .denied && oldStatus == .denied){
                        self.OpenSettingURL(type: PermissionsType.contacts)
                    }
                }
                NotificationCenter.default.post(name: Notification.Name("CheckPermissions"), object: nil)
            }
        case .bluetooth:
            break
        case .location:
           
            break
        case .notifications:
            
            
            
            break
        case .microphone:
            if #available(iOS 8.0, *) {
                if AVAudioSession.sharedInstance().recordPermission == .denied {
                    self.OpenSettingURL(type: PermissionsType.microphone)
                }else{
                    AVAudioSession.sharedInstance().requestRecordPermission { (result) in
                        if result {
                            self.status = .enabled
                        }else{
                            self.status = .disabled
                        }
                        
                        NotificationCenter.default.post(name: Notification.Name("CheckPermissions"), object: nil)
                    }
                }
            } else {
                //Microphone access should be always active on iOS 7
            }
            
            
            break
        case .calendar:
            break
        case .motionFitness:
            break
            
        }
       
    }
    //MARK:- OpenSettingURL
    
    func OpenSettingURL(type : PermissionsType) {
        let settingsURL = URL(string: UIApplication.openSettingsURLString)
        UIApplication.shared.open(settingsURL!, options: [:]) { (status) in
        }
    }
    
}
