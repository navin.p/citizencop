

#import <UIKit/UIKit.h>

@interface KSTCollectionViewPageHorizontalLayout : UICollectionViewLayout

@property (nonatomic, assign) CGFloat lineSpacing;
@property (nonatomic, assign) CGFloat interitemSpacing;
@property (nonatomic, assign) CGSize itemSize;
@property (nonatomic, assign) CGFloat sectionInsetTop;
@property (nonatomic, assign) NSInteger columnCount;

@end
