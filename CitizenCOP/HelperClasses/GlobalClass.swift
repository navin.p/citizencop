//
//  GlobalClass.swift
//  CitizenCOP
//
//  Created by Navin Patidar on 5/17/19.
//  Copyright © 2019 CitizenCop. All rights reserved.
//

import Foundation
import CoreTelephony
import MessageUI
import CoreData
import MapKit
import SystemConfiguration
import AVKit
import Alamofire

//MARK:
//MARK:  Internet validation

func isInternetAvailable() -> Bool
{
    var zeroAddress = sockaddr_in()
    zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
    zeroAddress.sin_family = sa_family_t(AF_INET)
    
    let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
        $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
            SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
        }
    }
    
    var flags = SCNetworkReachabilityFlags()
    if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
        return false
    }
    let isReachable = flags.contains(.reachable)
    let needsConnection = flags.contains(.connectionRequired)
    return (isReachable && !needsConnection)
}

//MARK:
//MARK:  UIColor

func hexStringToUIColor (hex:String) -> UIColor {
    var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
    if (cString.hasPrefix("#")) {
        cString.remove(at: cString.startIndex)
    }
    if ((cString.count) != 6) {
        return UIColor.gray
    }
    var rgbValue:UInt32 = 0
    Scanner(string: cString).scanHexInt32(&rgbValue)
    return UIColor(
        red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
        green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
        blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
        alpha: CGFloat(1.0)
    )
}
//MARK:
//MARK:  Null Handling

func removeNullFromDict (dict : NSMutableDictionary) -> NSMutableDictionary
{
    let dic = dict;
    for (key, value) in dict {
        let val : NSObject = value as! NSObject;
        if(val.isEqual(NSNull()))
        {
            dic.setValue("", forKey: (key as? String)!)
        } else if(val.isEqual("<null>"))
        {
            dic.setValue("", forKey: (key as? String)!)
        }
        else
        {
            dic.setValue(value, forKey: key as! String)
        }
    }
    return dic;
}
func removeSpace(str: String) -> String
{
    let trimmedString = str.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
    
    return trimmedString
}

//MARK:
//MARK:  Alert

func showAlertWithoutAnyAction(strtitle : String , strMessage : String ,viewcontrol : UIViewController)  {
    let alert = UIAlertController(title: strtitle, message: strMessage, preferredStyle: UIAlertController.Style.alert)
    
    // add the actions (buttons)
    alert.addAction(UIAlertAction (title: "OK", style: .default, handler: { (nil) in
    }))
    viewcontrol.present(alert, animated: true, completion: nil)
}
func showLocationAccessAlertAction(viewcontrol : UIViewController)  {
    let alert = UIAlertController(title: "Location Services Disabled", message: "Please enable Location Services in Settings", preferredStyle: .alert)
    alert.addAction(UIAlertAction (title: "Go to Setting", style: .default, handler: { (nil) in
        guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
            return
        }
        if UIApplication.shared.canOpenURL(settingsUrl) {
            UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                print("Settings opened: \(success)") // Prints true
            })
        }
    }))
    // add the actions (buttons)
    alert.addAction(UIAlertAction (title: "OK", style: .default, handler: { (nil) in
    }))
    viewcontrol.present(alert, animated: true, completion: nil)
}
//MARK:
//MARK:  Calling Function
func callingFunction(number : NSString) -> Bool{
    let str = number.replacingOccurrences(of: " ", with:"")
    
    if let url = URL(string: "tel://\(str)"), UIApplication.shared.canOpenURL(url) {
        if #available(iOS 10, *) {
            UIApplication.shared.open(url)
        } else {
            UIApplication.shared.openURL(url)
        }
        return true
    }
    else {
        return false
    }
}
//MARK:
//MARK:  estimatedHeightOfLabel Function
func estimatedHeightOfLabel(text: String , view : UIView) -> CGFloat {
    let size = CGSize(width: view.frame.width - 20 , height: 1000)
    let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
    let attributes = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 20)]
    let rectangleHeight = String(text).boundingRect(with: size, options: options, attributes: attributes, context: nil).height
    return rectangleHeight
}

//MARK:
//MARK:  Open MAP
func openMap(strTitle : String , GlobleLat : String , GlobleLong : String) {
    if !(strTitle == "<null>" || strTitle == "" || strTitle == "Not Available"){
        let latitude: CLLocationDegrees = Double(GlobleLat)!
        let longitude: CLLocationDegrees = Double(GlobleLong)!
        let regionDistance:CLLocationDistance = 10000
        let coordinates = CLLocationCoordinate2DMake(latitude, longitude)
        let regionSpan = MKCoordinateRegion(center: coordinates, latitudinalMeters: regionDistance, longitudinalMeters: regionDistance)
        let options = [
            MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
            MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
        ]
        let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = strTitle
        
        mapItem.openInMaps(launchOptions: options)
    }
    
    
}
//MARK: LoaderCustome
var viewLoader = CardView()
var btnBackGround = UIButton()


func customeLoaderShow( message : String , controller : UIViewController ) {
    customeLoaderRemove()
    // For Back
    btnBackGround.frame = controller.view.frame
    btnBackGround.alpha = 0.5
    btnBackGround.backgroundColor = UIColor.black
    controller.view.addSubview(btnBackGround)

    viewLoader.cornerRadius = 15
    viewLoader.frame = CGRect(x: controller.view.frame.width/2 - 60 , y: controller.view.frame.height/2 - 50, width: 120, height: 100)

    // For Activity
    let activityIndicator = UIActivityIndicatorView(frame: CGRect(x: viewLoader.frame.width/2 - 15 , y: viewLoader.frame.height/2 - 20, width: 30, height: 30))
    activityIndicator.startAnimating()
    activityIndicator.style = .gray
    viewLoader.backgroundColor = UIColor.white
    viewLoader.addSubview(activityIndicator)

    // For Message
    let lblMessage = UILabel(frame: CGRect(x: 5 , y: activityIndicator.frame.maxY, width: viewLoader.frame.width - 10, height: 20))
    lblMessage.text = message
    lblMessage.font = UIFont.systemFont(ofSize: 12)
    lblMessage.textAlignment = .center
    viewLoader.addSubview(lblMessage)

    controller.view.addSubview(viewLoader)


}
func customeLoaderRemove(){
    for item in viewLoader.subviews {
        item.removeFromSuperview()
    }
    viewLoader.removeFromSuperview()
    btnBackGround.removeFromSuperview()
}

//MARK: DotLoaderCustome
var dots = DotsLoader()
var btnTransprantDots = UIButton()
var viewForDot = UIView()


func customDotLoaderShowOnWeb(frame : CGRect , message : String , controller : UIViewController){
    customeDotLoaderRemove()
    btnTransprantDots = UIButton()
    btnTransprantDots.frame = controller.view.frame
    controller.view.addSubview(btnTransprantDots)
     viewForDot = UIView()
    viewForDot.frame = frame
    viewForDot.backgroundColor = UIColor.white

    dots = DotsLoader()
    dots.dotsCount = 5
    dots.dotsRadius = 5
    dots.startAnimating()
    dots.backgroundColor = UIColor.white
    dots.frame = CGRect(x:Int(viewForDot.frame.width / 2)  - 100 , y: Int(viewForDot.frame.height / 2 - 25), width: 200, height: 50)
    dots.tintColor = hexStringToUIColor(hex: primaryTheamColor)
    viewForDot.addSubview(dots)
    controller.view.addSubview(viewForDot)

}

func customDotLoaderShowOnFull(message : String , controller : UIViewController){
    customeDotLoaderRemove()
    btnTransprantDots = UIButton()

    btnTransprantDots.frame = controller.view.frame
    btnTransprantDots.backgroundColor = UIColor.black
    btnTransprantDots.alpha = 0.5
    controller.view.addSubview(btnTransprantDots)
    viewForDot = UIView()
    viewForDot.frame = CGRect(x: btnTransprantDots.frame.width / 2 - 75, y: btnTransprantDots.frame.height / 2 - 25, width: 150, height: 50)
    viewForDot.backgroundColor = UIColor.white
    viewForDot.layer.cornerRadius = 10.0
    controller.view.addSubview(viewForDot)
    dots = DotsLoader()
    dots.dotsCount = 5
    dots.dotsRadius = 5
    dots.startAnimating()
    dots.backgroundColor = UIColor.white
    dots.frame = CGRect(x: 5, y: 5, width: 140, height: 40)
    dots.tintColor = hexStringToUIColor(hex: primaryTheamColor)
    viewForDot.addSubview(dots)
    controller.view.addSubview(viewForDot)
    
}
func customDotLoaderShowOnButton(btn : UIButton ,view : UIView, controller : UIViewController){
    customeDotLoaderRemove()
    btnTransprantDots = UIButton()
    btnTransprantDots.frame = controller.view.frame
    controller.view.addSubview(btnTransprantDots)
    dots = DotsLoader()
    dots.dotsCount = 5
    dots.dotsRadius = 5
    dots.layer.cornerRadius = 5.0
    dots.startAnimating()
    dots.backgroundColor = UIColor.white
    dots.frame = btn.frame
    dots.tintColor = hexStringToUIColor(hex: primaryTheamColor)
    view.addSubview(dots)
    
}
func customeDotLoaderRemove(){
    for item in viewForDot.subviews {
        item.removeFromSuperview()
    }
    dots.removeFromSuperview()
    btnTransprantDots.removeFromSuperview()
    viewForDot.removeFromSuperview()

}



//MARK: CORE DATA Base

func uniqueString() -> String {
    let date = Date()
    let format = DateFormatter()
    format.dateFormat = "MMddHHmmss"
   return format.string(from: date)
}



//MARK: CORE DATA Base
func saveDataInLocalArray(strEntity: String , strKey : String , data : NSMutableArray)  {
    let context = AppDelegate.getContext()
    let entity =  NSEntityDescription.entity(forEntityName: "\(strEntity)", in: context)
    let transc = NSManagedObject(entity: entity!, insertInto: context)
    transc.setValue(data, forKey: "\(strKey)")
    do {
        try context.save()
    } catch _ as NSError  {
        
    } catch {
        
    }
    
}
func saveDataInLocalDictionary(strEntity: String , strKey : String , data : NSMutableDictionary)  {
    let context = AppDelegate.getContext()
    let entity =  NSEntityDescription.entity(forEntityName: "\(strEntity)", in: context)
    let transc = NSManagedObject(entity: entity!, insertInto: context)
    transc.setValue(data, forKey: "\(strKey)")
    do {
        try context.save()
    } catch _ as NSError  {
        
    } catch {
        
    }
    
}


func deleteAllRecords(strEntity: String ) {
    let delegate = UIApplication.shared.delegate as! AppDelegate
    let context = delegate.persistentContainer.viewContext
    
    let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "\(strEntity)")
    let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
    
    do {
        try context.execute(deleteRequest)
        try context.save()
        
    } catch {
        print ("There was an error")
    }
}
func getDataFromLocal(strEntity: String , strkey : String )-> NSArray {
    let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "\(strEntity)")
    
    do {
        return (try AppDelegate.getContext().fetch(fetchRequest) as NSArray)
        
    } catch
    {
        let fetchError = error as NSError
        print(fetchError)
    }
    return NSArray()
}
func getDataFromCoreDataBase(strEntity: String ,strkey : String )-> NSMutableArray   {
    let aryTemp = getDataFromLocal(strEntity: strEntity, strkey: strkey)
    
    let aryList = NSMutableArray()
    if aryTemp.count > 0 {
        for j in 0 ..< aryTemp.count {
            var obj = NSManagedObject()
            obj = aryTemp[j] as! NSManagedObject
            aryList.add(obj.value(forKey: strkey) ?? 0)
        }
    }
    if aryList.count !=  0{
        return (aryList.object(at: 0) as! NSArray).mutableCopy() as! NSMutableArray
        
    }else{
        return NSMutableArray()
    }
}
func getDataFromCoreDataBaseDict(strEntity: String ,strkey : String )-> NSDictionary   {
    let aryTemp = getDataFromLocal(strEntity: strEntity, strkey: strkey)
    
    let aryList = NSMutableArray()
    if aryTemp.count > 0 {
        for j in 0 ..< aryTemp.count {
            var obj = NSManagedObject()
            obj = aryTemp[j] as! NSManagedObject
            aryList.add(obj.value(forKey: strkey) ?? 0)
        }
    }
    if aryList.count !=  0{
        return (aryList.object(at: 0) as! NSDictionary).mutableCopy() as! NSMutableDictionary
        
    }else{
        return NSMutableDictionary()
    }
}
func deleteDataFomeCoreData(obj : NSManagedObject)  {
    let context = AppDelegate.getContext()
    //save the object
    do {
        context.delete(obj)
        try context.save()
    } catch let _ as NSError  {
    } catch {
    }
}
//MARK:
//MARK: DATE & TIME
func getCurrentDate_Time(strType : String) -> String {
    if(strType == "Date"){
        let dateFormatter : DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        let date = Date()
        return dateFormatter.string(from: date)
    }else if (strType == "Time"){
        let dateFormatter : DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm:ss"
        let date = Date()
        return dateFormatter.string(from: date)
    }
    return ""
}


//MARK:
//MARK: DocumentDirectory
func saveAudioVideoDocumentDirectory(strFileName : String , data : Data){
    let fileManager = FileManager.default
    let paths = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent(strFileName)
    print("Save......" + paths)
    fileManager.createFile(atPath: paths as String, contents: data, attributes: nil)
}
func saveImageDocumentDirectory(strFileName : String , image : UIImage){
    let fileManager = FileManager.default
    let paths = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent(strFileName)
    print("Save......" + paths)
    let imageData = image.jpegData(compressionQuality:0.5)
    fileManager.createFile(atPath: paths as String, contents: imageData, attributes: nil)
}
func GetAudioVideoFromDocumentDirectory(strFileName : String) -> Data{
    let fileManager = FileManager.default
    let imagePAth = (getDirectoryPath() as NSString).appendingPathComponent(strFileName)
    if fileManager.fileExists(atPath: imagePAth){
        return try! Data(contentsOf:URL(fileURLWithPath: imagePAth))
    }else{
        return Data()
    }
}
func GetImageFromDocumentDirectory(strFileName : String) -> UIImage{
    let fileManager = FileManager.default
    let imagePAth = (getDirectoryPath() as NSString).appendingPathComponent(strFileName)
    if fileManager.fileExists(atPath: imagePAth){
        return UIImage(contentsOfFile: imagePAth)!
    }else{
        return UIImage()
    }
}
func DeleteImageFromDocumentDirectory(strFileName : String){
    let fileManager = FileManager.default
    let imagePAth = (getDirectoryPath() as NSString).appendingPathComponent(strFileName)
    if fileManager.fileExists(atPath: imagePAth){
        try! fileManager.removeItem(atPath: imagePAth)
        print("Deleted......")

    }else{
        print("Something wronge.")
    }
}


func getDirectoryPath() -> String {
    let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
    let documentsDirectory = paths[0]
    return documentsDirectory
}
func loader_Show(controller: UIViewController , strMessage : String , title : String ,style : UIAlertController.Style) -> UIAlertController {
    let alert = UIAlertController(title: title, message:strMessage, preferredStyle: style)
    let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 5, y: 5, width: 50, height: 50))
    loadingIndicator.hidesWhenStopped = true
    if #available(iOS 13.0, *) {
        loadingIndicator.style = UIActivityIndicatorView.Style.medium
    } else {
        loadingIndicator.style = UIActivityIndicatorView.Style.gray
    }
    loadingIndicator.startAnimating();
    alert.view.addSubview(loadingIndicator)
    
   // controller.present(alert, animated: false, completion: nil)
    return alert
}

//MARK:
//MARK: getIPAddress
func getIPAddress() -> String {
    var address: String?
    var ifaddr: UnsafeMutablePointer<ifaddrs>? = nil
    if getifaddrs(&ifaddr) == 0 {
        var ptr = ifaddr
        while ptr != nil {
            defer { ptr = ptr?.pointee.ifa_next }

            guard let interface = ptr?.pointee else { return "" }
            let addrFamily = interface.ifa_addr.pointee.sa_family
            if addrFamily == UInt8(AF_INET) || addrFamily == UInt8(AF_INET6) {

                // wifi = ["en0"]
                // wired = ["en2", "en3", "en4"]
                // cellular = ["pdp_ip0","pdp_ip1","pdp_ip2","pdp_ip3"]

                let name: String = String(cString: (interface.ifa_name))
                if  name == "en0" || name == "en2" || name == "en3" || name == "en4" || name == "pdp_ip0" || name == "pdp_ip1" || name == "pdp_ip2" || name == "pdp_ip3" {
                    var hostname = [CChar](repeating: 0, count: Int(NI_MAXHOST))
                    getnameinfo(interface.ifa_addr, socklen_t((interface.ifa_addr.pointee.sa_len)), &hostname, socklen_t(hostname.count), nil, socklen_t(0), NI_NUMERICHOST)
                    address = String(cString: hostname)
                }
            }
        }
        freeifaddrs(ifaddr)
    }
    return address ?? ""
}

//MARK:
//MARK: Validation
func validateEmail(email: String) -> Bool{
    let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
    return NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluate(with: email)
}
//MARK:
//MARK: dateFormetViaFormatter



func dateFormetViaFormatter(from:String , to : String , datestr : String) -> String {
    if(datestr == ""){
       return ""
    }

       let dateFormatterFrom = DateFormatter()
        dateFormatterFrom.dateFormat = from
     let dateFormatterTo = DateFormatter()
        dateFormatterTo.dateFormat = to
     let date = dateFormatterFrom.date(from:datestr)!
    return dateFormatterTo.string(from: date)

}


//MARK:
//MARK: txtFiledValidation

func txtFiledValidation(textField : UITextField , string : String , returnOnly : String , limitValue : Int) -> Bool {
    let numberOnly = NSCharacterSet.init(charactersIn: "0123456789")
    let decimelnumberOnly = NSCharacterSet.init(charactersIn: "0123456789.")
    let strValidStr_Digit = NSCharacterSet.init(charactersIn: "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")
    let characterOnly = NSCharacterSet.init(charactersIn: "ABCDEFGHIJKLMNOPQRSTUVWXYZ abcdefghijklmnopqrstuvwxyz")
    
    let stringFromTextField = NSCharacterSet.init(charactersIn: string)
    let strValidnumber = numberOnly.isSuperset(of: stringFromTextField as CharacterSet)
    let strcharacterOnly = characterOnly.isSuperset(of: stringFromTextField as CharacterSet)
    
    let strValidDecimal = decimelnumberOnly.isSuperset(of: stringFromTextField as CharacterSet)
    let strValidDigitCharacter = strValidStr_Digit.isSuperset(of: stringFromTextField as CharacterSet)
    
    if returnOnly == "NUMBER" {
        if strValidnumber == false{
            return false
        }
    }
    if returnOnly == "CHAR_DIGIT" {
        if strValidDigitCharacter == false{
            return false
        }
    }
    
    if returnOnly == "DECIMEL" {
        if strValidDecimal == false{
            return false
        }
    }
    
    if returnOnly == "CHAR" {
        if strcharacterOnly == false{
            return false
        }
    }
    
    
    
    if ((textField.text?.count)! > 0) {
        if ((textField.text?.count)! > limitValue) {
            if (string == "")
            {
                return true;
            }
            return false;
        }
        if(returnOnly == "DECIMEL"){
            if(string == "."){
                if(textField.text!.contains(".")){
                    return false;
                }else{
                    return true;
                }
            }
        }
        return true;
    }
    if (string == " ") {
        return false;
    }
    if (string == ".") {
        return false;
    }
    return true;
}

func txtViewValidation(textField : UITextView , string : String , returnOnly : String , limitValue : Int) -> Bool {
    let numberOnly = NSCharacterSet.init(charactersIn: "0123456789")
    let stringFromTextField = NSCharacterSet.init(charactersIn: string)
    let strValid = numberOnly.isSuperset(of: stringFromTextField as CharacterSet)
    if returnOnly == "NUMBER" {
        if strValid == false{
            return false
        }
    }
    if returnOnly == "CHAR" {
        if strValid == true{
            return false
        }
    }
    if ((textField.text?.count)! > 0) {
        if ((textField.text?.count)! > limitValue) {
            if (string == "")
            {
                return true;
            }
            return false;
        }
        return true;
    }
    if (string == " ") {
        return false;
    }
    return true;
}
// MARK: - --------------HTML To String
// MARK: -
extension Data {
    var html2AttributedString: NSAttributedString? {
        do {
            return try NSAttributedString(data: self, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            print("error:", error)
            return  nil
        }
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
}
extension String {
    var html2AttributedString: NSAttributedString? {
        return Data(utf8).html2AttributedString
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
}
// MARK: - --------------Error
// MARK: -
class ErrorCell: UITableViewCell {
    //DashBoard
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnRetry: UIButton!
    
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}
// MARK: - --------------VersionCheck
// MARK: -
class VersionCheck {

    public static let shared = VersionCheck()

    var newVersionAvailable: Bool?
    var appStoreVersion: String?

    func checkAppStore(callback: ((_ versionAvailable: Bool?, _ version: String?, _ oldversion: String?, _ message: String?)->Void)? = nil) {
        let ourBundleId = Bundle.main.infoDictionary!["CFBundleIdentifier"] as! String
        AF.request("https://itunes.apple.com/lookup?bundleId=\(ourBundleId)").responseJSON { response in
            var isNew: Bool?
            var versionStr: String?
            var versionMsg: String?
            var OldversionStr: String?

            if let json = response.value as? NSDictionary,
               let results = json["results"] as? NSArray,
               let entry = results.firstObject as? NSDictionary,
               let appVersion = entry["version"] as? String,
               let ourVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String,let appVersionNotes = entry["releaseNotes"] as? String
            {
                isNew = ourVersion != appVersion
                versionStr = appVersion
                versionMsg = appVersionNotes
OldversionStr = ourVersion
            }


            self.appStoreVersion = versionStr
            self.newVersionAvailable = isNew
            callback?(isNew, versionStr, OldversionStr, versionMsg)
        }
    }
}
