//
//  ViewBillVC.swift
//  GNGPL
//
//  Created by Navin Patidar on 12/28/18.
//  Copyright © 2018 Saavan_patidar. All rights reserved.
//

import UIKit

class DrawerVC: UIViewController {
    @IBOutlet weak var tvlist: UITableView!
    @IBOutlet weak var lblVersion: UILabel!
    @IBOutlet weak var btnTransprant: UIButton!

    var aryList = NSMutableArray()
    weak var handleDrawerView: DrawerScreenDelegate?

    // MARK: - ---------------LifeCycle
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        btnTransprant.isHidden = true
        aryList = [["title":"Home","image":"DeveloperN"],["title":"Change City","image":"languageN"],["title":"Permissions","image":"DisclaimerN"],["title":"CitizenCOP Video","image":"video"],["title":"Shake to send help me! message","image":"shake_to_sendN"],["title":"Share","image":"ShareN"],["title":"About","image":"About"],["title":"Developer Info","image":"DeveloperN"],["title":"Disclaimer","image":"DisclaimerN"],["title":"Usage Tips","image":"usagetips"]]

        if(nsud.value(forKey: "CItizenCOP_Admin") != nil){
            if("\(nsud.value(forKey: "CItizenCOP_Admin")!)" == "True")
            {
            aryList = NSMutableArray()
             aryList = [["title":"Home","image":"DeveloperN"],["title":"User Profile","image":"my_profileN"],["title":"Change City","image":"languageN"],["title":"Permissions","image":"DisclaimerN"],["title":"CitizenCOP Video","image":"video"],["title":"Shake to send help me! message","image":"shake_to_sendN"],["title":"Share","image":"ShareN"],["title":"About","image":"About"],["title":"Developer Info","image":"DeveloperN"],["title":"Disclaimer","image":"DisclaimerN"],["title":"Usage Tips","image":"usagetips"]]
            }
        }
        
        
        lblVersion.text = "Version - " + app_Version
        tvlist.tableFooterView = UIView()
        tvlist.estimatedRowHeight = 80.0
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        tvlist.frame = CGRect(x: -self.view.frame.width, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        lblVersion.frame = CGRect(x: -self.view.frame.width, y: tvlist.frame.maxY, width: self.view.frame.width, height: 44)

                UIView.animate(withDuration: 0.5) {
                    self.tvlist.frame = CGRect(x:0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
                    self.lblVersion.frame = CGRect(x: 0, y: self.tvlist.frame.maxY, width: self.view.frame.width, height: 40)
                    self.btnTransprant.isHidden = false
                }
    }
    @IBAction func actionOnBack(_ sender: UIButton) {
        handleDrawerView?.refreshDrawerScreen(strType: "", tag: 0)
        UIView.animate(withDuration: 0.5) {
            self.tvlist.frame = CGRect(x: -self.view.frame.width, y: 0, width: self.view.frame.width, height: self.view.frame.height)
            self.dismiss(animated: false, completion: nil)
        }
        
    }
    @IBAction func actionOnVersion(_ sender: UIButton) {
        showAlertWithoutAnyAction(strtitle: alertInfo, strMessage:"App Version : \(app_Version)\nDate : \(app_VersionDate)\n\(app_VersionSupport)" , viewcontrol: self)
    }
}


// MARK: - ----------------UITableViewDelegate
// MARK: -

extension DrawerVC : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return section == 0 ? 1 : aryList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if(indexPath.section == 0){
            let cell = tvlist.dequeueReusableCell(withIdentifier: "Drawer1", for: indexPath as IndexPath) as! DrawerCell
            return cell
        }else{
            let dict = aryList.object(at: indexPath.row)as! NSDictionary
            let cell =  dict["title"]as? String == "Shake to send help me! message" ? tvlist.dequeueReusableCell(withIdentifier: "Drawer3", for: indexPath as IndexPath) as! DrawerCell : tvlist.dequeueReusableCell(withIdentifier: "Drawer2", for: indexPath as IndexPath) as! DrawerCell
            cell.lblTitle.text = dict["title"]as? String
            cell.imageMenu.image = UIImage(named: "\(dict["image"]!)")
            if (dict["title"]as? String == "Shake to send help me! message"){
                cell.onOffSwitch.addTarget(self, action: #selector(switchChanged), for: UIControl.Event.valueChanged)
                if((nsud.value(forKey: "CitiZenCOP_ShakeHelp")) != nil){
                    if(nsud.value(forKey: "CitiZenCOP_ShakeHelp")as! Bool){
                        cell.onOffSwitch.setOn(true, animated: true)
                    }else{
                        cell.onOffSwitch.setOn(false, animated: true)
                    }
                }else{
                    cell.onOffSwitch.setOn(false, animated: true)
                }
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return indexPath.section == 0 ? 0 : UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(indexPath.section == 1){
            let dict = aryList.object(at: indexPath.row)as! NSDictionary
            UIView.animate(withDuration: 0.5) {
                self.tvlist.frame = CGRect(x: -self.view.frame.width, y: 0, width: self.view.frame.width, height: self.view.frame.height)
                self.lblVersion.frame = CGRect(x: -self.view.frame.width, y: self.tvlist.frame.maxY, width: self.view.frame.width, height: 44)

                self.dismiss(animated: false) {
                    self.handleDrawerView?.refreshDrawerScreen(strType: (dict["title"]as? String)!, tag: indexPath.row)
                }
            }
        }
       
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        cell.alpha = 0.4
//        cell.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
//        UIView.animate(withDuration: 1.0) {
//            cell.alpha = 1
//            cell.transform = .identity
//        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    @objc func switchChanged(mySwitch: UISwitch) {
        mySwitch.isOn ? nsud.setValue(true, forKey: "CitiZenCOP_ShakeHelp") :  nsud.setValue(false, forKey: "CitiZenCOP_ShakeHelp")
    }
    
    
}

// MARK: - ----------------UserDashBoardCell
// MARK: -
class DrawerCell: UITableViewCell {
    //DashBoard
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imageMenu: UIImageView!
    @IBOutlet weak var profileimage: UIImageView!
    @IBOutlet weak var onOffSwitch: UISwitch!

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}
//MARK:
//MARK: ---------------Protocol-----------------
protocol DrawerScreenDelegate : class{
    func refreshDrawerScreen(strType : String ,tag : Int)
}
