//
//  PlayerVC.swift
//  GNGPL
//
//  Created by Navin Patidar on 1/30/19.
//  Copyright © 2019 Saavan_patidar. All rights reserved.
//

import UIKit
class PlayerVC: UIViewController  {
    
    @IBOutlet weak var player_view: WKYTPlayerView!
    var strURl = String()
    @IBOutlet weak var view_ForHeader: UIView!
    //MARK:
    //MARK : LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
      //  customDotLoaderShowOnWeb(frame: CGRect(x: 0, y: view_ForHeader.frame.maxY, width: self.view.frame.width, height: (self.view.frame.height - view_ForHeader.frame.maxY)), message: "", controller: self)
        view_ForHeader.backgroundColor = hexStringToUIColor(hex: primaryTheamColor)
        player_view.load(withVideoId: strURl)
        
        player_view.delegate = self
    }
    //MARK:
    //MARK : IBAction's
    @IBAction func actiononBack(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}
//MARK:
//MARK : WKYTPlayerViewDelegate
extension PlayerVC : WKYTPlayerViewDelegate{
    func playerViewDidBecomeReady(_ playerView: WKYTPlayerView) {
        playerView.playVideo()
     //  customeDotLoaderRemove()
    }
}
