//
//  SelectionClass.swift
//  CitizenCOP
//
//  Created by Navin Patidar on 5/17/19.
//  Copyright © 2019 CitizenCop. All rights reserved.
//


// tag 1,2,3 Country , State , City
// tag = 4 for state vehicle search
// tag = 5 for Radius
// tag = 6,7 for ReportAnIncident
// tag = 8 for ReportLostArticle
// tag = 9 for FareType
// tag = 10 for OfficialSearch
// tag = 11 for ReportAnIncident
// tag = 12 for Area CitizenEYE
// tag = 13 for Food Category

import UIKit

protocol backToRedius : class {
    func backRadius(value : String)
}

class SelectionClass: UIViewController {
    
    // MARK: - ----IBOutlet

    @IBOutlet weak var tvlist: UITableView!
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var searchBar: UISearchBar!
    open weak var delegate:PopUpDelegate?
    var citizenEyeDelegate : backToRedius?
    @IBOutlet weak var btnClear: UIButton!
    
    // MARK: - ----Variable
    
    var aryList = NSMutableArray()
    var aryForListData = NSMutableArray()
    var strTitle = String()
    var strTag = Int()
    var isCitizenEye = false
    var isClearBtnEnable = false
    
    // MARK: - ----Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblTitle.text = strTitle
        aryForListData = aryList
        tvlist.tableFooterView = UIView()
        viewHeader.backgroundColor = hexStringToUIColor(hex: primaryTheamColor)

        if isClearBtnEnable == true{
            self.btnClear.isHidden = false
        }else{
            self.btnClear.isHidden = true
        }
            
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    // MARK: - ----IBAction

    @IBAction func actionOnBack(_ sender: Any) {
        self.view.endEditing(true)
        self.dismiss(animated: true) {
            
        }
    }

    @IBAction func actionOnClear(_ sender: UIButton) {
        self.delegate?.getDataFromPopupDelegate(dictData: NSDictionary(), tag: self.strTag)
        self.dismiss(animated: false) {}
    }
}
// MARK: - ----------------UITableViewDelegate
// MARK: -

extension SelectionClass : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return aryList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tvlist.dequeueReusableCell(withIdentifier: "SelectionCell", for: indexPath as IndexPath) as! SelectionCell
        
        if isCitizenEye == true {
            let value = aryList.object(at: indexPath.row)
            cell.lblTitle.text = "\(value)"
        }else{
            
            let dict = aryList.object(at: indexPath.row)as? NSDictionary ?? [:]
            if(self.strTag == 1){
                cell.lblTitle.text = "\(dict.value(forKey: "country_name")!)"
                
            }else if (self.strTag == 2){
                cell.lblTitle.text = "\(dict.value(forKey: "state_name")!)"
                
            }else if (self.strTag == 3){
                cell.lblTitle.text = "\(dict.value(forKey: "city_name")!)"
            }else if (self.strTag == 4){
                cell.lblTitle.text = "\(dict.value(forKey: "state_name")!)"
            }else if (self.strTag == 5){
                cell.lblTitle.text = "\(dict.value(forKey: "name")!) KM"
            }else if (self.strTag == 6){
                cell.lblTitle.text = "\(dict.value(forKey: "CategoryName")!)"
            }else if (self.strTag == 7){
                cell.lblTitle.text = "\(dict.value(forKey: "SubCategoryName")!)"
            }else if (self.strTag == 8){
                cell.lblTitle.text = "\(dict.value(forKey: "ArticleName")!)"
            }else if (self.strTag == 9){
                cell.lblTitle.text = "\(dict.value(forKey: "VehicleType")!)"
            }else if (self.strTag == 10){
                cell.lblTitle.text = "\(dict.value(forKey: "PVCategoryName")!)"
            }else if (self.strTag == 11){
                cell.lblTitle.text = "\(dict.value(forKey: "CategoryName")!)"
            }else if (self.strTag == 12){
                cell.lblTitle.text = "\(dict.value(forKey: "locationarea")!)"
            }else if (self.strTag == 13){
                cell.lblTitle.text = "\(dict.value(forKey: "Category")!)"
            }else if (self.strTag == 14){
                cell.lblTitle.text = "\(dict.value(forKey: "Quantity")!)" + " \(dict.value(forKey: "UnitHindi")!)"
            }
            else if (self.strTag == 15){
                cell.lblTitle.text = "Ward no. \(dict.value(forKey: "WardNo")!)"
            }
            else if (self.strTag == 16){
                cell.lblTitle.text = "Zone no. \(dict.value(forKey: "ZoneNo")!)"
            }
            else if (self.strTag == 17){
                cell.lblTitle.text = "\(dict.value(forKey: "SubCategory")!)"
            }
            else if (self.strTag == 18){
                cell.lblTitle.text = "\(dict.value(forKey: "PoliceStationName")!)"
            }
            else if (self.strTag == 19){
                cell.lblTitle.text = "\(dict.value(forKey: "LocationArea")!)"
            }else if (self.strTag == 20){
                cell.lblTitle.text = "\(dict.value(forKey: "userType")!)"
            }else if (self.strTag == 21){
                cell.lblTitle.text = "\(dict.value(forKey: "OldSection")!)"
            }
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.view.endEditing(true)
        
        if isCitizenEye == true {
            let value = aryList.object(at: indexPath.row)
            citizenEyeDelegate?.backRadius(value: "\(value)")
            self.dismiss(animated: false) {}
        }else{
            
            let dict = aryList.object(at: indexPath.row)as? NSDictionary
            self.delegate?.getDataFromPopupDelegate(dictData: dict!, tag: self.strTag)
            self.dismiss(animated: false) {}
        }
    }
    
    
}
// MARK: - ----------------SlectionCell
// MARK: -
class SelectionCell: UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!
   
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}
// MARK: - -------------UISearchBarDelegate
// MARK: -
extension  SelectionClass : UISearchBarDelegate{
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        var txtAfterUpdate:NSString = searchBar.text! as NSString
        txtAfterUpdate = txtAfterUpdate.replacingCharacters(in: range, with: text) as NSString
        if isCitizenEye == true {
            
        }else{
            self.searchAutocomplete(Searching: txtAfterUpdate)
        }
        
        return true
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
        self.searchAutocomplete(Searching: "")
        searchBar.text = ""
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
        self.searchAutocomplete(Searching: "")
    }
    func searchAutocomplete(Searching: NSString) -> Void {
        let resultPredicate = NSPredicate(format: "country_name contains[c] %@ OR state_name contains[c] %@ OR city_name contains[c] %@ OR ArticleName contains[c] %@ OR SubCategoryName contains[c] %@ OR CategoryName contains[c] %@ OR PVCategoryName contains[c] %@ OR locationarea contains[c] %@ OR Category contains[c] %@ OR SubCategory contains[c] %@ OR PoliceStationName contains[c] %@ OR userType contains[c] %@ OR OldSection contains[c] %@", argumentArray: [Searching, Searching , Searching , Searching, Searching , Searching , Searching , Searching , Searching, Searching , Searching , Searching , Searching])
        if !(Searching.length == 0) {
            let arrayfilter = (self.aryForListData ).filtered(using: resultPredicate)
            let nsMutableArray = NSMutableArray(array: arrayfilter)
            self.aryList = NSMutableArray()
            self.aryList = nsMutableArray.mutableCopy() as! NSMutableArray
            self.tvlist.reloadData()
        }
        else{
            self.aryList = NSMutableArray()
            self.aryList = self.aryForListData.mutableCopy() as! NSMutableArray
            self.tvlist.reloadData()
            self.view.endEditing(true)
            searchBar.text = ""
        }
        if(aryList.count == 0){
        }
    }
}

//MARK: -------Protocol

protocol PopUpDelegate : class{
    func getDataFromPopupDelegate(dictData : NSDictionary ,tag : Int)
}
